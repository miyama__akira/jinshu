## ライセンスなど
このリストは、[クリエイティブ・コモンズ 表示 4.0 国際 ライセンス](https://creativecommons.org/licenses/by/4.0/deed.ja) (CC BY 4.0) のもとに公開します。
かなり自由に再利用がきくので、再利用してもっと良いリストを作って教えてください😊（実際の作業面でも再利用しやすいように markdownで書いています）。
あるいは、プルリクをくだされば、できる範囲で書き足します（GitLabを触るのが初めてなので不安ですが。[Twitter](https://twitter.com/miyama__akira)のリプないしDMでも結構です）。

（作成者: [深山](https://twitter.com/miyama__akira)）


