## 『晋書』全般

### 本文
* 中央研究院・歴史語言研究所の[漢籍電子文献資料庫](http://hanchi.ihp.sinica.edu.tw/ihp/hanji.htm)……『晋書』本文が見られる。でも個々のページのパーマリンクが不明（そもそもパーマリンクがあるのかがわからない）。
* Wikisource の[『晉書』](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8)……『晋書』本文が見られる。信用度は劣るだろうけれども、個々のページにパーマリンクできるのが便利。
* 中國哲學書電子化計劃の[『晉書』](https://ctext.org/wiki.pl?if=gb&res=788577)……画像版へのリンクもあり、本文の個別の段落にパーマリンクもできるので便利。
* 早稲田大学古典籍総合データベースの[『晋書』](https://www.wul.waseda.ac.jp/kotenseki/html/ri08/ri08_01735_0063/index.html)……画像
* 国立国会図書館デジタルコレクションで和刻本正史シリーズの『晋書』（[1](https://dl.ndl.go.jp/pid/12212167)・[2](https://dl.ndl.go.jp/pid/12212101)・[3](https://dl.ndl.go.jp/pid/12212100)）を閲覧できる。

### 『晉書斠注』
* Internet Archive の[『晉書斠注』](https://archive.org/search.php?query=%E6%99%89%E6%9B%B8%E6%96%A0%E6%B3%A8)……画像。
* Wikimedia Commons の[『晉書斠注』](https://commons.wikimedia.org/w/index.php?search=%E6%99%89%E6%9B%B8%E6%96%A0%E6%B3%A8&title=Special%3ASearch&go=Go&ns0=1&ns6=1&ns12=1&ns14=1&ns100=1&ns106=1)……画像。
* 中國哲學書電子化計劃の[『晉書斠注』](https://ctext.org/library.pl?if=gb&res=1681)……画像。
* [正史三國志硏究會](http://suisui.ojaru.jp)……『晉書斠注』の一部が文字データとして公開されている。
* [PDFあり]
  [吳士鑑「晉書斠注序」譯註稿](http://doi.org/10.34382/00014555)
  （赤羽 奈津子・猪俣 貴幸）

### 『晉書校勘記』
* Internet Archive の『晉書校勘記』
  [(一)](https://archive.org/details/02077716.cn)・
  [(二)](https://archive.org/details/02077717.cn)
  （周家祿）……画像。

### 『晉書音義』
* Internet Archive の[『晉書音義』](https://archive.org/details/06079130.cn/page/n1/mode/2up)……画像。
* Wikisource の『晉書音義』
  [序](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%BA%8F)・
  [卷之上](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%8D%B7%E4%B9%8B%E4%B8%8A)・
  [卷之中](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%8D%B7%E4%B9%8B%E4%B8%AD)・
  [卷之下](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%8D%B7%E4%B9%8B%E4%B8%8B)

### 『四庫全書總目提要』
* Wikisource の『四庫全書總目提要』のうち、[『晉書』の箇所](https://zh.wikisource.org/wiki/%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E7%B8%BD%E7%9B%AE%E6%8F%90%E8%A6%81/%E5%8D%B7045#%E6%99%89%E6%9B%B8%E2%80%A2%E4%B8%80%E7%99%BE%E4%B8%89%E5%8D%81%E5%8D%B7%E3%80%88%E5%85%A7%E5%BA%9C%E5%88%8A%E6%9C%AC%E3%80%89)

### 人物のリスト
* Wikipedia (ja) の[「晋書」のページ](https://ja.wikipedia.org/wiki/%E6%99%8B%E6%9B%B8#%E5%86%85%E5%AE%B9)……『晋書』に立伝されている人物のページへのリンク集として使える気がする。また、リンク先の個々の人物のページは、列伝の現代語訳に近い場合もあるので、漢文の読み方がわからないときのヒントとしても有用。
* Wikipedia (zh) の[「晋书」のページ](https://zh.wikipedia.org/wiki/%E6%99%8B%E4%B9%A6#%E3%80%8A%E6%99%8B%E4%B9%A6%E3%80%8B%E7%9B%AE%E5%BD%95)……同上（ただし中国語）。

### 現代中国語訳
* [白话晋书](https://www.duguoxue.com/ershisishi/baihuajinshu/)……『晋書』の現代中国語訳。
* [「古诗大全」というサイト](https://www.gushimi.org/)での『晋書』の現代中国語訳……目次が[《50章》](https://www.gushimi.org/24shi/jinshu/50/)、[《100章》](https://www.gushimi.org/24shi/jinshu/100/)、[《150章》](https://www.gushimi.org/24shi/jinshu/150/)と分かれている。
* [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/mode/2up)

### 各種考証
* [武英殿本二十三史考證（十五）](https://commons.wikimedia.org/wiki/File:CADAL02035620_%E6%AD%A6%E8%8B%B1%E6%AE%BF%E6%9C%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E5%8F%B2%E8%80%83%E8%AD%89%EF%BC%88%E5%8D%81%E4%BA%94%EF%BC%89.djvu)
* 国立国会図書館デジタルコレクションの『続国訳漢文大成』に所収の『廿二史劄記』で[『晋書』を論じている箇所](https://dl.ndl.go.jp/info:ndljp/pid/1239993/137)
* 同じくデジコレにある李慈銘編『越縵堂読史札記. 晋書 宋書 梁書 魏書 隋書 南、北史』で[『晋書』を論じている箇所](https://dl.ndl.go.jp/info:ndljp/pid/1912194/4)
* Internet Archive の『廿二史考異』（銭大昕）（画像）で『晋書』を論じている部分（[八](https://archive.org/details/02082255.cn/page/n52/mode/2up)・[九](https://archive.org/details/02082256.cn/page/n2/mode/2up)・[十](https://archive.org/details/02082257.cn/page/n2/mode/2up)）
* Internet Archive の『十七史商榷』（王鳴盛）（画像）で『晋書』を論じている部分
	* [巻43](https://archive.org/details/02082209.cn/page/n57/mode/2up)
	* [巻44](https://archive.org/details/02082210.cn/page/n1/mode/2up)
	* [巻45](https://archive.org/details/02082210.cn/page/n25/mode/2up)
	* [巻46](https://archive.org/details/02082210.cn/page/n47/mode/2up)
	* [巻47](https://archive.org/details/02082211.cn/page/n1/mode/2up)
	* [巻48](https://archive.org/details/02082211.cn/page/n25/mode/2up)
	* [巻49](https://archive.org/details/02082211.cn/page/n45/mode/2up)
	* [巻50](https://archive.org/details/02082211.cn/page/n61/mode/2up)
	* [巻51](https://archive.org/details/02082212.cn/page/n1/mode/2up)
	* [巻52](https://archive.org/details/02082212.cn/page/n23/mode/2up)
* Wikisource の『陔餘叢考』（趙翼）の[「《晉書》舛訛」](https://zh.wikisource.org/wiki/%E9%99%94%E9%A4%98%E5%8F%A2%E8%80%83/06#%E3%80%8A%E6%99%89%E6%9B%B8%E3%80%8B%E8%88%9B%E8%A8%9B)
* Internet Archive の『讀史舉正』（張熷）（画像）で『晋書』を論じている部分（[二](https://archive.org/details/02082291.cn/page/n14/mode/2up)・[三](https://archive.org/details/02082292.cn/page/n2/mode/2up)）
* Internet Archive の[『諸史拾遺』](https://archive.org/details/02082288.cn/page/n22/mode/2up)（錢大昕）
* Internet Archive の[『十駕齋養新錄』](https://archive.org/details/02096316.cn/page/n26/mode/2up)（錢大昕）
* Internet Archive の[『惜抱軒筆記』](https://archive.org/details/02096347.cn/page/n24/mode/2up)（姚鼐）
* Internet Archive の[『諸史考異』](https://archive.org/details/02082294.cn/page/n42/mode/2up)（洪頤煊）
* Internet Archive の[『瞥記』](https://archive.org/details/02096461.cn/page/n58/mode/2up)（梁玉繩）
* Internet Archive の[『讀史糾謬』](https://archive.org/details/02082167.cn/mode/2up)（牛運震）
* [Wikisource の『日知錄集釋』](https://zh.wikisource.org/wiki/%E6%97%A5%E7%9F%A5%E9%8C%84%E9%9B%86%E9%87%8B)（著：顧炎武、釋：黄汝成）を[「晉書」で検索した結果](https://zh.wikisource.org/w/index.php?search=%22%E6%99%89%E6%9B%B8%22&prefix=%E6%97%A5%E7%9F%A5%E9%8C%84%E9%9B%86%E9%87%8B&title=Special:%E6%90%9C%E7%B4%A2&profile=advanced&fulltext=1)
    * Internet Archive の[画像版](https://archive.org/search.php?query=%E6%97%A5%E7%9F%A5%E9%8C%84%E9%9B%86%E9%87%8B%20%E9%BB%83%E6%B1%9D%E6%88%90)

