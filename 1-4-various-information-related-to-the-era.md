## 西晋・東晋・五胡十六国に関係ある色々なもの
* Wikipedia (zh) の[「Category:晉朝人列表」のページ](https://zh.wikipedia.org/wiki/Category:%E6%99%89%E6%9C%9D%E4%BA%BA%E5%88%97%E8%A1%A8)……西晋・東晋の人物を種類別にまとめた各種の人物一覧表へのリンク集。
* Wikipedia (zh) の[「Category:五胡十六國人列表」のページ](https://zh.wikipedia.org/wiki/Category:%E4%BA%94%E8%83%A1%E5%8D%81%E5%85%AD%E5%9C%8B%E4%BA%BA%E5%88%97%E8%A1%A8)……五胡十六国に関する同様のリンク集。
* Wikipedia (zh) の[「Category:中国士族世系图」のページ](https://zh.wikipedia.org/wiki/Category:%E4%B8%AD%E5%9B%BD%E5%A3%AB%E6%97%8F%E4%B8%96%E7%B3%BB%E5%9B%BE)……系図の一覧表
* [転転大陸](http://home.att.ne.jp/iota/ten_tairiku/)……「二十五史家系図」と「皇帝（王）家系図」がある。
* 中央研究院・數位文化中心の[兩千年中西曆轉換](https://sinocal.sinica.edu.tw/)……日付の変換に便利。
* [浙江大學圖書館古籍碑帖研究墓與保護中心 中國歴代墓誌數據庫](http://csid.zju.edu.cn/tomb/stone)……墓誌データベース。ものによって拓本画像が見られたり、文字の書き起こしが見られたりする。
* 中國哲學書電子化計劃の[『漢魏南北朝墓志匯編』](https://ctext.org/wiki.pl?if=gb&res=872825)
* Wikimedia の『石刻史料新編 第三輯 一般類 漢魏南北朝墓誌集釋 上』のうち、晋に該当する箇所
	* [目次](https://commons.wikimedia.org/w/index.php?title=File:SSID-10486416_%E7%9F%B3%E5%88%BB%E5%8F%B2%E6%96%99%E6%96%B0%E7%B7%A8_%E7%AC%AC%E4%B8%89%E8%BC%AF_%E4%B8%80%E8%88%AC%E9%A1%9E_%E6%BC%A2%E9%AD%8F%E5%8D%97%E5%8C%97%E6%9C%9D%E5%A2%93%E8%AA%8C%E9%9B%86%E9%87%8B_%E4%B8%8A.pdf&page=12)
	* [説明](https://commons.wikimedia.org/w/index.php?title=File:SSID-10486416_%E7%9F%B3%E5%88%BB%E5%8F%B2%E6%96%99%E6%96%B0%E7%B7%A8_%E7%AC%AC%E4%B8%89%E8%BC%AF_%E4%B8%80%E8%88%AC%E9%A1%9E_%E6%BC%A2%E9%AD%8F%E5%8D%97%E5%8C%97%E6%9C%9D%E5%A2%93%E8%AA%8C%E9%9B%86%E9%87%8B_%E4%B8%8A.pdf&page=39)
	* [図版](https://commons.wikimedia.org/w/index.php?title=File:SSID-10486416_%E7%9F%B3%E5%88%BB%E5%8F%B2%E6%96%99%E6%96%B0%E7%B7%A8_%E7%AC%AC%E4%B8%89%E8%BC%AF_%E4%B8%80%E8%88%AC%E9%A1%9E_%E6%BC%A2%E9%AD%8F%E5%8D%97%E5%8C%97%E6%9C%9D%E5%A2%93%E8%AA%8C%E9%9B%86%E9%87%8B_%E4%B8%8A.pdf&page=279)
* Wikisource の『資治通鑑』（[巻79・晋紀1](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7079) 〜 [巻118・晋紀40](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7118)）
* 国立国会図書館デジタルコレクションの『続国訳漢文大成』に所収の『資治通鑑』（[巻79・晋紀1](https://dl.ndl.go.jp/info:ndljp/pid/1239900/99) 〜 [巻118・晋紀40](https://dl.ndl.go.jp/info:ndljp/pid/1239942/160)）
* 同じくデジコレの[『魏晋南北朝通史』](https://dl.ndl.go.jp/info:ndljp/pid/1917952?tocOpened=1)（岡崎文夫）……このうち[内編](https://www.heibonsha.co.jp/search/?search_word=%E9%AD%8F%E6%99%8B%E5%8D%97%E5%8C%97%E6%9C%9D%E9%80%9A%E5%8F%B2&x=0&y=0&search_menu=keyword)は平凡社東洋文庫としても出版されている。
* 同じくデジコレの『国訳漢文大成』に所収の[『国訳搜神記』](https://dl.ndl.go.jp/info:ndljp/pid/1913008/47)と[『国訳搜神後記』](https://dl.ndl.go.jp/info:ndljp/pid/1913008/79)……志怪だけれど、『晋書』とたまにネタが被っている。なおこれは部分訳。
* Wikisource の[『搜神記』](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E8%A8%98)あるいは中國哲學書電子化計劃の[『搜神記』](https://ctext.org/wiki.pl?if=gb&res=839038)
* Wikisource の[『搜神後記』](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E5%BE%8C%E8%A8%98)あるいは中國哲學書電子化計劃の[『搜神後記』](https://ctext.org/wiki.pl?if=gb&res=184634)
* デジコレにある各社の『蒙求』……『晋書』に出てくる人物に関する句の注として『晋書』が引かれることがそれなりにあるので、それにより列伝（の一部）の訓読がわかる。[有朋堂書店「漢文叢書」のもの](https://dl.ndl.go.jp/info:ndljp/pid/958947)、[博文館「漢文叢書」のもの](https://dl.ndl.go.jp/info:ndljp/pid/950174)、[研究社「研究社学生文庫」のもの](https://dl.ndl.go.jp/info:ndljp/pid/1111036)がある。
* [「魯迅『古小説鈎沈』校本」](http://hdl.handle.net/2433/227151)（中嶋長文・伊藤令子・平田昌司）
* Wikisource の[『世説新語』](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E)……『晋書』とよくネタが被っている。注が別の色になっていて見やすい。
* 中國哲學書電子化計劃の[『世説新語』](https://ctext.org/shi-shuo-xin-yu/zh)……条ごとにパーマリンクをはれるので便利。
* 中國哲學書電子化計劃の[『世説新語箋疏』](https://ctext.org/wiki.pl?if=gb&res=40889)
* [「世説新語 on the Web」](https://kakuyomu.jp/works/1177354054884883338)……『世説新語』全話がウェブ小説として読める。
* [「続三国志演義─通俗續三國志─」](https://kakuyomu.jp/works/1177354054884106768)……曰く、「三國志演義につづけ！と明代に書かれた二番煎じの二次創作『新刻續編三國志』、江戸時代に中村昂然が読み下した三番煎じを好事家のために超訳した四番煎じ」だそうで。
* [「続三国志演義II─通俗續後三國志前編─」](https://kakuyomu.jp/works/1177354054884740663)……その続き（第二弾）。
* [「続三国志演義III─通俗續後三國志後編─」](https://kakuyomu.jp/works/1177354054887842567)……その続き（第三弾）。
* Wikisource の[『九家舊晉書』](https://zh.wikisource.org/wiki/%E4%B9%9D%E5%AE%B6%E8%88%8A%E6%99%89%E6%9B%B8)
* Wikisource の[『晉紀』](https://zh.wikisource.org/wiki/%E6%99%89%E7%B4%80)（干宝）
* Wikisource の[『建康實錄』](https://zh.wikisource.org/wiki/%E5%BB%BA%E5%BA%B7%E5%AF%A6%E9%8C%84)
* 中華電子佛典協會 (Chinese Buddhist Electronic Text Association; CBETA) の[『高僧伝』](https://cbetaonline.dila.edu.tw/zh/T2059_001) ないし Wikisource の[『梁高僧伝』](https://zh.wikisource.org/wiki/%E6%A2%81%E9%AB%98%E5%83%A7%E5%82%B3)
* CBETA の[『弘明集』](https://cbetaonline.dila.edu.tw/zh/T2102) ないし Wikisource の[『弘明集』](https://zh.wikisource.org/wiki/%E5%BC%98%E6%98%8E%E9%9B%86) ないし 中國哲學書電子化計劃の[『弘明集』](https://ctext.org/wiki.pl?if=gb&res=393424)
* CBETA の[『比丘尼傳』卷第一](https://cbetaonline.dila.edu.tw/zh/T2063_001)……この巻は晋に対応する。
* Wikisource の[『全晉文』](https://zh.wikisource.org/wiki/%E5%85%A8%E6%99%89%E6%96%87)ないし中國哲學書電子化計劃の[『全晉文』](https://ctext.org/wiki.pl?if=gb&res=614696)
* Wikisource の[『昭明文選』](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8)
* デジコレの『国訳漢文大成』に所収の『文選』[上](https://dl.ndl.go.jp/info:ndljp/pid/1912763)・[中](https://dl.ndl.go.jp/info:ndljp/pid/1912796)・[下](https://dl.ndl.go.jp/info:ndljp/pid/1912835)
* 中文百科在线の[『先秦漢魏晉南北朝詩』](http://www.zwbk.org/zh-tw/Lemma_Show/201846.aspx)
* [魏晋南北ブログ](http://gishinnanboku.blog.fc2.com/)
* [五胡十六国の歴史を語るブログ](http://blog.livedoor.jp/masudazigo-2nd/)
* 早稲田大学古典籍総合データベースの[『十六国彊域志』](https://www.wul.waseda.ac.jp/kotenseki/html/ru05/ru05_01386/index.html)（洪亮吉）（画像）
* 早稲田大学古典籍総合データベースの[『水経注図』](https://www.wul.waseda.ac.jp/kotenseki/html/ru05/ru05_03617/index.html)（楊守敬・熊会貞）（画像）
* Wikisource の[『水經注』](https://zh.wikisource.org/wiki/%E6%B0%B4%E7%B6%93%E6%B3%A8)（酈道元）
* [「『法苑珠林』の總合的硏究 - 主として『法苑珠林』所錄『冥祥記』の本文校訂竝びに選注選譯 - : 『法苑珠林』所錄『冥祥記』の本文校訂竝びに選注選譯」](http://id.nii.ac.jp/1374/00000070/)（若槻俊秀・佐藤義寛・乾源俊・浦山あゆみ・今場正美・福井敏・本井牧子・仁木夏實・早川智美・長谷川愼・稻垣淳央・藤井雅彥・大角紘一・一澤美帆・嘉村誠）
* CBETA の[『法苑珠林』](https://cbetaonline.dila.edu.tw/zh/T2122)
* Wikisource の[『冥祥記』](https://zh.wikisource.org/wiki/%E5%86%A5%E7%A5%A5%E8%A8%98)
* Wikisource の[『太平廣記』](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98)
* Wikisource の[『太平御覽』](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%BD)
* Wikisource にある陶弘景の[『真誥』](https://zh.wikisource.org/wiki/%E7%9C%9F%E8%AA%A5)
* [「眞誥研究 : 譯注篇」](http://hdl.handle.net/2433/98008)（吉川忠夫・麥谷邦夫）
* [『漢魏六朝百三家集』へのリンク](https://gitlab.com/miyama__akira/pages/-/blob/master/md/103.md)
* [画像版『全晉文』へのリンク集](https://miyama__akira.gitlab.io/pages/prose-list/Jin.html)
* [書籍 (IA)] 『補歷代史表』（萬斯同）
    * [卷十二（晉諸王世表）](https://archive.org/details/06059532.cn/page/n15/mode/2up)
    * [卷十三（晉功臣世表）](https://archive.org/details/06059532.cn/page/n39/mode/2up)
    * [卷十四（晉將相大臣年表）](https://archive.org/details/06059532.cn/page/n95/mode/2up)
    * [卷十五（東晉將相大臣年表）](https://archive.org/details/06059533.cn/page/n1/mode/2up)
    * [卷十六（晉方鎮年表）](https://archive.org/details/06059533.cn/page/n55/mode/2up)
    * [卷十七（東晉方鎮年表）](https://archive.org/details/06059533.cn/page/n83/mode/2up)
    * [卷十八（僭偽諸國世表）](https://archive.org/details/06059534.cn/page/n1/mode/2up)
    * [卷十九（僭位諸國年表）](https://archive.org/details/06059534.cn/page/n15/mode/2up)
    * [卷二十（偽漢將相大臣年表）](https://archive.org/details/06059534.cn/page/n53/mode/2up)
    * [卷二十一（偽成將相大臣年表）](https://archive.org/details/06059534.cn/page/n65/mode/2up)
    * [卷二十二（偽趙將相大臣年表）](https://archive.org/details/06059534.cn/page/n79/mode/2up)
    * [卷二十三（偽燕將相大臣年表）](https://archive.org/details/06059534.cn/page/n91/mode/2up)
    * [卷二十四（偽秦將相大臣年表）](https://archive.org/details/06059534.cn/page/n103/mode/2up)
    * [卷二十五（偽後秦將相大臣年表）](https://archive.org/details/06059535.cn/page/n1/mode/2up)
    * [卷二十六（偽後燕將相大臣年表）](https://archive.org/details/06059535.cn/page/n11/mode/2up)
    * [卷二十七（偽南燕將相大臣年表）](https://archive.org/details/06059535.cn/page/n19/mode/2up)
    * [卷二十八（宋諸王世表）](https://archive.org/details/06059535.cn/page/n25/mode/2up)の[「附二王後」](https://archive.org/details/06059535.cn/page/n37/mode/2up)に零陵王（司馬氏）の系譜あり。
    * [卷三十一（齊諸王世表）](https://archive.org/details/06059535.cn/page/n103/mode/2up)の[末尾](https://archive.org/details/06059535.cn/page/n117/mode/2up)にも零陵王（司馬氏）の記載あり。

