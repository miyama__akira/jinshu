## 仮名漢字変換辞書の元データ
各人の環境に合わせてフィールドの並び順や改行コードなどを調整する必要はあるが（また、必ずしも網羅的データではないかもしれないが）、『晋書』で扱っている時期に登場する人たちの名前の仮名漢字変換辞書の元データとして使えるものを挙げておく。

* 佐藤氏の[晋宋春秋＆中原戦記の人名＆読みリスト](https://jinsung.chronicle.wiki/d/%C3%CF%CC%BE%BA%F7%B0%FA)
* 佐藤氏の[世説新語索引　素データ](https://jinsung.chronicle.wiki/d/%C0%A4%C0%E2%BF%B7%B8%EC%BA%F7%B0%FA%A1%A1%C1%C7%A5%C7%A1%BC%A5%BF)、ないしは、それを[元にして](https://gitlab.com/miyama__akira/pages/-/tree/master/data/sesetsu-shingo)生成した[人名辞書](https://gitlab.com/miyama__akira/pages/-/blob/master/data/sesetsu-shingo/personal-name-dict-generated-from-rawdata.csv)
* 自作の[『晋書』人名辞書](dict/names-of-people.txt)……対象人物は、このページで見出しに立てている人たち。形式は下記の『世説新語』人名辞書と同じ。複数の読みができそうな人には複数の読みを振り、複数の字体（「恵」と「惠」など）があるものはいずれも変換できるようにした（つもり）。
* 自作の[『世説新語』人名辞書](https://gitlab.com/miyama__akira/pages/-/blob/master/data/sesetsu-shingo/personal-name-dict.txt)と[その説明ページ](https://miyama__akira.gitlab.io/pages/sesetsu-shingo/personal-name-dict.html)

