## 紀

### 巻1 帝紀第1
#### 高祖宣帝（司馬懿）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%87%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [書籍 (IA)]
  [晉書斠注(一)](https://archive.org/details/02077718.cn/page/n84/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/n33/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「晉宣狼顧」（第51句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/47)
* [ブログ]
  [『晋書』宣帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/06/24/000100)〜
  [『晋書』宣帝紀を読んでみよう:その25](https://t-s.hatenablog.com/entry/2019/07/18/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [司馬懿伝/上。仲達が曹操より怖いもの](http://3guozhi.net/hito/bai1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [司馬懿伝/中。諸葛亮が分からない](http://3guozhi.net/hito/i1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [司馬懿伝/下「魏臣としての敗北者」](http://3guozhi.net/q/ctc1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m001.html)
* [ブログ]
  [宣帝紀](https://estar.jp/novels/25594548/viewer?page=2)
  （@淡々晋書）
* [ブログ]
  [晋書　高祖宣帝懿紀](http://strawberrymilk.gooside.com/text/shinjo/01-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p10)

### 巻2 帝紀第2
#### 世宗景帝（司馬師）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B8%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [書籍 (IA)]
  [晉書斠注(二)](https://archive.org/details/02077719.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/21/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』景帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/07/22/000100)〜
  [『晋書』景帝紀を読んでみよう:その11](https://t-s.hatenablog.com/entry/2019/08/07/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [ドＭ仲達の長男はドＳ。司馬師伝](http://3guozhi.net/hito/sibasi1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m002.html)

#### 太祖文帝（司馬昭）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%98%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [書籍 (IA)]
  [晉書斠注(二)](https://archive.org/details/02077719.cn/page/n30/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/27/mode/2up)
* [ブログ]
  [『晋書』文帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/08/12/000100)〜
  [『晋書』文帝紀を読んでみよう:その23](https://t-s.hatenablog.com/entry/2019/09/10/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [逆天下三分、逆出師ノ表。司馬昭伝](http://3guozhi.net/hito/sibasyo1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m002.html)
* [『三國志』巻4（高貴鄉公髦）](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B704#%E9%AB%98%E8%B2%B4%E9%84%89%E5%85%AC)の注にある、有名な「司馬昭之心，路人所知也。」という台詞を引用する記述：
    * 北宋で起きた[同文館の獄](https://ja.wikipedia.org/wiki/%E5%90%8C%E6%96%87%E9%A4%A8%E3%81%AE%E7%8D%84)において、「司馬昭之心，路人所知」云々とある文及甫の書状の意味をめぐる争いがあったことが、『宋史』の[巻200（刑法志）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7200)・[巻340（劉摯）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7340#%E5%8A%89%E6%91%AF)・[巻471（安惇）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7471#%E5%AE%89%E6%83%87)に記されている。
    * [『明史』巻246（王允成）](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7246)でも「司馬昭之心，路人知之矣」と引用している。
    * [『朝鮮王朝實錄』（純祖實錄）の十二年十一月八日](https://zh.wikisource.org/wiki/%E6%9C%9D%E9%AE%AE%E7%8E%8B%E6%9C%9D%E5%AF%A6%E9%8C%84/%E7%B4%94%E7%A5%96%E5%AF%A6%E9%8C%84/%E5%8D%81%E4%BA%8C%E5%B9%B4#11%E6%9C%888%E6%97%A5)にも「及夫後來，其侄龜柱之凶言有繼發，則唱喁相應，脈絡相續，如印一板，如貫一串，司馬昭之心，路人皆知之矣。吁！亦懍乎危哉。」とある。


### 巻3 帝紀第3
#### 世祖武帝（司馬炎）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%82%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [書籍 (IA)]
  [晉書斠注(三)](https://archive.org/details/02077720.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/41/mode/2up)
* [ブログ]
  [『晋書』武帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/09/11/000100)〜
  [『晋書』武帝紀を読んでみよう:その49](https://t-s.hatenablog.com/entry/2019/11/25/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [巻三　帝紀第三　武帝紀（1）](https://readingnotesofjinshu.com/translation/annals/vol-3_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/annals/vol-3_2)、
  [同（3）](https://readingnotesofjinshu.com/translation/annals/vol-3_3)
  （@晋書簡訳所）
* [ブログ]
  [武帝紀](http://strawberrymilk.gooside.com/text/shinjo/03-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [周曇の「詠史詩」の中の「晉武帝」と「再吟」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%99%89%E6%AD%A6%E5%B8%9D)
* [王夫之『讀通鑒論』巻11](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B711)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/324)）
* [PDFあり]
  [中国の故事・伝説に取材した大小暦](https://dl.ndl.go.jp/info:ndljp/pid/3051277)（相島 宏）
……[「日本の暦」展](https://www.ndl.go.jp/koyomi/index.html)に関連した論文。
この論文中の「晋武帝羊車遊宴図」の出典は[『恵合余見』第二冊](https://dl.ndl.go.jp/info:ndljp/pid/1286782/19)。
類似の「晋武帝羊車遊宴図」は、[『和漢名画苑』](https://www.kanazawa-bidai.ac.jp/edehon-db/quick.cgi?mode=result&id=83)の[四巻](https://www.kanazawa-bidai.ac.jp/edehon-db/img/83/wakan-meigaen04d021a.jpg)でも見られる。
論文中に引かれた[『通俗続三國志』](https://dl.ndl.go.jp/info:ndljp/pid/775963/25)には、[現代語訳](https://kakuyomu.jp/works/1177354054884106768/episodes/1177354054884109208)もある。

### 巻4 帝紀第4
#### 孝恵帝（司馬衷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A1%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [書籍 (IA)]
  [晉書斠注(四)](https://archive.org/details/02077721.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/67/mode/2up)
* [ブログ]
  [巻四　帝紀第四　恵帝紀（1）](https://readingnotesofjinshu.com/translation/annals/vol-4_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/annals/vol-4_2)
  （@晋書簡訳所）
* [ブログ]
  [恵帝紀](http://strawberrymilk.gooside.com/text/shinjo/04-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m004.html)
* [周曇の「詠史詩」の中の「惠帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%83%A0%E5%B8%9D)
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%83%A0%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/340)）


### 巻5 帝紀第5
#### 孝懐帝（司馬熾）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%86%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [書籍 (IA)]
  [晉書斠注(四)](https://archive.org/details/02077721.cn/page/n60/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/83/mode/2up)
* [ブログ]
  [巻五　帝紀第五　懐帝紀](https://readingnotesofjinshu.com/translation/annals/vol-5_1)
  （@晋書簡訳所）
* [ブログ]
  [孝懐帝紀](http://strawberrymilk.gooside.com/text/shinjo/05-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m005.html)
* [周曇の「詠史詩」の中の「懷帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%87%B7%E5%B8%9D)
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%87%B7%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/352)）


#### 孝愍帝（司馬鄴）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%84%B4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [書籍 (IA)]
  [晉書斠注(四)](https://archive.org/details/02077721.cn/page/n94/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/90/mode/2up)
* [ブログ]
  [巻五　帝紀第五　愍帝紀](https://readingnotesofjinshu.com/translation/annals/vol-5_2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m005.html)
* [周曇の「詠史詩」の中の「愍帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%84%8D%E5%B8%9D)
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%B9%A3%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/359)）

### 巻6 帝紀第6
#### 中宗元帝（司馬睿）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%9D%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [書籍 (IA)]
  [晉書斠注(五)](https://archive.org/details/02077722.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/103/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六　帝紀第六　元帝紀（1）](https://readingnotesofjinshu.com/translation/annals/vol-6_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/annals/vol-6_2)
  （@晋書簡訳所）
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)
* [ブログ]
  [『魏書』巻九十六　列伝第八十四　僭晋司馬叡（1）](https://readingnotesofjinshu.com/translation/appendix/bookofwei-vol96_1)
  （@晋書簡訳所）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、元帝紀に引かれた
  [「勸進表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/412)（劉琨によるもの）がある。
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%9D%B1%E6%99%89%E5%85%83%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/365)）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m006.html)

#### 粛宗明帝（司馬紹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B4%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [書籍 (IA)]
  [晉書斠注(五)](https://archive.org/details/02077722.cn/page/n44/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/116/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六　帝紀第六　明帝紀](https://readingnotesofjinshu.com/translation/annals/vol-6_3)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh1.html)
  （@いつか書きたい『三国志』）
* [『文選』所収の王巾「頭陀寺碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%A0%AD%E9%99%80%E5%AF%BA%E7%A2%91%E6%96%87)にある「漢晉兩明，並勒丹青之飾。」（『国訳漢文大成　文学部第四巻　文選下巻』の[該当ページ](https://dl.ndl.go.jp/info:ndljp/pid/1912835/388)）には、明帝が自ら仏画を描いた話が反映されている（この話は『晋書』巻77・蔡謨伝に載っている）。
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E6%98%8E%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/372)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m006.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

### 巻7 帝紀第7
#### 顕宗成帝（司馬衍）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/125/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻七　帝紀第七　成帝紀](https://readingnotesofjinshu.com/translation/annals/vol-7_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E6%88%90%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/373)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m007.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 康帝（司馬岳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B2%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n36/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/135/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh11.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻七　帝紀第七　康帝紀](https://readingnotesofjinshu.com/translation/annals/vol-7_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E5%BA%B7%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/387)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m007.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

### 巻8 帝紀第8
#### 孝宗穆帝（司馬聃）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%81%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n48/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/139/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh12.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻八　帝紀第八　穆帝紀](https://readingnotesofjinshu.com/translation/annals/vol-8_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E7%A9%86%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/389)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m008.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 哀帝（司馬丕）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%B8%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n74/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/149/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh17.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻八　帝紀第八　哀帝紀](https://readingnotesofjinshu.com/translation/annals/vol-8_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%93%80%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/396)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m008.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 廃帝海西公（司馬奕）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n84/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/152/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh18.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻八　帝紀第八　海西公紀](https://readingnotesofjinshu.com/translation/annals/vol-8_3)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%B8%9D%E5%A5%95)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/399)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m008.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

### 巻9 帝紀第9
#### 太宗簡文帝（司馬昱）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%98%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/159/mode/2up)
* [ブログ]
  [『晋書』帝紀９、簡文帝と孝武帝の翻訳](http://3guozhi.net/q/tkq1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻九　帝紀第九　簡文帝紀](https://readingnotesofjinshu.com/translation/annals/vol-9_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E7%B0%A1%E6%96%87%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/402)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m009.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 孝武帝（司馬曜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9B%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n18/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/163/mode/2up)
* [ブログ]
  [『晋書』帝紀９、簡文帝と孝武帝の翻訳](http://3guozhi.net/q/tkq4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [「紫微垣」晋書孝武帝本紀他](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854995)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [巻九　帝紀第九　孝武帝紀](https://readingnotesofjinshu.com/translation/annals/vol-9_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%AD%9D%E6%AD%A6%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/402)）
* [ブログ]
  晋書９　　孝武帝本紀（[孝武１　　３８４年　上](https://kakuyomu.jp/works/16816452219408440529/episodes/16816927860938784461)〜[孝武１５　３９６年](https://kakuyomu.jp/works/16816452219408440529/episodes/16816927861629857675)）
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m009.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)


### 巻10 帝紀第10
#### 安帝（司馬徳宗）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n56/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/179/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%C4%EB%B5%AA%C2%E8%BD%BD%A1%A1%B0%C2%C4%EB%A1%A1%BB%CA%C7%CF%F9%FE%BD%A1)
* [ブログ]
  [『晋書』帝紀１０、安帝と恭帝の翻訳](http://3guozhi.net/q/tkj1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻十　帝紀第十　安帝紀](https://readingnotesofjinshu.com/translation/annals/vol-10_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%AE%89%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/413)）
* [ブログ]
  晋書１０　安帝恭帝本紀（[安帝１　　３９７年](https://kakuyomu.jp/works/16816452219408440529/episodes/16816927861860662405)〜[安帝２２　４１８年　弑殺](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864862172)）
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m010.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 恭帝（司馬徳文）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n100/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/190/mode/2up)
* [ブログ]
  [『晋書』帝紀１０、安帝と恭帝の翻訳](http://3guozhi.net/q/tkj6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻十　帝紀第十　恭帝紀](https://readingnotesofjinshu.com/translation/annals/vol-10_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E6%81%AD%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/434)）
* [ブログ]
  晋書１０　安帝恭帝本紀（[恭帝１　　司馬徳文](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864875057)〜[恭帝２　　４１９年　元熙](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139555031262005)）
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m010.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

