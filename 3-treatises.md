## 志
### 巻11 志第1 天文上
* [書籍 (IA)]
  [晉書斠注(八)](https://archive.org/details/02077725.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [天文上](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/195/mode/2up)
    * [天體](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/196/mode/2up)
    * [儀象](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/202/mode/2up)
    * [天文經星](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/206/mode/2up)
    * [中宮](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/207/mode/2up)
    * [二十八舍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/218/mode/2up)
    * [星官在二十八宿之外者](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/222/mode/2up)
    * [天漢起沒](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/225/mode/2up)
    * [十二次度數](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/225/mode/2up)
    * [州郡躔次](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/227/mode/2up)
* [書籍 (NDL)]
  [『世界の名著』続1巻](https://dl.ndl.go.jp/info:ndljp/pid/2935195)所収
  「晋書天文志」
  （山田慶児・坂出祥伸・藪内清訳）
  ……「収録著作について」の中の[「『晋書』天文志」](https://dl.ndl.go.jp/info:ndljp/pid/2935195/33)のセクションも参考。
  「天文志」の中の下位セクションごとに、訳文と注へのリンクをまとめておく（数字はコマ番号）。

| 項目 | 訳 | 注 |
| :--- | ---: | ---: |
| （序） | [119](https://dl.ndl.go.jp/info:ndljp/pid/2935195/119) | [236](https://dl.ndl.go.jp/info:ndljp/pid/2935195/236) |
| 天體 | [120](https://dl.ndl.go.jp/info:ndljp/pid/2935195/120) | [237](https://dl.ndl.go.jp/info:ndljp/pid/2935195/237) |
| 儀象 | [124](https://dl.ndl.go.jp/info:ndljp/pid/2935195/124) | [239](https://dl.ndl.go.jp/info:ndljp/pid/2935195/239) |
| 天文經星 | [126](https://dl.ndl.go.jp/info:ndljp/pid/2935195/126) | [239](https://dl.ndl.go.jp/info:ndljp/pid/2935195/239) |
| 中宮 | [127](https://dl.ndl.go.jp/info:ndljp/pid/2935195/127) | [239](https://dl.ndl.go.jp/info:ndljp/pid/2935195/239) |
| 二十八舍 |  |  |
| 　東方 | [134](https://dl.ndl.go.jp/info:ndljp/pid/2935195/134) | — |
| 　北方 | [134](https://dl.ndl.go.jp/info:ndljp/pid/2935195/134) | — |
| 　西方 | [135](https://dl.ndl.go.jp/info:ndljp/pid/2935195/135) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |
| 　南方 | [136](https://dl.ndl.go.jp/info:ndljp/pid/2935195/136) | — |
| 星官在二十八宿之外者 | [136](https://dl.ndl.go.jp/info:ndljp/pid/2935195/136) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |
| 天漢起沒 | [138](https://dl.ndl.go.jp/info:ndljp/pid/2935195/138) | — |
| 十二次度數 | [139](https://dl.ndl.go.jp/info:ndljp/pid/2935195/139) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |
| 州郡躔次 | [140](https://dl.ndl.go.jp/info:ndljp/pid/2935195/140) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |


### 巻12 志第2 天文中
* [書籍 (IA)]
  [晉書斠注(九)](https://archive.org/details/02077726.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [天文中](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/231/mode/2up)
    * [七曜](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/231/mode/2up)
    * [雜星氣](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/237/mode/2up)
    * [史傳事驗](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/250/mode/2up)
    * [天變](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/250/mode/2up)
    * [日蝕](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/250/mode/2up)
    * [月變](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/257/mode/2up)
    * [月奄犯五緯](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/258/mode/2up)
    * [五星聚舍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/262/mode/2up)
* [書籍 (NDL)]
  [『世界の名著』続1巻](https://dl.ndl.go.jp/info:ndljp/pid/2935195)所収
  「晋書天文志」
  （山田慶児・坂出祥伸・藪内清訳）
  ……「収録著作について」の中の[「『晋書』天文志」](https://dl.ndl.go.jp/info:ndljp/pid/2935195/33)のセクションも参考。
  「天文志」の中の下位セクションごとに、訳文と注へのリンクをまとめておく（数字はコマ番号）。

| 項目 | 訳 | 注 |
| :--- | ---: | ---: |
| 七曜 | [141](https://dl.ndl.go.jp/info:ndljp/pid/2935195/141) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 雜星氣 | [144](https://dl.ndl.go.jp/info:ndljp/pid/2935195/144) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　瑞星 | [145](https://dl.ndl.go.jp/info:ndljp/pid/2935195/145) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　妖星 | [145](https://dl.ndl.go.jp/info:ndljp/pid/2935195/145) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　客星 | [147](https://dl.ndl.go.jp/info:ndljp/pid/2935195/147) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　流星 | [147](https://dl.ndl.go.jp/info:ndljp/pid/2935195/147) | — |
| 　雲氣 | [148](https://dl.ndl.go.jp/info:ndljp/pid/2935195/148) | — |
| 　十煇 | [148](https://dl.ndl.go.jp/info:ndljp/pid/2935195/148) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　雜氣 | [150](https://dl.ndl.go.jp/info:ndljp/pid/2935195/150) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 史傳事驗 |  |  |
| 　天變 | [153](https://dl.ndl.go.jp/info:ndljp/pid/2935195/153) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 　日蝕 | [154](https://dl.ndl.go.jp/info:ndljp/pid/2935195/154) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 　月變 | [158](https://dl.ndl.go.jp/info:ndljp/pid/2935195/158) | — |
| 　月奄犯五緯 | [158](https://dl.ndl.go.jp/info:ndljp/pid/2935195/158) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 　五星聚舍 | [161](https://dl.ndl.go.jp/info:ndljp/pid/2935195/161) | [243](https://dl.ndl.go.jp/info:ndljp/pid/2935195/243) |

* [ブログ]
  晋書１２　天文志（[天文１　　天変](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864949165)〜[天文１９　星変４０２年魏](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139556983195295)）
  (@ゆるふわ晋宋春秋４)……『宋書』天文志および『魏書』天象志もあわせて見てゆくもの。

### 巻13 志第3 天文下
* [書籍 (IA)]
  [晉書斠注(十)](https://archive.org/details/02077727.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [天文下](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/267/mode/2up)
    * [月五星犯列舍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/267/mode/2up)
    * [妖星客星](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/292/mode/2up)
    * [星流隕](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/300/mode/2up)
    * [雲氣](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/303/mode/2up)
* [書籍 (NDL)]
  [『世界の名著』続1巻](https://dl.ndl.go.jp/info:ndljp/pid/2935195)所収
  「晋書天文志」
  （山田慶児・坂出祥伸・藪内清訳）
  ……「収録著作について」の中の[「『晋書』天文志」](https://dl.ndl.go.jp/info:ndljp/pid/2935195/33)のセクションも参考。
  「天文志」の中の下位セクションごとに、訳文と注へのリンクをまとめておく（数字はコマ番号）。

| 項目 | 訳 | 注 |
| :--- | ---: | ---: |
| （史傳事驗） |  |  |
| 　月五星犯列舍　經星變附見 | [163](https://dl.ndl.go.jp/info:ndljp/pid/2935195/163) | [243](https://dl.ndl.go.jp/info:ndljp/pid/2935195/243) |
| 　妖星客星 | [178](https://dl.ndl.go.jp/info:ndljp/pid/2935195/178) | [243](https://dl.ndl.go.jp/info:ndljp/pid/2935195/243) |
| 　星流隕 | [183](https://dl.ndl.go.jp/info:ndljp/pid/2935195/183) | [244](https://dl.ndl.go.jp/info:ndljp/pid/2935195/244) |
| 　雲氣 | [184](https://dl.ndl.go.jp/info:ndljp/pid/2935195/184) | — |

* [ブログ]
  晋書１２　天文志（[天文１　　天変](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864949165)〜[天文１９　星変４０２年魏](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139556983195295)）
  (@ゆるふわ晋宋春秋４)……『宋書』天文志および『魏書』天象志もあわせて見てゆくもの。

### 巻14 志第4 地理上
* [書籍 (IA)]
  [晉書斠注(十一)](https://archive.org/details/02077728.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [地理上](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/305/mode/2up)
    * [總敘](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/305/mode/2up)
    * [司州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/315/mode/2up)
    * [兗州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/318/mode/2up)
    * [豫州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/319/mode/2up)
    * [冀州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/321/mode/2up)
    * [幽州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/322/mode/2up)
    * [平州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/324/mode/2up)
    * [并州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/325/mode/2up)
    * [雍州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/326/mode/2up)
    * [涼州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/328/mode/2up)
    * [秦州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/330/mode/2up)
    * [梁州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/331/mode/2up)
    * [益州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/332/mode/2up)
    * [寧州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/334/mode/2up)
* [書籍 (IA)]
  [晉書地理志新補正(一)](https://archive.org/details/02077799.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [晉書地理志新補正(二)](https://archive.org/details/02077800.cn/page/n2/mode/2up)
* [ブログ]
  [総叙](http://3guozhi.net/sy/m014a.html)・[司州～寧州](http://3guozhi.net/sy/m014b.html)
  （@いつか読みたい晋書訳）

### 巻15 志第5 地理下
* [書籍 (IA)]
  [晉書斠注(十二)](https://archive.org/details/02077729.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [地理下](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/337/mode/2up)
    * [青州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/337/mode/2up)
    * [徐州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/338/mode/2up)
    * [荊州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/341/mode/2up)
    * [揚州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/344/mode/2up)
    * [交州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/349/mode/2up)
    * [廣州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/351/mode/2up)
* [書籍 (IA)]
  [晉書地理志新補正(二)](https://archive.org/details/02077800.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m015.html)

### 巻16 志第6 律暦上
* [書籍 (IA)]
  [晉書斠注(十三)](https://archive.org/details/02077730.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/353/mode/2up)

### 巻17 志第7 律暦中
* [書籍 (IA)]
  [晉書斠注(十四)](https://archive.org/details/02077731.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/373/mode/2up)
    * [乾象曆](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/379/mode/2up)

### 巻18 志第8 律暦下
* [書籍 (IA)]
  [晉書斠注(十五)](https://archive.org/details/02077732.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/399/mode/2up)
    * [景初曆](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/400/mode/2up)
* [ブログ]
  [『晋書』巻十八 律暦志下所引 杜預『春秋長暦』](http://3guozhi.net/w/045.html)
  （@いつか書きたい三国志）

### 巻19 志第9 礼上
* [書籍 (IA)]
  [晉書斠注(十六)](https://archive.org/details/02077733.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/427/mode/2up)
* [ブログ]
  [礼志１　　郊](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557025529398)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志２　　殷祠](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557103823636)
  (@ゆるふわ晋宋春秋４)

### 巻20 志第10 礼中
* [書籍 (IA)]
  [晉書斠注(十七)](https://archive.org/details/02077734.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/459/mode/2up)
* [ブログ]
  [礼志３　　皇太后祭祀](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557167449553)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志４　　庶出家主祭母１](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557226259485)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志５　　庶出家主祭母２](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557309639150)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志６　　犯私諱解職請求](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557371033597)
  (@ゆるふわ晋宋春秋４)

### 巻21 志第11 礼下
* [書籍 (IA)]
  [晉書斠注(十八)](https://archive.org/details/02077735.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/491/mode/2up)
* [ブログ]
  [礼志７　　皇太子拝礼](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557447117098)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志８　　陳留王礼](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557509711338)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志９　　上礼](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557582287965)
  (@ゆるふわ晋宋春秋４)

### 巻22 志第12 楽上
* [書籍 (IA)]
  [晉書斠注(十八)](https://archive.org/details/02077735.cn/page/n60/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/513/mode/2up)

### 巻23 志第13 楽下
* [書籍 (IA)]
  [晉書斠注(十九)](https://archive.org/details/02077736.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/531/mode/2up)

### 巻24 志第14 職官
* [書籍 (IA)]
  [晉書斠注(二十)](https://archive.org/details/02077737.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/551/mode/2up)
* [ブログ]
  [『晋書』志第十四 「 職官」の翻訳](http://3guozhi.net/n/k3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [晋書巻二十四　志第十四　職官](http://suisui.ojaru.jp/shinjo_024_shi_14_shokkan.html)
  （@正史三國志硏究會）
* [ブログ]
  [巻二十四　志第十四　職官（1）](https://readingnotesofjinshu.com/translation/treatises/vol-24_1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m024.html)

### 巻25 志第15 輿服
* [書籍 (IA)]
  [晉書斠注(二十一)](https://archive.org/details/02077738.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/573/mode/2up)

### 巻26 志第16 食貨
* [書籍 (IA)]
  [晉書斠注(二十二)](https://archive.org/details/02077739.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/597/mode/2up)
* [書籍 (NDL)]
  [『食貨志彙編』](https://dl.ndl.go.jp/info:ndljp/pid/1271069/31)
  （南満洲鉄道調査部）……原文と注釈のみ。訓読文や訳文は無し。
  同じ本のようだが[別のデータ](https://dl.ndl.go.jp/info:ndljp/pid/1281725/33)もある。

### 巻27 志第17 五行上
* [書籍 (IA)]
  [晉書斠注(二十二)](https://archive.org/details/02077739.cn/page/n48/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [五行上](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/617/mode/2up)
    * [恒雨](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/636/mode/2up)
    * [服妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/637/mode/2up)
    * [雞禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/641/mode/2up)
    * [青祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/642/mode/2up)
    * [金沴木](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/643/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m027.html)

### 巻28 志第18 五行中
* [書籍 (IA)]
  [晉書斠注(二十三)](https://archive.org/details/02077740.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [五行中](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/645/mode/2up)
    * [恒陽](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/648/mode/2up)
    * [詩妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/653/mode/2up)
    * [毛蟲之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/659/mode/2up)
    * [犬禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/660/mode/2up)
    * [白眚白祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/662/mode/2up)
    * [木沴金](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/664/mode/2up)
    * [恒燠](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/665/mode/2up)
    * [草妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/666/mode/2up)
    * [羽蟲之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/669/mode/2up)
    * [羊禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/672/mode/2up)
    * [赤眚赤祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/672/mode/2up)

### 巻29 志第19 五行下
* [書籍 (IA)]
  [晉書斠注(二十四)](https://archive.org/details/02077741.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [五行下](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/675/mode/2up)
    * [恒寒](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/675/mode/2up)
    * [雷震](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/680/mode/2up)
    * [鼓妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/682/mode/2up)
    * [魚孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/682/mode/2up)
    * [蝗蟲](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/683/mode/2up)
    * [豕禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/684/mode/2up)
    * [黑眚黑祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/684/mode/2up)
    * [火沴水](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/685/mode/2up)
    * [恒風](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/686/mode/2up)
    * [夜妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/689/mode/2up)
    * [裸蟲之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/690/mode/2up)
    * [牛禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/691/mode/2up)
    * [黃眚黃祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/692/mode/2up)
    * [地震](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/693/mode/2up)
    * [山崩地陷裂](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/697/mode/2up)
    * [恒陰](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/700/mode/2up)
    * [射妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/700/mode/2up)
    * [龍蛇之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/701/mode/2up)
    * [馬禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/703/mode/2up)
    * [人痾](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/704/mode/2up)

### 巻30 志第20 刑法
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%BA%8C%E5%86%8C/page/n33/mode/2up)
* [PDFあり] 
  [訳注晋書刑法志 1〜11](https://cir.nii.ac.jp/articles?q=%E8%A8%B3%E6%B3%A8+%E6%99%8B%E6%9B%B8%E5%88%91%E6%B3%95%E5%BF%97&count=20&sortorder=0)
  （内田 智雄）
* [ブログ]
  晋書３０　刑法志（[刑法志　　復肉刑議](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558710374603)）
  (@ゆるふわ晋宋春秋４)


