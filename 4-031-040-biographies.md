## 列伝
### 巻31 列伝第1 后妃上
* [書誌のみ]
  [『晋書斠注』巻三十一 列伝第一 后妃上 訳注稿(上)](https://cir.nii.ac.jp/crid/1520853831956120448)
  （赤羽 奈津子・尾関 圭信・織田 めぐみ・小野 響・川見 健人・末川 洸介・千田 豊・安永 知晃）
* [書誌のみ]
  [『晋書斠注』巻三十一 列伝第一 后妃上 訳注稿(下)](https://cir.nii.ac.jp/crid/1520853833128966016)
  （赤羽 奈津子・伊藤 侑希・小野 響・三浦 雄城・安永 知晃）
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n74/mode/2up)

#### 宣穆張皇后（張春華）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%98%A5%E8%8F%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n76/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B0%EC%A1%A1%B9%A1%C8%DE%BE%E5)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 景懐夏侯皇后（夏侯徽）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E5%BE%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 景献羊皇后（羊徽瑜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E5%BE%BD%E7%91%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 文明王皇后（王元姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%85%83%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n80/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「文明皇后」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/218)

#### 武元楊皇后（楊艷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E8%89%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E8%89%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E8%89%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E8%89%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E8%89%B7)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n86/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 武悼楊皇后（楊芷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E8%8A%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E8%8A%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E8%8A%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E8%8A%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E8%8A%B7)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n90/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 左貴嬪（左芬）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B7%A6%E8%8A%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B7%A6%E8%8A%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B7%A6%E8%8A%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B7%A6%E8%8A%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B7%A6%E8%8A%AC)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n98/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 胡貴嬪（胡芳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E8%8A%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E8%8A%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E8%8A%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E8%8A%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E8%8A%B3)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* [PDFあり]
  [中国の故事・伝説に取材した大小暦](https://dl.ndl.go.jp/info:ndljp/pid/3051277)（相島 宏）
……[「日本の暦」展](https://www.ndl.go.jp/koyomi/index.html)に関連した論文。
この論文中の「晋武帝羊車遊宴図」の出典は[『恵合余見』第二冊](https://dl.ndl.go.jp/info:ndljp/pid/1286782/19)。
類似の「晋武帝羊車遊宴図」は、[『和漢名画苑』](https://www.kanazawa-bidai.ac.jp/edehon-db/quick.cgi?mode=result&id=83)の[四巻](https://www.kanazawa-bidai.ac.jp/edehon-db/img/83/wakan-meigaen04d021a.jpg)でも見られる。
論文中に引かれた[『通俗続三國志』](https://dl.ndl.go.jp/info:ndljp/pid/775963/25)には、[現代語訳](https://kakuyomu.jp/works/1177354054884106768/episodes/1177354054884109208)もある。
* [徐熥「晉宮怨」](https://zh.wikisource.org/wiki/%E6%99%89%E5%AE%AE%E6%80%A8)……「空聽羊車竹外音」とある。

#### 諸葛夫人（諸葛婉）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AB%B8%E8%91%9B%E5%A9%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 恵賈皇后（賈南風）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E5%8D%97%E9%A2%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p13)
* [賈南風乳母徐義墓誌銘](https://zh.wikisource.org/wiki/%E8%B3%88%E5%8D%97%E9%A2%A8%E4%B9%B3%E6%AF%8D%E5%BE%90%E7%BE%A9%E5%A2%93%E8%AA%8C%E9%8A%98)
* [周曇の「詠史詩」の中の「賈后」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%B3%88%E5%90%8E)

#### 恵羊皇后（羊献容）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%8C%AE%E5%AE%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n120/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* [顏希源『百美新詠』「羊獻容」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E7%BE%8A%E7%8D%BB%E5%AE%B9)

#### 謝夫人（謝玖）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%8E%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%8E%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%8E%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%8E%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%8E%96)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n124/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 懐王皇太后（王媛姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%AA%9B%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n126/mode/2up)
* [ブログ]
  [懐王皇太后伝](http://strawberrymilk.gooside.com/text/shinjo/31-9.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 元夏侯太妃（夏侯光姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n126/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)


### 巻32 列伝第2 后妃下
* [書誌のみ]
  [『晋書斠注』巻三十二 列伝第二 后妃下 訳注稿](https://cir.nii.ac.jp/crid/1520290885075378432)
  （赤羽 奈津子・猪俣 貴幸・尾関 圭信・織田 めぐみ・小野 響・川見 健人・末川 洸介・千田 豊・安永 知晃）
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n2/mode/2up)


#### 元敬虞皇后（虞孟母）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E5%AD%9F%E6%AF%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n2/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 荀豫章君
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n4/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 明穆庾皇后（庾文君）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%96%87%E5%90%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%96%87%E5%90%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%96%87%E5%90%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%96%87%E5%90%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%96%87%E5%90%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n4/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 成恭杜皇后（杜陵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E9%99%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E9%99%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E9%99%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E9%99%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E9%99%B5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n8/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「杜后生齒」（第37句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/38)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 章太妃周氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n10/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 康献褚皇后（褚蒜子）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E8%92%9C%E5%AD%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n10/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [褚皇后　　太后臨朝](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558781829259)
  (@ゆるふわ晋宋春秋４)

#### 穆章何皇后（何法倪）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%B3%95%E5%80%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%B3%95%E5%80%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%B3%95%E5%80%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%B3%95%E5%80%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%B3%95%E5%80%AA)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n18/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [姚承緒『吳趨訪古錄』の「烏夜村」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/4#%E7%83%8F%E5%A4%9C%E6%9D%91)
* [ブログ]
  [何皇后　　何后慟哭](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558863976538)
  (@ゆるふわ晋宋春秋４)

#### 哀靖王皇后（王穆之）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%A9%86%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 廃帝孝庾皇后（庾道憐）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%81%93%E6%86%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%81%93%E6%86%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%81%93%E6%86%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%81%93%E6%86%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%81%93%E6%86%90)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 簡文宣鄭太后（鄭阿春）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E9%98%BF%E6%98%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E9%98%BF%E6%98%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E9%98%BF%E6%98%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E9%98%BF%E6%98%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E9%98%BF%E6%98%A5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 簡文順王皇后（王簡姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%B0%A1%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 孝武文李太后（李陵容）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E9%99%B5%E5%AE%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [李太后　　母以子貴慶厚禮崇](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558968006958)
  (@ゆるふわ晋宋春秋４)

#### 孝武定王皇后（王法慧）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B3%95%E6%85%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 安徳陳太后（陳帰女）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E5%B8%B0%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [安恭后　　晋末期の妃](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139559010841973)
  (@ゆるふわ晋宋春秋４)

#### 安僖王皇后（王神愛）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%A5%9E%E6%84%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [安恭后　　晋末期の妃](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139559010841973)
  (@ゆるふわ晋宋春秋４)

#### 恭思褚皇后（褚霊媛）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E9%9C%8A%E5%AA%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n34/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [安恭后　　晋末期の妃](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139559010841973)
  (@ゆるふわ晋宋春秋４)


### 巻33 列伝第3
#### 王祥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%A5%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%A5%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%A5%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%A5%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%A5%A5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n40/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王覽友弟」（第60句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/52)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-1ohsho.html)
* 干宝[『搜神記』巻11](https://ctext.org/wiki.pl?if=gb&chapter=648373#p17)
* [書籍 (NDL)]
  空海『三教指帰』に[「魚を躍らしむるの感」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)とある
  （[注98](https://dl.ndl.go.jp/info:ndljp/pid/1040599/39)）。
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「王祥非孝子論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/20)
  （兪長城（字が寧世））
* [『三国志』巻18](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B718#%E7%8E%8B%E7%A5%A5)
* 『關帝靈籤』第十六籤に「王祥臥冰」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/16)
    * [画像版](https://zh.wikisource.org/w/index.php?title=File%3AHarvard_drs_53239458_%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.pdf&page=18)

#### 王覧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%A6%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%A6%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%A6%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%A6%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%A6%A7)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n50/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王覽友弟」（第60句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/52)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-1ohsho.html)

#### 鄭沖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n54/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-2teityu.html)

#### 何曾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%9B%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%9B%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%9B%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%9B%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%9B%BE)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n60/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [書籍 (NDL)]
  空海『三教指帰』に[「何曾が滋き味を願はず」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/20)とある
  （[注31](https://dl.ndl.go.jp/info:ndljp/pid/1040599/52)）。
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc1)
  （@晋書簡訳所）

#### 何劭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%8A%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%8A%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%8A%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%8A%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%8A%AD)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n72/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc2)
  （@晋書簡訳所）

#### 何遵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E9%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E9%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E9%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E9%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E9%81%B5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n76/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc3)
  （@晋書簡訳所）

#### 何嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n76/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 何綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n76/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 何機
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%A9%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%A9%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%A9%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%A9%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%A9%9F)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n78/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 何羨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%BE%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%BE%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%BE%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%BE%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%BE%A8)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n78/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 石苞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%8B%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%8B%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%8B%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%8B%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%8B%9E)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n78/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』石苞伝　訳注](https://www.pixiv.net/novel/show.php?id=4598808)
  （@晋書訳注シリーズ）

#### 石統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E7%B5%B1)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石越
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%B6%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%B6%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%B6%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%B6%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%B6%8A)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n88/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石儁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%84%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%84%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%84%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%84%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%84%81)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n88/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石崇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%B4%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%B4%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%B4%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%B4%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%B4%87)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n88/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* 楊衒之『洛陽伽藍記』
  [巻1](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD%E4%BC%BD%E8%97%8D%E8%A8%98/%E5%8D%B7%E4%B8%80)・
  [巻4](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD%E4%BC%BD%E8%97%8D%E8%A8%98/%E5%8D%B7%E5%9B%9B)
* 庾肩吾[「石崇金谷妓」](https://zh.wikisource.org/wiki/%E7%9F%B3%E5%B4%87%E9%87%91%E8%B0%B7%E5%A6%93)
* 楽史[『緑珠伝』](https://zh.wikisource.org/wiki/%E7%B6%A0%E7%8F%A0%E5%82%B3)
* 『太平広記』巻399「[緑珠井](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC399#%E7%B6%A0%E7%8F%A0%E4%BA%95)」
* 馮夢龍[『情史類略』巻1「緑珠」](https://zh.wikisource.org/wiki/%E6%83%85%E5%8F%B2%E9%A1%9E%E7%95%A5/01#%E7%B6%A0%E7%8F%A0)
* 袁枚[『子不語』巻7「石崇老奴才」](https://ctext.org/wiki.pl?if=gb&chapter=80611#%E7%9F%B3%E5%B4%87%E8%80%81%E5%A5%B4%E6%89%8D)
* 仇英[「金谷園及桃李園圖雙幅」](https://dl.ndl.go.jp/info:ndljp/pid/1014323/102)
* [『本朝文粋』巻14](https://dl.ndl.go.jp/info:ndljp/pid/2544432/37)の[菅三品（菅原文時）「為謙徳公修報恩善願文」](https://dl.ndl.go.jp/info:ndljp/pid/2544432/54)に、石崇を指す「[金谷醉花之地、花毎春匂而主不歸](https://dl.ndl.go.jp/info:ndljp/pid/2544432/55)」という部分があり、その部分が[『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-11)に引用されている（[『十訓抄詳解』の注](https://dl.ndl.go.jp/info:ndljp/pid/945602/34)を参照）。
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……題材の一つが石崇の金谷園。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）
* 高啓の[「石崇墓」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B709#%E7%9F%B3%E5%B4%87%E5%A2%93)（[『国訳漢文大成. 続 文学部第80冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140824/100)も参照）
* [曹雪芹の『紅楼夢』第64回](https://zh.wikisource.org/wiki/%E7%B4%85%E6%A8%93%E5%A4%A2/%E7%AC%AC064%E5%9B%9E)（[『国訳漢文大成』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1913066/101)も参照）に
  「瓦礫明珠一例拋，何曾石尉重嬌嬈。都緣頑福前生造，更有同歸慰寂寥。」
  という詩がある。
* [杜牧の「題桃花夫人廟」](https://zh.wikisource.org/wiki/%E9%A1%8C%E6%A1%83%E8%8A%B1%E5%A4%AB%E4%BA%BA%E5%BB%9F_(%E6%9D%9C%E7%89%A7))（[『杜樊川絶句詳解』](https://dl.ndl.go.jp/pid/1102333/1/47)）に「可憐金谷墜樓人」とある。
* [杜牧の「金谷園」](https://zh.wikisource.org/wiki/%E9%87%91%E8%B0%B7%E5%9C%92_(%E6%9D%9C%E7%89%A7))（[『杜樊川絶句詳解』](https://dl.ndl.go.jp/pid/1102333/1/86)）に「落花猶似墜樓人」とある。
* [胡曾の「金谷園」](https://zh.wikisource.org/wiki/%E9%87%91%E8%B0%B7%E5%9C%92_(%E8%83%A1%E6%9B%BE))
* [汪遵の「金谷」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7602#%E9%87%91%E8%B0%B7)
* [汪遵の「綠珠」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7602#%E7%B6%A0%E7%8F%A0)
* 五代十国時代の錢元璙も同名の「金谷園」を作った。石崇を意識したのかもしれない（？）。
    * [『十國春秋』巻83](https://zh.wikisource.org/wiki/%E5%8D%81%E5%9C%8B%E6%98%A5%E7%A7%8B_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7083#%E5%85%83%E7%92%99%E5%AD%97%E5%BE%B3%E8%BC%9D%E6%AD%A6%E8%82%85%E7%8E%8B%E7%AC%AC%E5%85%AD%E5%AD%90%E4%B9%9F)
    * [Wikipedia](https://en.wikipedia.org/wiki/Mountain_Villa_with_Embracing_Beauty)
    * [姚承緒『吳趨訪古錄』の「樂圃」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E6%A8%82%E5%9C%83)……錢元璙の金谷園を題材にした詩。
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc3)
  （@晋書簡訳所）
* [顏希源『百美新詠』「綠珠」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E7%B6%A0%E7%8F%A0)
* [權德輿「八音詩」](https://zh.wikisource.org/wiki/%E5%85%AB%E9%9F%B3%E8%A9%A9)
* 『關帝靈籤』第十七籤に「石崇被難」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/17)
    * [画像版](https://zh.wikisource.org/w/index.php?title=File:Harvard_drs_53239458_%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.pdf&page=19)

#### 石樸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E6%A8%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E6%A8%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E6%A8%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E6%A8%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E6%A8%B8)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n102/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc4)
  （@晋書簡訳所）

#### 欧陽建
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AC%A7%E9%99%BD%E5%BB%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n102/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、欧陽建伝で言及している
  [「臨終詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/101)がある。
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc5)
  （@晋書簡訳所）

#### 孫鑠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E9%91%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E9%91%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E9%91%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E9%91%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E9%91%A0)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n102/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc6)
  （@晋書簡訳所）


### 巻34 列伝第4
#### 羊祜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%A5%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%A5%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%A5%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%A5%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%A5%9C)
* [書籍 (IA)]
  [晉書斠注(二十七)](https://archive.org/details/02077744.cn/page/n2/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「羊祜識環」（第54句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/48)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、羊祜伝に引かれた
  [「讓開府表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/406)がある。
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [孫呉は二の次、洛陽が気がかりな羊祜伝](http://3guozhi.net/q/yk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m034.html)
* 干宝[『搜神記』巻15](https://ctext.org/wiki.pl?if=gb&chapter=634334#p12)
* [『十訓抄』1の4](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-04)
* [李白「襄陽歌」](https://zh.wikisource.org/wiki/%E8%A5%84%E9%99%BD%E6%AD%8C)
  （[『続国訳漢文大成 文学部第4冊（李太白詩集上の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139937/4)に訳注あり）
* 白居易「裴侍中晉公以集賢林亭即事詩二十六韻見贈猥蒙徴和才拙詞繁廣為五百言以伸酬獻」
    * [『白氏長慶集』（四庫全書本）巻29](https://zh.wikisource.org/wiki/%E7%99%BD%E6%B0%8F%E9%95%B7%E6%85%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B729)
    * [『全唐詩』巻452](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7452#%E8%A3%B4%E4%BE%8D%E4%B8%AD%E6%99%89%E5%85%AC%E4%BB%A5%E9%9B%86%E8%B3%A2%E6%9E%97%E4%BA%AD%E5%8D%B3%E4%BA%8B%E8%A9%A9%E4%B8%89%E5%8D%81%E5%85%AD%E9%9F%BB%E8%A6%8B%E8%B4%88%E7%8C%A5%E8%92%99%E5%BE%B5%E5%92%8C%E6%89%8D%E6%8B%99%E8%A9%9E%E7%B9%81%E8%BC%92%E5%BB%A3%E7%82%BA%E4%BA%94%E7%99%BE%E8%A8%80%E4%BB%A5%E4%BC%B8%E9%85%AC%E7%8D%BB)
    * [『続国訳漢文大成 文学部第42冊（白楽天詩集 三の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1140376/31)
* [胡曾の「峴山」](https://zh.wikisource.org/wiki/%E5%B3%B4%E5%B1%B1_(%E8%83%A1%E6%9B%BE))
* [陳子昂「峴山懷古」](https://zh.wikisource.org/wiki/%E5%B3%B4%E5%B1%B1%E6%87%B7%E5%8F%A4_(%E9%99%B3%E5%AD%90%E6%98%82))（[『漢籍國字解全書 : 先哲遺著 第10巻』](https://dl.ndl.go.jp/pid/1904127/1/268)）……「墮淚碣」に言及している詩。
* [ブログ]
  [羊祜伝](https://estar.jp/novels/25594548/viewer?page=191)
  （@淡々晋書）
* [ブログ]
  [羊法興　　功臣再評価](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086327685)
  （@デイリー晋宋春秋）
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/396)）で、羊祜の故事を指して「羊公深罷市之慕」とある。
* [李德裕「羊祜留賈充論」](https://zh.wikisource.org/wiki/%E7%BE%8A%E7%A5%9C%E7%95%99%E8%B3%88%E5%85%85%E8%AB%96)

#### 杜預
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E9%A0%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E9%A0%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E9%A0%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E9%A0%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E9%A0%90)
* [書籍 (IA)]
  [晉書斠注(二十七)](https://archive.org/details/02077744.cn/page/n36/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、杜預伝で言及している
  『春秋左氏伝』の[序](https://dl.ndl.go.jp/info:ndljp/pid/1912835/163)がある。
* [書籍 (NDL)]
  [『漢籍国字解全書　先哲遺著　第十三巻』](https://dl.ndl.go.jp/info:ndljp/pid/898786/14)にも『春秋左氏伝』の序がある。
* [ブログ]
  [杜預「春秋左伝序」現代語訳](http://3guozhi.net/w/003.html)
  （@いつか書きたい三国志）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m034.html)
* 『漢魏六朝百三家集』所収の『杜征南集』（杜預集）
    * [Wikisource](https://zh.wikisource.org/wiki/%E6%9D%9C%E5%BE%81%E5%8D%97%E9%9B%86)
    * [Wikisource（四庫全書本）](https://zh.wikisource.org/wiki/%E6%BC%A2%E9%AD%8F%E5%85%AD%E6%9C%9D%E7%99%BE%E4%B8%89%E5%AE%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7037)
    * [NDL](https://dl.ndl.go.jp/info:ndljp/pid/2551362/4)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)

#### 杜錫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E9%8C%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E9%8C%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E9%8C%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E9%8C%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E9%8C%AB)
* [書籍 (IA)]
  [晉書斠注(二十七)](https://archive.org/details/02077744.cn/page/n58/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m034.html)
* 干宝[『搜神記』巻15](https://ctext.org/wiki.pl?if=gb&chapter=634334#p15)


### 巻35 列伝第5
#### 陳騫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E9%A8%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E9%A8%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E9%A8%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E9%A8%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E9%A8%AB)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [『晋書』陳騫伝　訳注](https://www.pixiv.net/novel/show.php?id=5446111)
  （@晋書訳注シリーズ）

#### 陳輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n8/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)

#### 裴秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n8/mode/2up)
* [ブログ]
  [『晋書』列伝５より、「裴秀伝」を翻訳](http://3guozhi.net/zaka2/hs1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  『晋書』裴秀伝　訳注（[前篇](https://www.pixiv.net/novel/show.php?id=6048010)・[後篇](https://www.pixiv.net/novel/show.php?id=6062445)）
  （@晋書訳注シリーズ）
* [『三国志』巻23](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B723#%E5%AD%90_%E7%A7%80)

#### 裴頠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%A0%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%A0%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%A0%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%A0%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%A0%A0)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n22/mode/2up)
* [ブログ]
  [『晋書』列伝５より、「裴頠伝」を翻訳](http://3guozhi.net/zaka2/hs3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc1)
  （@晋書簡訳所）

#### 裴楷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E6%A5%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E6%A5%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E6%A5%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E6%A5%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E6%A5%B7)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n38/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「裴楷清通」（第2句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/17)
* [『文選』所収の王倹「褚淵碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B758#%E8%A4%9A%E6%B7%B5%E7%A2%91%E6%96%87)（[国訳](https://dl.ndl.go.jp/info:ndljp/pid/1912835/381)）に「裴楷清通」とある。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc2)
  （@晋書簡訳所）

#### 裴輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc2)
  （@晋書簡訳所）

#### 裴瓚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%93%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%93%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%93%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%93%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%93%9A)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc2)
  （@晋書簡訳所）

#### 裴憲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E6%86%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E6%86%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E6%86%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E6%86%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E6%86%B2)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc3)
  （@晋書簡訳所）

#### 裴黎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%BB%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%BB%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%BB%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%BB%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%BB%8E)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴康
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E5%BA%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E5%BA%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E5%BA%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E5%BA%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E5%BA%B7)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴盾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%9B%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%9B%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%9B%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%9B%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%9B%BE)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴邵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%82%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%82%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%82%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%82%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%82%B5)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴綽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%B6%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%B6%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%B6%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%B6%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%B6%BD)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%81%90)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）


### 巻36 列伝第6
#### 衛瓘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E7%93%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E7%93%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E7%93%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E7%93%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E7%93%98)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n56/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「瓘靖二妙」（第43句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/42)、
  [「衛瓘撫牀」（第84句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/66)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（1）](https://readingnotesofjinshu.com/translation/biographies/vol-36_1)
  （@晋書簡訳所）

#### 衛恒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E6%81%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E6%81%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E6%81%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E6%81%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E6%81%92)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc1)
  （@晋書簡訳所）

#### 衛璪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E7%92%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E7%92%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E7%92%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E7%92%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E7%92%AA)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc2)
  （@晋書簡訳所）

#### 衛玠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E7%8E%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E7%8E%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E7%8E%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E7%8E%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E7%8E%A0)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc3)
  （@晋書簡訳所）

#### 衛展
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E5%B1%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E5%B1%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E5%B1%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E5%B1%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E5%B1%95)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc4)
  （@晋書簡訳所）

#### 張華
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8F%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8F%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8F%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8F%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8F%AF)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n108/mode/2up)
* [PDFあり]
  [六朝文人伝「張華」 : 『晋書』張華伝](https://dl.ndl.go.jp/info:ndljp/pid/10504415)
  （佐藤 利行）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、張華伝に引かれた
  [「鷦鷯賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/253)がある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、張華伝で言及している
  [「女史箴」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/334)がある。
* 干宝[『搜神記』巻18](https://ctext.org/wiki.pl?if=gb&chapter=232809#p10)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（1）](https://readingnotesofjinshu.com/translation/biographies/vol-36_3)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc1)
  （@晋書簡訳所）
* 吳筠[「覽古十四首」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7853#%E8%A6%BD%E5%8F%A4%E5%8D%81%E5%9B%9B%E9%A6%96)に「茂先洽聞者，幽賾咸該通。弱年賦鷦鷯，可謂達養蒙。晩節希鸞鵠，長飛戾曾穹。知進不知退，遂令其道窮。」とある。茂先は張華の字。

#### 張禕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%A6%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%A6%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%A6%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%A6%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%A6%95)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n134/mode/2up)
* [PDFあり]
  [六朝文人伝「張華」 : 『晋書』張華伝](https://dl.ndl.go.jp/info:ndljp/pid/10504415)
  （佐藤 利行）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc2)
  （@晋書簡訳所）

#### 張韙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%9F%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%9F%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%9F%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%9F%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%9F%99)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n134/mode/2up)
* [PDFあり]
  [六朝文人伝「張華」 : 『晋書』張華伝](https://dl.ndl.go.jp/info:ndljp/pid/10504415)
  （佐藤 利行）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc2)
  （@晋書簡訳所）

#### 劉卞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%8D%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%8D%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%8D%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%8D%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%8D%9E)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n136/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc3)
  （@晋書簡訳所）


### 巻37 列伝第7 宗室
* [ブログ]
  [系図](https://readingnotesofjinshu.com/translation/appendix/familytree)
  （@晋書簡訳所）

#### 安平献王（司馬孚）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n2/mode/2up)
* [ブログ]
  [死ぬまで魏の純臣なり、司馬孚＆司馬望伝](http://3guozhi.net/hito/s1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（1）安平献王孚（I）](https://readingnotesofjinshu.com/translation/biographies/vol-37_1)
  （@晋書簡訳所）

#### 司馬邕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%82%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n14/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc1)
  （@晋書簡訳所）

#### 義陽成王（司馬望）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9C%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n14/mode/2up)
* [ブログ]
  [死ぬまで魏の純臣なり、司馬孚＆司馬望伝](http://3guozhi.net/hito/s2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 河間平王（司馬洪）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 司馬威
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%A8%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 隨穆王（司馬整）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%95%B4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 竟陵王（司馬楙）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A5%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 太原成王（司馬輔）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc3)
  （@晋書簡訳所）

#### 司馬翼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%BF%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc4)
  （@晋書簡訳所）

#### 下邳獻王（司馬晃）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n28/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc5)
  （@晋書簡訳所）

#### 太原烈王（司馬瓌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%93%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc6)
  （@晋書簡訳所）

#### 高陽元王（司馬珪）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%8F%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc7)
  （@晋書簡訳所）

#### 常山孝王（司馬衡）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A1%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc8)
  （@晋書簡訳所）

#### 沛順王（司馬景）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc9)
  （@晋書簡訳所）

#### 彭城穆王（司馬権）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A8%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
  ……「五馬」の一人の彭城王は、司馬権の子の司馬雄（と思われる）。
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 司馬紘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B4%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n34/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
  ……「五馬」の一人の彭城王を司馬紘とする説もあるようだ。
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 司馬俊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BF%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 高密文献王（司馬泰）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%B3%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc2)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 高密孝王（司馬略）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%95%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n40/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc2)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 新蔡武哀王（司馬騰）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A8%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n42/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc3)
  （@晋書簡訳所）

#### 司馬確
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%A2%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc3)
  （@晋書簡訳所）

#### 南陽王（司馬模）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A8%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc4)
  （@晋書簡訳所）

#### 司馬保
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BF%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc4)
  （@晋書簡訳所）

#### 范陽康王（司馬綏）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（4）范陽康王綏](https://readingnotesofjinshu.com/translation/biographies/vol-37_4)
  （@晋書簡訳所）

#### 司馬虓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%99%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（4）范陽康王綏](https://readingnotesofjinshu.com/translation/biographies/vol-37_4)
  （@晋書簡訳所）

#### 済南恵王（司馬遂）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 司馬勳（司馬勲）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%8B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙剛王（司馬遜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙閔王（司馬承／司馬氶／司馬丞）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙烈王（司馬無忌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙敬王（司馬恬）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%81%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n72/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [司馬恬　　大司馬を弾ず](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559106126284)
  （@デイリー晋宋春秋）

#### 譙忠王（司馬尚之）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n72/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [司馬尚之１　名族と対峙](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559106146862)〜
  （@デイリー晋宋春秋）

#### 司馬恢之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [司馬尚之３　死と継承者と弟](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559106182024)
  （@デイリー晋宋春秋）

#### 司馬休之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [『魏書』巻37](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B737#%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [『北史』巻29](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7029#%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)

#### 司馬允之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 韓延之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E5%BB%B6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [『魏書』巻38](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B738#%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [『北史』巻27](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7027#%E9%9F%93%E5%BB%B6%E4%B9%8B)

#### 司馬愔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%84%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 高陽王（司馬睦）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%9D%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 任城景王（司馬陵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%99%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 司馬順
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A0%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 西河繆王（司馬斌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%96%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)


### 巻38 列伝第8 宣五王・文六王
#### 平原王（司馬榦）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A6%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc1)
  （@晋書簡訳所）

#### 琅邪武王（司馬伷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BC%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc2)
  （@晋書簡訳所）

#### 琅邪恭王（司馬覲）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A6%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n8/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc3)
  （@晋書簡訳所）

#### 武陵荘王（司馬澹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BE%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n10/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc4)
  （@晋書簡訳所）

#### 東安王（司馬繇）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B9%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n10/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc5)
  （@晋書簡訳所）

#### 淮陵元王（司馬漼）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BC%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n12/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc6)
  （@晋書簡訳所）

#### 清恵亭侯（司馬京）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BA%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n12/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc7)
  （@晋書簡訳所）

#### 扶風武王（司馬駿）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A7%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n14/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc8)
  （@晋書簡訳所）

#### 司馬暢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9A%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc8)
  （@晋書簡訳所）

#### 新野荘王（司馬歆）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%AD%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc9)
  （@晋書簡訳所）

#### 梁孝王（司馬肜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%82%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n22/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc10)
  （@晋書簡訳所）

#### 斉献王（司馬攸）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%94%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列伝８より、「斉王攸伝」を翻訳してみた](http://3guozhi.net/zaka2/yu1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 司馬蕤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%95%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n42/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 広漢沖王（司馬贊）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%B4%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n44/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 司馬寔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AF%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n44/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 城陽哀王（司馬兆）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%85%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n44/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 遼東悼恵王（司馬定国）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 広漢殤王（司馬広徳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 楽安平王（司馬鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 楽平王（司馬延祚）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)


### 巻39 列伝第9
#### 王沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n52/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「王沈＆王浚伝」を翻訳](http://3guozhi.net/hito/ot1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 王浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n58/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「王沈＆王浚伝」を翻訳](http://3guozhi.net/hito/ot1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻三十九　列伝第九　王沈（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [王浚妻華芳墓誌銘](https://zh.wikisource.org/wiki/%E7%8E%8B%E6%B5%9A%E5%A6%BB%E8%8F%AF%E8%8A%B3%E5%A2%93%E8%AA%8C%E9%8A%98)

#### 荀顗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E9%A1%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E9%A1%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E9%A1%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E9%A1%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E9%A1%97)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n70/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「荀顗・荀勖伝」を翻訳](http://3guozhi.net/n/jg1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)
* [ブログ]
  [『晋書』荀顗伝　訳注](https://www.pixiv.net/novel/show.php?id=5334253)
  （@晋書訳注シリーズ）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/193)）に「荀摯競爽於晉世」とあり、注によれば「荀」は荀顗のことを指す。

#### 荀勗（荀勖）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%8B%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%8B%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%8B%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%8B%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%8B%96)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n76/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「荀顗・荀勖伝」を翻訳](http://3guozhi.net/n/jg1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)
* [ブログ]
  『晋書』荀勗伝　訳注（[前篇](https://www.pixiv.net/novel/show.php?id=5145666)・[後篇](https://www.pixiv.net/novel/show.php?id=5186528)）
  （@晋書訳注シリーズ）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)に「采公曾之中經」（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/189)）および「公曾甘鳳池之失」（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/192)）とある。「公曾」は荀勗の字。

#### 荀藩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E7%B1%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E7%B1%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E7%B1%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E7%B1%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E7%B1%93)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n92/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀邃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E9%82%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E9%82%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E9%82%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E9%82%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E9%82%83)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n94/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀闓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E9%97%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E9%97%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E9%97%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E9%97%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E9%97%93)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n94/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀組
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E7%B5%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E7%B5%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E7%B5%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E7%B5%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E7%B5%84)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n96/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n100/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 馮紞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AE%E7%B4%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AE%E7%B4%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AE%E7%B4%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AE%E7%B4%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AE%E7%B4%9E)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

### 巻40 列伝第10
#### 賈充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈充伝」を翻訳](http://3guozhi.net/zaka2/kj1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p13)
* 楊維楨[「贈杜彥清序」](https://zh.wikisource.org/wiki/%E8%B4%88%E6%9D%9C%E5%BD%A5%E6%B8%85%E5%BA%8F)
* [ブログ]
  『晋書』賈充伝　訳注（[壱](https://www.pixiv.net/novel/show.php?id=5485008)・[弐](https://www.pixiv.net/novel/show.php?id=5551225)）
  （@晋書訳注シリーズ）
* [『三国志』巻15](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B715#%E5%AD%90_%E5%85%85)
* [李德裕「羊祜留賈充論」](https://zh.wikisource.org/wiki/%E7%BE%8A%E7%A5%9C%E7%95%99%E8%B3%88%E5%85%85%E8%AB%96)

#### 郭槐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E6%A7%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E6%A7%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E6%A7%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E6%A7%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E6%A7%90)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n14/mode/2up)
* [PDFあり]
  [魏晉石刻資料選注( 一七 宜成宣君郭夫人之柩銘 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……郭夫人は郭槐のこと。
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [『晋書』賈充伝　訳注　(参)](https://www.pixiv.net/novel/show.php?id=5729263)
  （@晋書訳注シリーズ）

#### 韓謐（賈謐）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E8%AC%90)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n16/mode/2up)
* [PDFあり]
  [魏晉石刻資料選注( 二五 驃騎將軍韓壽墓碣 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……韓寿は賈謐の父。
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(参)](https://www.pixiv.net/novel/show.php?id=5729263#3)
  （@晋書訳注シリーズ）
* 韓謐（賈謐）の父母である韓寿と賈午の話に基づく創作物
    * [黄庭堅「酴醿」](https://zh.wikisource.org/wiki/%E5%B1%B1%E8%B0%B7%E9%9B%86%E8%A9%A9%E6%B3%A8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%A4%96%E9%9B%86%E5%8D%B712#%E9%85%B4%E9%86%BF)に「入骨濃薰賈女香」とある。
    * [周文質「蝶戀花・悟迷」](https://ctext.org/wiki.pl?if=gb&chapter=390671#p102)に「朱門深閉賈充香」とある。
    * [陸采『懷香記』](https://zh.wikisource.org/wiki/%E6%87%B7%E9%A6%99%E8%A8%98)

#### 賈混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n26/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(四)](https://www.pixiv.net/novel/show.php?id=5924762)
  （@晋書訳注シリーズ）

#### 賈模
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%A8%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%A8%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%A8%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%A8%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%A8%A1)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n26/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(四)](https://www.pixiv.net/novel/show.php?id=5924762)
  （@晋書訳注シリーズ）

#### 郭彰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E5%BD%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E5%BD%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E5%BD%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E5%BD%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E5%BD%B0)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(四)](https://www.pixiv.net/novel/show.php?id=5924762)
  （@晋書訳注シリーズ）

#### 楊駿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E9%A7%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E9%A7%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E9%A7%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E9%A7%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E9%A7%BF)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n28/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　楊駿](https://readingnotesofjinshu.com/translation/biographies/vol-40_3#toc1)
  （@晋書簡訳所）

#### 楊珧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E7%8F%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E7%8F%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E7%8F%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E7%8F%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E7%8F%A7)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　楊駿](https://readingnotesofjinshu.com/translation/biographies/vol-40_3#toc2)
  （@晋書簡訳所）

#### 楊済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n38/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　楊駿](https://readingnotesofjinshu.com/translation/biographies/vol-40_3#toc3)
  （@晋書簡訳所）


