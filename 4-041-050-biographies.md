### 巻41 列伝第11
#### 魏舒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E8%88%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E8%88%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E8%88%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E8%88%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E8%88%92)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n44/mode/2up)
* 干宝『搜神記』
  [巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p7)・
  [巻19](https://ctext.org/wiki.pl?if=gb&chapter=307958#p3)
* [『太平広記』巻58](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC058)
  ……娘の魏華存は有名な女仙（という話になっている）

#### 魏混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n52/mode/2up)

#### 李憙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%86%99)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n54/mode/2up)

#### 劉寔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%AF%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%AF%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%AF%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%AF%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%AF%94)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n58/mode/2up)

#### 劉智
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%99%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%99%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%99%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%99%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%99%BA)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n76/mode/2up)

#### 高光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E5%85%89)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n76/mode/2up)

#### 高韜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E9%9F%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E9%9F%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E9%9F%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E9%9F%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E9%9F%9C)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n78/mode/2up)


### 巻42 列伝第12
#### 王渾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B8%BE)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列伝１２より「王渾＆王済伝」を邦訳](http://3guozhi.net/zaka2/ok1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)

#### 王済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝１２より「王渾＆王済伝」を邦訳](http://3guozhi.net/zaka2/ok1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)

#### 王濬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%BF%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%BF%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%BF%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%BF%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%BF%AC)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n18/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [胡曾の「武昌」](https://zh.wikisource.org/wiki/%E6%AD%A6%E6%98%8C)
* [杜牧「題橫江館」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7523#%E9%A1%8C%E6%A9%AB%E6%B1%9F%E9%A4%A8)([『杜樊川絶句詳解』](https://dl.ndl.go.jp/pid/1102333/1/50?keyword=%E6%99%89%E9%BE%8D%E9%A9%A4)）……「晉龍驤」は王濬を指す。

#### 唐彬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%94%90%E5%BD%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%94%90%E5%BD%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%94%90%E5%BD%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%94%90%E5%BD%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%94%90%E5%BD%AC)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)

### 巻43 列伝第13
#### 山濤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B1%B1%E6%BF%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B1%B1%E6%BF%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B1%B1%E6%BF%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B1%B1%E6%BF%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B1%B1%E6%BF%A4)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n56/mode/2up)
* [書誌のみ]
  [『晉書』山濤伝訳注](https://cir.nii.ac.jp/crid/1520853835002721280)
  （鷹橋 明久）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「山濤識量」（第81句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/64)
  （ちなみに、「山濤識量」と評した任昉の「為范尚書讓吏部封侯第一表」も[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/430)で読めて、「山濤識量」という評言はリンク先の左ページ後ろから2行目にある）
* 『全晋文』
  [巻34](https://ctext.org/wiki.pl?if=gb&chapter=79602)
* [顏延之の「五君詠」の「阮始平」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E5%A7%8B%E5%B9%B3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　山濤（1）](https://readingnotesofjinshu.com/translation/biographies/vol-43_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/vol-43_2)
  （@晋書簡訳所）
* [唐の楊綰](https://zh.wikipedia.org/zh-tw/%E6%A5%8A%E7%B6%B0)が楊震（・邴吉）・山濤・謝安になぞらえられている。
	* [『旧唐書』巻119](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7119#%E6%A5%8A%E7%B6%B0)
	* [『新唐書』巻142](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7142#%E6%A5%8A%E7%B6%B0)
* 『關帝靈籤』第九十六籤に「山濤見王衍」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/96)
    * [画像版](https://commons.wikimedia.org/w/index.php?title=File%3A%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=98)
* 山濤の字の「巨源」と同名の人たち
    * 丘巨源
        * [『南斉書』巻52](https://zh.wikisource.org/wiki/%E5%8D%97%E9%BD%8A%E6%9B%B8/%E5%8D%B752#%E4%B8%98%E5%B7%A8%E6%BA%90)
        * [『南史』巻72](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B772#%E4%B8%98%E5%B7%A8%E6%BA%90)
    * 韋巨源
        * [『旧唐書』巻92](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B792#%E5%BE%9E%E7%A5%96%E5%85%84%E5%AD%90_%E5%B7%A8%E6%BA%90)
        * [『新唐書』巻123](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7123#%E9%9F%8B%E5%B7%A8%E6%BA%90)
    * 楊巨源
        * [『唐才子傳』卷5](https://zh.wikisource.org/wiki/%E5%94%90%E6%89%8D%E5%AD%90%E5%82%B3/%E5%8D%B75#%E6%A5%8A%E5%B7%A8%E6%BA%90)
    * 楊巨源
        * [『宋史』巻402](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7402#%E6%A5%8A%E5%B7%A8%E6%BA%90)
    * 何巨源
        * [『朱子語類』巻100（邵子之書）](https://zh.wikisource.org/wiki/%E6%9C%B1%E5%AD%90%E8%AA%9E%E9%A1%9E/100)（[訳注](https://hdl.handle.net/11094/56933)のp. 8）
    * 蕭巨源
        * [『皇明貢舉考』巻5](https://commons.wikimedia.org/w/index.php?title=File:CADAL02089103_%E7%9A%87%E6%98%8E%E8%B2%A2%E8%88%89%E8%80%83%EF%BC%88%E5%85%AD%EF%BC%89.djvu&page=142)
        * [Wikipedia](https://zh.wikipedia.org/zh-tw/%E8%95%AD%E5%B7%A8%E6%BA%90)

#### 山簡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B1%B1%E7%B0%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B1%B1%E7%B0%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B1%B1%E7%B0%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B1%B1%E7%B0%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B1%B1%E7%B0%A1)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n72/mode/2up)
* [孟浩然の「高陽池送朱二」](https://zh.wikisource.org/wiki/%E9%AB%98%E9%99%BD%E6%B1%A0%E9%80%81%E6%9C%B1%E4%BA%8C)の「當昔襄陽雄盛時，山公常醉習家池。」の「山公」は山簡を指す。
* 白居易「裴侍中晉公以集賢林亭即事詩二十六韻見贈猥蒙徴和才拙詞繁廣為五百言以伸酬獻」
    * [『白氏長慶集』（四庫全書本）巻29](https://zh.wikisource.org/wiki/%E7%99%BD%E6%B0%8F%E9%95%B7%E6%85%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B729)
    * [『全唐詩』巻452](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7452#%E8%A3%B4%E4%BE%8D%E4%B8%AD%E6%99%89%E5%85%AC%E4%BB%A5%E9%9B%86%E8%B3%A2%E6%9E%97%E4%BA%AD%E5%8D%B3%E4%BA%8B%E8%A9%A9%E4%B8%89%E5%8D%81%E5%85%AD%E9%9F%BB%E8%A6%8B%E8%B4%88%E7%8C%A5%E8%92%99%E5%BE%B5%E5%92%8C%E6%89%8D%E6%8B%99%E8%A9%9E%E7%B9%81%E8%BC%92%E5%BB%A3%E7%82%BA%E4%BA%94%E7%99%BE%E8%A8%80%E4%BB%A5%E4%BC%B8%E9%85%AC%E7%8D%BB)
    * [『続国訳漢文大成 文学部第42冊（白楽天詩集 三の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1140376/31)
* [胡曾の「高陽池」](https://zh.wikisource.org/wiki/%E9%AB%98%E9%99%BD%E6%B1%A0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　山濤（3）](https://readingnotesofjinshu.com/translation/biographies/vol-43_3#toc1)
  （@晋書簡訳所）
* [李白「峴山懷古」](https://zh.wikisource.org/wiki/%E5%B3%B4%E5%B1%B1%E6%87%B7%E5%8F%A4_(%E6%9D%8E%E7%99%BD))（[『国訳漢文大成 第8巻 第1輯』](https://dl.ndl.go.jp/pid/1685290/1/383)）……山簡を指して「山公」と述べている詩。

#### 山遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B1%B1%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B1%B1%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B1%B1%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B1%B1%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B1%B1%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　山濤（3）](https://readingnotesofjinshu.com/translation/biographies/vol-43_3#toc2)
  （@晋書簡訳所）

#### 王戎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%88%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%88%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%88%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%88%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%88%8E)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n80/mode/2up)
* [PDFあり] 
  [『晉書』王戎伝(巻四十三)訳注](http://dx.doi.org/10.18899/gei.07.04)
  （鷹橋 明久）
* [PDFあり]
  [『晋書』王戎伝訳注](https://dl.ndl.go.jp/info:ndljp/pid/10504387)
  （小松 英生）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王戎簡要」（第1句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/16)
* [ブログ]
  [『晋書』列伝１３より「王戎＆王衍伝」を訳出](http://3guozhi.net/zaka2/oj1.html)
  （@いつか書きたい『三国志』）
* [『文選』所収の王倹「褚淵碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B758#%E8%A4%9A%E6%B7%B5%E7%A2%91%E6%96%87)（[国訳](https://dl.ndl.go.jp/info:ndljp/pid/1912835/381)）に「王戎簡要」とある。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（1）](https://readingnotesofjinshu.com/translation/biographies/vol-43_4)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/%e5%b7%bb%e5%9b%9b%e5%8d%81%e4%b8%89%e3%80%80%e5%88%97%e4%bc%9d%e7%ac%ac%e5%8d%81%e4%b8%89%e3%80%80%e7%8e%8b%e6%88%8e%ef%bc%882%ef%bc%89)
  （@晋書簡訳所）
* [顏希源『百美新詠』「王戎婦」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E7%8E%8B%E6%88%8E%E5%A9%A6)
  ……[『世説新語』惑溺篇6](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E6%83%91%E6%BA%BA)の話が題材。

#### 王衍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%A1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%A1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%A1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%A1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%A1%8D)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n94/mode/2up)
* [ブログ]
  [『晋書』列伝１３より「王戎＆王衍伝」を訳出](http://3guozhi.net/zaka2/oj2.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王衍風鑑」（第75句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/61)
* 高啓の[「永嘉行」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E6%B0%B8%E5%98%89%E8%A1%8C)の「至今人説王夷甫」という句（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/143)も参照）
* [周曇の「詠史詩」の中の「王夷甫」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E7%8E%8B%E5%A4%B7%E7%94%AB)
* [胡曾の「洛陽」](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD_(%E8%83%A1%E6%9B%BE))
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（3）](https://readingnotesofjinshu.com/translation/biographies/vol-43_6)
  （@晋書簡訳所）
* [『旧唐書』巻99（張九齡）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E4%B9%9D%E9%BD%A1)で、「九齡奏曰：「祿山狼子野心，面有逆相，臣請因罪戮之，冀絕後患。」上曰：「卿勿以王夷甫知石勒故事，誤害忠良。」遂放歸藩。」と王衍と石勒に言及している。
* [『新唐書』巻126（張九齡）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7126#%E5%BC%B5%E4%B9%9D%E9%BD%A1)で、「九齡曰：「祿山狼子野心，有逆相，宜即事誅之，以絕後患。」帝曰：「卿無以王衍知石勒而害忠良。」卒不用。」と王衍と石勒に言及している。
* 『關帝靈籤』第九十六籤に「山濤見王衍」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/96)
    * [画像版](https://commons.wikimedia.org/w/index.php?title=File%3A%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=98)

#### 王澄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%BE%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%BE%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%BE%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%BE%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%BE%84)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n106/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（4）](https://readingnotesofjinshu.com/translation/biographies/vol-43_7#toc1)
  （@晋書簡訳所）

#### 郭舒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E8%88%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E8%88%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E8%88%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E8%88%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E8%88%92)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n114/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（4）](https://readingnotesofjinshu.com/translation/biographies/vol-43_7#toc2)
  （@晋書簡訳所）

#### 楽広
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%BD%E5%BA%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%BD%E5%BA%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%BD%E5%BA%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%BD%E5%BA%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%BD%E5%BA%83)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n118/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　楽広](https://readingnotesofjinshu.com/translation/biographies/vol-43_8)
  （@晋書簡訳所）


### 巻44 列伝第14
#### 鄭袤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E8%A2%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E8%A2%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E8%A2%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E8%A2%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E8%A2%A4)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n1/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb1.html)
  （@いつか書きたい『三国志』）

#### 鄭默（鄭黙）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E9%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E9%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E9%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E9%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E9%BB%99)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n7/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb2.html)
  （@いつか書きたい『三国志』）

#### 鄭球
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E7%90%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E7%90%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E7%90%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E7%90%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E7%90%83)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n9/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb2.html)
  （@いつか書きたい『三国志』）

#### 李胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n11/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb3.html)
  （@いつか書きたい『三国志』）

#### 盧欽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E6%AC%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E6%AC%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E6%AC%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E6%AC%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E6%AC%BD)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n15/mode/2up)
  ……最後の方のページが脱落している。
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc1)
  （@晋書簡訳所）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/190)）に「盧欽兼掌」とある。

#### 盧浮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E6%B5%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E6%B5%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E6%B5%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E6%B5%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E6%B5%AE)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc1)
  （@晋書簡訳所）

#### 盧珽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E7%8F%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E7%8F%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E7%8F%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E7%8F%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E7%8F%BD)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc2)
  （@晋書簡訳所）

#### 盧志
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E5%BF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E5%BF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E5%BF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E5%BF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E5%BF%97)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n19/mode/2up)
  ……最初の方のページは脱落しているので途中から。
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc2)
  （@晋書簡訳所）

#### 盧諶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E8%AB%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E8%AB%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E8%AB%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E8%AB%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E8%AB%B6)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n21/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc3)
  （@晋書簡訳所）
* [『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho02-02)に
  「また、陸士衡が文賦には、在木闕不材之質 処雁乏善鳴之分ともあり。」
  とあるが、
  [『十訓抄詳解』](https://dl.ndl.go.jp/info:ndljp/pid/945602/84)でも指摘されているとおり、これは、
  陸機の「文賦」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B717#%E6%96%87%E8%B3%A6)・
    [『国訳漢文大成　文学部第二卷　文選上巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912763/304)）
  ではなく、
  盧諶の「贈劉琨并書」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E8%B4%88%E5%8A%89%E7%90%A8%E5%B9%B6%E6%9B%B8)・
    [『国訳漢文大成　文学部第三卷　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/151)）
  にある。

#### 崔悦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B4%94%E6%82%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B4%94%E6%82%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B4%94%E6%82%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B4%94%E6%82%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B4%94%E6%82%A6)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n25/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb6.html)
  （@いつか書きたい『三国志』）

#### 華表
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%A1%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%A1%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%A1%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%A1%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%A1%A8)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n25/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb7.html)
  （@いつか書きたい『三国志』）

#### 華廙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E5%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E5%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E5%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E5%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E5%BB%99)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n27/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb7.html)
  （@いつか書きたい『三国志』）

#### 華混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb8.html)
  （@いつか書きたい『三国志』）

#### 華薈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%96%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%96%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%96%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%96%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%96%88)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb8.html)
  （@いつか書きたい『三国志』）

#### 華恒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E6%81%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E6%81%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E6%81%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E6%81%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E6%81%92)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb8.html)
  （@いつか書きたい『三国志』）

#### 華嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n35/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb9.html)
  （@いつか書きたい『三国志』）

#### 石鑒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n41/mode/2up)
* [西晉石尠墓誌](http://csid.zju.edu.cn/tomb/stone/detail?id=40288b955ab29f4b015b9fa0c49a006b&rubbingId=40288b955ab29f4b015b9fa0c4a2006c)……石尠（石尟）は石鑒の第二子。
* [PDFあり]
  [魏晉石刻資料選注( 一八 處士石定墓誌 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……石定は石鑒の孫（石尠（石尟）の子）。
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb10.html)
  （@いつか書きたい『三国志』）

#### 温羨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B8%A9%E7%BE%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B8%A9%E7%BE%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B8%A9%E7%BE%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B8%A9%E7%BE%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B8%A9%E7%BE%A8)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n45/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb11.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　温羨](https://readingnotesofjinshu.com/translation/biographies/vol-44_4)
  （@晋書簡訳所）

### 巻45 列伝第15
#### 劉毅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%AF%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%AF%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%AF%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%AF%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%AF%85)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n51/mode/2up)
* [杜甫「暮秋枉裴道州手札率爾遣興寄近呈蘇渙侍御」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7223#%E6%9A%AE%E7%A7%8B%E6%9E%89%E8%A3%B4%E9%81%93%E5%B7%9E%E6%89%8B%E5%8A%84%EF%BC%8C%E7%8E%87%E7%88%BE%E9%81%A3%E8%88%88%EF%BC%8C%E5%AF%84%E8%BF%91%E5%91%88%E8%98%87%E6%B8%99%E4%BE%8D%E7%A6%A6)
  （訳注は[『続国訳漢文大成 文学部 第6巻の下（杜少陵詩集 下巻の下）』](https://dl.ndl.go.jp/info:ndljp/pid/1239692/392)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 劉暾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%9A%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%9A%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%9A%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%9A%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%9A%BE)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n71/mode/2up)
* [ブログ]
  [巻四十五　列伝第十五　劉毅（2）](https://readingnotesofjinshu.com/translation/biographies/vol-45_2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 劉総
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%B7%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%B7%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%B7%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%B7%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%B7%8F)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n77/mode/2up)

#### 程衛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A8%8B%E8%A1%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A8%8B%E8%A1%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A8%8B%E8%A1%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A8%8B%E8%A1%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A8%8B%E8%A1%9B)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n77/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 和嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%92%8C%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%92%8C%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%92%8C%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%92%8C%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%92%8C%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n77/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「和嶠專車」（第66句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/55)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「長輿追專車之恨」とある。

#### 武陔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AD%A6%E9%99%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AD%A6%E9%99%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AD%A6%E9%99%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AD%A6%E9%99%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AD%A6%E9%99%94)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n83/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 任愷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BB%BB%E6%84%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BB%BB%E6%84%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BB%BB%E6%84%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BB%BB%E6%84%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BB%BB%E6%84%B7)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n87/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/45-5jingai.html)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 崔洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B4%94%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B4%94%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B4%94%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B4%94%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B4%94%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 郭奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n93/mode/2up)
* [顏延之の「五君詠」の「阮始平」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E5%A7%8B%E5%B9%B3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 侯史光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BE%AF%E5%8F%B2%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n97/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/45-7koushikou.html)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 何攀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%94%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%94%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%94%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%94%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%94%80)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n99/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)


### 巻46 列伝第16
#### 劉頌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%A0%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%A0%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%A0%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%A0%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%A0%8C)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n1/mode/2up)

#### 李重
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E9%87%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E9%87%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E9%87%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E9%87%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E9%87%8D)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n35/mode/2up)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/190)）に「李重之識會」とある。

#### 李毅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%AF%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%AF%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%AF%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%AF%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%AF%85)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n45/mode/2up)

### 巻47 列伝第17
#### 傅玄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E7%8E%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E7%8E%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E7%8E%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E7%8E%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E7%8E%84)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n53/mode/2up)

#### 傅咸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E5%92%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E5%92%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E5%92%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E5%92%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E5%92%B8)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n67/mode/2up)

#### 傅祗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E7%A5%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E7%A5%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E7%A5%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E7%A5%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E7%A5%97)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n89/mode/2up)

#### 傅宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n93/mode/2up)

#### 傅暢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E6%9A%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E6%9A%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E6%9A%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E6%9A%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E6%9A%A2)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n95/mode/2up)


### 巻48 列伝第18
#### 向雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%91%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%91%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%91%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%91%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%91%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n1/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 段灼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B5%E7%81%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B5%E7%81%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B5%E7%81%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B5%E7%81%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B5%E7%81%BC)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n5/mode/2up)

#### 閻纘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%96%BB%E7%BA%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%96%BB%E7%BA%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%96%BB%E7%BA%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%96%BB%E7%BA%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%96%BB%E7%BA%98)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n33/mode/2up)

#### 閻亨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%96%BB%E4%BA%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%96%BB%E4%BA%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%96%BB%E4%BA%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%96%BB%E4%BA%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%96%BB%E4%BA%A8)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n49/mode/2up)


### 巻49 列伝第19
#### 阮籍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E7%B1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E7%B1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E7%B1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E7%B1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E7%B1%8D)
* [PDFあり]
  [『晉書』阮籍傳訳註](https://dl.ndl.go.jp/info:ndljp/pid/10504514)
  （鷹橋 明久）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、阮籍伝で言及している
  [「詠懷詩」十七首](https://dl.ndl.go.jp/info:ndljp/pid/1912796/93)がある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、阮籍伝で言及している
  勧進文（ [「為鄭沖勸晉王牋」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/47) ）と
  阮籍伝に引かれた
  [「詣蔣公」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/53)がある。
* 『全三国文』
   [巻44](https://ctext.org/wiki.pl?if=gb&chapter=526858#p52)・
   [巻45](https://ctext.org/wiki.pl?if=gb&chapter=584252)・
   [巻46](https://ctext.org/wiki.pl?if=gb&chapter=924489)
   （画像版の
   [巻44](https://archive.org/details/02107157.cn/page/n34/mode/2up)・
   [巻45](https://archive.org/details/02107157.cn/page/n48/mode/2up)・
   [巻46](https://archive.org/details/02107157.cn/page/n70/mode/2up)）
* [書籍 (NDL)]
  空海『三教指帰』に[「達夜博奕して亦た嗣宗に踰えたり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/11)とある
  （[注59](https://dl.ndl.go.jp/info:ndljp/pid/1040599/37)）。
* 阮籍の青白眼を踏まえた表現
  * [劉因「宿田家」](https://zh.wikisource.org/wiki/%E9%9D%99%E4%BF%AE%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B704#%E5%AE%BF%E7%94%B0%E5%AE%B6)の「青白眼誰静」という句。
  * [倪瓚「述懐」](https://zh.wikisource.org/wiki/%E5%80%AA%E9%9B%B2%E6%9E%97%E8%A9%A9%E9%9B%86_(%E5%9B%9B%E9%83%A8%E5%8F%A2%E5%88%8A%E6%9C%AC)/%E5%8D%B7%E7%AC%AC%E4%B8%80)の「白眼視俗物」という句。
* [顏延之の「五君詠」の「阮步兵」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E6%AD%A5%E5%85%B5)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/68)）
* [杜甫の「秦州雜詩其十五」](https://zh.wikisource.org/wiki/%E7%A7%A6%E5%B7%9E%E9%9B%9C%E8%A9%A9%E5%85%B6%E5%8D%81%E4%BA%94)
  （[『続国訳漢文大成　文学部第17冊』](https://dl.ndl.go.jp/info:ndljp/pid/1140052/56)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「阮籍論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/21)
  （魏禧）
* [『三国志』巻21](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B721#%E9%99%84_%E9%98%AE%E7%B1%8D)

#### 阮咸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E5%92%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E5%92%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E5%92%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E5%92%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E5%92%B8)
* [書誌のみ]
  [阮咸伝(『晋書』巻四十九)訳注 (六朝詩の語彙および表現技巧の研究)](https://cir.nii.ac.jp/crid/1520853835396667648)
  （鷹橋 明久）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「仲容青雲」（第55句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/49)
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p51)
* 白居易「[和令狐僕射小飲聽阮咸](https://zh.wikisource.org/wiki/%E5%92%8C%E4%BB%A4%E7%8B%90%E5%83%95%E5%B0%84%E5%B0%8F%E9%A3%B2%E8%81%BD%E9%98%AE%E5%92%B8)」
  ……阮咸にちなんだ[楽器の阮咸](https://ja.wikipedia.org/wiki/%E9%98%AE%E5%92%B8_(%E6%A5%BD%E5%99%A8))の方を詠んだもの。
* [顏延之の「五君詠」の「阮始平」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E5%A7%8B%E5%B9%B3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E7%9E%BB)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「阮瞻三語」（第110句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/78)
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p57)
* 干宝[『搜神記』巻16](https://ctext.org/wiki.pl?if=gb&chapter=5557#p4)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮孚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E5%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E5%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E5%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E5%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E5%AD%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮脩（阮修）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E4%BF%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E4%BF%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E4%BF%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E4%BF%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E4%BF%AE)
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p61)
* [書籍 (NDL)]
  空海『三教指帰』に[「一百の青鳧常に杖の頭に懸けたり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)とある
  （[注74・75](https://dl.ndl.go.jp/info:ndljp/pid/1040599/38)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮放
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E6%94%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E6%94%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E6%94%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E6%94%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E6%94%BE)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮裕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E8%A3%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E8%A3%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E8%A3%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E8%A3%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E8%A3%95)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 嵆康（嵇康）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B5%87%E5%BA%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B5%87%E5%BA%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B5%87%E5%BA%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B5%87%E5%BA%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B5%87%E5%BA%B7)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「嵇呂命駕」（第25句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/32)と
  [「叔夜玉山」（第56句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/50)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、嵆康伝に引かれた
  [「幽憤詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/102)がある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、嵆康伝に（間引きながら）引かれた
  [「與山巨源絕交書」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/99)があり、
  嵆康伝では言及しているだけの
  [「養生論」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/285)もある。
* 『全三国文』
  [巻47](https://ctext.org/wiki.pl?if=gb&chapter=383681)・
  [巻48](https://ctext.org/wiki.pl?if=gb&chapter=930561)・
  [巻49](https://ctext.org/wiki.pl?if=gb&chapter=611925)・
  [巻50](https://ctext.org/wiki.pl?if=gb&chapter=997803)・
  [巻51](https://ctext.org/wiki.pl?if=gb&chapter=409217)・
  [巻52](https://ctext.org/wiki.pl?if=gb&chapter=607386)
* [顏延之の「五君詠」の「嵇中散」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%B5%87%E4%B8%AD%E6%95%A3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/68)）
* [顏延之の「五君詠」の「向常侍」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%90%91%E5%B8%B8%E4%BE%8D)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)
* [『三国志』巻21](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B721#%E9%99%84_%E5%B5%87%E5%BA%B7)
* [李攀龍「白雪樓」](https://zh.wikisource.org/wiki/%E6%BB%84%E6%BA%9F%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B709#%E7%99%BD%E9%9B%AA%E6%A8%93)に「無那嵇生成懶慢」とある。
* 嵆康（嵇康）の字の「叔夜」と同名の人（または字が同じ人）
    * 叔夜
        * [『論語』微子篇](https://zh.wikisource.org/wiki/%E8%AB%96%E8%AA%9E/%E5%BE%AE%E5%AD%90%E7%AC%AC%E5%8D%81%E5%85%AB)（[『漢籍國字解全書 : 先哲遺著追補』第30巻](https://dl.ndl.go.jp/pid/1226101/1/199?keyword=%E5%8F%94%E5%A4%9C)）に「周有八士：伯達、伯适、仲突、仲忽、叔夜、叔夏、季隨、季騧。」とある。
    * 柳叔夜
        * [『南史』巻73](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B773)
    * 鄭叔夜（立伝されている鄭羲の兄）
        * [『魏書』巻56](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B756#%E9%84%AD%E7%BE%B2)
        * [『北史』巻35](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7035#%E7%BE%B2%E5%BE%9E%E5%AD%90%E3%80%88%E7%BE%B2%E6%AC%A1%E5%85%84%E5%B0%8F%E7%99%BD%E5%AD%90%E3%80%89_%E8%83%A4%E4%BC%AF)
    * 康叔夜
        * [『旧唐書』巻151（伊慎）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7151#%E4%BC%8A%E6%85%8E)
        * [『資治通鑑』巻228](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7228)（[『続国訳漢文大成 経子史部』第13巻](https://dl.ndl.go.jp/pid/1239987/1/105?keyword=%E5%BA%B7%E5%8F%94%E5%A4%9C)）
        * [『資治通鑑』巻231](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7231)（[『続国訳漢文大成 経子史部』第13巻](https://dl.ndl.go.jp/pid/1239987/1/156?keyword=%E5%BA%B7%E5%8F%94%E5%A4%9C)）
    * 宋叔夜（立伝されている宋申錫の父）
        * [『旧唐書』巻167](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7167#%E5%AE%8B%E7%94%B3%E9%8C%AB)
    * 朱叔夜
        * [『旧唐書』巻17下（大和七年十一月）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B717%E4%B8%8B#%E5%A4%A7%E5%92%8C%E4%B8%83%E5%B9%B4)
        * [『新唐書』巻164（殷侑）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7164#%E6%AE%B7%E4%BE%91)
    * 趙叔夜
        * [『宋史』巻234（宗室世系表）](https://commons.wikimedia.org/w/index.php?title=File:CADAL06060999_%E5%AE%8B%E5%8F%B2%C2%B7%E5%8D%B7%E4%BA%8C%E7%99%BE%E4%B8%89%E5%8D%81%E5%9B%9B.djvu&page=13)……[Wikisourceのこの巻](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7234)は未完成。
    * 張叔夜（しかもこの人の字は「嵇仲」である）
        * [『宋史』巻353](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7353#%E5%BC%B5%E5%8F%94%E5%A4%9C)
    * 周思兼（字が叔夜）
        * [『明史』巻208](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7208)
* 陸龜蒙[「秘色越器」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7629#%E7%A7%98%E8%89%B2%E8%B6%8A%E5%99%A8)に「共嵇中散鬬遺杯」とあり、幸田露伴[『蝸牛庵聯話』](https://dl.ndl.go.jp/pid/1130069/1/11?keyword=%E5%B5%87%E4%B8%AD%E6%95%A3)で「嵇中散は晉の高士嵇康なり」云々と言及されている。

#### 向秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%91%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%91%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%91%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%91%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%91%E7%A7%80)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、向秀伝に引かれた
  [「思舊賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/291)がある。
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p38)
* [顏延之の「五君詠」の「向常侍」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%90%91%E5%B8%B8%E4%BE%8D)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 劉伶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E4%BC%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E4%BC%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E4%BC%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E4%BC%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E4%BC%B6)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、劉伶伝に引かれた
  [「酒德頌」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/200)がある。
* 『全晋文』
  [巻66](https://ctext.org/wiki.pl?if=gb&chapter=171814)
* [顏延之の「五君詠」の「劉參軍」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%8A%89%E5%8F%83%E8%BB%8D)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/68)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 謝鯤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%AF%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%AF%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%AF%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%AF%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%AF%A4)
* [東晉謝鯤墓誌](http://csid.zju.edu.cn/tomb/stone/detail?id=40288b955ab29f4b015b9fa0c4e00073&rubbingId=40288b955ab29f4b015b9fa0c4e80074)
* [PDFあり]
  [魏晉石刻資料選注( 一九 謝府君神道闕 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……謝府君こと謝纘は謝鯤の祖父。
* 干宝[『搜神記』巻18](https://ctext.org/wiki.pl?if=gb&chapter=232809#p18)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)
* [『六十種曲』](https://zh.wikisource.org/wiki/%E5%85%AD%E5%8D%81%E7%A8%AE%E6%9B%B2)所収の[『投梭記』](https://commons.wikimedia.org/w/index.php?search=%22%E6%8A%95%E6%A2%AD%E8%A8%98%22&title=Special:MediaSearch&go=Go&type=other)は謝鯤を題材にしている。

#### 胡毋輔之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [ブログ]
  [胡毋輔之伝](http://strawberrymilk.gooside.com/text/shinjo/49-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 胡毋謙之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [ブログ]
  [胡毋輔之伝](http://strawberrymilk.gooside.com/text/shinjo/49-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 畢卓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%95%A2%E5%8D%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%95%A2%E5%8D%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%95%A2%E5%8D%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%95%A2%E5%8D%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%95%A2%E5%8D%93)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [畢卓伝](http://strawberrymilk.gooside.com/text/shinjo/49-7.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [書籍 (NDL)]
  空海『三教指帰』に[「觴を提げ蟹を捕るの行専ら胸の中に蘊めり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)とある
  （[注72](https://dl.ndl.go.jp/info:ndljp/pid/1040599/38)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 王尼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%B0%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%B0%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%B0%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%B0%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%B0%BC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 羊曼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E6%9B%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E6%9B%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E6%9B%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E6%9B%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E6%9B%BC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 羊聃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E8%81%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E8%81%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E8%81%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E8%81%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E8%81%83)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 光逸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%85%89%E9%80%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%85%89%E9%80%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%85%89%E9%80%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%85%89%E9%80%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%85%89%E9%80%B8)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)


### 巻50 列伝第20
#### 曹志
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9B%B9%E5%BF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9B%B9%E5%BF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9B%B9%E5%BF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9B%B9%E5%BF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9B%B9%E5%BF%97)
* [ブログ]
  [『晋書』「曹志伝」(前半)](http://humiarisaka.blog40.fc2.com/blog-entry-60.html)と[『晋書』「曹志伝」（後半）](http://humiarisaka.blog40.fc2.com/blog-entry-61.html)
  （@「私家版 曹子建集」ブログ）
* [ブログ]
  [『晋書』列２０より、曹操の「孫」](http://3guozhi.net/s/ss1.html)
  （@いつか書きたい『三国志』）


#### 庾峻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%B3%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%B3%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%B3%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%B3%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%B3%BB)

#### 庾珉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%91%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%91%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%91%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%91%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%91%89)

#### 庾敳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%95%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%95%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%95%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%95%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%95%B3)
* [書籍 (NDL)]
  空海『三教指帰』に[「森森たる千仭此の庾嵩に比せん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注125](https://dl.ndl.go.jp/info:ndljp/pid/1040599/44)にもあるように、庾敳の字は子嵩）。

#### 郭象
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E8%B1%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E8%B1%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E8%B1%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E8%B1%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E8%B1%A1)
* [書籍 (NDL)]
  空海『三教指帰』に[「張儀郭象も遙かに瞻て聲を飲む」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/9)とあり
  （[注7](https://dl.ndl.go.jp/info:ndljp/pid/1040599/34)）、
  [「郭象が靈翰萬たび集めても何ぞ論ぜん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/29)ともある
  （[注151](https://dl.ndl.go.jp/info:ndljp/pid/1040599/61)）。

#### 庾純
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%B4%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%B4%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%B4%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%B4%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%B4%94)

#### 庾旉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%97%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%97%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%97%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%97%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%97%89)

#### 秦秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A7%A6%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A7%A6%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A7%A6%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A7%A6%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A7%A6%E7%A7%80)
* [ブログ]
  [『晋書』列２０より、曹操の「孫」](http://3guozhi.net/s/ss2.html)
  （@いつか書きたい『三国志』）


