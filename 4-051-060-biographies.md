### 巻51 列伝第21
#### 皇甫謐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E8%AC%90)

#### 皇甫方回
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)

#### 摯虞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%91%AF%E8%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%91%AF%E8%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%91%AF%E8%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%91%AF%E8%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%91%AF%E8%99%9E)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/193)）に「荀摯競爽於晉世」とあり、注によれば「摯」は摯虞のことを指す。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「太叔辨洽（大叔辨給）」「摯仲辭翰（摯仲詞翰）」（第79・80句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/63)

#### 束晳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9F%E6%99%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9F%E6%99%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9F%E6%99%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9F%E6%99%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9F%E6%99%B3)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、束晳伝で言及している
  [「補亡詩」六首](https://dl.ndl.go.jp/info:ndljp/pid/1912796/22)がある。

#### 王接
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%8E%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%8E%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%8E%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%8E%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%8E%A5)


### 巻52 列伝第22
#### 郤詵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%A4%E8%A9%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%A4%E8%A9%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%A4%E8%A9%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%A4%E8%A9%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%A4%E8%A9%B5)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「郤詵一枝」（第45句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/44)

#### 阮种（阮種）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E7%A8%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E7%A8%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E7%A8%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E7%A8%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E7%A8%AE)

#### 華譚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%AD%9A)

#### 袁甫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E7%94%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E7%94%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E7%94%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E7%94%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E7%94%AB)


### 巻53 列伝第23
#### 愍懐太子（司馬遹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [ブログ]
  [『晋書』列伝２３、司馬遹「愍懐太子伝」翻訳！](http://3guozhi.net/zaka2/bin1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)

#### 司馬虨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%99%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)

#### 司馬臧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%87%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)

#### 司馬尚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B0%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)


### 巻54 列伝第24
#### 陸機
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E6%A9%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E6%A9%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E6%A9%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E6%A9%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E6%A9%9F)
* [PDFあり]
  [六朝文人伝 : 陸機・陸雲伝(晋書)](https://dl.ndl.go.jp/info:ndljp/pid/10504338)
  （長谷川 滋成）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「士衡患多」（第22句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/30)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、陸機伝に引かれた
  [「豪士賦序」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/173)と、
  [「辯亡論上下二首」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/294)と、
  [「五等論」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/302)とがある。
* [『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho02-02)に
  「また、陸士衡が文賦には、在木闕不材之質 処雁乏善鳴之分ともあり。」
  とあるが、
  [『十訓抄詳解』](https://dl.ndl.go.jp/info:ndljp/pid/945602/84)でも指摘されているとおり、これは、
  陸機の「文賦」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B717#%E6%96%87%E8%B3%A6)・
    [『国訳漢文大成　文学部第二卷　文選上巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912763/304)）
  ではなく、
  盧諶の「贈劉琨并書」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E8%B4%88%E5%8A%89%E7%90%A8%E5%B9%B6%E6%9B%B8)・
    [『国訳漢文大成　文学部第三卷　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/151)）
  にある。
* [胡曾の「華亭」](https://zh.wikisource.org/wiki/%E8%8F%AF%E4%BA%AD)
* [姚承緒『吳趨訪古錄』の「張翰墓」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/6#%E5%BC%B5%E7%BF%B0%E5%A2%93)
* [姚承緒『吳趨訪古錄』の「菽園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E8%8F%BD%E5%9C%92)
  ……冒頭の「平原」は陸機のことではなかろうか。
* [李白「行路難」三](https://zh.wikisource.org/wiki/%E8%A1%8C%E8%B7%AF%E9%9B%A3_(%E6%9C%89%E8%80%B3%E8%8E%AB%E6%B4%97%E6%BD%81%E5%B7%9D%E6%B0%B4))
  （[『続国訳漢文大成 文学部第2冊（李太白詩集 上の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139917/43)）
* [章太炎（章炳麟）「陸機贊」](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%82%8E%E6%96%87%E9%8C%84%E5%88%9D%E7%B7%A8/%E9%99%B8%E6%A9%9F%E8%B4%8A)

#### 孫拯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%8B%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%8B%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%8B%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%8B%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%8B%AF)

#### 陸雲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E9%9B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E9%9B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E9%9B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E9%9B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E9%9B%B2)
* [PDFあり]
  [六朝文人伝 : 陸機・陸雲伝(晋書)](https://dl.ndl.go.jp/info:ndljp/pid/10504338)
  （長谷川 滋成）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「鳴鶴日下」「士龍雲間」（第49・50句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/46)
* 清の時代に黃子雲という人がおり、諱の「雲」の文字が陸雲と共通するだけでなく、字も陸雲とお揃いの「士龍」である。ちなみに号は「野鴻」。
    * [百度百科](https://baike.baidu.com/item/%E9%BB%84%E5%AD%90%E4%BA%91/5582620)
    * 黃子雲[『野鴻詩的』](https://commons.wikimedia.org/w/index.php?title=File:SSID-12362315_%E6%B8%85%E8%A9%A9%E8%A9%B1_%E7%A7%8B%E7%AA%97%E9%9A%A8%E7%AD%86%E8%87%B3%E9%87%8E%E9%B4%BB%E8%A9%A9%E7%9A%84.pdf&page=34)
    * 沈藻采[『元和唯亭志』](https://commons.wikimedia.org/w/index.php?title=File%3ANLC403-312001066074-66329_%E5%85%83%E5%92%8C%E5%94%AF%E4%BA%AD%E5%BF%97_%E6%B8%85%E9%81%93%E5%85%8928%E5%B9%B4(1848)_%E5%8D%B7%E4%B8%80%E5%8D%81%E5%85%AB.pdf&page=6)
    * 徐傳詩[『星湄詩話』](https://commons.wikimedia.org/w/index.php?title=File%3ACADAL02120888_%E6%98%9F%E6%B9%84%E8%A9%A9%E8%A9%B1%EF%BC%88%E4%BA%8C%EF%BC%89.djvu&page=10)

#### 陸耽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E8%80%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E8%80%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E8%80%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E8%80%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E8%80%BD)

#### 陸喜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E5%96%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E5%96%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E5%96%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E5%96%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E5%96%9C)


### 巻55 列伝第25
#### 夏侯湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「岳湛連璧」（第44句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/43)

#### 夏侯淳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E6%B7%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E6%B7%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E6%B7%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E6%B7%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E6%B7%B3)

#### 夏侯承
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E6%89%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E6%89%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E6%89%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E6%89%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E6%89%BF)

#### 潘岳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BD%98%E5%B2%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BD%98%E5%B2%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BD%98%E5%B2%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BD%98%E5%B2%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BD%98%E5%B2%B3)
* [PDFあり]
  [六朝文人伝 : 「晋書」潘岳伝](https://dl.ndl.go.jp/info:ndljp/pid/10504358)
  （坂元 悦夫）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「岳湛連璧」（第44句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/43)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、潘岳伝に引かれた
  [「藉田賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/136)と
  [「閒居賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/284)があり、
  潘岳伝では言及しているだけの
  [「西征賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/178)もある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、潘岳伝に一部が引かれた
  [「金谷集作詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/54)がある。
* [書籍 (NDL)]
  空海『三教指帰』に[「潘安が詩を詠じて彌〻哀哭を増し」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/26)とある
  （[注132](https://dl.ndl.go.jp/info:ndljp/pid/1040599/60)）。
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……「河陽一縣花」は潘岳に関する故事らしい。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）
* [周文質「蝶戀花・悟迷」](https://ctext.org/wiki.pl?if=gb&chapter=390671#p102)に「青樓空擲潘安果」とある。

#### 潘尼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BD%98%E5%B0%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BD%98%E5%B0%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BD%98%E5%B0%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BD%98%E5%B0%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BD%98%E5%B0%BC)

#### 張載
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%BC%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%BC%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%BC%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%BC%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%BC%89)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、張載伝に引かれた
  [「劍閣銘」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/337)がある。
* [『旧五代史』巻136の「史臣曰」のところ](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B7136#%E5%8F%B2%E8%87%A3%E6%9B%B0)に張載の「劍閣銘」が引かれている。

#### 張協
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%8D%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%8D%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%8D%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%8D%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%8D%94)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、張協伝に引かれた
  [「七命」八首](https://dl.ndl.go.jp/info:ndljp/pid/1912796/365)がある。

#### 張亢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E4%BA%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E4%BA%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E4%BA%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E4%BA%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E4%BA%A2)


### 巻56 列伝第26
#### 江統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E7%B5%B1)
* [ブログ]
  [「江統伝」を訳し、『晋書』列伝２６をコンプ](http://3guozhi.net/zaka2/ss2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻五十六 列伝第二十六 江統(1)](https://readingnotesofjinshu.com/translation/biographies/vol-56_1)・
  [(2)](https://readingnotesofjinshu.com/translation/biographies/vol-56_2)
  （@晋書簡訳所）
* [ブログ]
  [徙戎論その1](https://t-s.hatenablog.com/entry/20110527/1306422647)・
  [その2](https://t-s.hatenablog.com/entry/20110528/1306555175)・
  [その3](https://t-s.hatenablog.com/entry/20110529/1306595018)・
  [その4](https://t-s.hatenablog.com/entry/20110531/1306767771)・
  [その5](https://t-s.hatenablog.com/entry/20110601/1306854098)・
  [その6](https://t-s.hatenablog.com/entry/20110602/1306940568)・
  [その7](https://t-s.hatenablog.com/entry/20110604/1307116473)・
  [その8](https://t-s.hatenablog.com/entry/20110605/1307199754)・
  [その9](https://t-s.hatenablog.com/entry/20110606/1307286166)
  （@てぃーえすのメモ帳）

#### 江虨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E8%99%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E8%99%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E8%99%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E8%99%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E8%99%A8)
* [ブログ]
  [巻五十六 列伝第二十六 江統(3)](https://readingnotesofjinshu.com/translation/biographies/vol-56_3#toc1)
  （@晋書簡訳所）

#### 江惇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E6%83%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E6%83%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E6%83%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E6%83%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E6%83%87)
* [ブログ]
  [巻五十六 列伝第二十六 江統(3)](https://readingnotesofjinshu.com/translation/biographies/vol-56_3#toc2)
  （@晋書簡訳所）

#### 孫楚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%A5%9A)
* [ブログ]
  [『晋書』列伝２６より、「孫楚伝」を翻訳](http://3guozhi.net/zaka2/ss1.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、孫楚伝に引かれた
  [「為石仲容與孫皓書」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/105)がある。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「孫楚漱石」（第71句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/58)

#### 孫統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%B5%B1)

#### 孫綽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%B6%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%B6%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%B6%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%B6%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%B6%BD)
* [書誌のみ]
  [『晋書』巻五六孫綽伝訳注](https://cir.nii.ac.jp/crid/1572261551687976448)
  （長谷川 滋成）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、孫綽伝で言及している
  [「遊天台山賦并序」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/204)がある。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「孫綽才冠」（第78句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/63)
* [書籍 (NDL)]
  空海『三教指帰』に[「玲玲として玉のごとくに振ひ孫馬を凌いで以て瑤を連ね」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注131](https://dl.ndl.go.jp/info:ndljp/pid/1040599/45)）。
* 謝肇淛の[『五雜俎』](https://zh.wikisource.org/wiki/%E4%BA%94%E9%9B%9C%E4%BF%8E/%E5%8D%B716)に「或改之曰：「孫綽賦天臺景，赤城霞起而建標；杜牧詠江南春，十里鶯啼而映綠。」」云々という言及があり、同様の言及（ただし「赤城」でなく「高城」）が蔣一葵の[『堯山堂偶雋』](https://zh.wikisource.org/wiki/%E5%A0%AF%E5%B1%B1%E5%A0%82%E5%81%B6%E9%9B%8B/%E5%8D%B7%E4%B8%89)にもある。

### 巻57 列伝第27
#### 羅憲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E6%86%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E6%86%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E6%86%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E6%86%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E6%86%B2)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [呉に益州は獲らせぬ、羅憲伝。](http://3guozhi.net/hito/rake1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 羅尚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E5%B0%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E5%B0%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E5%B0%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E5%B0%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E5%B0%9A)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [呉に益州は獲らせぬ、羅憲伝。](http://3guozhi.net/hito/rake2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 滕脩（滕修）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BB%95%E4%BF%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BB%95%E4%BF%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BB%95%E4%BF%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BB%95%E4%BF%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BB%95%E4%BF%AE)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 馬隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AC%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AC%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AC%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AC%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AC%E9%9A%86)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 胡奮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E5%A5%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E5%A5%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E5%A5%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E5%A5%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E5%A5%AE)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 陶璜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%92%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%92%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%92%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%92%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%92%9C)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 吾彦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%BE%E5%BD%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%BE%E5%BD%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%BE%E5%BD%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%BE%E5%BD%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%BE%E5%BD%A6)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%C6%F3%BD%BD%BC%B7)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 張光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%85%89)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 趙誘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B6%99%E8%AA%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B6%99%E8%AA%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B6%99%E8%AA%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B6%99%E8%AA%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B6%99%E8%AA%98)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%C6%F3%BD%BD%BC%B7)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)


### 巻58 列伝第28
#### 周処
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%87%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%87%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%87%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%87%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%87%A6)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「周處三害」（第28句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/33)
* 『漢魏六朝百三名家集』所収『陸平原集』の「晉平西將軍孝侯周處碑」
  （[デジコレの画像版](https://dl.ndl.go.jp/info:ndljp/pid/2551366/90)と[Wikisourceのテキスト版](https://zh.wikisource.org/wiki/%E6%BC%A2%E9%AD%8F%E5%85%AD%E6%9C%9D%E7%99%BE%E4%B8%89%E5%AE%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7048#%E7%A2%91)）
* [PDFあり]
  [正史成立前夜 : 碑・誌の時代](https://doi.org/10.14945/00009538)
  （山田 智）
  ……「晉平西將軍孝侯周處碑」について論じている。
* [書籍 (NDL)]
  空海『三教指帰』に[「周處心を改めて忠孝の名を得たり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/11)とある
  （[注49](https://dl.ndl.go.jp/info:ndljp/pid/1040599/36)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周玘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E7%8E%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E7%8E%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E7%8E%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E7%8E%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E7%8E%98)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周勰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%8B%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%8B%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%8B%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%8B%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%8B%B0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周彝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%BD%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%BD%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%BD%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%BD%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%BD%9D)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周札
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%9C%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%9C%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%9C%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%9C%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%9C%AD)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周莚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%8E%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%8E%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%8E%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%8E%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%8E%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周訪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%A8%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%A8%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%A8%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%A8%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%A8%AA)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周撫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%92%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%92%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%92%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%92%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%92%AB)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周楚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%A5%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周瓊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E7%93%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E7%93%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E7%93%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E7%93%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E7%93%8A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周虓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%99%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%99%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%99%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%99%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%99%93)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%85%89)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周仲孫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E4%BB%B2%E5%AD%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)


### 巻59 列伝第29
#### 汝南文成王（司馬亮）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BA%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc1)
  （@晋書簡訳所）

#### 司馬粹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B2%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc2)
  （@晋書簡訳所）

#### 司馬矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc2)
  （@晋書簡訳所）

#### 司馬祐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%A5%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc2)
  （@晋書簡訳所）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)

#### 司馬羕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%BE%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc3)
  （@晋書簡訳所）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)

#### 司馬宗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AE%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc4)
  （@晋書簡訳所）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)

#### 司馬熙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc5)
  （@晋書簡訳所）

#### 楚隠王（司馬瑋）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%91%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc6)
  （@晋書簡訳所）

#### 趙王（司馬倫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%80%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　趙王倫](https://readingnotesofjinshu.com/translation/biographies/vol-59_3)
  （@晋書簡訳所）
* [周曇の「詠史詩」の中の「賈后」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%B3%88%E5%90%8E)

#### 斉武閔王（司馬冏）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%86%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [ブログ]
  [『晋書』列伝２９より、「司馬冏伝」を抄訳](http://3guozhi.net/zaka2/sk.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　斉王冏](https://readingnotesofjinshu.com/translation/biographies/vol-59_4#toc1)
  （@晋書簡訳所）
* [周曇の「詠史詩」の中の「賈后」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%B3%88%E5%90%8E)

#### 鄭方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E6%96%B9)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　斉王冏](https://readingnotesofjinshu.com/translation/biographies/vol-59_4#toc2)
  （@晋書簡訳所）

#### 長沙厲王（司馬乂）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%B9%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [ブログ]
  [『晋書』列伝２９より、「司馬乂伝」を翻訳](http://3guozhi.net/n/sg.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　長沙王乂　成都王穎](https://readingnotesofjinshu.com/translation/biographies/vol-59_5#toc1)
  （@晋書簡訳所）
* 干宝[『搜神記』巻7](https://ctext.org/wiki.pl?if=gb&chapter=749239#p29)

#### 成都王（司馬潁）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BD%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [ブログ]
  [『晋書』列伝２９より、「司馬頴伝」を翻訳](http://3guozhi.net/n/se.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　長沙王乂　成都王穎](https://readingnotesofjinshu.com/translation/biographies/vol-59_5#toc2)
  （@晋書簡訳所）
* 干宝[『搜神記』巻7](https://ctext.org/wiki.pl?if=gb&chapter=749239#p29)

#### 河間王（司馬顒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A1%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [ブログ]
  [『晋書』列伝２９より、「司馬顒伝」を翻訳](http://3guozhi.net/zaka2/sg.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　河間王顒　東海王越](https://readingnotesofjinshu.com/translation/biographies/vol-59_6#toc1)
  （@晋書簡訳所）

#### 東海孝献王（司馬越）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%B6%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [ブログ]
  [『晋書』列伝２９より、「司馬越伝」を翻訳](http://3guozhi.net/zaka2/et1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　河間王顒　東海王越](https://readingnotesofjinshu.com/translation/biographies/vol-59_6#toc2)
  （@晋書簡訳所）


### 巻60 列伝第30
#### 解系
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A7%A3%E7%B3%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A7%A3%E7%B3%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A7%A3%E7%B3%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A7%A3%E7%B3%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A7%A3%E7%B3%BB)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 解結
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A7%A3%E7%B5%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A7%A3%E7%B5%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A7%A3%E7%B5%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A7%A3%E7%B5%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A7%A3%E7%B5%90)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 解育
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A7%A3%E8%82%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A7%A3%E8%82%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A7%A3%E8%82%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A7%A3%E8%82%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A7%A3%E8%82%B2)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 孫旂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%97%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%97%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%97%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%97%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%97%82)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 孟観
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E8%A6%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E8%A6%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E8%A6%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E8%A6%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E8%A6%B3)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 牽秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%89%BD%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%89%BD%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%89%BD%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%89%BD%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%89%BD%E7%A7%80)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc4)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 繆播
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B9%86%E6%92%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B9%86%E6%92%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B9%86%E6%92%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B9%86%E6%92%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B9%86%E6%92%AD)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 繆胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B9%86%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B9%86%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B9%86%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B9%86%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B9%86%E8%83%A4)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 皇甫重
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E9%87%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E9%87%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E9%87%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E9%87%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E9%87%8D)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 張輔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%BC%94)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 李含
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%90%AB)
* [ブログ]
  [巻六十 列伝第三十 李含 張方](https://readingnotesofjinshu.com/translation/biographies/vol-60_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 張方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%96%B9)
* [ブログ]
  [巻六十 列伝第三十 李含 張方](https://readingnotesofjinshu.com/translation/biographies/vol-60_3#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 閻鼎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%96%BB%E9%BC%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%96%BB%E9%BC%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%96%BB%E9%BC%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%96%BB%E9%BC%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%96%BB%E9%BC%8E)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 索靖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E9%9D%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E9%9D%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E9%9D%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E9%9D%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E9%9D%96)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「瓘靖二妙」（第43句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/42)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 索綝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E7%B6%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E7%B6%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E7%B6%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E7%B6%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E7%B6%9D)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 賈疋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E7%96%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E7%96%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E7%96%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E7%96%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E7%96%8B)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc4)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)


