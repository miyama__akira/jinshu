### 巻61 列伝第31
#### 周浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n56/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 周嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n60/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 周謨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%AC%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%AC%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%AC%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%AC%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%AC%A8)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n66/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 周馥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%A6%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%A6%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%A6%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%A6%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%A6%A5)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n68/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 成公簡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%90%E5%85%AC%E7%B0%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%90%E5%85%AC%E7%B0%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%90%E5%85%AC%E7%B0%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%90%E5%85%AC%E7%B0%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%90%E5%85%AC%E7%B0%A1)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n74/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 苟晞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%9F%E6%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%9F%E6%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%9F%E6%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%9F%E6%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%9F%E6%99%9E)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n74/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 華軼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%BB%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%BB%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%BB%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%BB%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%BB%BC)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n88/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 劉喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n92/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [黄中通理を知っているか？劉廙、孫劉喬伝](http://3guozhi.net/hito/riy1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 劉耽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%80%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%80%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%80%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%80%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%80%BD)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n100/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* 干宝[『搜神記』巻5](https://ctext.org/wiki.pl?if=gb&chapter=503309#p4)
  （劉耽の子が出てくる）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 劉柳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%9F%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%9F%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%9F%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%9F%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%9F%B3)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n100/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)


### 巻62 列伝第32
#### 劉琨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%90%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%90%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%90%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%90%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%90%A8)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n2/mode/2up)
* [PDFあり]
  [劉琨年譜稿](https://dl.ndl.go.jp/info:ndljp/pid/10504350)
  （坂元 悦夫）
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（1）](https://readingnotesofjinshu.com/translation/biographies/vol-62_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/vol-62_2)、
  [同（3）](https://readingnotesofjinshu.com/translation/biographies/vol-62_3)
  （@晋書簡訳所）
* [『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho02-02)に
  「また、陸士衡が文賦には、在木闕不材之質 処雁乏善鳴之分ともあり。」
  とあるが、
  [『十訓抄詳解』](https://dl.ndl.go.jp/info:ndljp/pid/945602/84)でも指摘されているとおり、これは、
  陸機の「文賦」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B717#%E6%96%87%E8%B3%A6)・
    [『国訳漢文大成　文学部第二卷　文選上巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912763/304)）
  ではなく、
  盧諶の「贈劉琨并書」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E8%B4%88%E5%8A%89%E7%90%A8%E5%B9%B6%E6%9B%B8)・
    [『国訳漢文大成　文学部第三卷　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/151)）
  にある。

#### 劉羣（劉群）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%BE%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%BE%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%BE%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%BE%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%BE%A4)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n34/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc1)
  （@晋書簡訳所）

#### 劉輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n36/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc2)
  （@晋書簡訳所）

#### 劉演
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%BC%94)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉挹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%8C%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%8C%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%8C%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%8C%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%8C%B9)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉啟（劉啓）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%95%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%95%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%95%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%95%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%95%93)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉述
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%BF%B0)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 祖逖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E9%80%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E9%80%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E9%80%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E9%80%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E9%80%96)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [文天祥「正氣歌」](https://zh.wikisource.org/wiki/%E6%AD%A3%E6%B0%A3%E6%AD%8C)の「或為渡江楫　慷慨吞胡羯」という句
* [ブログ]
  [巻六十二　列伝第三十二　祖逖（1）](https://readingnotesofjinshu.com/translation/biographies/vol-62_5)
  （@晋書簡訳所）
* [胡曾の「豫州」](https://zh.wikisource.org/wiki/%E8%B1%AB%E5%B7%9E)

#### 祖納
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E7%B4%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E7%B4%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E7%B4%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E7%B4%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E7%B4%8D)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n54/mode/2up)
* [ブログ]
  [巻六十二　列伝第三十二　祖逖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-62_6)
  （@晋書簡訳所）

### 巻63 列伝第33
#### 邵続
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%82%B5%E7%B6%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%82%B5%E7%B6%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%82%B5%E7%B6%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%82%B5%E7%B6%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%82%B5%E7%B6%9A)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n2/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　邵続　李矩](https://readingnotesofjinshu.com/translation/biographies/vol-63_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 李矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E7%9F%A9)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n8/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　邵続　李矩](https://readingnotesofjinshu.com/translation/biographies/vol-63_1#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 段匹磾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B5%E5%8C%B9%E7%A3%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n18/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 魏浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n24/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 魏該
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E8%A9%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E8%A9%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E8%A9%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E8%A9%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E8%A9%B2)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n26/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 郭黙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E9%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E9%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E9%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E9%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E9%BB%99)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n28/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)



### 巻64 列伝第34 武十三王・元四王・簡文三子
#### 毗陵悼王（司馬軌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%BB%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 秦献王（司馬柬）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9F%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 城陽懷王（司馬景）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 東海沖王（司馬祗）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%A5%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 始平哀王（司馬裕）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A3%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n42/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 淮南忠壮王（司馬允）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%85%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n42/mode/2up)
* [ブログ]
  [『晋書』列伝３４、「司馬允＆司馬覃伝」翻訳](http://3guozhi.net/n/si.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 代哀王（司馬演）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 新都王（司馬該）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A9%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 清河康王（司馬遐）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、「司馬允＆司馬覃伝」翻訳](http://3guozhi.net/n/si.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬覃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A6%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、「司馬允＆司馬覃伝」翻訳](http://3guozhi.net/n/si.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬籥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B1%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n48/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬銓（司馬詮）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A9%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n48/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬端
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%AB%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n50/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 汝陰哀王（司馬謨）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%AC%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n50/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 呉孝王（司馬晏）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n50/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 渤海殤王（司馬恢）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%81%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n52/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 琅邪孝王（司馬裒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A3%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n52/mode/2up)
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g61.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 東海哀王（司馬沖）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n52/mode/2up)
  ……途中からページ抜けの箇所に当たっている。
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g61.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 武陵威王（司馬晞）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n54/mode/2up)
  ……末尾以外はページ抜けの箇所に当たっている。
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g62.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 梁王（司馬㻱）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E3%BB%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n54/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 武陵忠敬王（司馬遵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n54/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 琅邪悼王（司馬煥）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%85%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n56/mode/2up)
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g63.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 会稽思世子（司馬道生）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 臨川献王（司馬郁）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%83%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 会稽文孝王（司馬道子）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n64/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕（司馬道子の子）・桓玄への言及がある。

### 巻65 列伝第35
#### 王導
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%B0%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%B0%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%B0%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%B0%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%B0%8E)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n2/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王導公忠」（第8句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/21)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)
* [周曇の「詠史詩」の中の「王茂弘」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E7%8E%8B%E8%8C%82%E5%BC%98)

#### 王悦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%82%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%82%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%82%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%82%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%82%A6)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n28/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王恬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%81%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%81%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%81%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%81%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%81%AC)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王洽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B4%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B4%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B4%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B4%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B4%BD)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王珣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8F%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8F%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8F%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8F%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8F%A3)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n34/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王珣短簿」（第16句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/25)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)
* 伯遠帖
    * [王珣行书伯远帖](https://www.dpm.org.cn/collection/handwriting/228199.html)
    （@故宮博物院）
    * [伯遠帖](https://zh.wikipedia.org/wiki/%E4%BC%AF%E9%81%A0%E5%B8%96#/media/File:The_Calligraphy_Model_Boyuan_by_Wang_Xun.jpg)
    （@ Wikipedia）
* [姚承緒『吳趨訪古錄』の「王珣琴臺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%99%84%EF%BC%9A%E7%8E%8B%E7%8F%A3%E7%90%B4%E8%87%BA)
* [姚承緒『吳趨訪古錄』の「雲巖寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%9B%B2%E5%B7%96%E5%AF%BA)
* [姚承緒『吳趨訪古錄』の「短簿祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E7%9F%AD%E7%B0%BF%E7%A5%A0)
* 「團扇郎」
    * [『樂府詩集』45卷](https://zh.wikisource.org/wiki/%E6%A8%82%E5%BA%9C%E8%A9%A9%E9%9B%86/045%E5%8D%B7#%E5%9C%98%E6%89%87%E9%83%8E%E5%85%AD%E9%A6%96)
    * [高啓「團扇郎」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E5%9C%98%E6%89%87%E9%83%8E)（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/97)）

#### 王珉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8F%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8F%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8F%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8F%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8F%89)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)
* [姚承緒『吳趨訪古錄』の「古杉」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%99%84%EF%BC%9A%E5%8F%A4%E6%9D%89)
* [姚承緒『吳趨訪古錄』の「雲巖寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%9B%B2%E5%B7%96%E5%AF%BA)
* [姚承緒『吳趨訪古錄』の「景德寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/4#%E6%99%AF%E5%BE%B7%E5%AF%BA)
* [姚承緒『吳趨訪古錄』の「短簿祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E7%9F%AD%E7%B0%BF%E7%A5%A0)
* 「團扇郎」
    * [『樂府詩集』45卷](https://zh.wikisource.org/wiki/%E6%A8%82%E5%BA%9C%E8%A9%A9%E9%9B%86/045%E5%8D%B7#%E5%9C%98%E6%89%87%E9%83%8E%E5%85%AD%E9%A6%96)
    * [高啓「團扇郎」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E5%9C%98%E6%89%87%E9%83%8E)（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/97)）

#### 王協
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%8D%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%8D%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%8D%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%8D%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%8D%94)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n42/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王謐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%AC%90)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n44/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王劭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%8A%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%8A%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%8A%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%8A%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%8A%AD)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王薈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%96%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%96%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%96%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%96%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%96%88)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王廞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BB%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BB%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BB%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BB%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BB%9E)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

### 巻66 列伝第36
#### 劉弘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%BC%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%BC%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%BC%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%BC%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%BC%98)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n54/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [（劉馥＋劉表）／２＝劉弘伝](http://3guozhi.net/hito/rk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m066.html)

#### 陶侃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E4%BE%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E4%BE%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E4%BE%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E4%BE%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E4%BE%83)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n70/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* 『關帝靈籤』第八十籤に「陶侃卜牛眠」とある（[『晋書』巻58](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7058)周訪伝の末尾（附伝の後）にある話を指すと思われる）。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/80)のみ……[画像版](https://commons.wikimedia.org/w/index.php?title=File:%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=82)は「郭璞召陰葬地」という違う内容。

#### 陶洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%9E%BB)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶夏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E5%A4%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E5%A4%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E5%A4%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E5%A4%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E5%A4%8F)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶琦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%90%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%90%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%90%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%90%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%90%A6)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶旗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%97%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%97%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%97%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%97%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%97%97)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶斌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%96%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%96%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%96%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%96%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%96%8C)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶称
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%A7%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%A7%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%A7%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%A7%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%A7%B0)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶範
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E8%8C%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E8%8C%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E8%8C%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E8%8C%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E8%8C%83)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶岱
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E5%B2%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E5%B2%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E5%B2%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E5%B2%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E5%B2%B1)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶臻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E8%87%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E8%87%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E8%87%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E8%87%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E8%87%BB)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)


### 巻67 列伝第37
#### 温嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B8%A9%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B8%A9%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B8%A9%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B8%A9%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B8%A9%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n2/mode/2up)
* [關漢卿『溫太真玉鏡臺』](https://zh.wikisource.org/wiki/%E6%BA%AB%E5%A4%AA%E7%9C%9F%E7%8E%89%E9%8F%A1%E8%87%BA)
* 朱鼎『玉鏡臺記』……
  [Wikisource](https://zh.wikisource.org/wiki/%E7%8E%89%E9%8F%A1%E8%87%BA%E8%A8%98)、
  [中国都市芸能研究会](http://www.chengyan.wagang.jp/?%E5%85%AD%E5%8D%81%E7%A8%AE%E6%9B%B2/%E7%8E%89%E9%8F%A1%E8%87%BA%E8%A8%98)、
  [Wikimedia（画像版）](https://commons.wikimedia.org/w/index.php?title=File%3ACADAL06386264_%E7%8E%89%E9%8F%A1%E8%87%BA%E8%A8%98.djvu&page=1)
* [胡曾の「牛渚」](https://zh.wikisource.org/wiki/%E7%89%9B%E6%B8%9A_(%E8%83%A1%E6%9B%BE))
* [溫嶠墓誌](http://csid.zju.edu.cn/tomb/stone/detail?id=8a8fbda74ca22703014ca22a72fb16ce&rubbingId=8a8fbda74d093780014d270157b216e1)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「前郡尹溫太真劉真長，或功銘鼎彝，或德標素尚」とある。「太真」は温嶠の字。

#### 郗鑒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n32/mode/2up)
* [『隋書』巻35（経籍志四）](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B735#%E5%88%A5%E9%9B%86)に「晉太尉《郗鑒集》十卷錄一卷」
* [『旧唐書』巻47（経籍志下）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B747#%E5%88%A5%E9%9B%86)に「《郤鑒集》十卷」
* [『新唐書』巻60（芸文志四）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7060)に「郗鑒集十卷」
* [『太平広記』巻28](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC028)

#### 郗愔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E6%84%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E6%84%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E6%84%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E6%84%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E6%84%94)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n46/mode/2up)
* [蘇軾「郗方回郗嘉賓父子事」](https://zh.wikisource.org/wiki/%E9%83%97%E6%96%B9%E5%9B%9E%E9%83%97%E5%98%89%E8%B3%93%E7%88%B6%E5%AD%90%E4%BA%8B)
* [劉義慶『幽明錄』](https://ctext.org/wiki.pl?if=gb&chapter=303891#p83)に郗愔の話あり。
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)（中嶋 長文・伊藤 令子・ 平田 昌司）のp. 397, No. 80でも読める。

#### 郗超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n48/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「郗超髯參」（第15句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/25)
* [蘇軾「郗方回郗嘉賓父子事」](https://zh.wikisource.org/wiki/%E9%83%97%E6%96%B9%E5%9B%9E%E9%83%97%E5%98%89%E8%B3%93%E7%88%B6%E5%AD%90%E4%BA%8B)
* [『搜神後記』巻2「杜不愆」](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E5%BE%8C%E8%A8%98/02#%E6%9D%9C%E4%B8%8D%E6%84%86)
  （[『国訳漢文大成』第十二巻](https://dl.ndl.go.jp/info:ndljp/pid/1913008/87)に訓読あり）
* 「與桓溫箋」
    * [『淳化閣帖』巻2](https://dl.ndl.go.jp/info:ndljp/pid/2587028/26)
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p4)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
* 「與親友書論支道林」（支道林は支遁のこと）
    * [『高僧伝』巻4支遁伝](https://cbetaonline.dila.edu.tw/zh/T2059_004)（「郄超」と書かれている。この直前の郗超と謝安の会話は[『世説新語』品藻篇67](https://ctext.org/wiki.pl?if=gb&chapter=380418#p690)と同様）
    * [『歴代三宝紀』巻7](https://cbetaonline.dila.edu.tw/zh/T2034_007)（「却超」と書かれている）
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p6)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
* 「與謝慶緒書論三幡義」（謝慶緒は巻94の謝敷のこと）
    * 『文選』所収の[孫綽の「遊天台山賦」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B711#%E9%81%8A%E5%A4%A9%E5%8F%B0%E5%B1%B1%E8%B3%A6)の注に「郤敬輿與謝慶緒書論三幡義曰」云々とある。
      なお、「遊天台山賦」本文の日本語は[『国訳漢文大成』第二卷](https://dl.ndl.go.jp/info:ndljp/pid/1912763/204)で読める。
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p8)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
    * これと関連するのか分からないが、『文選』所収の[王儉の「褚淵碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B758#%E8%A4%9A%E6%B7%B5%E7%A2%91%E6%96%87)の注に「謝慶緒答郤敬書曰」云々とある。
      なお、「褚淵碑文」本文の日本語は[『国訳漢文大成』第四卷](https://dl.ndl.go.jp/info:ndljp/pid/1912835/380)で読める。
* 「奉法要」
    * [『弘明集』巻13](https://cbetaonline.dila.edu.tw/zh/T2102_013)
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p10)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
    * [書籍 (NDL)]
    [『中国仏教通史』第一巻](https://dl.ndl.go.jp/info:ndljp/pid/2989703)
    （塚本善隆）
    第六章第二節・附
    [「郗超の仏教要義（奉法要）——在俗貴族の実践的仏教要義の範例として——」](https://dl.ndl.go.jp/info:ndljp/pid/2989703/188)
    （福永光司訳）
    * [書籍 (NDL)]
    [『弘明集研究 巻下 (訳注篇 下(巻6-14))』](https://dl.ndl.go.jp/pid/12223260)
    （牧田諦亮 編）
    [「弘明集巻十三　（一）佛教信仰の要義〔奉法要〕」](https://dl.ndl.go.jp/pid/12223260/1/194)
    * [「奉法要」でCiNii Researchを検索](https://cir.nii.ac.jp/all?q=%E5%A5%89%E6%B3%95%E8%A6%81)
    * [「奉法要」でNDLを検索](https://ndlonline.ndl.go.jp/#!/search?searchCode=SIMPLE&lang=jp&keyword=%E5%A5%89%E6%B3%95%E8%A6%81)
    * [「奉法要」で次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A5%89%E6%B3%95%E8%A6%81&searchfield=&withouthighlight=)
* 『文館詞林』巻157所収「答傅郎詩」
  ……[早稲田大学図書館古典籍総合データベース](https://www.wul.waseda.ac.jp/kotenseki/html/he16/he16_00993/index.html)で見られる。
    * [PDF版](https://archive.wul.waseda.ac.jp/kosho/he16/he16_00993/he16_00993_0002/he16_00993_0002.pdf#page=32)
    * JPEG版
      （[1ページ目](https://archive.wul.waseda.ac.jp/kosho/he16/he16_00993/he16_00993_0002/he16_00993_0002_p0032.jpg)・
      [2ページ目](https://archive.wul.waseda.ac.jp/kosho/he16/he16_00993/he16_00993_0002/he16_00993_0002_p0033.jpg)）
* [釋僧祐撰『出三藏記集』巻12](https://cbetaonline.dila.edu.tw/zh/T2145_012)の「宋明帝勅中書侍郎陸澄撰法論目錄序第一」に挙げられたもので郗超に関わるもの。
    * 本無難問(郗嘉賓竺法汰難并郗答往反四首)
    * 郗與法𤀹書
    * 郗與開法師書
    * 郗與支法師書
    * 支書與郗嘉賓
    * 奉法要(郗嘉賓)
    * 通神呪(郗嘉賓)
    * 明感論(郗嘉賓)
    * 論三行上(郗嘉賓)
    * 敘通三行(郗嘉賓)
    * 郗與謝慶緒書往反五首
    * 論三行下(郗嘉賓)
    * 郗與傅叔玉書往反三首
    * 全生論(郗嘉賓)
    * 五陰三達釋(郗嘉賓)
* [釋慧皎撰『高僧傳』卷14・序錄](https://cbetaonline.dila.edu.tw/zh/T2059_014)に
  「中書郎郄景興《東山僧傳》、治中張孝秀《廬山僧傳》、中書陸明霞《沙門傳》，各競舉一方，不通今古；務存一善，不及餘行。」
  とあるので、郗超に『東山僧傳』という著作があったと分かる。
  「景興偶採居山之人，僧寶偏綴遊方之士，法濟唯張高逸之例，法安止命志節之科。」
  「孝秀染毫，復獲景興之誚。」
  ともある。
* [『隋書』巻35（経籍志四）](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B735#%E5%88%A5%E9%9B%86)に「晉中書郎《郤超集合》九卷梁十卷」
* [『旧唐書』巻47（経籍志下）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B747#%E5%88%A5%E9%9B%86)に「《郤超集合》十五卷」
* [『新唐書』巻60（藝文志四）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7060)に「郗超集十五卷」
* [傅亮の『光世音應驗記』の「呂竦」の条](https://zh.wikisource.org/wiki/%E5%85%89%E4%B8%96%E9%9F%B3%E6%87%89%E9%A9%97%E8%A8%98#%E5%91%82%E7%AB%A6)の最後にも「竦後與郗嘉賓周旋，郗口所説。」と出てくる。
  [『法苑珠林』巻65](https://cbetaonline.dila.edu.tw/zh/T2122_065)も同様。
* [ブログ]
  [郗超相关史料拾遗](https://sanjingjiuhuang.net/chaomemo/)
  （@一把甘蔗渣）
* [于知微「明堂令于大猷碑」](https://zh.wikisource.org/wiki/%E6%98%8E%E5%A0%82%E4%BB%A4%E6%96%BC%E5%A4%A7%E7%8C%B7%E7%A2%91)に「郗超以蕃伯之望，職總■河」および「郗嘉賓之卓犖高雋」とある。
* [趙蕃『淳熈稿』巻13](https://zh.wikisource.org/wiki/%E6%B7%B3%E7%86%88%E7%A8%BF_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B713#%E8%A8%AA%E5%AD%AB%E5%AD%90%E9%80%B2%E5%AD%90%E8%82%85%E6%96%BC%E9%BB%84%E5%A0%B0)に「劉伯山書來云有施主為造一亭劉子澄名曰竹溪索詩為賦二首」とある二首目の中に「今人那有郗嘉賓」とある。
* 黄庭堅・撰、任淵・注『山谷内集詩注』巻17の「武昌松風閣」に対する注に「世説曰郗嘉賓開庫得錢一日乞與親友周旋略盡」とある。
    * [早稲田大学図書館古典籍総合データベース](https://www.wul.waseda.ac.jp/kotenseki/html/he18/he18_01286/index.html)の[画像版](https://archive.wul.waseda.ac.jp/kosho/he18/he18_01286/he18_01286_0009/he18_01286_0009_p0013.jpg)
    * [Wikisourceのテキスト版](https://zh.wikisource.org/wiki/%E5%B1%B1%E8%B0%B7%E9%9B%86%E8%A9%A9%E6%B3%A8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%86%85%E9%9B%86%E5%8D%B717#%E6%AD%A6%E6%98%8C%E6%9D%BE%E9%A2%A8%E9%96%A3)
* 郗超の書に関して
    * [『南斉書』巻33・王僧虔伝](https://zh.wikisource.org/wiki/%E5%8D%97%E9%BD%8A%E6%9B%B8/%E5%8D%B733#%E7%8E%8B%E5%83%A7%E8%99%94)および[『南史』巻22・王僧虔伝](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B722#%E5%AD%90_%E5%83%A7%E8%99%94)に引く『論書』に「郗嘉賓草亞于二王，緊媚過其父」とある。
    * [李嗣真『書後品』](https://zh.wikisource.org/wiki/%E6%9B%B8%E5%BE%8C%E5%93%81)では中上品とされる。
    * [張懷瓘『書斷』巻中](https://zh.wikisource.org/wiki/%E6%9B%B8%E6%96%B7/%E5%8D%B7%E4%B8%AD)に「子超字景興，小字嘉賓，亦善書。」とある。
    * [竇臮『述書賦』](https://ctext.org/wiki.pl?if=gb&chapter=644061#p6)に「景興當年，曷云世乏。正草輕利，脫略古法。跡因心而謂何，為吏士之所多。惜森然之俊爽，嗟薎爾於中和。」「郗超，字景興，愔子。晉臨海太守。今見具姓名行草書共四紙。」とある。
    * [『宣和書譜』巻14](https://zh.wikisource.org/wiki/%E5%AE%A3%E5%92%8C%E6%9B%B8%E8%AD%9C#%E5%8D%B7%E5%8D%81%E5%9B%9B)に「子超尤得家傳之妙」とある。
* [姚承緒『吳趨訪古錄』の「短簿祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E7%9F%AD%E7%B0%BF%E7%A5%A0)
* 王讜の[『唐語林』巻5](https://zh.wikisource.org/wiki/%E5%94%90%E8%AA%9E%E6%9E%97/%E5%8D%B7%E4%BA%94)の郗昂と源乾曜のやりとりは、[『世説新語』雅量篇27](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E9%9B%85%E9%87%8F)の「入幕の賓」をネタにしており、直接的には郗超と謝安に言及している。

#### 郗僧施
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E5%83%A7%E6%96%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E5%83%A7%E6%96%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E5%83%A7%E6%96%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E5%83%A7%E6%96%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E5%83%A7%E6%96%BD)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n56/mode/2up)

#### 郗曇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E6%9B%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E6%9B%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E6%9B%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E6%9B%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E6%9B%87)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n56/mode/2up)

#### 郗恢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E6%81%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E6%81%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E6%81%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E6%81%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E6%81%A2)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n56/mode/2up)
* [劉義慶『幽明錄』](https://ctext.org/wiki.pl?if=gb&chapter=303891#p146)に郗恢の話あり。
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)（中嶋 長文・伊藤 令子・ 平田 昌司）のp. 429, No. 143でも読める。

#### 郗隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E9%9A%86)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n60/mode/2up)


### 巻68 列伝第38
#### 顧栄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E6%A0%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E6%A0%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E6%A0%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E6%A0%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E6%A0%84)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk1.html)
  （@いつか書きたい『三国志』）
* 高啓の[「顧榮廟」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B704#%E9%A1%A7%E6%A6%AE%E5%BB%9F)（[『国訳漢文大成. 続 文学部第76冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140757/13)も参照）
* [姚承緒『吳趨訪古錄』の「顧榮祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%A1%A7%E6%A6%AE%E7%A5%A0%E3%80%88%E5%92%8C%E9%AB%98%E9%9D%92%E9%82%B1%E9%9F%BB%E3%80%89)
* [姚承緒『吳趨訪古錄』の「永定寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E6%B0%B8%E5%AE%9A%E5%AF%BA%E3%80%88%E6%AC%A1%E9%9F%8B%E8%98%87%E5%B7%9E%E3%80%8A%E6%B0%B8%E5%AE%9A%E7%B2%BE%E8%88%8D%E3%80%8B%E9%9F%BB%E3%80%89)
  ……永定寺は顧栄の家の跡地にあるらしい（[『江南通志』巻31](https://zh.wikisource.org/wiki/%E6%B1%9F%E5%8D%97%E9%80%9A%E5%BF%97_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7031)）

#### 紀瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%80%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%80%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%80%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%80%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%80%E7%9E%BB)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk3.html)
  （@いつか書きたい『三国志』）

#### 賀循
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%80%E5%BE%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%80%E5%BE%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%80%E5%BE%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%80%E5%BE%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%80%E5%BE%AA)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n36/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk6.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「賀循儒宗」（第77句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/62)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/187)）に「賀生達禮之宗」とある。

#### 楊方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E6%96%B9)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n54/mode/2up)
* [書誌のみ]
  [楊方詩訳注 : 并『晋書』本伝訳](https://cir.nii.ac.jp/crid/1571135651788146176)
  （長谷川 滋成）
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk10.html)
  （@いつか書きたい『三国志』）

#### 薛兼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%96%9B%E5%85%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%96%9B%E5%85%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%96%9B%E5%85%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%96%9B%E5%85%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%96%9B%E5%85%BC)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n56/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk10.html)
  （@いつか書きたい『三国志』）


### 巻69 列伝第39
#### 劉隗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%9A%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%9A%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%9A%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%9A%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%9A%97)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉波
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%B3%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%B3%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%B3%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%B3%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%B3%A2)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n10/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉訥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%A8%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%A8%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%A8%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%A8%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%A8%A5)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n16/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉疇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%96%87)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n16/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉劭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%8A%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%8A%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%8A%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%8A%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%8A%AD)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n18/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉黄老
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%BB%84%E8%80%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%BB%84%E8%80%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%BB%84%E8%80%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%BB%84%E8%80%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%BB%84%E8%80%81)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n18/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 刁協
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%88%81%E5%8D%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%88%81%E5%8D%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%88%81%E5%8D%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%88%81%E5%8D%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%88%81%E5%8D%94)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n18/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r4.html)
  （@いつか書きたい『三国志』）

#### 刁彝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%88%81%E5%BD%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%88%81%E5%BD%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%88%81%E5%BD%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%88%81%E5%BD%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%88%81%E5%BD%9D)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n24/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r4.html)
  （@いつか書きたい『三国志』）

#### 刁逵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%88%81%E9%80%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%88%81%E9%80%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%88%81%E9%80%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%88%81%E9%80%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%88%81%E9%80%B5)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n26/mode/2up)

#### 戴若思（戴淵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E6%B7%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E6%B7%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E6%B7%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E6%B7%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E6%B7%B5)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r5.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  空海『三教指帰』に[「戴淵志を變じて將軍の位に登り」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/11)とある
  （[注48](https://dl.ndl.go.jp/info:ndljp/pid/1040599/36)）。

#### 戴邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E9%82%88)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n32/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r6.html)
  （@いつか書きたい『三国志』）

#### 周顗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%A1%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%A1%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%A1%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%A1%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%A1%97)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n36/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「周嵩狼抗」（第13句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/23)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r7.html)
  （@いつか書きたい『三国志』）

#### 周閔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%96%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%96%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%96%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%96%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%96%94)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n48/mode/2up)


### 巻70 列伝第40
#### 応詹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BF%9C%E8%A9%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BF%9C%E8%A9%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BF%9C%E8%A9%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BF%9C%E8%A9%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BF%9C%E8%A9%B9)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n52/mode/2up)

#### 甘卓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%94%98%E5%8D%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%94%98%E5%8D%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%94%98%E5%8D%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%94%98%E5%8D%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%94%98%E5%8D%93)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n64/mode/2up)

#### 鄧騫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E9%A8%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E9%A8%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E9%A8%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E9%A8%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E9%A8%AB)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n74/mode/2up)

#### 卞壼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9E%E5%A3%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9E%E5%A3%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9E%E5%A3%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9E%E5%A3%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9E%E5%A3%BC)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n74/mode/2up)

#### 卞敦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9E%E6%95%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9E%E6%95%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9E%E6%95%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9E%E6%95%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9E%E6%95%A6)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n92/mode/2up)

#### 劉超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n94/mode/2up)

#### 鍾雅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%8D%BE%E9%9B%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%8D%BE%E9%9B%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%8D%BE%E9%9B%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%8D%BE%E9%9B%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%8D%BE%E9%9B%85)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n100/mode/2up)


