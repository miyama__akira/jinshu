### 巻71 列伝第41
#### 孫恵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%81%B5)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n2/mode/2up)

#### 熊遠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%86%8A%E9%81%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%86%8A%E9%81%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%86%8A%E9%81%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%86%8A%E9%81%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%86%8A%E9%81%A0)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n10/mode/2up)

#### 王鑑（王鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n20/mode/2up)

#### 陳頵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E9%A0%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E9%A0%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E9%A0%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E9%A0%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E9%A0%B5)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n26/mode/2up)

#### 高崧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E5%B4%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E5%B4%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E5%B4%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E5%B4%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E5%B4%A7)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n32/mode/2up)


### 巻72 列伝第42
#### 郭璞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%92%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%92%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%92%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%92%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%92%9E)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n38/mode/2up)
* [PDFあり]
  [六朝文人伝 : 「晋書」郭璞伝](https://dl.ndl.go.jp/info:ndljp/pid/10504359)
  （長谷川 滋成）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、郭璞伝で言及している
  [「江賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/230)がある。
* 干宝『搜神記』
  [巻3](https://ctext.org/wiki.pl?if=gb&chapter=412733#p14)・
  [巻4](https://ctext.org/wiki.pl?if=gb&chapter=879820#p13)
* [劉克莊の「郭璞墓」](https://zh.wikisource.org/wiki/%E5%BE%8C%E6%9D%91%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E9%83%AD%E7%92%9E%E5%A2%93)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/187)）に「郭璞誓以淮水」とある。
* 『關帝靈籤』第八十籤に「郭璞召陰葬地」とある。
    * [画像版](https://commons.wikimedia.org/w/index.php?title=File:%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=82)のみ……[テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/80)は「陶侃卜牛眠」という違う内容。

#### 葛洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%9B%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%9B%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%9B%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%9B%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%9B%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n70/mode/2up)
* [墨浪子『西湖佳話』巻1「葛嶺仙跡」](https://zh.wikisource.org/wiki/%E8%A5%BF%E6%B9%96%E4%BD%B3%E8%A9%B1/01)


### 巻73 列伝第43
#### 庾亮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E4%BA%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E4%BA%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E4%BA%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E4%BA%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E4%BA%AE)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n2/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、庾亮伝に引かれた
  [「讓中書令表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/419)がある。
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p14)
* 李白「[陪宋中丞武昌夜飲懷古](https://zh.wikisource.org/wiki/%E9%99%AA%E5%AE%8B%E4%B8%AD%E4%B8%9E%E6%AD%A6%E6%98%8C%E5%A4%9C%E9%A3%B2%E6%87%B7%E5%8F%A4)」
  （[『国訳漢文大成　続　文学部』第10冊](https://dl.ndl.go.jp/info:ndljp/pid/1139993/81)）
* [『隋書』巻15音楽志下](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B715)
  （文康楽のところ）
* [『顔氏家訓』書証第十七](https://ctext.org/yan-shi-jia-xun/shu-zheng/zh#n92153)
  ……同じく「猶文康象庾亮耳」の記述あり。
* [ブログ]
  [デク](https://t-s.hatenablog.com/entry/20110318/1300374101)
  （@てぃーえすのメモ帳）
  ……『顔氏家訓』の上記箇所を含む段落について。
* [『本朝文粋』巻14](https://dl.ndl.go.jp/info:ndljp/pid/2544432/37)の[菅三品（菅原文時）「為謙徳公修報恩善願文」](https://dl.ndl.go.jp/info:ndljp/pid/2544432/54)に、庾亮を指す「[南樓翫月之人、月與秋期、而身何去](https://dl.ndl.go.jp/info:ndljp/pid/2544432/55)」という部分がある（この直前の部分が[『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-11)に引用されているため、[『十訓抄詳解』の注](https://dl.ndl.go.jp/info:ndljp/pid/945602/34)で解説されている）。
* 唐の謝観の「白賦」に「夜登庾亮之樓，月明千里。」という部分があり、ここが文天祥の[「五色賦記」](https://zh.wikisource.org/wiki/%E6%96%87%E5%B1%B1%E5%85%88%E7%94%9F%E6%96%87%E9%9B%86/%E5%8D%B709#%E4%BA%94%E8%89%B2%E8%B3%A6%E8%A8%98)、謝肇淛の[『五雜俎』](https://zh.wikisource.org/wiki/%E4%BA%94%E9%9B%9C%E4%BF%8E/%E5%8D%B716)、蔣一葵の[『堯山堂偶雋』](https://zh.wikisource.org/wiki/%E5%A0%AF%E5%B1%B1%E5%A0%82%E5%81%B6%E9%9B%8B/%E5%8D%B7%E4%B8%89)に引かれている。

#### 庾彬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%BD%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%BD%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%BD%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%BD%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%BD%AC)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n24/mode/2up)

#### 庾羲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%BE%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%BE%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%BE%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%BE%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%BE%B2)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n24/mode/2up)

#### 庾龢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%BE%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%BE%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%BE%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%BE%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%BE%A2)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n26/mode/2up)

#### 庾懌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%87%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%87%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%87%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%87%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%87%8C)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n28/mode/2up)

#### 庾冰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%86%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%86%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%86%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%86%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%86%B0)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n30/mode/2up)

#### 庾希
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%B8%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%B8%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%B8%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%B8%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%B8%8C)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n38/mode/2up)
  ……途中からページ抜けの箇所に当たっている。

#### 庾襲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E8%A5%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E8%A5%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E8%A5%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E8%A5%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E8%A5%B2)

#### 庾友
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%8F%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%8F%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%8F%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%8F%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%8F%8B)

#### 庾蘊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E8%98%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E8%98%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E8%98%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E8%98%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E8%98%8A)

#### 庾倩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%80%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%80%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%80%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%80%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%80%A9)

#### 庾邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%82%88)

#### 庾柔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%9F%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%9F%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%9F%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%9F%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%9F%94)

#### 庾條
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%A2%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%A2%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%A2%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%A2%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%A2%9D)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n40/mode/2up)
  ……末尾以外はページ抜けの箇所に当たっている。

#### 庾翼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%BF%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%BF%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%BF%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%BF%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%BF%BC)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n40/mode/2up)
* [ブログ]
  [キャプテン翼 北伐篇 『晋書 庾翼伝（抄訳）』 第１回](http://tombstonedriver.blog.fc2.com/blog-entry-130.html)〜
  [キャプテン翼 北伐篇 『晋書 庾翼伝（抄訳）』 最終回](http://tombstonedriver.blog.fc2.com/blog-entry-138.html)（第８回）
  （@大司馬府 作戦日誌）

#### 庾方之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%96%B9%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n50/mode/2up)

#### 庾爰之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%88%B0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n50/mode/2up)


### 巻74 列伝第44
#### 桓彝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%BD%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%BD%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%BD%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%BD%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%BD%9D)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n54/mode/2up)
* 高啓の[「桓簡公廟」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B712#%E6%A1%93%E7%B0%A1%E5%85%AC%E5%BB%9F)（[『国訳漢文大成. 続 文学部第83冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140864/35)も参照）

#### 桓雲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E9%9B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E9%9B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E9%9B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E9%9B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E9%9B%B2)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n58/mode/2up)

#### 桓豁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E8%B1%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E8%B1%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E8%B1%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E8%B1%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E8%B1%81)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n60/mode/2up)

#### 桓石虔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E8%99%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E8%99%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E8%99%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E8%99%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E8%99%94)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n64/mode/2up)

#### 桓振
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%8C%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%8C%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%8C%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%8C%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%8C%AF)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n68/mode/2up)

#### 桓石秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n70/mode/2up)

#### 桓石民
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E6%B0%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n72/mode/2up)

#### 桓石生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓石綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓石康
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E5%BA%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓秘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%A7%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%A7%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%A7%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%A7%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%A7%98)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓沖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n76/mode/2up)

#### 桓嗣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%97%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%97%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%97%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%97%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%97%A3)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n88/mode/2up)

#### 桓胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n90/mode/2up)

#### 桓謙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E8%AC%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E8%AC%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E8%AC%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E8%AC%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E8%AC%99)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n90/mode/2up)

#### 桓修
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E4%BF%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E4%BF%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E4%BF%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E4%BF%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E4%BF%AE)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n92/mode/2up)

#### 徐寧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E5%AF%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E5%AF%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E5%AF%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E5%AF%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E5%AF%A7)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n94/mode/2up)


### 巻75 列伝第45
#### 王湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B9%9B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n2/mode/2up)

#### 王承
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%89%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%89%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%89%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%89%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%89%BF)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n6/mode/2up)

#### 王述
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%BF%B0)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n8/mode/2up)

#### 王坦之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%9D%A6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n16/mode/2up)
* [蘇軾「蝶戀花」](https://zh.wikisource.org/wiki/%E8%9D%B6%E6%88%80%E8%8A%B1%EF%BC%88%E5%90%8C%E5%AE%89%E7%94%9F%E6%97%A5%E6%94%BE%E9%AD%9A%EF%BC%8C%E5%8F%96%E9%87%91%E5%85%89%E6%98%8E%E7%B6%93%E6%95%91%E9%AD%9A%E4%BA%8B%EF%BC%89)の「膝上王文度」という比喩は、王坦之が成人してもまだ父の王述が王坦之を膝の上にだっこしていた（！）という話による。
  （[林雪云「蘇軾の妻妾に対する観念」](http://doi.org/10.24729/00004371)に、この詞の訓読あり）
* [書籍 (NDL)]
  [『先哲像伝 近世畸人伝 百家琦行伝』](https://dl.ndl.go.jp/pid/1186674/1/25)
  （得斎原義胤・他）
  ……林讀耕齋が幼児の頃、讀耕齋をだっこした父の林羅山が「是膝上王文度也」と言っている。

#### 王愷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%84%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%84%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%84%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%84%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%84%B7)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n28/mode/2up)

#### 王愉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%84%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%84%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%84%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%84%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%84%89)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n28/mode/2up)

#### 王国宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%9B%BD%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n30/mode/2up)

#### 王忱
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BF%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BF%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BF%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BF%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BF%B1)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n36/mode/2up)

#### 王綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n38/mode/2up)

#### 王嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n40/mode/2up)

#### 袁悦之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E6%82%A6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n42/mode/2up)

#### 祖台之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E5%8F%B0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n42/mode/2up)
* [PDFあり]
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)
  （中嶋 長文・伊藤 令子・ 平田 昌司）
  ……p. 326〜 に祖台之の「志怪」あり。
* 祖台之の「志怪」には [Wikisource 版](https://zh.wikisource.org/wiki/%E7%A5%96%E5%8F%B0%E4%B9%8B%E5%BF%97%E6%80%AA)もある。

#### 荀崧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%B4%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%B4%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%B4%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%B4%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%B4%A7)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n42/mode/2up)

#### 荀蕤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E8%95%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E8%95%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E8%95%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E8%95%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E8%95%A4)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n54/mode/2up)

#### 荀羨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E7%BE%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E7%BE%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E7%BE%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E7%BE%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E7%BE%A8)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n54/mode/2up)

#### 范汪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E6%B1%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E6%B1%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E6%B1%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E6%B1%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E6%B1%AA)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n58/mode/2up)

#### 范寧（范甯）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E7%94%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E7%94%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E7%94%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E7%94%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E7%94%AF)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n66/mode/2up)

#### 范堅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%A0%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%A0%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%A0%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%A0%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%A0%85)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n78/mode/2up)

#### 劉惔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%83%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%83%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%83%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%83%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%83%94)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n80/mode/2up)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「前郡尹溫太真劉真長，或功銘鼎彝，或德標素尚」とある。「真長」は劉惔の字。

#### 張憑
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%86%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%86%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%86%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%86%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%86%91)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n86/mode/2up)

#### 韓伯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E4%BC%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E4%BC%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E4%BC%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E4%BC%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E4%BC%AF)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n88/mode/2up)
* 干宝[『搜神記』巻5](https://ctext.org/wiki.pl?if=gb&chapter=503309#p4)
  （韓伯の子が出てくる）


### 巻76 列伝第46
#### 王舒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%88%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%88%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%88%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%88%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%88%92)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n2/mode/2up)

#### 王允之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%85%81%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n8/mode/2up)

#### 王廙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BB%99)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n10/mode/2up)

#### 王彬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BD%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BD%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BD%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BD%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BD%AC)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n18/mode/2up)

#### 王彪之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BD%AA%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n22/mode/2up)

#### 王棱
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%A3%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%A3%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%A3%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%A3%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%A3%B1)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n36/mode/2up)

#### 王侃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E4%BE%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E4%BE%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E4%BE%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E4%BE%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E4%BE%83)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n36/mode/2up)

#### 虞潭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%BD%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%BD%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%BD%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%BD%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%BD%AD)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n36/mode/2up)

#### 虞嘯父
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E5%98%AF%E7%88%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E5%98%AF%E7%88%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E5%98%AF%E7%88%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E5%98%AF%E7%88%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E5%98%AF%E7%88%B6)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n42/mode/2up)

#### 虞𩦎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%F0%A9%A6%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%F0%A9%A6%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%F0%A9%A6%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%F0%A9%A6%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%F0%A9%A6%8E)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n44/mode/2up)

#### 顧衆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E8%A1%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E8%A1%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E8%A1%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E8%A1%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E8%A1%86)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n44/mode/2up)

#### 張闓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%97%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%97%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%97%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%97%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%97%93)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n50/mode/2up)


### 巻77 列伝第47
#### 陸曄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E6%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E6%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E6%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E6%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E6%9B%84)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n56/mode/2up)

#### 陸玩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E7%8E%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E7%8E%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E7%8E%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E7%8E%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E7%8E%A9)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n58/mode/2up)
* [姚承緒『吳趨訪古錄』の「靈巖寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E9%9D%88%E5%B7%96%E5%AF%BA)
  ……序文によると、陸玩が寄進した土地に建てた寺院を題材にした詩らしい。

#### 陸納
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E7%B4%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E7%B4%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E7%B4%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E7%B4%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E7%B4%8D)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n64/mode/2up)

#### 何充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n68/mode/2up)

#### 褚翜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E7%BF%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E7%BF%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E7%BF%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E7%BF%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E7%BF%9C)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n78/mode/2up)

#### 蔡謨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%94%A1%E8%AC%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%94%A1%E8%AC%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%94%A1%E8%AC%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%94%A1%E8%AC%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%94%A1%E8%AC%A8)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n82/mode/2up)
* [ブログ]
  [餓狼おどる海　『晋書 蔡謨伝』 第一回](http://tombstonedriver.blog.fc2.com/blog-entry-114.html)〜
  [餓狼おどる海　『晋書 蔡謨伝』 最終回](http://tombstonedriver.blog.fc2.com/blog-entry-128.html) （第十一回）
  （@大司馬府 作戦日誌）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/187)）に「蔡公儒林之亞」とある。

#### 諸葛恢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AB%B8%E8%91%9B%E6%81%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n104/mode/2up)

#### 殷浩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E6%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E6%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E6%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E6%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E6%B5%A9)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n108/mode/2up)
* [ブログ]
  [永和十年（354年） 桓温の殷浩を断罪する上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-96.html)
  （@大司馬府 作戦日誌）

#### 顧悦之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E6%82%A6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n122/mode/2up)

#### 蔡裔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%94%A1%E8%A3%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%94%A1%E8%A3%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%94%A1%E8%A3%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%94%A1%E8%A3%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%94%A1%E8%A3%94)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n122/mode/2up)


### 巻78 列伝第48
#### 孔愉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E6%84%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E6%84%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E6%84%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E6%84%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E6%84%89)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n1/mode/2up)
* 干宝[『搜神記』巻20](https://ctext.org/wiki.pl?if=gb&chapter=629401#p7)
* [『十訓抄』1の4](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-04)

#### 孔汪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E6%B1%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E6%B1%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E6%B1%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E6%B1%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E6%B1%AA)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n7/mode/2up)

#### 孔安国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%AE%89%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n7/mode/2up)

#### 孔祗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E7%A5%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E7%A5%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E7%A5%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E7%A5%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E7%A5%97)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n9/mode/2up)

#### 孔坦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%9D%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%9D%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%9D%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%9D%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%9D%A6)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n9/mode/2up)

#### 孔厳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%8E%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%8E%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%8E%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%8E%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%8E%B3)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n21/mode/2up)

#### 孔群
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E7%BE%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E7%BE%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E7%BE%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E7%BE%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E7%BE%A4)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n25/mode/2up)

#### 孔沉（孔沈）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n27/mode/2up)

#### 孔廞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%BB%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%BB%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%BB%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%BB%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%BB%9E)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n29/mode/2up)

#### 丁潭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%81%E6%BD%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%81%E6%BD%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%81%E6%BD%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%81%E6%BD%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%81%E6%BD%AD)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n29/mode/2up)

#### 張茂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8C%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8C%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8C%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8C%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8C%82)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n35/mode/2up)

#### 陶回
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E5%9B%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E5%9B%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E5%9B%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E5%9B%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E5%9B%9E)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n35/mode/2up)


### 巻79 列伝第49
#### 謝尚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E5%B0%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E5%B0%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E5%B0%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E5%B0%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E5%B0%9A)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n41/mode/2up)
* [PDFあり]
  [西晋謝尚伝--『晋書』謝尚伝訳注](https://dl.ndl.go.jp/info:ndljp/pid/10504663)
  （小松 英生）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「謝尚鴝鵒」（第96句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/72)
* 干宝[『搜神記』巻2](https://ctext.org/wiki.pl?if=gb&chapter=95966#p18)
* [李白「夜泊牛渚懷古」](https://zh.wikisource.org/wiki/%E5%A4%9C%E6%B3%8A%E7%89%9B%E6%B8%9A%E6%87%B7%E5%8F%A4)
  （[『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/88)）
* [崔塗「牛渚夜泊」](https://zh.wikisource.org/wiki/%E7%89%9B%E6%B8%9A%E5%A4%9C%E6%B3%8A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* [李白「對雪醉後贈王歷陽」](https://zh.wikisource.org/wiki/%E5%B0%8D%E9%9B%AA%E9%86%89%E5%BE%8C%E8%B4%88%E7%8E%8B%E6%AD%B7%E9%99%BD)（[『国訳漢文大成』続 文学部第6冊](https://dl.ndl.go.jp/pid/1139956/1/59)）に「謝尚自能鴝鵒舞」とある。

#### 謝安
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E5%AE%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E5%AE%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E5%AE%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E5%AE%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E5%AE%89)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n49/mode/2up)
* [PDFあり]
  [魏晉石刻資料選注( 一九 謝府君神道闕 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……謝府君こと謝纘は謝安の曽祖父。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「謝安高潔」（第7句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/20)
* [『冊府元龜』卷三百十](https://zh.wikisource.org/wiki/%E5%86%8A%E5%BA%9C%E5%85%83%E9%BE%9C_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B70310)と[『言行龜鑒』卷五・出處門](https://zh.wikisource.org/wiki/%E8%A8%80%E8%A1%8C%E9%BE%9C%E9%91%92/%E5%8D%B75)で馮道を謝安と比べている。
* [ブログ]
  [古文詩詞常用的典故：東山再起](https://kknews.cc/culture/vn8m2qy.html)
  （@每日頭條）
  ……「東山再起」やそれに類する表現を使って謝安の故事を織り込んだ詩文がいろいろ挙げられている。ネット上で読めるものへのリンクを以下に貼る。
    * [文康『兒女英雄傳』第三九回](https://zh.wikisource.org/wiki/%E5%85%92%E5%A5%B3%E8%8B%B1%E9%9B%84%E5%82%B3/%E7%AC%AC%E4%B8%89%E5%8D%81%E4%B9%9D%E5%9B%9E)
    * [杜甫「暮秋枉裴道州手札率爾遣興寄近呈蘇渙侍御」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7223#%E6%9A%AE%E7%A7%8B%E6%9E%89%E8%A3%B4%E9%81%93%E5%B7%9E%E6%89%8B%E5%8A%84%EF%BC%8C%E7%8E%87%E7%88%BE%E9%81%A3%E8%88%88%EF%BC%8C%E5%AF%84%E8%BF%91%E5%91%88%E8%98%87%E6%B8%99%E4%BE%8D%E7%A6%A6)
        * [『続国訳漢文大成 文学部 第6巻の下（杜少陵詩集 下巻の下）』](https://dl.ndl.go.jp/info:ndljp/pid/1239692/392)
    * [溫庭筠「題裴晉公林亭」](https://zh.wikisource.org/wiki/%E9%A1%8C%E8%A3%B4%E6%99%89%E5%85%AC%E6%9E%97%E4%BA%AD)
    * [李白「送裴十八圖南歸嵩山」](https://zh.wikisource.org/wiki/%E9%80%81%E8%A3%B4%E5%8D%81%E5%85%AB%E5%9C%96%E5%8D%97%E6%AD%B8%E5%B5%A9%E5%B1%B1)
        * [『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/58)
    * [李白「梁園吟」](https://zh.wikisource.org/wiki/%E6%A2%81%E5%9C%92%E5%90%9F)
        * [『続国訳漢文大成 文学部第4冊（李太白詩集 上の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139937/24)
    * [王惲「望海潮・為故相雲叟公壽」](https://zh.wikisource.org/wiki/%E7%A7%8B%E6%BE%97%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7074#%E4%BA%8C)
    * [鄭廷玉『忍字記』第四折](https://zh.wikisource.org/wiki/%E5%B8%83%E8%A2%8B%E5%92%8C%E5%B0%9A%E5%BF%8D%E5%AD%97%E8%A8%98#%E7%AC%AC%E5%9B%9B%E6%8A%98)
    * [李白「送梁四歸東平」](https://zh.wikisource.org/wiki/%E9%80%81%E6%A2%81%E5%9B%9B%E6%AD%B8%E6%9D%B1%E5%B9%B3)
        * [『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/107)
    * [謝靈運「還舊園作見顏范二中書」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E9%82%84%E8%88%8A%E5%9C%92%E4%BD%9C%E8%A6%8B%E9%A1%8F%E8%8C%83%E4%BA%8C%E4%B8%AD%E6%9B%B8)
        * [『国訳漢文大成 第3巻（文選 中巻）』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/162)
    * [王維「戲贈張五弟諲」](https://zh.wikisource.org/wiki/%E6%88%B2%E8%B4%88%E5%BC%B5%E4%BA%94%E5%BC%9F%E8%AB%B2)
        * [『続国訳漢文大成 文学部第70冊（王右丞集の一）』](https://dl.ndl.go.jp/info:ndljp/pid/1140684/53)
    * [王維「與工部李侍郎書」](https://zh.wikisource.org/wiki/%E8%88%87%E5%B7%A5%E9%83%A8%E6%9D%8E%E4%BE%8D%E9%83%8E%E6%9B%B8)
* 白居易「裴侍中晉公以集賢林亭即事詩二十六韻見贈猥蒙徴和才拙詞繁廣為五百言以伸酬獻」
    * [『白氏長慶集』（四庫全書本）巻29](https://zh.wikisource.org/wiki/%E7%99%BD%E6%B0%8F%E9%95%B7%E6%85%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B729)
    * [『全唐詩』巻452](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7452#%E8%A3%B4%E4%BE%8D%E4%B8%AD%E6%99%89%E5%85%AC%E4%BB%A5%E9%9B%86%E8%B3%A2%E6%9E%97%E4%BA%AD%E5%8D%B3%E4%BA%8B%E8%A9%A9%E4%B8%89%E5%8D%81%E5%85%AD%E9%9F%BB%E8%A6%8B%E8%B4%88%E7%8C%A5%E8%92%99%E5%BE%B5%E5%92%8C%E6%89%8D%E6%8B%99%E8%A9%9E%E7%B9%81%E8%BC%92%E5%BB%A3%E7%82%BA%E4%BA%94%E7%99%BE%E8%A8%80%E4%BB%A5%E4%BC%B8%E9%85%AC%E7%8D%BB)
    * [『続国訳漢文大成 文学部第42冊（白楽天詩集 三の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1140376/31)
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
* [姚承緒『吳趨訪古錄』の「弇山園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E5%BC%87%E5%B1%B1%E5%9C%92)
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「謝安論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/23)
  （侯方域）
* [李白「登金陵冶城西北謝安墩」](https://zh.wikisource.org/wiki/%E7%99%BB%E9%87%91%E9%99%B5%E5%86%B6%E5%9F%8E%E8%A5%BF%E5%8C%97%E8%AC%9D%E5%AE%89%E5%A2%A9)
    * [『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/14)
* [唐の楊綰](https://zh.wikipedia.org/zh-tw/%E6%A5%8A%E7%B6%B0)が楊震（・邴吉）・山濤・謝安になぞらえられている。
	* [『旧唐書』巻119](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7119#%E6%A5%8A%E7%B6%B0)
	* [『新唐書』巻142](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7142#%E6%A5%8A%E7%B6%B0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* 杜甫「入衡州」に「謝安乘興長」という句がある。
    * [『全唐詩』卷223](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7223#%E5%85%A5%E8%A1%A1%E5%B7%9E)
    * [『九家集注杜詩』卷16](https://zh.wikisource.org/wiki/%E4%B9%9D%E5%AE%B6%E9%9B%86%E6%B3%A8%E6%9D%9C%E8%A9%A9_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B716#%E5%85%A5%E8%A1%A1%E5%B7%9E)
    * [『国訳漢文大成』続 文学部第24の下冊](https://dl.ndl.go.jp/pid/1140152/1/128)
* 王讜の[『唐語林』巻5](https://zh.wikisource.org/wiki/%E5%94%90%E8%AA%9E%E6%9E%97/%E5%8D%B7%E4%BA%94)の郗昂と源乾曜のやりとりは、[『世説新語』雅量篇27](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E9%9B%85%E9%87%8F)の「入幕の賓」をネタにしており、直接的には郗超と謝安に言及している。
* [李華「謝文靖」](https://zh.wikisource.org/wiki/%E8%AC%9D%E6%96%87%E9%9D%96)……「文靖」は謝安の諡。苻堅や謝玄らにも言及している。
* [王丘「詠史」](https://zh.wikisource.org/wiki/%E8%A9%A0%E5%8F%B2_(%E7%8E%8B%E4%B8%98))に「偉哉謝安石，攜妓入東山。」とある。
* 謝安の字の「安石」と同名の人たち
    * 韋安石
        * [『旧唐書』巻92](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B792#%E9%9F%8B%E5%AE%89%E7%9F%B3)
        * [『新唐書』巻122](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7122#%E9%9F%8B%E5%AE%89%E7%9F%B3)
    * 王安石
        * [『宋史』巻327](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7327#%E7%8E%8B%E5%AE%89%E7%9F%B3)
* [『金瓶梅』第49回](https://zh.wikisource.org/wiki/%E9%87%91%E7%93%B6%E6%A2%85/%E7%AC%AC49%E5%9B%9E)に「蔡御史看見，欲進不能，欲退不舍。便說道：「四泉，你如何這等愛厚？恐使不得。」西門慶笑道：「與昔日東山之游，又何異乎？」蔡御史道：「恐我不如安石之才，而君有王右軍之高致矣。」」という会話があり、「東山之游」と「安石之才」が謝安への言及、「王右軍之高致」が王羲之への言及である。

#### 羊曇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E6%9B%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E6%9B%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E6%9B%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E6%9B%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E6%9B%87)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝瑤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%91%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%91%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%91%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%91%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%91%A4)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝琰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%90%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%90%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%90%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%90%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%90%B0)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/394)）に「謝琰功高而後至」とある。

#### 謝混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n71/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n73/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝玄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%8E%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%8E%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%8E%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%8E%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%8E%84)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n75/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* [『文選』所収の謝霊運「述祖德詩」二首](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B719#%E8%BF%B0%E7%A5%96%E5%BE%B7%E8%A9%A9%E4%BA%8C%E9%A6%96)（[『国訳漢文大成』第三卷](https://dl.ndl.go.jp/pid/1912796/1/25)）は謝霊運の祖父である謝玄について述べたもの。
* [李華「謝文靖」](https://zh.wikisource.org/wiki/%E8%AC%9D%E6%96%87%E9%9D%96)……「文靖」は謝安の諡。苻堅や謝玄らにも言及している。

#### 何謙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E8%AC%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E8%AC%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E8%AC%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E8%AC%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E8%AC%99)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 戴逯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E9%80%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E9%80%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E9%80%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E9%80%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E9%80%AF)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝万
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E4%B8%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E4%B8%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E4%B8%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E4%B8%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E4%B8%87)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝韶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%9F%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%9F%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%9F%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%9F%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%9F%B6)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n95/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝朗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%9C%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%9C%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%9C%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%9C%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%9C%97)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n95/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝重
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%87%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%87%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%87%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%87%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%87%8D)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n97/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝絢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%B5%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%B5%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%B5%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%B5%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%B5%A2)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n97/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝石
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%9F%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%9F%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%9F%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%9F%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%9F%B3)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n97/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%82%88)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n101/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

### 巻80 列伝第50
#### 王羲之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%BE%B2%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n105/mode/2up)
* [書誌のみ・なぜか2と3のみ]
  [晋書王羲之伝訳注](https://cir.nii.ac.jp/all?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B%E4%BC%9D%E8%A8%B3%E6%B3%A8&count=20&sortorder=0)
  （杉村 邦彦）
* [書誌のみ]
  [六朝文人傳 : 王羲之(『晉書』卷八十)](https://cir.nii.ac.jp/crid/1520290885032417664)
  （森野 繁夫）
* 李白「[王右軍](https://zh.wikisource.org/wiki/%E7%8E%8B%E5%8F%B3%E8%BB%8D)」
  （[『国訳漢文大成　続　文学部』第10冊](https://dl.ndl.go.jp/info:ndljp/pid/1139993/66)）
* 蘇軾「[題王逸少帖](https://zh.wikisource.org/wiki/%E9%A1%8C%E7%8E%8B%E9%80%B8%E5%B0%91%E5%B8%96)」
  （[『国訳漢文大成　続　文学部』第61冊](https://dl.ndl.go.jp/info:ndljp/pid/1140559/34)）
* 王羲之の書を[検索](https://theme.npm.edu.tw/opendata/DigitImageSets.aspx?Key=%e7%8e%8b%e7%be%b2%e4%b9%8b%5E12%5E13)
  （@故宮Open Data專區）
* [書籍 (NDL)]
  空海『三教指帰』に[「鍾張王歐も毫を擲って耻を懷かん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注108](https://dl.ndl.go.jp/info:ndljp/pid/1040599/41)）。
* [李白「登金陵冶城西北謝安墩」](https://zh.wikisource.org/wiki/%E7%99%BB%E9%87%91%E9%99%B5%E5%86%B6%E5%9F%8E%E8%A5%BF%E5%8C%97%E8%AC%9D%E5%AE%89%E5%A2%A9)
    * [『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/14)
* [王師乾「王右軍祠堂碑」](https://zh.wikisource.org/wiki/%E7%8E%8B%E5%8F%B3%E8%BB%8D%E7%A5%A0%E5%A0%82%E7%A2%91)
* [『金瓶梅』第49回](https://zh.wikisource.org/wiki/%E9%87%91%E7%93%B6%E6%A2%85/%E7%AC%AC49%E5%9B%9E)に「蔡御史看見，欲進不能，欲退不舍。便說道：「四泉，你如何這等愛厚？恐使不得。」西門慶笑道：「與昔日東山之游，又何異乎？」蔡御史道：「恐我不如安石之才，而君有王右軍之高致矣。」」という会話があり、「東山之游」と「安石之才」が謝安への言及、「王右軍之高致」が王羲之への言及である。
* [錢選「王羲之觀鵝圖」](https://www.metmuseum.org/ja/art/collection/search/40081)

#### 王凝之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%87%9D%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n131/mode/2up)

#### 王徽之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BE%BD%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n133/mode/2up)
* [清少納言『枕草子』の「五月ばかりに月もなくいとくらき夜……」の段](https://ja.wikisource.org/wiki/%E6%9E%95%E8%8D%89%E7%B4%99_(%E5%9C%8B%E6%96%87%E5%A4%A7%E8%A7%80))の「くれたけ」と「このきみ」は王徽之の逸話を踏まえている。
* [服部南郭の「聞笛」](https://dl.ndl.go.jp/info:ndljp/pid/2559343/40)にある「三弄都作断腸声」の「三弄」は、桓伊と王徽之の故事に基づく。
    * [ブログ]
      [漢文日録２６．３．１５](http://www.mugyu.biz-web.jp/nikki.26.03.15.htm)
      （@肝冷斎日録）
    * 「江戸中期の儒者・漢詩人服部南郭の漢詩に「笛を聞く」という作品がある。この作の最後の句は「三弄都て断腸の声と為る」（さんろうすべてだんちょうのこえとなる）となっているが、句頭の「三弄」という言葉は「「三路が笛・三弄が笛」の物語り」といわれる説話を示唆しているらしい（『大言海』による）。この説話の内容を知りたいが何か資料はないか。」という[レファレンス事例](https://crd.ndl.go.jp/reference/detail?page=ref_view&id=1000284888)
      （@レファレンス協同データベース）
* [李白「對雪醉後贈王歷陽」](https://zh.wikisource.org/wiki/%E5%B0%8D%E9%9B%AA%E9%86%89%E5%BE%8C%E8%B4%88%E7%8E%8B%E6%AD%B7%E9%99%BD)（[『国訳漢文大成』続 文学部第6冊](https://dl.ndl.go.jp/pid/1139956/1/59)）に「子猷聞風動窗竹」とある。子猷は王徽之の字。

#### 王楨之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%A5%A8%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n137/mode/2up)

#### 王操之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%93%8D%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n137/mode/2up)

#### 王献之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8C%AE%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n137/mode/2up)
* 王献之の書を[検索](https://theme.npm.edu.tw/opendata/DigitImageSets.aspx?Key=%e7%8e%8b%e7%8d%bb%e4%b9%8b)
  （@故宮Open Data專區）
* [『樂府詩集』45卷「桃葉歌」](https://zh.wikisource.org/wiki/%E6%A8%82%E5%BA%9C%E8%A9%A9%E9%9B%86/045%E5%8D%B7#%E6%A1%83%E8%91%89%E6%AD%8C%E4%B8%89%E9%A6%96)
* [書籍 (NDL)]
  空海『三教指帰』に[「鍾張王歐も毫を擲って耻を懷かん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注108](https://dl.ndl.go.jp/info:ndljp/pid/1040599/41)）。
* [姚承緒『吳趨訪古錄』の「辟疆園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E8%BE%9F%E7%96%86%E5%9C%92)
  ……王献之と絡めて[『世說新語』簡傲篇17](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E7%B0%A1%E5%82%B2)で語られる顧辟疆の庭園を題材にした詩。
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「王献之論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/27)
  （呉成佐）

#### 許邁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A8%B1%E9%82%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A8%B1%E9%82%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A8%B1%E9%82%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A8%B1%E9%82%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A8%B1%E9%82%81)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n145/mode/2up)


