### 巻81 列伝第51
#### 王遜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%81%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%81%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%81%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%81%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%81%9C)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n1/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「捐駒」とあり、これは注によれば王遜の行為を指す。

#### 蔡豹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%94%A1%E8%B1%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%94%A1%E8%B1%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%94%A1%E8%B1%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%94%A1%E8%B1%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%94%A1%E8%B1%B9)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n7/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B8%DE%BD%BD%B0%EC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 羊鑑（羊鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n11/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B8%DE%BD%BD%B0%EC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 劉胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n13/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 桓宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n17/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 桓伊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E4%BC%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E4%BC%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E4%BC%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E4%BC%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E4%BC%8A)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n23/mode/2up)
* [服部南郭の「聞笛」](https://dl.ndl.go.jp/info:ndljp/pid/2559343/40)にある「三弄都作断腸声」の「三弄」は、桓伊と王徽之の故事に基づく。
    * [ブログ]
      [漢文日録２６．３．１５](http://www.mugyu.biz-web.jp/nikki.26.03.15.htm)
      （@肝冷斎日録）
    * 「江戸中期の儒者・漢詩人服部南郭の漢詩に「笛を聞く」という作品がある。この作の最後の句は「三弄都て断腸の声と為る」（さんろうすべてだんちょうのこえとなる）となっているが、句頭の「三弄」という言葉は「「三路が笛・三弄が笛」の物語り」といわれる説話を示唆しているらしい（『大言海』による）。この説話の内容を知りたいが何か資料はないか。」という[レファレンス事例](https://crd.ndl.go.jp/reference/detail?page=ref_view&id=1000284888)
      （@レファレンス協同データベース）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 朱伺
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9C%B1%E4%BC%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9C%B1%E4%BC%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9C%B1%E4%BC%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9C%B1%E4%BC%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9C%B1%E4%BC%BA)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n29/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛穆之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E7%A9%86%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n41/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛璩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E7%92%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E7%92%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E7%92%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E7%92%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E7%92%A9)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n45/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛脩之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E8%84%A9%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n49/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)
* [『宋書』巻48](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B748#%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [『南史』巻16](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B716#%E6%AF%9B%E4%BF%AE%E4%B9%8B)
* [『魏書』巻43](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B743#%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [『北史』巻27](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7027#%E6%AF%9B%E4%BF%AE%E4%B9%8B)
* [ブログ]
  [毛脩之１　刺史の家柄](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054892456514)〜
  (@ゆるふわ晋宋春秋２)

#### 毛安之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E5%AE%89%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛徳祖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E5%BE%B3%E7%A5%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 劉遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n55/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B8%DE%BD%BD%B0%EC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 鄧嶽（鄧岳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E5%B2%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E5%B2%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E5%B2%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E5%B2%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E5%B2%B3)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n57/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 鄧遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n59/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 朱序
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9C%B1%E5%BA%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9C%B1%E5%BA%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9C%B1%E5%BA%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9C%B1%E5%BA%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9C%B1%E5%BA%8F)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n61/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)


### 巻82 列伝第52
#### 陳寿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E5%AF%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E5%AF%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E5%AF%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E5%AF%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E5%AF%BF)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n1/mode/2up)
* [書誌のみ]
  [前四史撰者列傳の研究『晉書』陳壽傳譯注](https://cir.nii.ac.jp/crid/1520853833570345472)
  （前四史撰者列傳研究ゼミナール）
* [PDFあり]
  [六朝文人伝 : 『晋書』(巻八十二)陳寿伝](https://cir.nii.ac.jp/crid/1541980095194737152)
  （佐藤 利行）
* [ブログ]
  [陳寿伝](https://estar.jp/novels/25594548/viewer?page=185)
  （@淡々晋書）
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「陳壽論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/28)
  （朱彛尊（朱彝尊））
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 王長文
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%95%B7%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%95%B7%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%95%B7%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%95%B7%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%95%B7%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n5/mode/2up)
* [ブログ]
  [王長文伝](https://estar.jp/novels/25594548/viewer?page=226)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 虞溥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%BA%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%BA%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%BA%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%BA%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%BA%A5)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n9/mode/2up)
* [ブログ]
  [『晋書』虞溥伝を訳し、『江表伝』の作者を知る](http://3guozhi.net/b/gf.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [虞溥伝](https://estar.jp/novels/25594548/viewer?page=229)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 司馬彪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BD%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n13/mode/2up)
* [ブログ]
  [司馬彪伝](https://estar.jp/novels/25594548/viewer?page=235)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 王隠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%9A%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%9A%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%9A%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%9A%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%9A%A0)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n17/mode/2up)
* [ブログ]
  [王隠伝](https://estar.jp/novels/25594548/viewer?page=239)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 虞預
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E9%A0%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E9%A0%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E9%A0%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E9%A0%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E9%A0%90)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n21/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 孫盛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%9B%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%9B%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%9B%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%9B%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%9B%9B)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n29/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 孫潜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%BD%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%BD%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%BD%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%BD%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%BD%9C)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 孫放
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%94%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%94%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%94%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%94%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%94%BE)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 干宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B9%B2%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B9%B2%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B9%B2%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B9%B2%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B9%B2%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n37/mode/2up)
* [PDFあり]
  [六朝文人伝 : 『晋書』(巻八十二)干宝伝](https://dl.ndl.go.jp/info:ndljp/pid/10504525)
  （高西 成介）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、干宝伝で言及している『晉紀』のうち、
  [「論晉武帝革命」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/235)と
  [「總論」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/236)とがある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第十二巻　晋唐小説』には、干宝伝にその序を引く
  [『搜神記』](https://dl.ndl.go.jp/info:ndljp/pid/1913008/47)の部分訳がある。
* 干宝[『搜神記』](https://ctext.org/wiki.pl?if=gb&res=839038)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 鄧粲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E7%B2%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E7%B2%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E7%B2%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E7%B2%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E7%B2%B2)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n43/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 謝沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n45/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 習鑿歯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BF%92%E9%91%BF%E6%AD%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n47/mode/2up)
* [ブログ]
  [『晋書』列伝５２、蜀漢正統論の習鑿歯伝を翻訳](http://3guozhi.net/q/sss1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 徐広
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E5%BA%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E5%BA%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E5%BA%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E5%BA%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E5%BA%83)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n63/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)
* [ブログ]
  [徐広１　　晋国の知恵袋](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054894418359)〜
  (@ゆるふわ晋宋春秋２)
* [『宋書』巻55](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B755#%E5%BE%90%E5%BB%A3)
* [『南史』巻33](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B733#%E5%BE%90%E5%BB%A3)


### 巻83 列伝第53
#### 顧和
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E5%92%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E5%92%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E5%92%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E5%92%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E5%92%8C)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n1/mode/2up)

#### 袁瓌（袁瑰）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E7%91%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E7%91%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E7%91%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E7%91%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E7%91%B0)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n9/mode/2up)

#### 袁喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n13/mode/2up)

#### 袁山松
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E5%B1%B1%E6%9D%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [姚承緒『吳趨訪古錄』の「滬瀆壘」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/7#%E6%BB%AC%E7%80%86%E5%A3%98)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n15/mode/2up)

#### 袁猷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E7%8C%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E7%8C%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E7%8C%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E7%8C%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E7%8C%B7)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n17/mode/2up)

#### 袁準
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E6%BA%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E6%BA%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E6%BA%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E6%BA%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E6%BA%96)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n19/mode/2up)

#### 袁耽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E8%80%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E8%80%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E8%80%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E8%80%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E8%80%BD)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n19/mode/2up)

#### 袁質
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E8%B3%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E8%B3%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E8%B3%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E8%B3%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E8%B3%AA)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n21/mode/2up)

#### 袁湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E6%B9%9B)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n21/mode/2up)
* [『宋書』巻52](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B752#%E8%A2%81%E6%B9%9B)
* [『南史』巻26](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B726#%E8%A2%81%E6%B9%9B)
* [ブログ]
  [袁湛１　　陳郡の名家](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054893896064)〜
  (@ゆるふわ晋宋春秋２)

#### 袁豹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E8%B1%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E8%B1%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E8%B1%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E8%B1%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E8%B1%B9)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n21/mode/2up)
* [『宋書』巻52](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B752#%E5%BC%9F_%E8%B1%B9)
* [『南史』巻26](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B726#%E5%BC%9F_%E8%B1%B9)
* [ブログ]
  [袁豹１　　雅俗の善きひと](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054893956605)〜
  (@ゆるふわ晋宋春秋２)

#### 江逌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E9%80%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E9%80%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E9%80%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E9%80%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E9%80%8C)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n23/mode/2up)

#### 江灌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E7%81%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E7%81%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E7%81%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E7%81%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E7%81%8C)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n31/mode/2up)
* [ブログ]
  [江逌伝補伝江灌伝](http://strawberrymilk.gooside.com/text/shinjo/83-3-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）

#### 江績
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E7%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E7%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E7%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E7%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E7%B8%BE)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n33/mode/2up)
* [ブログ]
  [江逌伝補伝・江績伝](http://strawberrymilk.gooside.com/text/shinjo/83-3-2.html)
  （@漢籍訳出プロジェクト「漢々學々」）

#### 車胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BB%8A%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BB%8A%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BB%8A%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BB%8A%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BB%8A%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n35/mode/2up)
* [ブログ]
  [車胤伝](http://strawberrymilk.gooside.com/text/shinjo/83-4.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [書籍 (NDL)]
  空海『三教指帰』に[「雪螢を猶ほ怠るに拉ぎ」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/8)とあり
  （[注10](https://dl.ndl.go.jp/info:ndljp/pid/1040599/33)）、
  [「數十の熠燿囊の中に聚めず」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)ともある
  （[注73](https://dl.ndl.go.jp/info:ndljp/pid/1040599/38)）。

#### 殷顗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E9%A1%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E9%A1%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E9%A1%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E9%A1%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E9%A1%97)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n37/mode/2up)
* [ブログ]
  [殷顗伝](http://strawberrymilk.gooside.com/text/shinjo/83-5.html)
  （@漢籍訳出プロジェクト「漢々學々」）

#### 王雅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%9B%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%9B%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%9B%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%9B%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%9B%85)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n39/mode/2up)
* [ブログ]
  [王雅伝](http://strawberrymilk.gooside.com/text/shinjo/83-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）


### 巻84 列伝第54
#### 王恭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%81%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%81%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%81%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%81%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%81%AD)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n47/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕・桓玄への言及がある。

#### 庾楷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%A5%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%A5%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%A5%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%A5%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%A5%B7)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n59/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)

#### 劉牢之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%89%A2%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n61/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕・桓玄への言及がある。

#### 劉敬宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%95%AC%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n71/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)
* [『宋書』巻47](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B747#%E5%8A%89%E6%95%AC%E5%AE%A3)
* [『南史』巻17](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B717#%E5%8A%89%E6%95%AC%E5%AE%A3)
* [ブログ]
  [劉敬宣１　幼き孝子](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054891873014)〜
  (@ゆるふわ晋宋春秋２)

#### 殷仲堪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E4%BB%B2%E5%A0%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n75/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)

#### 楊佺期
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E4%BD%BA%E6%9C%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)


### 巻85 列伝第55
#### 劉毅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%AF%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%AF%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%AF%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%AF%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%AF%85)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n1/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 劉邁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%82%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%82%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%82%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%82%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%82%81)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n19/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 諸葛長民
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n19/mode/2up)
* [ブログ]
  [「祭已畢焉」晋書諸葛長民伝](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854985)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 何無忌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%84%A1%E5%BF%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n25/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 檀憑之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AA%80%E6%86%91%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AA%80%E6%86%91%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AA%80%E6%86%91%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AA%80%E6%86%91%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AA%80%E6%86%91%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n33/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 魏詠之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E8%A9%A0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)


### 巻86 列伝第56
#### 張軌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%BB%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%BB%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%BB%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%BB%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%BB%8C)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n39/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [周曇の「詠史詩」の中の「前涼張軌」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E5%89%8D%E6%B6%BC%E5%BC%B5%E8%BB%8C)
* [ブログ]
  [太平御覧 張軌](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E8%BB%8C)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張寔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%AF%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%AF%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%AF%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%AF%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%AF%94)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n55/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張寔](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E5%AF%94)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張茂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8C%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8C%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8C%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8C%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8C%82)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張茂](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E8%8C%82)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張駿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%A7%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%A7%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%A7%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%A7%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%A7%BF)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n71/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張駿](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E9%A7%BF)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張重華
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%87%8D%E8%8F%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n93/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張重華](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E9%87%8D%E8%8F%AF)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張耀霊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%80%80%E9%9C%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n107/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張祚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%A5%9A)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n109/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張祚](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E7%A5%9A)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張玄靚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%8E%84%E9%9D%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n115/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張玄靚](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E7%8E%84%E9%9D%9A)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張天錫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%A4%A9%E9%8C%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n121/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張天錫](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E5%A4%A9%E9%8C%AB)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

### 巻87 列伝第57
#### 涼武昭王（李暠）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%9A%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%9A%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%9A%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%9A%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%9A%A0)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n1/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m087.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E6%9D%8E%E6%9A%A0)
* [『北史』巻100](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7100)

#### 涼後主（李歆）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%AD%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%AD%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%AD%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%AD%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%AD%86)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n29/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m087.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E6%9D%8E%E6%9A%A0)
* [『北史』巻100](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7100)

### 巻88 列伝第58 孝友
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n41/mode/2up)

#### 李密
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%AF%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%AF%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%AF%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%AF%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%AF%86)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n43/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、李密伝に引かれた
  [「陳情事表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/407)がある。

#### 盛彦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%9B%E5%BD%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%9B%E5%BD%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%9B%E5%BD%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%9B%E5%BD%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%9B%E5%BD%A6)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n49/mode/2up)

#### 夏方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E6%96%B9)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n51/mode/2up)

#### 王裒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%A3%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%A3%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%A3%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%A3%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%A3%92)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n51/mode/2up)

#### 許孜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A8%B1%E5%AD%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A8%B1%E5%AD%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A8%B1%E5%AD%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A8%B1%E5%AD%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A8%B1%E5%AD%9C)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n57/mode/2up)

#### 庾袞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E8%A2%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E8%A2%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E8%A2%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E8%A2%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E8%A2%9E)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n59/mode/2up)

#### 孫晷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%99%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%99%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%99%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%99%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%99%B7)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n69/mode/2up)

#### 顔含
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%94%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%94%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%94%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%94%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%94%E5%90%AB)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n71/mode/2up)
* 顔之推の『顔氏家訓』
    * [治家篇](https://zh.wikisource.org/wiki/%E9%A1%8F%E6%B0%8F%E5%AE%B6%E8%A8%93/%E5%8D%B7%E7%AC%AC1#%E6%B2%BB%E5%AE%B6%E7%AC%AC%E4%BA%94)に「婚姻素對，靖侯成規。」とある。
    * [止足篇](https://zh.wikisource.org/wiki/%E9%A1%8F%E6%B0%8F%E5%AE%B6%E8%A8%93/%E5%8D%B7%E7%AC%AC5#%E6%AD%A2%E8%B6%B3%E7%AC%AC%E5%8D%81%E4%B8%89)に「先祖靖侯戒子侄曰：『汝家書生門戶，世無富貴；自今仕宦不可過二千石，婚姻勿貪勢家。』」とある。
* 顔真卿の[「晉侍中右光祿大夫本州大中正西平靖侯顏公大宗碑」](https://zh.wikisource.org/wiki/%E6%99%89%E4%BE%8D%E4%B8%AD%E5%8F%B3%E5%85%89%E7%A5%BF%E5%A4%A7%E5%A4%AB%E6%9C%AC%E5%B7%9E%E5%A4%A7%E4%B8%AD%E6%AD%A3%E8%A5%BF%E5%B9%B3%E9%9D%96%E4%BE%AF%E9%A1%8F%E5%85%AC%E5%A4%A7%E5%AE%97%E7%A2%91)

#### 劉殷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%AE%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%AE%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%AE%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%AE%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%AE%B7)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n79/mode/2up)

#### 王延
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BB%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BB%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BB%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BB%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BB%B6)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n83/mode/2up)

#### 王談
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%AB%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%AB%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%AB%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%AB%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%AB%87)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n85/mode/2up)

#### 桑虞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%91%E8%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%91%E8%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%91%E8%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%91%E8%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%91%E8%99%9E)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n87/mode/2up)

#### 何琦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%90%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%90%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%90%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%90%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%90%A6)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n89/mode/2up)

#### 呉逵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%89%E9%80%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%89%E9%80%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%89%E9%80%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%89%E9%80%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%89%E9%80%B5)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n91/mode/2up)


### 巻89 列伝第59 忠義
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n2/mode/2up)

#### 嵆紹（嵇紹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B5%87%E7%B4%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B5%87%E7%B4%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B5%87%E7%B4%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B5%87%E7%B4%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B5%87%E7%B4%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n3/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch1.html)
  （@いつか書きたい『三国志』）
* [文天祥「正氣歌」](https://zh.wikisource.org/wiki/%E6%AD%A3%E6%B0%A3%E6%AD%8C)の「為嵇侍中血」という句
* [岡本綺堂『明治劇談　ランプの下にて』](https://www.aozora.gr.jp/cards/000082/files/49526_42385.html)所収「『船弁慶』と『夢物語』」
  ……「手水などが要るものか。稽侍中の血、洗う勿れじゃ。」という台詞にまつわる話がある（「稽」は「嵇」の誤植または誤入力か）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(2)嵆紹 王豹](https://readingnotesofjinshu.com/translation/biographies/vol-89_2#toc1)
  （@晋書簡訳所）
* 高啓の[「永嘉行」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E6%B0%B8%E5%98%89%E8%A1%8C)の「帝衣濺血忠臣死」という句（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/143)も参照）
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「稽紹非忠臣論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/18)
  （兪長城（字が寧世））

#### 嵆含（嵇含）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B5%87%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B5%87%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B5%87%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B5%87%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B5%87%E5%90%AB)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n11/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(2)嵆紹 王豹](https://readingnotesofjinshu.com/translation/biographies/vol-89_2#toc2)
  （@晋書簡訳所）

#### 王豹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%B1%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%B1%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%B1%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%B1%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%B1%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n15/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(2)嵆紹 王豹](https://readingnotesofjinshu.com/translation/biographies/vol-89_2#toc3)
  （@晋書簡訳所）

#### 劉沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n21/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch7.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(3)劉沈 麹允](https://readingnotesofjinshu.com/translation/biographies/vol-89_3#toc1)
  （@晋書簡訳所）

#### 麹允
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BA%B9%E5%85%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BA%B9%E5%85%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BA%B9%E5%85%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BA%B9%E5%85%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BA%B9%E5%85%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n25/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(3)劉沈 麹允](https://readingnotesofjinshu.com/translation/biographies/vol-89_3#toc2)
  （@晋書簡訳所）

#### 焦嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%84%A6%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%84%A6%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%84%A6%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%84%A6%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%84%A6%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n27/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(3)劉沈 麹允](https://readingnotesofjinshu.com/translation/biographies/vol-89_3#toc2)
  （@晋書簡訳所）

#### 賈渾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%B8%BE)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n27/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 王育
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%82%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%82%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%82%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%82%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%82%B2)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n29/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 韋忠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%8B%E5%BF%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%8B%E5%BF%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%8B%E5%BF%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%8B%E5%BF%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%8B%E5%BF%A0)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n29/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 辛勉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E5%8B%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E5%8B%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E5%8B%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E5%8B%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E5%8B%89)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch10.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 辛賓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E8%B3%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E8%B3%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E8%B3%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E8%B3%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E8%B3%93)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n33/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 劉敏元
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%95%8F%E5%85%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%95%8F%E5%85%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%95%8F%E5%85%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%95%8F%E5%85%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%95%8F%E5%85%83)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n33/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch10.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 周該
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%A9%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%A9%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%A9%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%A9%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%A9%B2)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n35/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 桓雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n35/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 韓階
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E9%9A%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E9%9A%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E9%9A%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E9%9A%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E9%9A%8E)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n37/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 周崎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%B4%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%B4%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%B4%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%B4%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%B4%8E)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n37/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 易雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%98%93%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%98%93%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%98%93%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%98%93%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%98%93%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n37/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 楽道融
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%BD%E9%81%93%E8%9E%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n41/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 虞悝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%82%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%82%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%82%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%82%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%82%9D)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n41/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 沈勁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B2%88%E5%8B%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B2%88%E5%8B%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B2%88%E5%8B%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B2%88%E5%8B%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B2%88%E5%8B%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n45/mode/2up)
* [ブログ]
  [『晋書』列伝５９忠義、洛陽に殉じた、東晋人・沈勁](http://3guozhi.net/q/tt1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 吉挹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%89%E6%8C%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%89%E6%8C%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%89%E6%8C%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%89%E6%8C%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%89%E6%8C%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n47/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 王諒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%AB%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%AB%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%AB%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%AB%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%AB%92)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n49/mode/2up)
* 干宝[『搜神記』巻7](https://ctext.org/wiki.pl?if=gb&chapter=749239#p42)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 宋矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AE%8B%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AE%8B%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AE%8B%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AE%8B%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AE%8B%E7%9F%A9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 車済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BB%8A%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BB%8A%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BB%8A%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BB%8A%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BB%8A%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 丁穆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%81%E7%A9%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%81%E7%A9%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%81%E7%A9%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%81%E7%A9%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%81%E7%A9%86)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n53/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 辛恭靖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E6%81%AD%E9%9D%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n53/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 羅企生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E4%BC%81%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E4%BC%81%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E4%BC%81%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E4%BC%81%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E4%BC%81%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n55/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 張禕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%A6%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%A6%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%A6%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%A6%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%A6%95)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n57/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)


### 巻90 列伝第60 良吏
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n61/mode/2up)
  ……途中のページが欠けている。

#### 魯芝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%AF%E8%8A%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%AF%E8%8A%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%AF%E8%8A%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%AF%E8%8A%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%AF%E8%8A%9D)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n63/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 胡威
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E5%A8%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E5%A8%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E5%A8%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E5%A8%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E5%A8%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n67/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 胡奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E5%A5%95)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜軫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E8%BB%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E8%BB%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E8%BB%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E8%BB%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E8%BB%AB)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜毗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E6%AF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E6%AF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E6%AF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E6%AF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E6%AF%97)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E7%A7%80)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜烈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E7%83%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E7%83%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E7%83%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E7%83%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E7%83%88)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜良
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E8%89%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E8%89%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E8%89%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E8%89%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E8%89%AF)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 竇允
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%AB%87%E5%85%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%AB%87%E5%85%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%AB%87%E5%85%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%AB%87%E5%85%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%AB%87%E5%85%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n71/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 王宏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%AE%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%AE%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%AE%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%AE%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%AE%8F)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n71/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 曹攄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9B%B9%E6%94%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9B%B9%E6%94%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9B%B9%E6%94%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9B%B9%E6%94%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9B%B9%E6%94%84)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n73/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc1)
  （@晋書簡訳所）

#### 潘京
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BD%98%E4%BA%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BD%98%E4%BA%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BD%98%E4%BA%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BD%98%E4%BA%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BD%98%E4%BA%AC)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n77/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc2)
  （@晋書簡訳所）

#### 范晷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E6%99%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E6%99%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E6%99%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E6%99%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E6%99%B7)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n79/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc3)
  （@晋書簡訳所）

#### 范広
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%BA%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%BA%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%BA%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%BA%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%BA%83)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n79/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc3)
  （@晋書簡訳所）

#### 范稚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E7%A8%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E7%A8%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E7%A8%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E7%A8%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E7%A8%9A)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n81/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc3)
  （@晋書簡訳所）

#### 丁紹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%81%E7%B4%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%81%E7%B4%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%81%E7%B4%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%81%E7%B4%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%81%E7%B4%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n81/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc4)
  （@晋書簡訳所）

#### 喬智明
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%96%AC%E6%99%BA%E6%98%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%96%AC%E6%99%BA%E6%98%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%96%AC%E6%99%BA%E6%98%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%96%AC%E6%99%BA%E6%98%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%96%AC%E6%99%BA%E6%98%8E)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n83/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 鄧攸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E6%94%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E6%94%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E6%94%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E6%94%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E6%94%B8)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n83/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/393)）に「鄧攸之緝熙萌庶」とある。

#### 呉隠之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%89%E9%9A%A0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [周曇の「詠史詩」の中の「吳隱之」と「再吟」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E5%90%B3%E9%9A%B1%E4%B9%8B)

