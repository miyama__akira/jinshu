### 巻91 列伝第61 儒林
* [書誌のみ]
  [晋書儒林伝研究(訳注編)](https://cir.nii.ac.jp/all?q=%E6%99%8B%E6%9B%B8%E5%84%92%E6%9E%97%E4%BC%9D%E7%A0%94%E7%A9%B6) 1〜4
  （石黒 宣俊・野村 茂夫・中鉢 雅量・安本 博・宇野 茂彦・塘 耕次）
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n2/mode/2up)

#### 范平
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%B9%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%B9%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%B9%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%B9%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%B9%B3)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n4/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 文立
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%96%87%E7%AB%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%96%87%E7%AB%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%96%87%E7%AB%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%96%87%E7%AB%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%96%87%E7%AB%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陳邵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E9%82%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E9%82%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E9%82%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E9%82%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E9%82%B5)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n8/mode/2up)

#### 虞喜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E5%96%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E5%96%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E5%96%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E5%96%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E5%96%9C)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n8/mode/2up)

#### 劉兆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%85%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%85%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%85%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%85%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%85%86)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n12/mode/2up)

#### 氾毓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B0%BE%E6%AF%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B0%BE%E6%AF%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B0%BE%E6%AF%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B0%BE%E6%AF%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B0%BE%E6%AF%93)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n14/mode/2up)
* [『文選』所収の任昉の「奏彈劉整」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B740#%E5%A5%8F%E5%BD%88%E5%8A%89%E6%95%B4)に「氾毓字孤，家無常子。」とある（[『国訳漢文大成　文学部第四卷　文選下卷』](https://dl.ndl.go.jp/pid/1912835/1/33)も参照）。

#### 徐苗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E8%8B%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E8%8B%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E8%8B%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E8%8B%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E8%8B%97)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n16/mode/2up)

#### 崔遊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B4%94%E9%81%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B4%94%E9%81%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B4%94%E9%81%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B4%94%E9%81%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B4%94%E9%81%8A)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n18/mode/2up)

#### 范隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E9%9A%86)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n18/mode/2up)

#### 杜夷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E5%A4%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E5%A4%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E5%A4%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E5%A4%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E5%A4%B7)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n20/mode/2up)

#### 董景道
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%A3%E6%99%AF%E9%81%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%A3%E6%99%AF%E9%81%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%A3%E6%99%AF%E9%81%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%A3%E6%99%AF%E9%81%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%A3%E6%99%AF%E9%81%93)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n24/mode/2up)

#### 続咸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B6%9A%E5%92%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B6%9A%E5%92%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B6%9A%E5%92%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B6%9A%E5%92%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B6%9A%E5%92%B8)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n26/mode/2up)

#### 徐邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E9%82%88)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n26/mode/2up)

#### 孔衍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E8%A1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E8%A1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E8%A1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E8%A1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E8%A1%8D)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n34/mode/2up)

#### 范宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n36/mode/2up)

#### 韋謏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%8B%E8%AC%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%8B%E8%AC%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%8B%E8%AC%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%8B%E8%AC%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%8B%E8%AC%8F)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n40/mode/2up)

#### 范弘之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%BC%98%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n42/mode/2up)

#### 王歓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%AD%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%AD%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%AD%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%AD%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%AD%93)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n52/mode/2up)


### 巻92 列伝第62 文苑
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n54/mode/2up)

#### 応貞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BF%9C%E8%B2%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BF%9C%E8%B2%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BF%9C%E8%B2%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BF%9C%E8%B2%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BF%9C%E8%B2%9E)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n56/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、応貞伝に引かれた
  [「晉武帝華林園集詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/43)がある。

#### 成公綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%90%E5%85%AC%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%90%E5%85%AC%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%90%E5%85%AC%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%90%E5%85%AC%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%90%E5%85%AC%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n58/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、成公綏伝に引かれた
  [「嘯賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/337)がある。

#### 左思
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B7%A6%E6%80%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B7%A6%E6%80%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B7%A6%E6%80%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B7%A6%E6%80%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B7%A6%E6%80%9D)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n68/mode/2up)
* [PDFあり]
  [六朝文人傳:左思(『晋書』)](https://dl.ndl.go.jp/info:ndljp/pid/10504535)
  （佐藤 利行）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、左思伝で言及している
  [「三都賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/80)がある。

#### 趙至
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B6%99%E8%87%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B6%99%E8%87%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B6%99%E8%87%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B6%99%E8%87%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B6%99%E8%87%B3)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n76/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、趙至伝に引かれた
  [「與嵇茂齊書」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/109)がある。

#### 鄒湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%92%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%92%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%92%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%92%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%92%E6%B9%9B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n82/mode/2up)

#### 棗拠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A3%97%E6%8B%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A3%97%E6%8B%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A3%97%E6%8B%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A3%97%E6%8B%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A3%97%E6%8B%A0)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n84/mode/2up)

#### 褚陶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E9%99%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E9%99%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E9%99%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E9%99%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E9%99%B6)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n86/mode/2up)

#### 王沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n86/mode/2up)

#### 張翰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%BF%B0)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n92/mode/2up)
* [『文選』所収の「雜詩」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B729#%E9%9B%9C%E8%A9%A9_10)
  （[『国訳漢文大成 文学部第三巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/259)）
* [姚承緒『吳趨訪古錄』の「三高祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/6#%E4%B8%89%E9%AB%98%E7%A5%A0)
* [姚承緒『吳趨訪古錄』の「張翰墓」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/6#%E5%BC%B5%E7%BF%B0%E5%A2%93)
* [李白「送張舍人之江東」](https://zh.wikisource.org/wiki/%E9%80%81%E5%BC%B5%E8%88%8D%E4%BA%BA%E4%B9%8B%E6%B1%9F%E6%9D%B1)
  （[『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/4)）
* [李白「行路難」三](https://zh.wikisource.org/wiki/%E8%A1%8C%E8%B7%AF%E9%9B%A3_(%E6%9C%89%E8%80%B3%E8%8E%AB%E6%B4%97%E6%BD%81%E5%B7%9D%E6%B0%B4))
  （[『続国訳漢文大成 文学部第2冊（李太白詩集 上の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139917/43)）
* [李白「金陵送張十一再遊東吳」](https://zh.wikisource.org/wiki/%E9%87%91%E9%99%B5%E9%80%81%E5%BC%B5%E5%8D%81%E4%B8%80%E5%86%8D%E9%81%8A%E6%9D%B1%E5%90%B3)
  （[『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/74)）

#### 庾闡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%97%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%97%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%97%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%97%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%97%A1)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n94/mode/2up)

#### 曹毗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9B%B9%E6%AF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9B%B9%E6%AF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9B%B9%E6%AF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9B%B9%E6%AF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9B%B9%E6%AF%97)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n100/mode/2up)
* [PDFあり]
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)
  （中嶋 長文・伊藤 令子・ 平田 昌司）
  ……p. 586〜 に曹毗の「志怪」あり。

#### 李充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n106/mode/2up)
* [書誌のみ]
  [『晋書』巻九二李充伝訳注](https://cir.nii.ac.jp/crid/1572543026664686464)
  （長谷川 滋成）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/189)）に「刊弘度之四部」とある。「弘度」は李充の字。

#### 袁宏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E5%AE%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E5%AE%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E5%AE%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E5%AE%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E5%AE%8F)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n112/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、袁宏伝に引かれた
  [「三國名臣序贊」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/211)がある。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「袁宏泊渚」（第112句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/80)
* 文天祥の[「五色賦記」](https://zh.wikisource.org/wiki/%E6%96%87%E5%B1%B1%E5%85%88%E7%94%9F%E6%96%87%E9%9B%86/%E5%8D%B709#%E4%BA%94%E8%89%B2%E8%B3%A6%E8%A8%98)に「一客又曰：「『夜登庾亮之樓，月明千里』如何對？」或對曰：「秋泊袁宏之渚，水浸一天。」」という会話がある。
* [李白「夜泊牛渚懷古」](https://zh.wikisource.org/wiki/%E5%A4%9C%E6%B3%8A%E7%89%9B%E6%B8%9A%E6%87%B7%E5%8F%A4)
  （[『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/88)）
* [崔塗「牛渚夜泊」](https://zh.wikisource.org/wiki/%E7%89%9B%E6%B8%9A%E5%A4%9C%E6%B3%8A)

#### 伏滔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BC%8F%E6%BB%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BC%8F%E6%BB%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BC%8F%E6%BB%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BC%8F%E6%BB%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BC%8F%E6%BB%94)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n130/mode/2up)

#### 羅含
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E5%90%AB)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n140/mode/2up)

#### 顧愷之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E6%84%B7%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n142/mode/2up)

#### 郭澄之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E6%BE%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n148/mode/2up)


### 巻93 列伝第63 外戚
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n2/mode/2up)

#### 羊琇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%90%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%90%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%90%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%90%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%90%87)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n4/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/394)）に「羊琇願言而匪獲」とある。

#### 王恂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%81%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%81%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%81%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%81%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%81%82)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n8/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王愷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%84%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%84%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%84%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%84%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%84%B7)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n10/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 楊文宗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E6%96%87%E5%AE%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E6%96%87%E5%AE%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E6%96%87%E5%AE%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E6%96%87%E5%AE%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E6%96%87%E5%AE%97)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 羊玄之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%8E%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 虞豫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E8%B1%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E8%B1%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E8%B1%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E8%B1%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E8%B1%AB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 虞胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 庾琛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%90%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%90%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%90%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%90%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%90%9B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 杜乂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E4%B9%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E4%B9%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E4%B9%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E4%B9%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E4%B9%82)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 褚裒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E8%A3%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E8%A3%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E8%A3%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E8%A3%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E8%A3%92)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n16/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk3.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「季野陽秋」（第98句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/73)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 何準（何准）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%87%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%87%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%87%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%87%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%87%86)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)
* [姚承緒『吳趨訪古錄』の「烏夜村」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/4#%E7%83%8F%E5%A4%9C%E6%9D%91)

#### 何澄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%BE%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%BE%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%BE%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%BE%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%BE%84)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王濛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%BF%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%BF%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%BF%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%BF%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%BF%9B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王脩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%84%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%84%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%84%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%84%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%84%A9)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王蘊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%98%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%98%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%98%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%98%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%98%8A)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)
* 干宝[『搜神記』巻5](https://ctext.org/wiki.pl?if=gb&chapter=503309#p4)
  （王蘊の子が出てくる）

#### 王爽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%88%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%88%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%88%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%88%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%88%BD)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 褚爽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E7%88%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E7%88%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E7%88%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E7%88%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E7%88%BD)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)


### 巻94 列伝第64 隠逸
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n40/mode/2up)

#### 孫登
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%99%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%99%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%99%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%99%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%99%BB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n40/mode/2up)
* [書誌のみ]
  [「晋書」孫登伝訳注](https://cir.nii.ac.jp/crid/1520009410055687296)
  （中国文学研究会）
* [書籍 (NDL)]
  空海『三教指帰』に[「世は子登に異なり、何ぞ隻枕す可き」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/14)とある
  （[注148](https://dl.ndl.go.jp/info:ndljp/pid/1040599/47)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 董京
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%A3%E4%BA%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%A3%E4%BA%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%A3%E4%BA%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%A3%E4%BA%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%A3%E4%BA%AC)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n44/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 夏統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E7%B5%B1)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n48/mode/2up)
* 楊維楨[「贈杜彥清序」](https://zh.wikisource.org/wiki/%E8%B4%88%E6%9D%9C%E5%BD%A5%E6%B8%85%E5%BA%8F)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 朱沖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9C%B1%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9C%B1%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9C%B1%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9C%B1%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9C%B1%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 范粲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E7%B2%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E7%B2%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E7%B2%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E7%B2%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E7%B2%B2)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n58/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 范喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 魯勝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%AF%E5%8B%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%AF%E5%8B%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%AF%E5%8B%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%AF%E5%8B%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%AF%E5%8B%9D)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 董養
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%A3%E9%A4%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%A3%E9%A4%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%A3%E9%A4%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%A3%E9%A4%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%A3%E9%A4%8A)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 霍原
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9C%8D%E5%8E%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9C%8D%E5%8E%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9C%8D%E5%8E%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9C%8D%E5%8E%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9C%8D%E5%8E%9F)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭琦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%90%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%90%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%90%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%90%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%90%A6)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n72/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 伍朝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BC%8D%E6%9C%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BC%8D%E6%9C%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BC%8D%E6%9C%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BC%8D%E6%9C%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BC%8D%E6%9C%9D)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 魯褒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%AF%E8%A4%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%AF%E8%A4%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%AF%E8%A4%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%AF%E8%A4%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%AF%E8%A4%92)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 氾騰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B0%BE%E9%A8%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B0%BE%E9%A8%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B0%BE%E9%A8%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B0%BE%E9%A8%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B0%BE%E9%A8%B0)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 任旭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BB%BB%E6%97%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BB%BB%E6%97%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BB%BB%E6%97%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BB%BB%E6%97%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BB%BB%E6%97%AD)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭文
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n82/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「郭文游山（郭文遊山）」（第111句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/79)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)
* 吳筠[「天柱山天柱觀記」](https://zh.wikisource.org/wiki/%E5%A4%A9%E6%9F%B1%E5%B1%B1%E5%A4%A9%E6%9F%B1%E8%A7%80%E8%A8%98)に「於是旁訊有識，稽諸實錄，乃知昔高士郭文舉創隱於茲，以雲林為家，遂長往不複。元和貫於異類，猛獸為之馴擾。《晉書·逸人傳》具紀其事，可略而言。」とある。「文舉」は郭文の字。
* 錢鏐[「天柱觀記」](https://zh.wikisource.org/wiki/%E5%A4%A9%E6%9F%B1%E8%A7%80%E8%A8%98)に「東晉有郭文舉先生，得飛化之道，隱居此山，群虎來柔，史籍具載。」とある。

#### 龔壮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BE%94%E5%A3%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BE%94%E5%A3%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BE%94%E5%A3%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BE%94%E5%A3%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BE%94%E5%A3%AE)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 孟陋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E9%99%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E9%99%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E9%99%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E9%99%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E9%99%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n90/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 韓績
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E7%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E7%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E7%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E7%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E7%B8%BE)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n92/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 譙秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AD%99%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AD%99%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AD%99%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AD%99%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AD%99%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n94/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、譙秀伝で言及している
  [「薦譙元彥表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/421)（桓温によるもの）がある。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)
* [『三国志』巻42](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B742#%E5%AD%AB_%E8%AD%99%E7%A7%80)

#### 翟湯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BF%9F%E6%B9%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BF%9F%E6%B9%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BF%9F%E6%B9%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BF%9F%E6%B9%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BF%9F%E6%B9%AF)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n96/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 翟荘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BF%9F%E8%8D%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BF%9F%E8%8D%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BF%9F%E8%8D%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BF%9F%E8%8D%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BF%9F%E8%8D%98)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n96/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭翻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%BF%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%BF%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%BF%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%BF%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%BF%BB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n98/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 辛謐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E8%AC%90)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n100/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 劉驎之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%A9%8E%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 索襲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E8%A5%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E8%A5%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E8%A5%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E8%A5%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E8%A5%B2)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n104/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 楊軻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E8%BB%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E8%BB%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E8%BB%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E8%BB%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E8%BB%BB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n106/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 公孫鳳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%85%AC%E5%AD%AB%E9%B3%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 公孫永
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%85%AC%E5%AD%AB%E6%B0%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 張忠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%BF%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%BF%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%BF%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%BF%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%BF%A0)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 石垣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%9E%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%9E%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%9E%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%9E%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%9E%A3)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n114/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 宋繊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AE%8B%E7%B9%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AE%8B%E7%B9%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AE%8B%E7%B9%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AE%8B%E7%B9%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AE%8B%E7%B9%8A)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n114/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭荷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E8%8D%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E8%8D%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E8%8D%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E8%8D%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E8%8D%B7)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n116/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭瑀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%91%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%91%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%91%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%91%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%91%80)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n116/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 祈嘉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%88%E5%98%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%88%E5%98%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%88%E5%98%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%88%E5%98%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%88%E5%98%89)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n120/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 瞿硎先生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n122/mode/2up)
* [姚承緒『吳趨訪古錄』の「重岡」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E9%87%8D%E5%B2%A1%E3%80%88%E7%94%A8%E5%94%90%E7%91%AA%E3%80%8A%E9%87%8D%E5%B2%A1%E6%98%A5%E6%9C%9B%E3%80%8B%E9%9F%BB%E3%80%89)
* [姚承緒『吳趨訪古錄』の「雙鳳」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E9%9B%99%E9%B3%B3)
* [姚承緒『吳趨訪古錄』の「樂隱園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E6%A8%82%E9%9A%B1%E5%9C%92)
* [姚承緒『吳趨訪古錄』の「瞿硎先生墓」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F%E5%A2%93)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 謝敷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%95%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%95%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%95%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%95%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%95%B7)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n122/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 戴逵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E9%80%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E9%80%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E9%80%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E9%80%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E9%80%B5)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n124/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 龔玄之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BE%94%E7%8E%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n130/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 陶淡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%B7%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%B7%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%B7%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%B7%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%B7%A1)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n132/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 陶潜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%BD%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%BD%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%BD%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%BD%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%BD%9C)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n132/mode/2up)
* [『宋書』巻93](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B793#%E9%99%B6%E6%BD%9B)
* [『南史』巻75](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B775#%E9%99%B6%E6%BD%9B)
* [PDFあり]
  [六朝文人傳 陶潛傳(『宋書』巻九三)](https://dl.ndl.go.jp/info:ndljp/pid/10504835)
  (森野 繁夫)
  ……『晋書』巻94の陶潜伝の訳注ではないが、参考までに。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、陶潜伝に引かれた
  [「歸去來」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/157)がある。
* [胡曾の「武陵溪」](https://zh.wikisource.org/wiki/%E6%AD%A6%E9%99%B5%E6%BA%AA)
  ……[陶潜の「桃花源記」](https://zh.wikisource.org/wiki/%E6%A1%83%E8%8A%B1%E6%BA%90%E8%A8%98)を踏まえていると思われる。
* [姚承緒『吳趨訪古錄』の「趙孟俯石刻」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E8%B6%99%E5%AD%9F%E4%BF%AF%E7%9F%B3%E5%88%BB)
  ……「歸去來辭」に関連のある詩。
* [姚承緒『吳趨訪古錄』の「五柳園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E4%BA%94%E6%9F%B3%E5%9C%92)
* [ブログ]
  [藤澤東畡「陶靖節」七言絶句](https://www.kansai-u.ac.jp/hakuen/culture/culture_archive/post_34.html)
  （@幕末・明治・大正・昭和の大阪最大の私塾　泊園書院）
* [汪遵の「隋柳」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7602#%E9%9A%8B%E6%9F%B3)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)
* [ブログ]
  [陶潜１　　五柳先生伝](https://kakuyomu.jp/works/1177354054895306127/episodes/1177354055447832273)〜
  （@ゆるふわ晋宋春秋３）
  ……主に『宋書』を追ったもの。
* [李攀龍「白雪樓」](https://zh.wikisource.org/wiki/%E6%BB%84%E6%BA%9F%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B709#%E7%99%BD%E9%9B%AA%E6%A8%93)に「可知陶令賦歸來」とある。
* 『關帝靈籤』第三十九籤に「陶淵明賞菊」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/39)
    * [画像版](https://zh.wikisource.org/w/index.php?title=File%3AHarvard_drs_53239458_%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.pdf&page=41)


### 巻95 列伝第65 芸術
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n2/mode/2up)

#### 陳訓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E8%A8%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E8%A8%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E8%A8%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E8%A8%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E8%A8%93)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 戴洋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E6%B4%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E6%B4%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E6%B4%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E6%B4%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E6%B4%8B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 韓友
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E5%8F%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E5%8F%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E5%8F%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E5%8F%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E5%8F%8B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n22/mode/2up)

#### 淳于智
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B7%B3%E4%BA%8E%E6%99%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n26/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 歩熊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AD%A9%E7%86%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AD%A9%E7%86%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AD%A9%E7%86%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AD%A9%E7%86%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AD%A9%E7%86%8A)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 杜不愆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E4%B8%8D%E6%84%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [『搜神後記』巻2「杜不愆」](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E5%BE%8C%E8%A8%98/02#%E6%9D%9C%E4%B8%8D%E6%84%86)
  （[『国訳漢文大成』第十二巻](https://dl.ndl.go.jp/info:ndljp/pid/1913008/87)に訓読あり）

#### 厳卿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8E%B3%E5%8D%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8E%B3%E5%8D%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8E%B3%E5%8D%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8E%B3%E5%8D%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8E%B3%E5%8D%BF)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 隗炤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9A%97%E7%82%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9A%97%E7%82%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9A%97%E7%82%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9A%97%E7%82%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9A%97%E7%82%A4)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 卜珝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9C%E7%8F%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9C%E7%8F%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9C%E7%8F%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9C%E7%8F%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9C%E7%8F%9D)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n34/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 鮑靚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AE%91%E9%9D%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AE%91%E9%9D%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AE%91%E9%9D%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AE%91%E9%9D%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AE%91%E9%9D%9A)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n36/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「鮑靚記井」（第53句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/48)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 呉猛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%89%E7%8C%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%89%E7%8C%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%89%E7%8C%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%89%E7%8C%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%89%E7%8C%9B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 幸霊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B9%B8%E9%9C%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B9%B8%E9%9C%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B9%B8%E9%9C%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B9%B8%E9%9C%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B9%B8%E9%9C%8A)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n44/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 仏図澄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BB%8F%E5%9B%B3%E6%BE%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n48/mode/2up)
* [『高僧伝』巻9](https://cbetaonline.dila.edu.tw/zh/T2059_009)
* [『文選』所収の王巾「頭陀寺碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%A0%AD%E9%99%80%E5%AF%BA%E7%A2%91%E6%96%87)にある「澄什結轍於山西」（『国訳漢文大成　文学部第四巻　文選下巻』の[該当ページ](https://dl.ndl.go.jp/info:ndljp/pid/1912835/388)）の「澄」は仏図澄を指す。
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 麻襦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BA%BB%E8%A5%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BA%BB%E8%A5%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BA%BB%E8%A5%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BA%BB%E8%A5%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BA%BB%E8%A5%A6)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n66/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 単道開
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%98%E9%81%93%E9%96%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%98%E9%81%93%E9%96%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%98%E9%81%93%E9%96%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%98%E9%81%93%E9%96%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%98%E9%81%93%E9%96%8B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n68/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [『高僧伝』巻9](https://cbetaonline.dila.edu.tw/zh/T2059_009)

#### 黄泓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BB%84%E6%B3%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BB%84%E6%B3%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BB%84%E6%B3%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BB%84%E6%B3%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BB%84%E6%B3%93)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n72/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 索紞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E7%B4%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E7%B4%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E7%B4%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E7%B4%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E7%B4%9E)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n74/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 孟欽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E6%AC%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E6%AC%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E6%AC%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E6%AC%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E6%AC%BD)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n78/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王嘉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%98%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%98%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%98%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%98%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%98%89)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n78/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 僧渉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%83%A7%E6%B8%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%83%A7%E6%B8%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%83%A7%E6%B8%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%83%A7%E6%B8%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%83%A7%E6%B8%89)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n82/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 郭黁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E9%BB%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E9%BB%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E9%BB%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E9%BB%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E9%BB%81)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n84/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 鳩摩羅什
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n86/mode/2up)
* [『高僧伝』巻2](https://cbetaonline.dila.edu.tw/zh/T2059_002)
* 『欽定古今圖書集成』（方輿彙編　職方典　第511巻）の「[淨土樹](https://zh.wikisource.org/wiki/%E6%AC%BD%E5%AE%9A%E5%8F%A4%E4%BB%8A%E5%9C%96%E6%9B%B8%E9%9B%86%E6%88%90/%E6%96%B9%E8%BC%BF%E5%BD%99%E7%B7%A8/%E8%81%B7%E6%96%B9%E5%85%B8/%E7%AC%AC0511%E5%8D%B7)」
* [『佛說阿彌陀經註釋會要』](https://cbetaonline.dila.edu.tw/zh/B0024_001)
* [『海八德經』](https://cbetaonline.dila.edu.tw/zh/T0035_001)
* [『佛說放牛經』](https://cbetaonline.dila.edu.tw/zh/T0123_001)
* [『大莊嚴論經』](https://cbetaonline.dila.edu.tw/zh/T0201_001)
* [『眾經撰雜譬喻』](https://cbetaonline.dila.edu.tw/zh/T0208_001)
* [『摩訶般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0223_001)
* [『小品般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0227_001)
* [『金剛般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0235_001)
* [『佛說仁王般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0245_001)
* [『摩訶般若波羅蜜大明呪經』](https://cbetaonline.dila.edu.tw/zh/T0250_001)
* [『妙法蓮華經』](https://cbetaonline.dila.edu.tw/zh/T0262_001)
* [『十住經』](https://cbetaonline.dila.edu.tw/zh/T0286_001)
* [『佛說莊嚴菩提心經』](https://cbetaonline.dila.edu.tw/zh/T0307_001)
* [『佛說須摩提菩薩經』](https://cbetaonline.dila.edu.tw/zh/T0335_001)
* [『佛說阿彌陀經』](https://cbetaonline.dila.edu.tw/zh/T0366_001)
* [『集一切福德三昧經』](https://cbetaonline.dila.edu.tw/zh/T0382_001)
* [『佛垂般涅槃略說教誡經』](https://cbetaonline.dila.edu.tw/zh/T0389_001)
* [『自在王菩薩經』](https://cbetaonline.dila.edu.tw/zh/T0420_001)
* [『佛說千佛因緣經』](https://cbetaonline.dila.edu.tw/zh/T0426_001)
* [『佛說彌勒下生成佛經』](https://cbetaonline.dila.edu.tw/zh/T0454_001)
* [『佛說彌勒大成佛經』](https://cbetaonline.dila.edu.tw/zh/T0456_001)
* [『文殊師利問菩提經』](https://cbetaonline.dila.edu.tw/zh/T0464_001)
* [『維摩詰所說經』](https://cbetaonline.dila.edu.tw/zh/T0475_001)
* [『持世經』](https://cbetaonline.dila.edu.tw/zh/T0482_001)
* [『不思議光菩薩所說經』](https://cbetaonline.dila.edu.tw/zh/T0484_001)
* [『思益梵天所問經』](https://cbetaonline.dila.edu.tw/zh/T0586_001)
* [『禪祕要法經』](https://cbetaonline.dila.edu.tw/zh/T0613_001)
* [『坐禪三昧經』](https://cbetaonline.dila.edu.tw/zh/T0614_001)
* [『菩薩訶色欲法經』](https://cbetaonline.dila.edu.tw/zh/T0615_001)
* [『禪法要解』](https://cbetaonline.dila.edu.tw/zh/T0616_001)
* [『思惟略要法』](https://cbetaonline.dila.edu.tw/zh/T0617_001)
* [『大樹緊那羅王所問經』](https://cbetaonline.dila.edu.tw/zh/T0625_001)
* [『佛說首楞嚴三昧經』](https://cbetaonline.dila.edu.tw/zh/T0642_001)
* [『諸法無行經』](https://cbetaonline.dila.edu.tw/zh/T0650_001)
* [『佛藏經』](https://cbetaonline.dila.edu.tw/zh/T0653_001)
* [『佛說華手經』](https://cbetaonline.dila.edu.tw/zh/T0657_001)
* [『燈指因緣經』](https://cbetaonline.dila.edu.tw/zh/T0703_001)
* [『孔雀王呪經』](https://cbetaonline.dila.edu.tw/zh/T0988_001)
* [『十誦律』](https://cbetaonline.dila.edu.tw/zh/T1435_001)
* [『十誦比丘波羅提木叉戒本』](https://cbetaonline.dila.edu.tw/zh/T1436_001)
* [『梵網經』](https://cbetaonline.dila.edu.tw/zh/T1484_001)
* [『清淨毘尼方廣經』](https://cbetaonline.dila.edu.tw/zh/T1489_001)
* [『大智度論』](https://cbetaonline.dila.edu.tw/zh/T1509_001)
* [『十住毘婆沙論』](https://cbetaonline.dila.edu.tw/zh/T1521_001)
* [『中論』](https://cbetaonline.dila.edu.tw/zh/T1564_001)
* [『十二門論』](https://cbetaonline.dila.edu.tw/zh/T1568_001)
* [『百論』](https://cbetaonline.dila.edu.tw/zh/T1569_001)
* [『成實論』](https://cbetaonline.dila.edu.tw/zh/T1646_001)
* [『發菩提心經論』](https://cbetaonline.dila.edu.tw/zh/T1659_001)
* [『鳩摩羅什法師大義』](https://cbetaonline.dila.edu.tw/zh/T1856_001)
* [『馬鳴菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2046_001)
* [『龍樹菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2047a_001)
* [『龍樹菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2047b_001)
* [『提婆菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2048_001)
* [『文選』所収の王巾「頭陀寺碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%A0%AD%E9%99%80%E5%AF%BA%E7%A2%91%E6%96%87)にある「澄什結轍於山西」（『国訳漢文大成　文学部第四巻　文選下巻』の[該当ページ](https://dl.ndl.go.jp/info:ndljp/pid/1912835/388)）の「什」は鳩摩羅什を指す。
* [ブログ]
  [鳩摩羅什１　懐胎](https://kakuyomu.jp/works/1177354055365070928/episodes/16816452219208220039)〜
  （@ゆるふわ中原戦記２）
* 『今昔物語集』の[「鳩摩羅焔奉盗仏伝震旦語」](http://yatanavi.org/text/k_konjaku/k_konjaku6-5)
  （リンク先のテキストデータは[やたがらすナビ](http://yatanavi.org/index.html)）

#### 僧曇霍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%83%A7%E6%9B%87%E9%9C%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n102/mode/2up)
* [『高僧伝』巻10](https://cbetaonline.dila.edu.tw/zh/T2059_010)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 臺産（台産）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B0%E7%94%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B0%E7%94%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B0%E7%94%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B0%E7%94%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B0%E7%94%A3)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)


### 巻96 列伝第66 列女
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n2/mode/2up)

#### 羊耽妻辛憲英
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n4/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 杜有道妻厳憲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王渾妻鍾琰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n8/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 鄭袤妻曹氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n10/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 愍懐太子妃王恵風
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n12/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「愍懐太子の妃」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/219)
* [『太平御覧』巻664](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%BD/0664)
  （[四庫全書本](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%BD_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B70664)）に引く『南嶽魏夫人内傳』や
  陶弘景の[『真誥』](https://zh.wikisource.org/wiki/%E7%9C%9F%E8%AA%A5/%E5%8D%B7013)（[「眞誥研究 : 譯注篇」](http://hdl.handle.net/2433/98008)（吉川忠夫・麥谷邦夫）の474ページ）では、通りすがりの女仙に助けられる。

#### 鄭休妻石氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n14/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶侃母湛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n14/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「陶侃の母」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/219)

#### 賈渾妻宗氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n16/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「賈渾の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/221)

#### 梁緯妻辛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n16/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「梁緯の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/220)

#### 許延妻杜氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「許延の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/222)

#### 虞潭母孫氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「虞潭が母」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/220)

#### 周顗母李絡秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n20/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 張茂妻陸氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n22/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 尹虞二女
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n22/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 荀崧小女灌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n24/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王凝之妻謝道韞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n24/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* 蘇軾「[題王逸少帖](https://zh.wikisource.org/wiki/%E9%A1%8C%E7%8E%8B%E9%80%B8%E5%B0%91%E5%B8%96)」
  （[『国訳漢文大成　続　文学部』第61冊](https://dl.ndl.go.jp/info:ndljp/pid/1140559/34)）の「謝家夫人淡豐容，蕭然自有林下風。」の部分。
* [王士禎『池北偶談』巻15](https://zh.wikisource.org/wiki/%E6%B1%A0%E5%8C%97%E5%81%B6%E8%AB%87/%E5%8D%B7%E5%8D%81%E4%BA%94)に「謝道韞硯」という話がある。

#### 劉臻妻陳氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n28/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 皮京妻龍憐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n28/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 孟昶妻周氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 何無忌母劉氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉聡妻劉娥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王広女
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BA%83%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n34/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陝婦人
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%9D%E5%A9%A6%E4%BA%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n36/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 靳康女
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9D%B3%E5%BA%B7%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n36/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 韋逞母宋氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n36/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「韋が母宋氏」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/224)

#### 張天錫妾閻氏・薛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n38/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「張天錫が妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/222)

#### 苻堅妾張氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 竇滔妻蘇氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [顏希源『百美新詠』「蘇蕙」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E8%98%87%E8%95%99)

#### 苻登妻毛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n42/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「苻登の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/223)
* [ブログ]
  [晋書　苻登・毛氏列伝](http://blog.livedoor.jp/oboro0083/archives/2498254.html)
  （@★香華酔月☆）

#### 慕容垂妻段元妃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n42/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [段元妃１　後段后](https://kakuyomu.jp/works/1177354055365070928/episodes/16816700428295830475)〜
  （@ゆるふわ中原戦記２）

#### 段豊妻慕容氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 呂纂妻楊氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「呂纂の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/223)

#### 呂紹妻張氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「呂紹の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/224)

#### 涼武昭王李玄盛后尹氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)


### 巻97 列伝第67 四夷（東夷・西戎・南蛮・北狄）
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n54/mode/2up)

#### 夫餘国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%AB%E9%A4%98%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 三韓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%89%E9%9F%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%89%E9%9F%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%89%E9%9F%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%89%E9%9F%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%89%E9%9F%93)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

##### 馬韓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AC%E9%9F%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AC%E9%9F%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AC%E9%9F%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AC%E9%9F%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AC%E9%9F%93)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

##### 辰韓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%B0%E9%9F%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%B0%E9%9F%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%B0%E9%9F%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%B0%E9%9F%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%B0%E9%9F%93)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n64/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 肅慎氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%82%85%E6%85%8E%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%82%85%E6%85%8E%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%82%85%E6%85%8E%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%82%85%E6%85%8E%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%82%85%E6%85%8E%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n66/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 倭人
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%80%AD%E4%BA%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%80%AD%E4%BA%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%80%AD%E4%BA%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%80%AD%E4%BA%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%80%AD%E4%BA%BA)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)
* [『宋書』巻97](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B797#%E6%9D%B1%E5%A4%B7)
* [『南史』巻79](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B779#%E5%80%AD%E5%9C%8B)
* [『北史』巻94](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7094#%E5%80%AD)

#### 裨離
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%A8%E9%9B%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%A8%E9%9B%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%A8%E9%9B%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%A8%E9%9B%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%A8%E9%9B%A2)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n80/mode/2up)
* [ブログ]
  [東夷伝中裨離等十国条](http://strawberrymilk.gooside.com/text/shinjo/97-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 吐谷渾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%90%E8%B0%B7%E6%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n82/mode/2up)
* [『宋書』巻96](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B796)
* [『南史』巻79](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B779#%E6%B2%B3%E5%8D%97%E5%9C%8B)
* [『魏書』巻101](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7101#%E5%90%90%E8%B0%B7%E6%B8%BE)
* [『北史』巻96](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7096#%E5%90%90%E8%B0%B7%E6%B8%BE)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 焉耆国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%84%89%E8%80%86%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%84%89%E8%80%86%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%84%89%E8%80%86%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%84%89%E8%80%86%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%84%89%E8%80%86%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n96/mode/2up)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E7%84%89%E8%80%86)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E7%84%89%E8%80%86%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 亀茲国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BA%80%E8%8C%B2%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n100/mode/2up)
* [『南史』巻79](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B779#%E9%BE%9C%E8%8C%B2%E5%9C%8B)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E9%BE%9C%E8%8C%B2)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E9%BE%9C%E8%8C%B2%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 大宛国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%A7%E5%AE%9B%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n100/mode/2up)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 康居国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%B7%E5%B1%85%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n102/mode/2up)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E5%9A%88%E5%99%A0)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E5%BA%B7%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 大秦国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%A7%E7%A7%A6%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n104/mode/2up)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E5%A4%A7%E7%A7%A6%E5%9C%8B)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E5%A4%A7%E7%A7%A6%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 林邑
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9E%97%E9%82%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9E%97%E9%82%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9E%97%E9%82%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9E%97%E9%82%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9E%97%E9%82%91)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)
* [『宋書』巻97](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B797#%E5%8D%97%E5%A4%B7)
* [『南史』巻78](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B778#%E6%9E%97%E9%82%91%E5%9C%8B)
* [『北史』巻95](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7095#%E6%9E%97%E9%82%91)

#### 扶南
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%89%B6%E5%8D%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%89%B6%E5%8D%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%89%B6%E5%8D%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%89%B6%E5%8D%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%89%B6%E5%8D%97)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n122/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)
* [『宋書』巻97](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B797#%E5%8D%97%E5%A4%B7)
* [『南史』巻78](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B778#%E6%89%B6%E5%8D%97%E5%9C%8B)

#### 匈奴
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8C%88%E5%A5%B4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8C%88%E5%A5%B4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8C%88%E5%A5%B4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8C%88%E5%A5%B4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8C%88%E5%A5%B4)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n126/mode/2up)
* [ブログ]
  [巻九十七 列伝六十七 四夷(4)北狄](https://readingnotesofjinshu.com/translation/biographies/vol-97_4)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

### 巻98 列伝第68
#### 王敦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%95%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%95%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%95%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%95%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%95%A6)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列伝６８、王敦伝](http://3guozhi.net/h/ot1.html)
  （@いつか書きたい『三国志』）

#### 沈充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B2%88%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B2%88%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B2%88%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B2%88%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B2%88%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n34/mode/2up)

#### 桓温
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%B8%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%B8%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%B8%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%B8%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%B8%A9)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』桓温伝の翻訳と小考](http://3guozhi.net/q/ke1.html)
  （@いつか書きたい『三国志』）
* 『欽定古今圖書集成』（方輿彙編　職方典　第511巻）の「[桓公堆](https://zh.wikisource.org/wiki/%E6%AC%BD%E5%AE%9A%E5%8F%A4%E4%BB%8A%E5%9C%96%E6%9B%B8%E9%9B%86%E6%88%90/%E6%96%B9%E8%BC%BF%E5%BD%99%E7%B7%A8/%E8%81%B7%E6%96%B9%E5%85%B8/%E7%AC%AC0511%E5%8D%B7)」
* 李白「[桓公井](https://zh.wikisource.org/wiki/%E5%A7%91%E5%AD%B0%E5%8D%81%E8%A9%A0#%E6%A1%93%E5%85%AC%E4%BA%95)」
  （[『国訳漢文大成　続　文学部』第10冊](https://dl.ndl.go.jp/info:ndljp/pid/1139993/92)）
* 王安石「[白紵山](https://zh.wikisource.org/wiki/%E7%99%BD%E7%B4%B5%E5%B1%B1)」
* [ブログ]
  [永和七年（351年） 桓温の武昌に次して止みたる後の上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-109.html)
  （@大司馬府 作戦日誌）
* [ブログ]
  [隆和元年（362年）五月 桓温の洛陽に遷都を請う上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-87.html)
  （@大司馬府 作戦日誌）
* [ブログ]
  [興寧二年（364年）五月 桓温の入参朝政を辞する上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-88.html)
  （@大司馬府 作戦日誌）
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……題材の一つが桓温（最後の「桓大司馬」云々のところ）。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）
* [顏希源『百美新詠』「李勢女」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E6%9D%8E%E5%8B%A2%E5%A5%B3)
  ……[『世説新語』賢媛篇21](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E8%B3%A2%E5%AA%9B)の話が題材。この詩の「老奴」は桓温のこと。

#### 桓熙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%86%99)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓歆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%AD%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%AD%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%AD%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%AD%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%AD%86)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓禕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%A6%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%A6%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%A6%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%A6%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%A6%95)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓偉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%81%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%81%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%81%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%81%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%81%89)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 孟嘉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E5%98%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E5%98%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E5%98%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E5%98%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E5%98%89)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n74/mode/2up)


### 巻99 列伝第69
#### 桓玄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%8E%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%8E%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%8E%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%8E%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%8E%84)
* [書籍 (IA)]
  [晉書斠注(六十五)](https://archive.org/details/02077782.cn/page/n2/mode/2up)
* [『魏書』巻97](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B797#%E6%A1%93%E7%8E%84)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕・桓玄への言及がある。

#### 卞範之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9E%E7%AF%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十五)](https://archive.org/details/02077782.cn/page/n52/mode/2up)

#### 殷仲文
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E4%BB%B2%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(六十五)](https://archive.org/details/02077782.cn/page/n54/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、殷仲文伝に引かれた
  [「解尚書表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/423)がある。
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……題材の一つが殷仲文。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）

### 巻100 列伝第70
#### 王弥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BC%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BC%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BC%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BC%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BC%A5)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [王弥伝](https://estar.jp/novels/25594548/viewer?page=107)
  （@淡々晋書）
* [ブログ]
  [巻一百　列伝第七十　王弥　張昌　陳敏](https://readingnotesofjinshu.com/translation/biographies/vol-100_1#toc1)
  （@晋書簡訳所）

#### 張昌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%98%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%98%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%98%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%98%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%98%8C)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n8/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王弥　張昌　陳敏](https://readingnotesofjinshu.com/translation/biographies/vol-100_1#toc2)
  （@晋書簡訳所）

#### 陳敏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E6%95%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E6%95%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E6%95%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E6%95%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E6%95%8F)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王弥　張昌　陳敏](https://readingnotesofjinshu.com/translation/biographies/vol-100_1#toc3)
  （@晋書簡訳所）

#### 王如
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%A6%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%A6%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%A6%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%A6%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%A6%82)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n22/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%BC%B7%BD%BD)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc1)
  （@晋書簡訳所）

#### 杜曾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E6%9B%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E6%9B%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E6%9B%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E6%9B%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E6%9B%BE)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n24/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y7.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc2)
  （@晋書簡訳所）

#### 杜弢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E5%BC%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E5%BC%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E5%BC%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E5%BC%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E5%BC%A2)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc3)
  （@晋書簡訳所）

#### 王機
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%A9%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%A9%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%A9%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%A9%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%A9%9F)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n36/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc4)
  （@晋書簡訳所）

#### 王矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%9F%A9)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc4)
  （@晋書簡訳所）

#### 祖約
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E7%B4%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E7%B4%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E7%B4%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E7%B4%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E7%B4%84)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　祖約　蘇峻](https://readingnotesofjinshu.com/translation/biographies/vol-100_3#toc1)
  （@晋書簡訳所）


#### 蘇峻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%98%87%E5%B3%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%98%87%E5%B3%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%98%87%E5%B3%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%98%87%E5%B3%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%98%87%E5%B3%BB)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n44/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　祖約　蘇峻](https://readingnotesofjinshu.com/translation/biographies/vol-100_3#toc2)
  （@晋書簡訳所）

#### 孫恩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%81%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%81%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%81%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%81%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%81%A9)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n54/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）

#### 盧循
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E5%BE%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E5%BE%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E5%BE%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E5%BE%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E5%BE%AA)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n64/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）

#### 譙縦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AD%99%E7%B8%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AD%99%E7%B8%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AD%99%E7%B8%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AD%99%E7%B8%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AD%99%E7%B8%A6)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n70/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）


