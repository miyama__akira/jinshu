Split_files = 1-1-heading.md 1-2-license.md 1-3-general-information-related-to-Jinshu.md 1-4-various-information-related-to-the-era.md 1-5-kana-dictionary.md 1-6-item-types.md 2-annals.md 3-treatises.md 4-031-040-biographies.md 4-041-050-biographies.md 4-051-060-biographies.md 4-061-070-biographies.md 4-071-080-biographies.md 4-081-090-biographies.md 4-091-100-biographies.md 5-101-110-records.md 5-111-120-records.md 5-121-130-records.md
Single_ver_file = single-page.md

all : $(Single_ver_file)

$(Single_ver_file) : $(Split_files)
	cat $(Split_files) > $(Single_ver_file)

.PHONY : clean
clean :
	rm $(Single_ver_file)
