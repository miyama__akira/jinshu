# 『晋書』の原文・訳注（と、西晋・東晋に関係あるその他諸々）のリスト

とりあえず存在を把握したもののリスト。
読んだわけではない。
と言うか原文を読める力がない😢。
そのくせ_以下すべて敬称略_である。

* [単一ページ版](single-page.md)
* 分割版
    * 前置き
        * [ヘッダ](1-1-heading.md)
        * [ライセンスなど](1-2-license.md)
        * [『晋書』全般](1-3-general-information-related-to-Jinshu.md)
        * [西晋・東晋・五胡十六国に関係ある色々なもの](1-4-various-information-related-to-the-era.md)
        * [仮名漢字変換辞書の元データ](1-5-kana-dictionary.md)
        * [リンク先の情報源の種別](1-6-item-types.md)……この頃あまりこの形式を守れていないが。
    * [紀](2-annals.md)
    * [志](3-treatises.md)
    * 列伝
        * [巻31〜巻40](4-031-040-biographies.md)
        * [巻41〜巻50](4-041-050-biographies.md)
        * [巻51〜巻60](4-051-060-biographies.md)
        * [巻61〜巻70](4-061-070-biographies.md)
        * [巻71〜巻80](4-071-080-biographies.md)
        * [巻81〜巻90](4-081-090-biographies.md)
        * [巻91〜巻100](4-091-100-biographies.md)
    * 載記
        * [巻101〜巻110](5-101-110-records.md)
        * [巻111〜巻120](5-111-120-records.md)
        * [巻121〜巻130](5-121-130-records.md)
