# 『晋書』の原文・訳注（と、西晋・東晋に関係あるその他諸々）のリスト

とりあえず存在を把握したもののリスト。
読んだわけではない。
と言うか原文を読める力がない😢。
そのくせ_以下すべて敬称略_である。

## ライセンスなど
このリストは、[クリエイティブ・コモンズ 表示 4.0 国際 ライセンス](https://creativecommons.org/licenses/by/4.0/deed.ja) (CC BY 4.0) のもとに公開します。
かなり自由に再利用がきくので、再利用してもっと良いリストを作って教えてください😊（実際の作業面でも再利用しやすいように markdownで書いています）。
あるいは、プルリクをくだされば、できる範囲で書き足します（GitLabを触るのが初めてなので不安ですが。[Twitter](https://twitter.com/miyama__akira)のリプないしDMでも結構です）。

（作成者: [深山](https://twitter.com/miyama__akira)）


## 『晋書』全般

### 本文
* 中央研究院・歴史語言研究所の[漢籍電子文献資料庫](http://hanchi.ihp.sinica.edu.tw/ihp/hanji.htm)……『晋書』本文が見られる。でも個々のページのパーマリンクが不明（そもそもパーマリンクがあるのかがわからない）。
* Wikisource の[『晉書』](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8)……『晋書』本文が見られる。信用度は劣るだろうけれども、個々のページにパーマリンクできるのが便利。
* 中國哲學書電子化計劃の[『晉書』](https://ctext.org/wiki.pl?if=gb&res=788577)……画像版へのリンクもあり、本文の個別の段落にパーマリンクもできるので便利。
* 早稲田大学古典籍総合データベースの[『晋書』](https://www.wul.waseda.ac.jp/kotenseki/html/ri08/ri08_01735_0063/index.html)……画像
* 国立国会図書館デジタルコレクションで和刻本正史シリーズの『晋書』（[1](https://dl.ndl.go.jp/pid/12212167)・[2](https://dl.ndl.go.jp/pid/12212101)・[3](https://dl.ndl.go.jp/pid/12212100)）を閲覧できる。

### 『晉書斠注』
* Internet Archive の[『晉書斠注』](https://archive.org/search.php?query=%E6%99%89%E6%9B%B8%E6%96%A0%E6%B3%A8)……画像。
* Wikimedia Commons の[『晉書斠注』](https://commons.wikimedia.org/w/index.php?search=%E6%99%89%E6%9B%B8%E6%96%A0%E6%B3%A8&title=Special%3ASearch&go=Go&ns0=1&ns6=1&ns12=1&ns14=1&ns100=1&ns106=1)……画像。
* 中國哲學書電子化計劃の[『晉書斠注』](https://ctext.org/library.pl?if=gb&res=1681)……画像。
* [正史三國志硏究會](http://suisui.ojaru.jp)……『晉書斠注』の一部が文字データとして公開されている。
* [PDFあり]
  [吳士鑑「晉書斠注序」譯註稿](http://doi.org/10.34382/00014555)
  （赤羽 奈津子・猪俣 貴幸）

### 『晉書校勘記』
* Internet Archive の『晉書校勘記』
  [(一)](https://archive.org/details/02077716.cn)・
  [(二)](https://archive.org/details/02077717.cn)
  （周家祿）……画像。

### 『晉書音義』
* Internet Archive の[『晉書音義』](https://archive.org/details/06079130.cn/page/n1/mode/2up)……画像。
* Wikisource の『晉書音義』
  [序](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%BA%8F)・
  [卷之上](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%8D%B7%E4%B9%8B%E4%B8%8A)・
  [卷之中](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%8D%B7%E4%B9%8B%E4%B8%AD)・
  [卷之下](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E9%9F%B3%E7%BE%A9%E5%8D%B7%E4%B9%8B%E4%B8%8B)

### 『四庫全書總目提要』
* Wikisource の『四庫全書總目提要』のうち、[『晉書』の箇所](https://zh.wikisource.org/wiki/%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E7%B8%BD%E7%9B%AE%E6%8F%90%E8%A6%81/%E5%8D%B7045#%E6%99%89%E6%9B%B8%E2%80%A2%E4%B8%80%E7%99%BE%E4%B8%89%E5%8D%81%E5%8D%B7%E3%80%88%E5%85%A7%E5%BA%9C%E5%88%8A%E6%9C%AC%E3%80%89)

### 人物のリスト
* Wikipedia (ja) の[「晋書」のページ](https://ja.wikipedia.org/wiki/%E6%99%8B%E6%9B%B8#%E5%86%85%E5%AE%B9)……『晋書』に立伝されている人物のページへのリンク集として使える気がする。また、リンク先の個々の人物のページは、列伝の現代語訳に近い場合もあるので、漢文の読み方がわからないときのヒントとしても有用。
* Wikipedia (zh) の[「晋书」のページ](https://zh.wikipedia.org/wiki/%E6%99%8B%E4%B9%A6#%E3%80%8A%E6%99%8B%E4%B9%A6%E3%80%8B%E7%9B%AE%E5%BD%95)……同上（ただし中国語）。

### 現代中国語訳
* [白话晋书](https://www.duguoxue.com/ershisishi/baihuajinshu/)……『晋書』の現代中国語訳。
* [「古诗大全」というサイト](https://www.gushimi.org/)での『晋書』の現代中国語訳……目次が[《50章》](https://www.gushimi.org/24shi/jinshu/50/)、[《100章》](https://www.gushimi.org/24shi/jinshu/100/)、[《150章》](https://www.gushimi.org/24shi/jinshu/150/)と分かれている。
* [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/mode/2up)

### 各種考証
* [武英殿本二十三史考證（十五）](https://commons.wikimedia.org/wiki/File:CADAL02035620_%E6%AD%A6%E8%8B%B1%E6%AE%BF%E6%9C%AC%E4%BA%8C%E5%8D%81%E4%B8%89%E5%8F%B2%E8%80%83%E8%AD%89%EF%BC%88%E5%8D%81%E4%BA%94%EF%BC%89.djvu)
* 国立国会図書館デジタルコレクションの『続国訳漢文大成』に所収の『廿二史劄記』で[『晋書』を論じている箇所](https://dl.ndl.go.jp/info:ndljp/pid/1239993/137)
* 同じくデジコレにある李慈銘編『越縵堂読史札記. 晋書 宋書 梁書 魏書 隋書 南、北史』で[『晋書』を論じている箇所](https://dl.ndl.go.jp/info:ndljp/pid/1912194/4)
* Internet Archive の『廿二史考異』（銭大昕）（画像）で『晋書』を論じている部分（[八](https://archive.org/details/02082255.cn/page/n52/mode/2up)・[九](https://archive.org/details/02082256.cn/page/n2/mode/2up)・[十](https://archive.org/details/02082257.cn/page/n2/mode/2up)）
* Internet Archive の『十七史商榷』（王鳴盛）（画像）で『晋書』を論じている部分
	* [巻43](https://archive.org/details/02082209.cn/page/n57/mode/2up)
	* [巻44](https://archive.org/details/02082210.cn/page/n1/mode/2up)
	* [巻45](https://archive.org/details/02082210.cn/page/n25/mode/2up)
	* [巻46](https://archive.org/details/02082210.cn/page/n47/mode/2up)
	* [巻47](https://archive.org/details/02082211.cn/page/n1/mode/2up)
	* [巻48](https://archive.org/details/02082211.cn/page/n25/mode/2up)
	* [巻49](https://archive.org/details/02082211.cn/page/n45/mode/2up)
	* [巻50](https://archive.org/details/02082211.cn/page/n61/mode/2up)
	* [巻51](https://archive.org/details/02082212.cn/page/n1/mode/2up)
	* [巻52](https://archive.org/details/02082212.cn/page/n23/mode/2up)
* Wikisource の『陔餘叢考』（趙翼）の[「《晉書》舛訛」](https://zh.wikisource.org/wiki/%E9%99%94%E9%A4%98%E5%8F%A2%E8%80%83/06#%E3%80%8A%E6%99%89%E6%9B%B8%E3%80%8B%E8%88%9B%E8%A8%9B)
* Internet Archive の『讀史舉正』（張熷）（画像）で『晋書』を論じている部分（[二](https://archive.org/details/02082291.cn/page/n14/mode/2up)・[三](https://archive.org/details/02082292.cn/page/n2/mode/2up)）
* Internet Archive の[『諸史拾遺』](https://archive.org/details/02082288.cn/page/n22/mode/2up)（錢大昕）
* Internet Archive の[『十駕齋養新錄』](https://archive.org/details/02096316.cn/page/n26/mode/2up)（錢大昕）
* Internet Archive の[『惜抱軒筆記』](https://archive.org/details/02096347.cn/page/n24/mode/2up)（姚鼐）
* Internet Archive の[『諸史考異』](https://archive.org/details/02082294.cn/page/n42/mode/2up)（洪頤煊）
* Internet Archive の[『瞥記』](https://archive.org/details/02096461.cn/page/n58/mode/2up)（梁玉繩）
* Internet Archive の[『讀史糾謬』](https://archive.org/details/02082167.cn/mode/2up)（牛運震）
* [Wikisource の『日知錄集釋』](https://zh.wikisource.org/wiki/%E6%97%A5%E7%9F%A5%E9%8C%84%E9%9B%86%E9%87%8B)（著：顧炎武、釋：黄汝成）を[「晉書」で検索した結果](https://zh.wikisource.org/w/index.php?search=%22%E6%99%89%E6%9B%B8%22&prefix=%E6%97%A5%E7%9F%A5%E9%8C%84%E9%9B%86%E9%87%8B&title=Special:%E6%90%9C%E7%B4%A2&profile=advanced&fulltext=1)
    * Internet Archive の[画像版](https://archive.org/search.php?query=%E6%97%A5%E7%9F%A5%E9%8C%84%E9%9B%86%E9%87%8B%20%E9%BB%83%E6%B1%9D%E6%88%90)

## 西晋・東晋・五胡十六国に関係ある色々なもの
* Wikipedia (zh) の[「Category:晉朝人列表」のページ](https://zh.wikipedia.org/wiki/Category:%E6%99%89%E6%9C%9D%E4%BA%BA%E5%88%97%E8%A1%A8)……西晋・東晋の人物を種類別にまとめた各種の人物一覧表へのリンク集。
* Wikipedia (zh) の[「Category:五胡十六國人列表」のページ](https://zh.wikipedia.org/wiki/Category:%E4%BA%94%E8%83%A1%E5%8D%81%E5%85%AD%E5%9C%8B%E4%BA%BA%E5%88%97%E8%A1%A8)……五胡十六国に関する同様のリンク集。
* Wikipedia (zh) の[「Category:中国士族世系图」のページ](https://zh.wikipedia.org/wiki/Category:%E4%B8%AD%E5%9B%BD%E5%A3%AB%E6%97%8F%E4%B8%96%E7%B3%BB%E5%9B%BE)……系図の一覧表
* [転転大陸](http://home.att.ne.jp/iota/ten_tairiku/)……「二十五史家系図」と「皇帝（王）家系図」がある。
* 中央研究院・數位文化中心の[兩千年中西曆轉換](https://sinocal.sinica.edu.tw/)……日付の変換に便利。
* [浙江大學圖書館古籍碑帖研究墓與保護中心 中國歴代墓誌數據庫](http://csid.zju.edu.cn/tomb/stone)……墓誌データベース。ものによって拓本画像が見られたり、文字の書き起こしが見られたりする。
* 中國哲學書電子化計劃の[『漢魏南北朝墓志匯編』](https://ctext.org/wiki.pl?if=gb&res=872825)
* Wikimedia の『石刻史料新編 第三輯 一般類 漢魏南北朝墓誌集釋 上』のうち、晋に該当する箇所
	* [目次](https://commons.wikimedia.org/w/index.php?title=File:SSID-10486416_%E7%9F%B3%E5%88%BB%E5%8F%B2%E6%96%99%E6%96%B0%E7%B7%A8_%E7%AC%AC%E4%B8%89%E8%BC%AF_%E4%B8%80%E8%88%AC%E9%A1%9E_%E6%BC%A2%E9%AD%8F%E5%8D%97%E5%8C%97%E6%9C%9D%E5%A2%93%E8%AA%8C%E9%9B%86%E9%87%8B_%E4%B8%8A.pdf&page=12)
	* [説明](https://commons.wikimedia.org/w/index.php?title=File:SSID-10486416_%E7%9F%B3%E5%88%BB%E5%8F%B2%E6%96%99%E6%96%B0%E7%B7%A8_%E7%AC%AC%E4%B8%89%E8%BC%AF_%E4%B8%80%E8%88%AC%E9%A1%9E_%E6%BC%A2%E9%AD%8F%E5%8D%97%E5%8C%97%E6%9C%9D%E5%A2%93%E8%AA%8C%E9%9B%86%E9%87%8B_%E4%B8%8A.pdf&page=39)
	* [図版](https://commons.wikimedia.org/w/index.php?title=File:SSID-10486416_%E7%9F%B3%E5%88%BB%E5%8F%B2%E6%96%99%E6%96%B0%E7%B7%A8_%E7%AC%AC%E4%B8%89%E8%BC%AF_%E4%B8%80%E8%88%AC%E9%A1%9E_%E6%BC%A2%E9%AD%8F%E5%8D%97%E5%8C%97%E6%9C%9D%E5%A2%93%E8%AA%8C%E9%9B%86%E9%87%8B_%E4%B8%8A.pdf&page=279)
* Wikisource の『資治通鑑』（[巻79・晋紀1](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7079) 〜 [巻118・晋紀40](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7118)）
* 国立国会図書館デジタルコレクションの『続国訳漢文大成』に所収の『資治通鑑』（[巻79・晋紀1](https://dl.ndl.go.jp/info:ndljp/pid/1239900/99) 〜 [巻118・晋紀40](https://dl.ndl.go.jp/info:ndljp/pid/1239942/160)）
* 同じくデジコレの[『魏晋南北朝通史』](https://dl.ndl.go.jp/info:ndljp/pid/1917952?tocOpened=1)（岡崎文夫）……このうち[内編](https://www.heibonsha.co.jp/search/?search_word=%E9%AD%8F%E6%99%8B%E5%8D%97%E5%8C%97%E6%9C%9D%E9%80%9A%E5%8F%B2&x=0&y=0&search_menu=keyword)は平凡社東洋文庫としても出版されている。
* 同じくデジコレの『国訳漢文大成』に所収の[『国訳搜神記』](https://dl.ndl.go.jp/info:ndljp/pid/1913008/47)と[『国訳搜神後記』](https://dl.ndl.go.jp/info:ndljp/pid/1913008/79)……志怪だけれど、『晋書』とたまにネタが被っている。なおこれは部分訳。
* Wikisource の[『搜神記』](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E8%A8%98)あるいは中國哲學書電子化計劃の[『搜神記』](https://ctext.org/wiki.pl?if=gb&res=839038)
* Wikisource の[『搜神後記』](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E5%BE%8C%E8%A8%98)あるいは中國哲學書電子化計劃の[『搜神後記』](https://ctext.org/wiki.pl?if=gb&res=184634)
* デジコレにある各社の『蒙求』……『晋書』に出てくる人物に関する句の注として『晋書』が引かれることがそれなりにあるので、それにより列伝（の一部）の訓読がわかる。[有朋堂書店「漢文叢書」のもの](https://dl.ndl.go.jp/info:ndljp/pid/958947)、[博文館「漢文叢書」のもの](https://dl.ndl.go.jp/info:ndljp/pid/950174)、[研究社「研究社学生文庫」のもの](https://dl.ndl.go.jp/info:ndljp/pid/1111036)がある。
* [「魯迅『古小説鈎沈』校本」](http://hdl.handle.net/2433/227151)（中嶋長文・伊藤令子・平田昌司）
* Wikisource の[『世説新語』](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E)……『晋書』とよくネタが被っている。注が別の色になっていて見やすい。
* 中國哲學書電子化計劃の[『世説新語』](https://ctext.org/shi-shuo-xin-yu/zh)……条ごとにパーマリンクをはれるので便利。
* 中國哲學書電子化計劃の[『世説新語箋疏』](https://ctext.org/wiki.pl?if=gb&res=40889)
* [「世説新語 on the Web」](https://kakuyomu.jp/works/1177354054884883338)……『世説新語』全話がウェブ小説として読める。
* [「続三国志演義─通俗續三國志─」](https://kakuyomu.jp/works/1177354054884106768)……曰く、「三國志演義につづけ！と明代に書かれた二番煎じの二次創作『新刻續編三國志』、江戸時代に中村昂然が読み下した三番煎じを好事家のために超訳した四番煎じ」だそうで。
* [「続三国志演義II─通俗續後三國志前編─」](https://kakuyomu.jp/works/1177354054884740663)……その続き（第二弾）。
* [「続三国志演義III─通俗續後三國志後編─」](https://kakuyomu.jp/works/1177354054887842567)……その続き（第三弾）。
* Wikisource の[『九家舊晉書』](https://zh.wikisource.org/wiki/%E4%B9%9D%E5%AE%B6%E8%88%8A%E6%99%89%E6%9B%B8)
* Wikisource の[『晉紀』](https://zh.wikisource.org/wiki/%E6%99%89%E7%B4%80)（干宝）
* Wikisource の[『建康實錄』](https://zh.wikisource.org/wiki/%E5%BB%BA%E5%BA%B7%E5%AF%A6%E9%8C%84)
* 中華電子佛典協會 (Chinese Buddhist Electronic Text Association; CBETA) の[『高僧伝』](https://cbetaonline.dila.edu.tw/zh/T2059_001) ないし Wikisource の[『梁高僧伝』](https://zh.wikisource.org/wiki/%E6%A2%81%E9%AB%98%E5%83%A7%E5%82%B3)
* CBETA の[『弘明集』](https://cbetaonline.dila.edu.tw/zh/T2102) ないし Wikisource の[『弘明集』](https://zh.wikisource.org/wiki/%E5%BC%98%E6%98%8E%E9%9B%86) ないし 中國哲學書電子化計劃の[『弘明集』](https://ctext.org/wiki.pl?if=gb&res=393424)
* CBETA の[『比丘尼傳』卷第一](https://cbetaonline.dila.edu.tw/zh/T2063_001)……この巻は晋に対応する。
* Wikisource の[『全晉文』](https://zh.wikisource.org/wiki/%E5%85%A8%E6%99%89%E6%96%87)ないし中國哲學書電子化計劃の[『全晉文』](https://ctext.org/wiki.pl?if=gb&res=614696)
* Wikisource の[『昭明文選』](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8)
* デジコレの『国訳漢文大成』に所収の『文選』[上](https://dl.ndl.go.jp/info:ndljp/pid/1912763)・[中](https://dl.ndl.go.jp/info:ndljp/pid/1912796)・[下](https://dl.ndl.go.jp/info:ndljp/pid/1912835)
* 中文百科在线の[『先秦漢魏晉南北朝詩』](http://www.zwbk.org/zh-tw/Lemma_Show/201846.aspx)
* [魏晋南北ブログ](http://gishinnanboku.blog.fc2.com/)
* [五胡十六国の歴史を語るブログ](http://blog.livedoor.jp/masudazigo-2nd/)
* 早稲田大学古典籍総合データベースの[『十六国彊域志』](https://www.wul.waseda.ac.jp/kotenseki/html/ru05/ru05_01386/index.html)（洪亮吉）（画像）
* 早稲田大学古典籍総合データベースの[『水経注図』](https://www.wul.waseda.ac.jp/kotenseki/html/ru05/ru05_03617/index.html)（楊守敬・熊会貞）（画像）
* Wikisource の[『水經注』](https://zh.wikisource.org/wiki/%E6%B0%B4%E7%B6%93%E6%B3%A8)（酈道元）
* [「『法苑珠林』の總合的硏究 - 主として『法苑珠林』所錄『冥祥記』の本文校訂竝びに選注選譯 - : 『法苑珠林』所錄『冥祥記』の本文校訂竝びに選注選譯」](http://id.nii.ac.jp/1374/00000070/)（若槻俊秀・佐藤義寛・乾源俊・浦山あゆみ・今場正美・福井敏・本井牧子・仁木夏實・早川智美・長谷川愼・稻垣淳央・藤井雅彥・大角紘一・一澤美帆・嘉村誠）
* CBETA の[『法苑珠林』](https://cbetaonline.dila.edu.tw/zh/T2122)
* Wikisource の[『冥祥記』](https://zh.wikisource.org/wiki/%E5%86%A5%E7%A5%A5%E8%A8%98)
* Wikisource の[『太平廣記』](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98)
* Wikisource の[『太平御覽』](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%BD)
* Wikisource にある陶弘景の[『真誥』](https://zh.wikisource.org/wiki/%E7%9C%9F%E8%AA%A5)
* [「眞誥研究 : 譯注篇」](http://hdl.handle.net/2433/98008)（吉川忠夫・麥谷邦夫）
* [『漢魏六朝百三家集』へのリンク](https://gitlab.com/miyama__akira/pages/-/blob/master/md/103.md)
* [画像版『全晉文』へのリンク集](https://miyama__akira.gitlab.io/pages/prose-list/Jin.html)
* [書籍 (IA)] 『補歷代史表』（萬斯同）
    * [卷十二（晉諸王世表）](https://archive.org/details/06059532.cn/page/n15/mode/2up)
    * [卷十三（晉功臣世表）](https://archive.org/details/06059532.cn/page/n39/mode/2up)
    * [卷十四（晉將相大臣年表）](https://archive.org/details/06059532.cn/page/n95/mode/2up)
    * [卷十五（東晉將相大臣年表）](https://archive.org/details/06059533.cn/page/n1/mode/2up)
    * [卷十六（晉方鎮年表）](https://archive.org/details/06059533.cn/page/n55/mode/2up)
    * [卷十七（東晉方鎮年表）](https://archive.org/details/06059533.cn/page/n83/mode/2up)
    * [卷十八（僭偽諸國世表）](https://archive.org/details/06059534.cn/page/n1/mode/2up)
    * [卷十九（僭位諸國年表）](https://archive.org/details/06059534.cn/page/n15/mode/2up)
    * [卷二十（偽漢將相大臣年表）](https://archive.org/details/06059534.cn/page/n53/mode/2up)
    * [卷二十一（偽成將相大臣年表）](https://archive.org/details/06059534.cn/page/n65/mode/2up)
    * [卷二十二（偽趙將相大臣年表）](https://archive.org/details/06059534.cn/page/n79/mode/2up)
    * [卷二十三（偽燕將相大臣年表）](https://archive.org/details/06059534.cn/page/n91/mode/2up)
    * [卷二十四（偽秦將相大臣年表）](https://archive.org/details/06059534.cn/page/n103/mode/2up)
    * [卷二十五（偽後秦將相大臣年表）](https://archive.org/details/06059535.cn/page/n1/mode/2up)
    * [卷二十六（偽後燕將相大臣年表）](https://archive.org/details/06059535.cn/page/n11/mode/2up)
    * [卷二十七（偽南燕將相大臣年表）](https://archive.org/details/06059535.cn/page/n19/mode/2up)
    * [卷二十八（宋諸王世表）](https://archive.org/details/06059535.cn/page/n25/mode/2up)の[「附二王後」](https://archive.org/details/06059535.cn/page/n37/mode/2up)に零陵王（司馬氏）の系譜あり。
    * [卷三十一（齊諸王世表）](https://archive.org/details/06059535.cn/page/n103/mode/2up)の[末尾](https://archive.org/details/06059535.cn/page/n117/mode/2up)にも零陵王（司馬氏）の記載あり。

## 仮名漢字変換辞書の元データ
各人の環境に合わせてフィールドの並び順や改行コードなどを調整する必要はあるが（また、必ずしも網羅的データではないかもしれないが）、『晋書』で扱っている時期に登場する人たちの名前の仮名漢字変換辞書の元データとして使えるものを挙げておく。

* 佐藤氏の[晋宋春秋＆中原戦記の人名＆読みリスト](https://jinsung.chronicle.wiki/d/%C3%CF%CC%BE%BA%F7%B0%FA)
* 佐藤氏の[世説新語索引　素データ](https://jinsung.chronicle.wiki/d/%C0%A4%C0%E2%BF%B7%B8%EC%BA%F7%B0%FA%A1%A1%C1%C7%A5%C7%A1%BC%A5%BF)、ないしは、それを[元にして](https://gitlab.com/miyama__akira/pages/-/tree/master/data/sesetsu-shingo)生成した[人名辞書](https://gitlab.com/miyama__akira/pages/-/blob/master/data/sesetsu-shingo/personal-name-dict-generated-from-rawdata.csv)
* 自作の[『晋書』人名辞書](dict/names-of-people.txt)……対象人物は、このページで見出しに立てている人たち。形式は下記の『世説新語』人名辞書と同じ。複数の読みができそうな人には複数の読みを振り、複数の字体（「恵」と「惠」など）があるものはいずれも変換できるようにした（つもり）。
* 自作の[『世説新語』人名辞書](https://gitlab.com/miyama__akira/pages/-/blob/master/data/sesetsu-shingo/personal-name-dict.txt)と[その説明ページ](https://miyama__akira.gitlab.io/pages/sesetsu-shingo/personal-name-dict.html)

## リンク先の情報源の種別
以下では、リンク先の情報源の種別を次のように表す。

* 人物名をキーワードにした[CiNii Research](https://cir.nii.ac.jp)の検索結果ページへのリンクは、「CiNii Research検索」と示す。
* 学術雑誌の記事は「書誌のみ」または「PDFあり」と示す。「書誌のみ」のもののリンク先はCiNii Researchのページ。
* 国立国会図書館デジタルコレクションで公開されている書籍は「書籍 (NDL)」と示す。
公開範囲が「インターネット公開（保護期間満了）」のものに加えて、[個人向けデジタル化資料送信サービス](https://www.ndl.go.jp/jp/use/digital_transmission/individuals_index.html)により閲覧可能なものも含む。
* Internet Archive で公開されている書籍（漢籍）は「書籍 (IA)」と示す。
* 紙媒体の書籍をリストに含めるかどうかは考え中だが、含めるとしたら、国会図書館の書誌情報のページにリンクして、「紙書籍」と示す予定。
* 「ブログ」という大雑把な呼び方が適切なのかはやや悩むところだが、個人のウェブサイトやブログの類には「ブログ」と示す。
ブログ全体の名称のみを示すか、あるいは、個別記事の題名の後に「
（@ブログ全体の名称）」のように追記する。
「解體晉書」などのframeタグを使ったサイトは、トップページにリンクする。

<!--
* [書誌のみ]
  []()
  （）
* [PDFあり]
  []()
  （）
* [ブログ]
  []()
  （@）
* [紙書籍]
  []()
  （）
* [書籍 (NDL)]
  []()
  （）
-->

<hr>

## 紀

### 巻1 帝紀第1
#### 高祖宣帝（司馬懿）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%87%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%87%BF)
* [書籍 (IA)]
  [晉書斠注(一)](https://archive.org/details/02077718.cn/page/n84/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/n33/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「晉宣狼顧」（第51句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/47)
* [ブログ]
  [『晋書』宣帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/06/24/000100)〜
  [『晋書』宣帝紀を読んでみよう:その25](https://t-s.hatenablog.com/entry/2019/07/18/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [司馬懿伝/上。仲達が曹操より怖いもの](http://3guozhi.net/hito/bai1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [司馬懿伝/中。諸葛亮が分からない](http://3guozhi.net/hito/i1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [司馬懿伝/下「魏臣としての敗北者」](http://3guozhi.net/q/ctc1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m001.html)
* [ブログ]
  [宣帝紀](https://estar.jp/novels/25594548/viewer?page=2)
  （@淡々晋書）
* [ブログ]
  [晋書　高祖宣帝懿紀](http://strawberrymilk.gooside.com/text/shinjo/01-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p10)

### 巻2 帝紀第2
#### 世宗景帝（司馬師）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B8%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B8%AB)
* [書籍 (IA)]
  [晉書斠注(二)](https://archive.org/details/02077719.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/21/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』景帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/07/22/000100)〜
  [『晋書』景帝紀を読んでみよう:その11](https://t-s.hatenablog.com/entry/2019/08/07/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [ドＭ仲達の長男はドＳ。司馬師伝](http://3guozhi.net/hito/sibasi1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m002.html)

#### 太祖文帝（司馬昭）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%98%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%98%AD)
* [書籍 (IA)]
  [晉書斠注(二)](https://archive.org/details/02077719.cn/page/n30/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/27/mode/2up)
* [ブログ]
  [『晋書』文帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/08/12/000100)〜
  [『晋書』文帝紀を読んでみよう:その23](https://t-s.hatenablog.com/entry/2019/09/10/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [逆天下三分、逆出師ノ表。司馬昭伝](http://3guozhi.net/hito/sibasyo1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m002.html)
* [『三國志』巻4（高貴鄉公髦）](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B704#%E9%AB%98%E8%B2%B4%E9%84%89%E5%85%AC)の注にある、有名な「司馬昭之心，路人所知也。」という台詞を引用する記述：
    * 北宋で起きた[同文館の獄](https://ja.wikipedia.org/wiki/%E5%90%8C%E6%96%87%E9%A4%A8%E3%81%AE%E7%8D%84)において、「司馬昭之心，路人所知」云々とある文及甫の書状の意味をめぐる争いがあったことが、『宋史』の[巻200（刑法志）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7200)・[巻340（劉摯）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7340#%E5%8A%89%E6%91%AF)・[巻471（安惇）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7471#%E5%AE%89%E6%83%87)に記されている。
    * [『明史』巻246（王允成）](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7246)でも「司馬昭之心，路人知之矣」と引用している。
    * [『朝鮮王朝實錄』（純祖實錄）の十二年十一月八日](https://zh.wikisource.org/wiki/%E6%9C%9D%E9%AE%AE%E7%8E%8B%E6%9C%9D%E5%AF%A6%E9%8C%84/%E7%B4%94%E7%A5%96%E5%AF%A6%E9%8C%84/%E5%8D%81%E4%BA%8C%E5%B9%B4#11%E6%9C%888%E6%97%A5)にも「及夫後來，其侄龜柱之凶言有繼發，則唱喁相應，脈絡相續，如印一板，如貫一串，司馬昭之心，路人皆知之矣。吁！亦懍乎危哉。」とある。


### 巻3 帝紀第3
#### 世祖武帝（司馬炎）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%82%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%82%8E)
* [書籍 (IA)]
  [晉書斠注(三)](https://archive.org/details/02077720.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/41/mode/2up)
* [ブログ]
  [『晋書』武帝紀を読んでみよう:その1](https://t-s.hatenablog.com/entry/2019/09/11/000100)〜
  [『晋書』武帝紀を読んでみよう:その49](https://t-s.hatenablog.com/entry/2019/11/25/000100)
  （@てぃーえすのメモ帳）
* [ブログ]
  [巻三　帝紀第三　武帝紀（1）](https://readingnotesofjinshu.com/translation/annals/vol-3_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/annals/vol-3_2)、
  [同（3）](https://readingnotesofjinshu.com/translation/annals/vol-3_3)
  （@晋書簡訳所）
* [ブログ]
  [武帝紀](http://strawberrymilk.gooside.com/text/shinjo/03-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [周曇の「詠史詩」の中の「晉武帝」と「再吟」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%99%89%E6%AD%A6%E5%B8%9D)
* [王夫之『讀通鑒論』巻11](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B711)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/324)）
* [PDFあり]
  [中国の故事・伝説に取材した大小暦](https://dl.ndl.go.jp/info:ndljp/pid/3051277)（相島 宏）
……[「日本の暦」展](https://www.ndl.go.jp/koyomi/index.html)に関連した論文。
この論文中の「晋武帝羊車遊宴図」の出典は[『恵合余見』第二冊](https://dl.ndl.go.jp/info:ndljp/pid/1286782/19)。
類似の「晋武帝羊車遊宴図」は、[『和漢名画苑』](https://www.kanazawa-bidai.ac.jp/edehon-db/quick.cgi?mode=result&id=83)の[四巻](https://www.kanazawa-bidai.ac.jp/edehon-db/img/83/wakan-meigaen04d021a.jpg)でも見られる。
論文中に引かれた[『通俗続三國志』](https://dl.ndl.go.jp/info:ndljp/pid/775963/25)には、[現代語訳](https://kakuyomu.jp/works/1177354054884106768/episodes/1177354054884109208)もある。

### 巻4 帝紀第4
#### 孝恵帝（司馬衷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A1%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A1%B7)
* [書籍 (IA)]
  [晉書斠注(四)](https://archive.org/details/02077721.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/67/mode/2up)
* [ブログ]
  [巻四　帝紀第四　恵帝紀（1）](https://readingnotesofjinshu.com/translation/annals/vol-4_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/annals/vol-4_2)
  （@晋書簡訳所）
* [ブログ]
  [恵帝紀](http://strawberrymilk.gooside.com/text/shinjo/04-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m004.html)
* [周曇の「詠史詩」の中の「惠帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%83%A0%E5%B8%9D)
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%83%A0%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/340)）


### 巻5 帝紀第5
#### 孝懐帝（司馬熾）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%86%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%86%BE)
* [書籍 (IA)]
  [晉書斠注(四)](https://archive.org/details/02077721.cn/page/n60/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/83/mode/2up)
* [ブログ]
  [巻五　帝紀第五　懐帝紀](https://readingnotesofjinshu.com/translation/annals/vol-5_1)
  （@晋書簡訳所）
* [ブログ]
  [孝懐帝紀](http://strawberrymilk.gooside.com/text/shinjo/05-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m005.html)
* [周曇の「詠史詩」の中の「懷帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%87%B7%E5%B8%9D)
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%87%B7%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/352)）


#### 孝愍帝（司馬鄴）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%84%B4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%84%B4)
* [書籍 (IA)]
  [晉書斠注(四)](https://archive.org/details/02077721.cn/page/n94/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/90/mode/2up)
* [ブログ]
  [巻五　帝紀第五　愍帝紀](https://readingnotesofjinshu.com/translation/annals/vol-5_2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m005.html)
* [周曇の「詠史詩」の中の「愍帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%84%8D%E5%B8%9D)
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%B9%A3%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/359)）

### 巻6 帝紀第6
#### 中宗元帝（司馬睿）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%9D%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%9D%BF)
* [書籍 (IA)]
  [晉書斠注(五)](https://archive.org/details/02077722.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/103/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六　帝紀第六　元帝紀（1）](https://readingnotesofjinshu.com/translation/annals/vol-6_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/annals/vol-6_2)
  （@晋書簡訳所）
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)
* [ブログ]
  [『魏書』巻九十六　列伝第八十四　僭晋司馬叡（1）](https://readingnotesofjinshu.com/translation/appendix/bookofwei-vol96_1)
  （@晋書簡訳所）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、元帝紀に引かれた
  [「勸進表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/412)（劉琨によるもの）がある。
* [王夫之『讀通鑒論』巻12](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B712#%E6%9D%B1%E6%99%89%E5%85%83%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/365)）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m006.html)

#### 粛宗明帝（司馬紹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B4%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B4%B9)
* [書籍 (IA)]
  [晉書斠注(五)](https://archive.org/details/02077722.cn/page/n44/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/116/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六　帝紀第六　明帝紀](https://readingnotesofjinshu.com/translation/annals/vol-6_3)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh1.html)
  （@いつか書きたい『三国志』）
* [『文選』所収の王巾「頭陀寺碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%A0%AD%E9%99%80%E5%AF%BA%E7%A2%91%E6%96%87)にある「漢晉兩明，並勒丹青之飾。」（『国訳漢文大成　文学部第四巻　文選下巻』の[該当ページ](https://dl.ndl.go.jp/info:ndljp/pid/1912835/388)）には、明帝が自ら仏画を描いた話が反映されている（この話は『晋書』巻77・蔡謨伝に載っている）。
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E6%98%8E%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/372)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m006.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

### 巻7 帝紀第7
#### 顕宗成帝（司馬衍）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A1%8D)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/125/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻七　帝紀第七　成帝紀](https://readingnotesofjinshu.com/translation/annals/vol-7_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E6%88%90%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/373)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m007.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 康帝（司馬岳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B2%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B2%B3)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n36/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/135/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh11.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻七　帝紀第七　康帝紀](https://readingnotesofjinshu.com/translation/annals/vol-7_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E5%BA%B7%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/387)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m007.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

### 巻8 帝紀第8
#### 孝宗穆帝（司馬聃）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%81%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%81%83)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n48/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/139/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh12.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻八　帝紀第八　穆帝紀](https://readingnotesofjinshu.com/translation/annals/vol-8_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻13](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B713#%E7%A9%86%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/389)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m008.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 哀帝（司馬丕）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%B8%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%B8%95)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n74/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/149/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh17.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻八　帝紀第八　哀帝紀](https://readingnotesofjinshu.com/translation/annals/vol-8_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%93%80%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/396)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m008.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 廃帝海西公（司馬奕）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(六)](https://archive.org/details/02077723.cn/page/n84/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/152/mode/2up)
* [ブログ]
  [『晋書』東晋の本紀を、贅沢に訳す](http://3guozhi.net/q/tsh18.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻八　帝紀第八　海西公紀](https://readingnotesofjinshu.com/translation/annals/vol-8_3)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%B8%9D%E5%A5%95)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/399)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m008.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

### 巻9 帝紀第9
#### 太宗簡文帝（司馬昱）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%98%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%98%B1)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/159/mode/2up)
* [ブログ]
  [『晋書』帝紀９、簡文帝と孝武帝の翻訳](http://3guozhi.net/q/tkq1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻九　帝紀第九　簡文帝紀](https://readingnotesofjinshu.com/translation/annals/vol-9_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E7%B0%A1%E6%96%87%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/402)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m009.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 孝武帝（司馬曜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9B%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9B%9C)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n18/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/163/mode/2up)
* [ブログ]
  [『晋書』帝紀９、簡文帝と孝武帝の翻訳](http://3guozhi.net/q/tkq4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [「紫微垣」晋書孝武帝本紀他](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854995)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [巻九　帝紀第九　孝武帝紀](https://readingnotesofjinshu.com/translation/annals/vol-9_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%AD%9D%E6%AD%A6%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/402)）
* [ブログ]
  晋書９　　孝武帝本紀（[孝武１　　３８４年　上](https://kakuyomu.jp/works/16816452219408440529/episodes/16816927860938784461)〜[孝武１５　３９６年](https://kakuyomu.jp/works/16816452219408440529/episodes/16816927861629857675)）
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m009.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)


### 巻10 帝紀第10
#### 安帝（司馬徳宗）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E5%AE%97)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n56/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/179/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%C4%EB%B5%AA%C2%E8%BD%BD%A1%A1%B0%C2%C4%EB%A1%A1%BB%CA%C7%CF%F9%FE%BD%A1)
* [ブログ]
  [『晋書』帝紀１０、安帝と恭帝の翻訳](http://3guozhi.net/q/tkj1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻十　帝紀第十　安帝紀](https://readingnotesofjinshu.com/translation/annals/vol-10_1)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E5%AE%89%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/413)）
* [ブログ]
  晋書１０　安帝恭帝本紀（[安帝１　　３９７年](https://kakuyomu.jp/works/16816452219408440529/episodes/16816927861860662405)〜[安帝２２　４１８年　弑殺](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864862172)）
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m010.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

#### 恭帝（司馬徳文）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BE%B3%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(七)](https://archive.org/details/02077724.cn/page/n100/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/190/mode/2up)
* [ブログ]
  [『晋書』帝紀１０、安帝と恭帝の翻訳](http://3guozhi.net/q/tkj6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻十　帝紀第十　恭帝紀](https://readingnotesofjinshu.com/translation/annals/vol-10_2)
  （@晋書簡訳所）
* [王夫之『讀通鑒論』巻14](https://zh.wikisource.org/wiki/%E8%AE%80%E9%80%9A%E9%91%92%E8%AB%96/%E5%8D%B714#%E6%81%AD%E5%B8%9D)
  （訓読と注は[『続国訳漢文大成 経子史部 第23巻』](https://dl.ndl.go.jp/info:ndljp/pid/1239997/434)）
* [ブログ]
  晋書１０　安帝恭帝本紀（[恭帝１　　司馬徳文](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864875057)〜[恭帝２　　４１９年　元熙](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139555031262005)）
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m010.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E5%8F%B8%E9%A6%AC%E5%8F%A1)

## 志
### 巻11 志第1 天文上
* [書籍 (IA)]
  [晉書斠注(八)](https://archive.org/details/02077725.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [天文上](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/195/mode/2up)
    * [天體](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/196/mode/2up)
    * [儀象](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/202/mode/2up)
    * [天文經星](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/206/mode/2up)
    * [中宮](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/207/mode/2up)
    * [二十八舍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/218/mode/2up)
    * [星官在二十八宿之外者](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/222/mode/2up)
    * [天漢起沒](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/225/mode/2up)
    * [十二次度數](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/225/mode/2up)
    * [州郡躔次](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/227/mode/2up)
* [書籍 (NDL)]
  [『世界の名著』続1巻](https://dl.ndl.go.jp/info:ndljp/pid/2935195)所収
  「晋書天文志」
  （山田慶児・坂出祥伸・藪内清訳）
  ……「収録著作について」の中の[「『晋書』天文志」](https://dl.ndl.go.jp/info:ndljp/pid/2935195/33)のセクションも参考。
  「天文志」の中の下位セクションごとに、訳文と注へのリンクをまとめておく（数字はコマ番号）。

| 項目 | 訳 | 注 |
| :--- | ---: | ---: |
| （序） | [119](https://dl.ndl.go.jp/info:ndljp/pid/2935195/119) | [236](https://dl.ndl.go.jp/info:ndljp/pid/2935195/236) |
| 天體 | [120](https://dl.ndl.go.jp/info:ndljp/pid/2935195/120) | [237](https://dl.ndl.go.jp/info:ndljp/pid/2935195/237) |
| 儀象 | [124](https://dl.ndl.go.jp/info:ndljp/pid/2935195/124) | [239](https://dl.ndl.go.jp/info:ndljp/pid/2935195/239) |
| 天文經星 | [126](https://dl.ndl.go.jp/info:ndljp/pid/2935195/126) | [239](https://dl.ndl.go.jp/info:ndljp/pid/2935195/239) |
| 中宮 | [127](https://dl.ndl.go.jp/info:ndljp/pid/2935195/127) | [239](https://dl.ndl.go.jp/info:ndljp/pid/2935195/239) |
| 二十八舍 |  |  |
| 　東方 | [134](https://dl.ndl.go.jp/info:ndljp/pid/2935195/134) | — |
| 　北方 | [134](https://dl.ndl.go.jp/info:ndljp/pid/2935195/134) | — |
| 　西方 | [135](https://dl.ndl.go.jp/info:ndljp/pid/2935195/135) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |
| 　南方 | [136](https://dl.ndl.go.jp/info:ndljp/pid/2935195/136) | — |
| 星官在二十八宿之外者 | [136](https://dl.ndl.go.jp/info:ndljp/pid/2935195/136) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |
| 天漢起沒 | [138](https://dl.ndl.go.jp/info:ndljp/pid/2935195/138) | — |
| 十二次度數 | [139](https://dl.ndl.go.jp/info:ndljp/pid/2935195/139) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |
| 州郡躔次 | [140](https://dl.ndl.go.jp/info:ndljp/pid/2935195/140) | [240](https://dl.ndl.go.jp/info:ndljp/pid/2935195/240) |


### 巻12 志第2 天文中
* [書籍 (IA)]
  [晉書斠注(九)](https://archive.org/details/02077726.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [天文中](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/231/mode/2up)
    * [七曜](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/231/mode/2up)
    * [雜星氣](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/237/mode/2up)
    * [史傳事驗](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/250/mode/2up)
    * [天變](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/250/mode/2up)
    * [日蝕](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/250/mode/2up)
    * [月變](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/257/mode/2up)
    * [月奄犯五緯](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/258/mode/2up)
    * [五星聚舍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/262/mode/2up)
* [書籍 (NDL)]
  [『世界の名著』続1巻](https://dl.ndl.go.jp/info:ndljp/pid/2935195)所収
  「晋書天文志」
  （山田慶児・坂出祥伸・藪内清訳）
  ……「収録著作について」の中の[「『晋書』天文志」](https://dl.ndl.go.jp/info:ndljp/pid/2935195/33)のセクションも参考。
  「天文志」の中の下位セクションごとに、訳文と注へのリンクをまとめておく（数字はコマ番号）。

| 項目 | 訳 | 注 |
| :--- | ---: | ---: |
| 七曜 | [141](https://dl.ndl.go.jp/info:ndljp/pid/2935195/141) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 雜星氣 | [144](https://dl.ndl.go.jp/info:ndljp/pid/2935195/144) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　瑞星 | [145](https://dl.ndl.go.jp/info:ndljp/pid/2935195/145) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　妖星 | [145](https://dl.ndl.go.jp/info:ndljp/pid/2935195/145) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　客星 | [147](https://dl.ndl.go.jp/info:ndljp/pid/2935195/147) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　流星 | [147](https://dl.ndl.go.jp/info:ndljp/pid/2935195/147) | — |
| 　雲氣 | [148](https://dl.ndl.go.jp/info:ndljp/pid/2935195/148) | — |
| 　十煇 | [148](https://dl.ndl.go.jp/info:ndljp/pid/2935195/148) | [241](https://dl.ndl.go.jp/info:ndljp/pid/2935195/241) |
| 　雜氣 | [150](https://dl.ndl.go.jp/info:ndljp/pid/2935195/150) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 史傳事驗 |  |  |
| 　天變 | [153](https://dl.ndl.go.jp/info:ndljp/pid/2935195/153) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 　日蝕 | [154](https://dl.ndl.go.jp/info:ndljp/pid/2935195/154) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 　月變 | [158](https://dl.ndl.go.jp/info:ndljp/pid/2935195/158) | — |
| 　月奄犯五緯 | [158](https://dl.ndl.go.jp/info:ndljp/pid/2935195/158) | [242](https://dl.ndl.go.jp/info:ndljp/pid/2935195/242) |
| 　五星聚舍 | [161](https://dl.ndl.go.jp/info:ndljp/pid/2935195/161) | [243](https://dl.ndl.go.jp/info:ndljp/pid/2935195/243) |

* [ブログ]
  晋書１２　天文志（[天文１　　天変](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864949165)〜[天文１９　星変４０２年魏](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139556983195295)）
  (@ゆるふわ晋宋春秋４)……『宋書』天文志および『魏書』天象志もあわせて見てゆくもの。

### 巻13 志第3 天文下
* [書籍 (IA)]
  [晉書斠注(十)](https://archive.org/details/02077727.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [天文下](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/267/mode/2up)
    * [月五星犯列舍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/267/mode/2up)
    * [妖星客星](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/292/mode/2up)
    * [星流隕](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/300/mode/2up)
    * [雲氣](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/303/mode/2up)
* [書籍 (NDL)]
  [『世界の名著』続1巻](https://dl.ndl.go.jp/info:ndljp/pid/2935195)所収
  「晋書天文志」
  （山田慶児・坂出祥伸・藪内清訳）
  ……「収録著作について」の中の[「『晋書』天文志」](https://dl.ndl.go.jp/info:ndljp/pid/2935195/33)のセクションも参考。
  「天文志」の中の下位セクションごとに、訳文と注へのリンクをまとめておく（数字はコマ番号）。

| 項目 | 訳 | 注 |
| :--- | ---: | ---: |
| （史傳事驗） |  |  |
| 　月五星犯列舍　經星變附見 | [163](https://dl.ndl.go.jp/info:ndljp/pid/2935195/163) | [243](https://dl.ndl.go.jp/info:ndljp/pid/2935195/243) |
| 　妖星客星 | [178](https://dl.ndl.go.jp/info:ndljp/pid/2935195/178) | [243](https://dl.ndl.go.jp/info:ndljp/pid/2935195/243) |
| 　星流隕 | [183](https://dl.ndl.go.jp/info:ndljp/pid/2935195/183) | [244](https://dl.ndl.go.jp/info:ndljp/pid/2935195/244) |
| 　雲氣 | [184](https://dl.ndl.go.jp/info:ndljp/pid/2935195/184) | — |

* [ブログ]
  晋書１２　天文志（[天文１　　天変](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139554864949165)〜[天文１９　星変４０２年魏](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139556983195295)）
  (@ゆるふわ晋宋春秋４)……『宋書』天文志および『魏書』天象志もあわせて見てゆくもの。

### 巻14 志第4 地理上
* [書籍 (IA)]
  [晉書斠注(十一)](https://archive.org/details/02077728.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [地理上](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/305/mode/2up)
    * [總敘](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/305/mode/2up)
    * [司州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/315/mode/2up)
    * [兗州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/318/mode/2up)
    * [豫州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/319/mode/2up)
    * [冀州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/321/mode/2up)
    * [幽州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/322/mode/2up)
    * [平州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/324/mode/2up)
    * [并州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/325/mode/2up)
    * [雍州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/326/mode/2up)
    * [涼州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/328/mode/2up)
    * [秦州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/330/mode/2up)
    * [梁州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/331/mode/2up)
    * [益州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/332/mode/2up)
    * [寧州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/334/mode/2up)
* [書籍 (IA)]
  [晉書地理志新補正(一)](https://archive.org/details/02077799.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [晉書地理志新補正(二)](https://archive.org/details/02077800.cn/page/n2/mode/2up)
* [ブログ]
  [総叙](http://3guozhi.net/sy/m014a.html)・[司州～寧州](http://3guozhi.net/sy/m014b.html)
  （@いつか読みたい晋書訳）

### 巻15 志第5 地理下
* [書籍 (IA)]
  [晉書斠注(十二)](https://archive.org/details/02077729.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [地理下](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/337/mode/2up)
    * [青州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/337/mode/2up)
    * [徐州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/338/mode/2up)
    * [荊州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/341/mode/2up)
    * [揚州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/344/mode/2up)
    * [交州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/349/mode/2up)
    * [廣州](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/351/mode/2up)
* [書籍 (IA)]
  [晉書地理志新補正(二)](https://archive.org/details/02077800.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m015.html)

### 巻16 志第6 律暦上
* [書籍 (IA)]
  [晉書斠注(十三)](https://archive.org/details/02077730.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/353/mode/2up)

### 巻17 志第7 律暦中
* [書籍 (IA)]
  [晉書斠注(十四)](https://archive.org/details/02077731.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/373/mode/2up)
    * [乾象曆](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/379/mode/2up)

### 巻18 志第8 律暦下
* [書籍 (IA)]
  [晉書斠注(十五)](https://archive.org/details/02077732.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/399/mode/2up)
    * [景初曆](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/400/mode/2up)
* [ブログ]
  [『晋書』巻十八 律暦志下所引 杜預『春秋長暦』](http://3guozhi.net/w/045.html)
  （@いつか書きたい三国志）

### 巻19 志第9 礼上
* [書籍 (IA)]
  [晉書斠注(十六)](https://archive.org/details/02077733.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/427/mode/2up)
* [ブログ]
  [礼志１　　郊](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557025529398)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志２　　殷祠](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557103823636)
  (@ゆるふわ晋宋春秋４)

### 巻20 志第10 礼中
* [書籍 (IA)]
  [晉書斠注(十七)](https://archive.org/details/02077734.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/459/mode/2up)
* [ブログ]
  [礼志３　　皇太后祭祀](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557167449553)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志４　　庶出家主祭母１](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557226259485)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志５　　庶出家主祭母２](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557309639150)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志６　　犯私諱解職請求](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557371033597)
  (@ゆるふわ晋宋春秋４)

### 巻21 志第11 礼下
* [書籍 (IA)]
  [晉書斠注(十八)](https://archive.org/details/02077735.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/491/mode/2up)
* [ブログ]
  [礼志７　　皇太子拝礼](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557447117098)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志８　　陳留王礼](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557509711338)
  (@ゆるふわ晋宋春秋４)
* [ブログ]
  [礼志９　　上礼](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139557582287965)
  (@ゆるふわ晋宋春秋４)

### 巻22 志第12 楽上
* [書籍 (IA)]
  [晉書斠注(十八)](https://archive.org/details/02077735.cn/page/n60/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/513/mode/2up)

### 巻23 志第13 楽下
* [書籍 (IA)]
  [晉書斠注(十九)](https://archive.org/details/02077736.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/531/mode/2up)

### 巻24 志第14 職官
* [書籍 (IA)]
  [晉書斠注(二十)](https://archive.org/details/02077737.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/551/mode/2up)
* [ブログ]
  [『晋書』志第十四 「 職官」の翻訳](http://3guozhi.net/n/k3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [晋書巻二十四　志第十四　職官](http://suisui.ojaru.jp/shinjo_024_shi_14_shokkan.html)
  （@正史三國志硏究會）
* [ブログ]
  [巻二十四　志第十四　職官（1）](https://readingnotesofjinshu.com/translation/treatises/vol-24_1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m024.html)

### 巻25 志第15 輿服
* [書籍 (IA)]
  [晉書斠注(二十一)](https://archive.org/details/02077738.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/573/mode/2up)

### 巻26 志第16 食貨
* [書籍 (IA)]
  [晉書斠注(二十二)](https://archive.org/details/02077739.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/597/mode/2up)
* [書籍 (NDL)]
  [『食貨志彙編』](https://dl.ndl.go.jp/info:ndljp/pid/1271069/31)
  （南満洲鉄道調査部）……原文と注釈のみ。訓読文や訳文は無し。
  同じ本のようだが[別のデータ](https://dl.ndl.go.jp/info:ndljp/pid/1281725/33)もある。

### 巻27 志第17 五行上
* [書籍 (IA)]
  [晉書斠注(二十二)](https://archive.org/details/02077739.cn/page/n48/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [五行上](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/617/mode/2up)
    * [恒雨](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/636/mode/2up)
    * [服妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/637/mode/2up)
    * [雞禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/641/mode/2up)
    * [青祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/642/mode/2up)
    * [金沴木](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/643/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m027.html)

### 巻28 志第18 五行中
* [書籍 (IA)]
  [晉書斠注(二十三)](https://archive.org/details/02077740.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [五行中](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/645/mode/2up)
    * [恒陽](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/648/mode/2up)
    * [詩妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/653/mode/2up)
    * [毛蟲之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/659/mode/2up)
    * [犬禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/660/mode/2up)
    * [白眚白祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/662/mode/2up)
    * [木沴金](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/664/mode/2up)
    * [恒燠](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/665/mode/2up)
    * [草妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/666/mode/2up)
    * [羽蟲之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/669/mode/2up)
    * [羊禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/672/mode/2up)
    * [赤眚赤祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/672/mode/2up)

### 巻29 志第19 五行下
* [書籍 (IA)]
  [晉書斠注(二十四)](https://archive.org/details/02077741.cn/page/n2/mode/2up)
* [書籍 (IA)]
  二十四史全译 Full Modern Chinese Translation of 24-Histories
    * [五行下](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/675/mode/2up)
    * [恒寒](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/675/mode/2up)
    * [雷震](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/680/mode/2up)
    * [鼓妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/682/mode/2up)
    * [魚孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/682/mode/2up)
    * [蝗蟲](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/683/mode/2up)
    * [豕禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/684/mode/2up)
    * [黑眚黑祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/684/mode/2up)
    * [火沴水](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/685/mode/2up)
    * [恒風](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/686/mode/2up)
    * [夜妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/689/mode/2up)
    * [裸蟲之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/690/mode/2up)
    * [牛禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/691/mode/2up)
    * [黃眚黃祥](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/692/mode/2up)
    * [地震](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/693/mode/2up)
    * [山崩地陷裂](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/697/mode/2up)
    * [恒陰](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/700/mode/2up)
    * [射妖](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/700/mode/2up)
    * [龍蛇之孼](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/701/mode/2up)
    * [馬禍](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/703/mode/2up)
    * [人痾](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%B8%80%E5%86%8C/page/704/mode/2up)

### 巻30 志第20 刑法
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n2/mode/2up)
* [書籍 (IA)]
  [二十四史全译 Full Modern Chinese Translation of 24-Histories](https://archive.org/details/er-shi-si-shi-fan-yi/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91/5%E3%80%81%20%E6%99%8B%E4%B9%A6%E3%80%90%E5%94%90%E6%9C%9D%C2%B7%E6%88%BF%E7%8E%84%E9%BE%84%E7%AD%89%E3%80%91%EF%BC%88%E5%85%A84%E5%86%8C%EF%BC%89/%E4%BA%8C%E5%8D%81%E5%9B%9B%E5%8F%B2%E5%85%A8%E8%AF%91.%E6%99%8B%E4%B9%A6.%E7%AC%AC%E4%BA%8C%E5%86%8C/page/n33/mode/2up)
* [PDFあり] 
  [訳注晋書刑法志 1〜11](https://cir.nii.ac.jp/articles?q=%E8%A8%B3%E6%B3%A8+%E6%99%8B%E6%9B%B8%E5%88%91%E6%B3%95%E5%BF%97&count=20&sortorder=0)
  （内田 智雄）
* [ブログ]
  晋書３０　刑法志（[刑法志　　復肉刑議](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558710374603)）
  (@ゆるふわ晋宋春秋４)


## 列伝
### 巻31 列伝第1 后妃上
* [書誌のみ]
  [『晋書斠注』巻三十一 列伝第一 后妃上 訳注稿(上)](https://cir.nii.ac.jp/crid/1520853831956120448)
  （赤羽 奈津子・尾関 圭信・織田 めぐみ・小野 響・川見 健人・末川 洸介・千田 豊・安永 知晃）
* [書誌のみ]
  [『晋書斠注』巻三十一 列伝第一 后妃上 訳注稿(下)](https://cir.nii.ac.jp/crid/1520853833128966016)
  （赤羽 奈津子・伊藤 侑希・小野 響・三浦 雄城・安永 知晃）
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n74/mode/2up)

#### 宣穆張皇后（張春華）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%98%A5%E8%8F%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%98%A5%E8%8F%AF)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n76/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B0%EC%A1%A1%B9%A1%C8%DE%BE%E5)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 景懐夏侯皇后（夏侯徽）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E5%BE%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E5%BE%BD)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 景献羊皇后（羊徽瑜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E5%BE%BD%E7%91%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E5%BE%BD%E7%91%9C)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 文明王皇后（王元姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%85%83%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%85%83%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n80/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「文明皇后」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/218)

#### 武元楊皇后（楊艷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E8%89%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E8%89%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E8%89%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E8%89%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E8%89%B7)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n86/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 武悼楊皇后（楊芷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E8%8A%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E8%8A%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E8%8A%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E8%8A%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E8%8A%B7)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n90/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 左貴嬪（左芬）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B7%A6%E8%8A%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B7%A6%E8%8A%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B7%A6%E8%8A%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B7%A6%E8%8A%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B7%A6%E8%8A%AC)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n98/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 胡貴嬪（胡芳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E8%8A%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E8%8A%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E8%8A%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E8%8A%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E8%8A%B3)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* [PDFあり]
  [中国の故事・伝説に取材した大小暦](https://dl.ndl.go.jp/info:ndljp/pid/3051277)（相島 宏）
……[「日本の暦」展](https://www.ndl.go.jp/koyomi/index.html)に関連した論文。
この論文中の「晋武帝羊車遊宴図」の出典は[『恵合余見』第二冊](https://dl.ndl.go.jp/info:ndljp/pid/1286782/19)。
類似の「晋武帝羊車遊宴図」は、[『和漢名画苑』](https://www.kanazawa-bidai.ac.jp/edehon-db/quick.cgi?mode=result&id=83)の[四巻](https://www.kanazawa-bidai.ac.jp/edehon-db/img/83/wakan-meigaen04d021a.jpg)でも見られる。
論文中に引かれた[『通俗続三國志』](https://dl.ndl.go.jp/info:ndljp/pid/775963/25)には、[現代語訳](https://kakuyomu.jp/works/1177354054884106768/episodes/1177354054884109208)もある。
* [徐熥「晉宮怨」](https://zh.wikisource.org/wiki/%E6%99%89%E5%AE%AE%E6%80%A8)……「空聽羊車竹外音」とある。

#### 諸葛夫人（諸葛婉）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AB%B8%E8%91%9B%E5%A9%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AB%B8%E8%91%9B%E5%A9%89)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 恵賈皇后（賈南風）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E5%8D%97%E9%A2%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E5%8D%97%E9%A2%A8)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p13)
* [賈南風乳母徐義墓誌銘](https://zh.wikisource.org/wiki/%E8%B3%88%E5%8D%97%E9%A2%A8%E4%B9%B3%E6%AF%8D%E5%BE%90%E7%BE%A9%E5%A2%93%E8%AA%8C%E9%8A%98)
* [周曇の「詠史詩」の中の「賈后」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%B3%88%E5%90%8E)

#### 恵羊皇后（羊献容）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%8C%AE%E5%AE%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%8C%AE%E5%AE%B9)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n120/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)
* [顏希源『百美新詠』「羊獻容」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E7%BE%8A%E7%8D%BB%E5%AE%B9)

#### 謝夫人（謝玖）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%8E%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%8E%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%8E%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%8E%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%8E%96)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n124/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 懐王皇太后（王媛姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%AA%9B%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%AA%9B%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n126/mode/2up)
* [ブログ]
  [懐王皇太后伝](http://strawberrymilk.gooside.com/text/shinjo/31-9.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)

#### 元夏侯太妃（夏侯光姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E5%85%89%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十五)](https://archive.org/details/02077742.cn/page/n126/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m031.html)


### 巻32 列伝第2 后妃下
* [書誌のみ]
  [『晋書斠注』巻三十二 列伝第二 后妃下 訳注稿](https://cir.nii.ac.jp/crid/1520290885075378432)
  （赤羽 奈津子・猪俣 貴幸・尾関 圭信・織田 めぐみ・小野 響・川見 健人・末川 洸介・千田 豊・安永 知晃）
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n2/mode/2up)


#### 元敬虞皇后（虞孟母）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E5%AD%9F%E6%AF%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E5%AD%9F%E6%AF%8D)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n2/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 荀豫章君
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E8%B1%AB%E7%AB%A0%E5%90%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n4/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 明穆庾皇后（庾文君）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%96%87%E5%90%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%96%87%E5%90%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%96%87%E5%90%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%96%87%E5%90%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%96%87%E5%90%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n4/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 成恭杜皇后（杜陵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E9%99%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E9%99%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E9%99%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E9%99%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E9%99%B5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n8/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「杜后生齒」（第37句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/38)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 章太妃周氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%AB%A0%E5%A4%AA%E5%A6%83%E5%91%A8%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n10/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 康献褚皇后（褚蒜子）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E8%92%9C%E5%AD%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E8%92%9C%E5%AD%90)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n10/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [褚皇后　　太后臨朝](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558781829259)
  (@ゆるふわ晋宋春秋４)

#### 穆章何皇后（何法倪）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%B3%95%E5%80%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%B3%95%E5%80%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%B3%95%E5%80%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%B3%95%E5%80%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%B3%95%E5%80%AA)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n18/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [姚承緒『吳趨訪古錄』の「烏夜村」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/4#%E7%83%8F%E5%A4%9C%E6%9D%91)
* [ブログ]
  [何皇后　　何后慟哭](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558863976538)
  (@ゆるふわ晋宋春秋４)

#### 哀靖王皇后（王穆之）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%A9%86%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%A9%86%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 廃帝孝庾皇后（庾道憐）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%81%93%E6%86%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%81%93%E6%86%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%81%93%E6%86%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%81%93%E6%86%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%81%93%E6%86%90)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 簡文宣鄭太后（鄭阿春）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E9%98%BF%E6%98%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E9%98%BF%E6%98%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E9%98%BF%E6%98%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E9%98%BF%E6%98%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E9%98%BF%E6%98%A5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 簡文順王皇后（王簡姫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%B0%A1%E5%A7%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%B0%A1%E5%A7%AB)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 孝武文李太后（李陵容）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E9%99%B5%E5%AE%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E9%99%B5%E5%AE%B9)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [李太后　　母以子貴慶厚禮崇](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139558968006958)
  (@ゆるふわ晋宋春秋４)

#### 孝武定王皇后（王法慧）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B3%95%E6%85%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B3%95%E6%85%A7)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)

#### 安徳陳太后（陳帰女）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E5%B8%B0%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E5%B8%B0%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [安恭后　　晋末期の妃](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139559010841973)
  (@ゆるふわ晋宋春秋４)

#### 安僖王皇后（王神愛）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%A5%9E%E6%84%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%A5%9E%E6%84%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [安恭后　　晋末期の妃](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139559010841973)
  (@ゆるふわ晋宋春秋４)

#### 恭思褚皇后（褚霊媛）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E9%9C%8A%E5%AA%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E9%9C%8A%E5%AA%9B)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n34/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m032.html)
* [ブログ]
  [安恭后　　晋末期の妃](https://kakuyomu.jp/works/16816452219408440529/episodes/16817139559010841973)
  (@ゆるふわ晋宋春秋４)


### 巻33 列伝第3
#### 王祥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%A5%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%A5%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%A5%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%A5%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%A5%A5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n40/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王覽友弟」（第60句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/52)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-1ohsho.html)
* 干宝[『搜神記』巻11](https://ctext.org/wiki.pl?if=gb&chapter=648373#p17)
* [書籍 (NDL)]
  空海『三教指帰』に[「魚を躍らしむるの感」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)とある
  （[注98](https://dl.ndl.go.jp/info:ndljp/pid/1040599/39)）。
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「王祥非孝子論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/20)
  （兪長城（字が寧世））
* [『三国志』巻18](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B718#%E7%8E%8B%E7%A5%A5)
* 『關帝靈籤』第十六籤に「王祥臥冰」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/16)
    * [画像版](https://zh.wikisource.org/w/index.php?title=File%3AHarvard_drs_53239458_%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.pdf&page=18)

#### 王覧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%A6%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%A6%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%A6%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%A6%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%A6%A7)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n50/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王覽友弟」（第60句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/52)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-1ohsho.html)

#### 鄭沖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n54/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-2teityu.html)

#### 何曾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%9B%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%9B%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%9B%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%9B%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%9B%BE)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n60/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [書籍 (NDL)]
  空海『三教指帰』に[「何曾が滋き味を願はず」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/20)とある
  （[注31](https://dl.ndl.go.jp/info:ndljp/pid/1040599/52)）。
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc1)
  （@晋書簡訳所）

#### 何劭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%8A%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%8A%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%8A%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%8A%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%8A%AD)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n72/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc2)
  （@晋書簡訳所）

#### 何遵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E9%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E9%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E9%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E9%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E9%81%B5)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n76/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc3)
  （@晋書簡訳所）

#### 何嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n76/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 何綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n76/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 何機
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%A9%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%A9%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%A9%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%A9%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%A9%9F)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n78/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 何羨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%BE%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%BE%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%BE%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%BE%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%BE%A8)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n78/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-3kasou.html)
* [ブログ]
  [巻三十三　列伝第三　何曾](https://readingnotesofjinshu.com/translation/biographies/vol-33_2#toc4)
  （@晋書簡訳所）

#### 石苞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%8B%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%8B%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%8B%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%8B%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%8B%9E)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n78/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』石苞伝　訳注](https://www.pixiv.net/novel/show.php?id=4598808)
  （@晋書訳注シリーズ）

#### 石統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E7%B5%B1)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石越
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%B6%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%B6%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%B6%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%B6%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%B6%8A)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n86/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n88/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石儁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%84%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%84%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%84%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%84%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%84%81)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n88/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc2)
  （@晋書簡訳所）

#### 石崇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%B4%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%B4%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%B4%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%B4%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%B4%87)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n88/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* 楊衒之『洛陽伽藍記』
  [巻1](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD%E4%BC%BD%E8%97%8D%E8%A8%98/%E5%8D%B7%E4%B8%80)・
  [巻4](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD%E4%BC%BD%E8%97%8D%E8%A8%98/%E5%8D%B7%E5%9B%9B)
* 庾肩吾[「石崇金谷妓」](https://zh.wikisource.org/wiki/%E7%9F%B3%E5%B4%87%E9%87%91%E8%B0%B7%E5%A6%93)
* 楽史[『緑珠伝』](https://zh.wikisource.org/wiki/%E7%B6%A0%E7%8F%A0%E5%82%B3)
* 『太平広記』巻399「[緑珠井](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC399#%E7%B6%A0%E7%8F%A0%E4%BA%95)」
* 馮夢龍[『情史類略』巻1「緑珠」](https://zh.wikisource.org/wiki/%E6%83%85%E5%8F%B2%E9%A1%9E%E7%95%A5/01#%E7%B6%A0%E7%8F%A0)
* 袁枚[『子不語』巻7「石崇老奴才」](https://ctext.org/wiki.pl?if=gb&chapter=80611#%E7%9F%B3%E5%B4%87%E8%80%81%E5%A5%B4%E6%89%8D)
* 仇英[「金谷園及桃李園圖雙幅」](https://dl.ndl.go.jp/info:ndljp/pid/1014323/102)
* [『本朝文粋』巻14](https://dl.ndl.go.jp/info:ndljp/pid/2544432/37)の[菅三品（菅原文時）「為謙徳公修報恩善願文」](https://dl.ndl.go.jp/info:ndljp/pid/2544432/54)に、石崇を指す「[金谷醉花之地、花毎春匂而主不歸](https://dl.ndl.go.jp/info:ndljp/pid/2544432/55)」という部分があり、その部分が[『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-11)に引用されている（[『十訓抄詳解』の注](https://dl.ndl.go.jp/info:ndljp/pid/945602/34)を参照）。
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……題材の一つが石崇の金谷園。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）
* 高啓の[「石崇墓」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B709#%E7%9F%B3%E5%B4%87%E5%A2%93)（[『国訳漢文大成. 続 文学部第80冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140824/100)も参照）
* [曹雪芹の『紅楼夢』第64回](https://zh.wikisource.org/wiki/%E7%B4%85%E6%A8%93%E5%A4%A2/%E7%AC%AC064%E5%9B%9E)（[『国訳漢文大成』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1913066/101)も参照）に
  「瓦礫明珠一例拋，何曾石尉重嬌嬈。都緣頑福前生造，更有同歸慰寂寥。」
  という詩がある。
* [杜牧の「題桃花夫人廟」](https://zh.wikisource.org/wiki/%E9%A1%8C%E6%A1%83%E8%8A%B1%E5%A4%AB%E4%BA%BA%E5%BB%9F_(%E6%9D%9C%E7%89%A7))（[『杜樊川絶句詳解』](https://dl.ndl.go.jp/pid/1102333/1/47)）に「可憐金谷墜樓人」とある。
* [杜牧の「金谷園」](https://zh.wikisource.org/wiki/%E9%87%91%E8%B0%B7%E5%9C%92_(%E6%9D%9C%E7%89%A7))（[『杜樊川絶句詳解』](https://dl.ndl.go.jp/pid/1102333/1/86)）に「落花猶似墜樓人」とある。
* [胡曾の「金谷園」](https://zh.wikisource.org/wiki/%E9%87%91%E8%B0%B7%E5%9C%92_(%E8%83%A1%E6%9B%BE))
* [汪遵の「金谷」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7602#%E9%87%91%E8%B0%B7)
* [汪遵の「綠珠」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7602#%E7%B6%A0%E7%8F%A0)
* 五代十国時代の錢元璙も同名の「金谷園」を作った。石崇を意識したのかもしれない（？）。
    * [『十國春秋』巻83](https://zh.wikisource.org/wiki/%E5%8D%81%E5%9C%8B%E6%98%A5%E7%A7%8B_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7083#%E5%85%83%E7%92%99%E5%AD%97%E5%BE%B3%E8%BC%9D%E6%AD%A6%E8%82%85%E7%8E%8B%E7%AC%AC%E5%85%AD%E5%AD%90%E4%B9%9F)
    * [Wikipedia](https://en.wikipedia.org/wiki/Mountain_Villa_with_Embracing_Beauty)
    * [姚承緒『吳趨訪古錄』の「樂圃」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E6%A8%82%E5%9C%83)……錢元璙の金谷園を題材にした詩。
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc3)
  （@晋書簡訳所）
* [顏希源『百美新詠』「綠珠」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E7%B6%A0%E7%8F%A0)
* [權德輿「八音詩」](https://zh.wikisource.org/wiki/%E5%85%AB%E9%9F%B3%E8%A9%A9)
* 『關帝靈籤』第十七籤に「石崇被難」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/17)
    * [画像版](https://zh.wikisource.org/w/index.php?title=File:Harvard_drs_53239458_%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.pdf&page=19)

#### 石樸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E6%A8%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E6%A8%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E6%A8%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E6%A8%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E6%A8%B8)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n102/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc4)
  （@晋書簡訳所）

#### 欧陽建
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AC%A7%E9%99%BD%E5%BB%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AC%A7%E9%99%BD%E5%BB%BA)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n102/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、欧陽建伝で言及している
  [「臨終詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/101)がある。
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc5)
  （@晋書簡訳所）

#### 孫鑠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E9%91%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E9%91%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E9%91%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E9%91%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E9%91%A0)
* [書籍 (IA)]
  [晉書斠注(二十六)](https://archive.org/details/02077743.cn/page/n102/mode/2up)
* [ブログ]
  [『晋書』列伝３、建国の功臣](http://3guozhi.net/h/jj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/33-4sekihou.html)
* [ブログ]
  [巻三十三　列伝第三　石苞](https://readingnotesofjinshu.com/translation/biographies/vol-33_3#toc6)
  （@晋書簡訳所）


### 巻34 列伝第4
#### 羊祜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%A5%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%A5%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%A5%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%A5%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%A5%9C)
* [書籍 (IA)]
  [晉書斠注(二十七)](https://archive.org/details/02077744.cn/page/n2/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「羊祜識環」（第54句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/48)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、羊祜伝に引かれた
  [「讓開府表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/406)がある。
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [孫呉は二の次、洛陽が気がかりな羊祜伝](http://3guozhi.net/q/yk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m034.html)
* 干宝[『搜神記』巻15](https://ctext.org/wiki.pl?if=gb&chapter=634334#p12)
* [『十訓抄』1の4](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-04)
* [李白「襄陽歌」](https://zh.wikisource.org/wiki/%E8%A5%84%E9%99%BD%E6%AD%8C)
  （[『続国訳漢文大成 文学部第4冊（李太白詩集上の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139937/4)に訳注あり）
* 白居易「裴侍中晉公以集賢林亭即事詩二十六韻見贈猥蒙徴和才拙詞繁廣為五百言以伸酬獻」
    * [『白氏長慶集』（四庫全書本）巻29](https://zh.wikisource.org/wiki/%E7%99%BD%E6%B0%8F%E9%95%B7%E6%85%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B729)
    * [『全唐詩』巻452](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7452#%E8%A3%B4%E4%BE%8D%E4%B8%AD%E6%99%89%E5%85%AC%E4%BB%A5%E9%9B%86%E8%B3%A2%E6%9E%97%E4%BA%AD%E5%8D%B3%E4%BA%8B%E8%A9%A9%E4%B8%89%E5%8D%81%E5%85%AD%E9%9F%BB%E8%A6%8B%E8%B4%88%E7%8C%A5%E8%92%99%E5%BE%B5%E5%92%8C%E6%89%8D%E6%8B%99%E8%A9%9E%E7%B9%81%E8%BC%92%E5%BB%A3%E7%82%BA%E4%BA%94%E7%99%BE%E8%A8%80%E4%BB%A5%E4%BC%B8%E9%85%AC%E7%8D%BB)
    * [『続国訳漢文大成 文学部第42冊（白楽天詩集 三の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1140376/31)
* [胡曾の「峴山」](https://zh.wikisource.org/wiki/%E5%B3%B4%E5%B1%B1_(%E8%83%A1%E6%9B%BE))
* [陳子昂「峴山懷古」](https://zh.wikisource.org/wiki/%E5%B3%B4%E5%B1%B1%E6%87%B7%E5%8F%A4_(%E9%99%B3%E5%AD%90%E6%98%82))（[『漢籍國字解全書 : 先哲遺著 第10巻』](https://dl.ndl.go.jp/pid/1904127/1/268)）……「墮淚碣」に言及している詩。
* [ブログ]
  [羊祜伝](https://estar.jp/novels/25594548/viewer?page=191)
  （@淡々晋書）
* [ブログ]
  [羊法興　　功臣再評価](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086327685)
  （@デイリー晋宋春秋）
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/396)）で、羊祜の故事を指して「羊公深罷市之慕」とある。
* [李德裕「羊祜留賈充論」](https://zh.wikisource.org/wiki/%E7%BE%8A%E7%A5%9C%E7%95%99%E8%B3%88%E5%85%85%E8%AB%96)

#### 杜預
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E9%A0%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E9%A0%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E9%A0%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E9%A0%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E9%A0%90)
* [書籍 (IA)]
  [晉書斠注(二十七)](https://archive.org/details/02077744.cn/page/n36/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、杜預伝で言及している
  『春秋左氏伝』の[序](https://dl.ndl.go.jp/info:ndljp/pid/1912835/163)がある。
* [書籍 (NDL)]
  [『漢籍国字解全書　先哲遺著　第十三巻』](https://dl.ndl.go.jp/info:ndljp/pid/898786/14)にも『春秋左氏伝』の序がある。
* [ブログ]
  [杜預「春秋左伝序」現代語訳](http://3guozhi.net/w/003.html)
  （@いつか書きたい三国志）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m034.html)
* 『漢魏六朝百三家集』所収の『杜征南集』（杜預集）
    * [Wikisource](https://zh.wikisource.org/wiki/%E6%9D%9C%E5%BE%81%E5%8D%97%E9%9B%86)
    * [Wikisource（四庫全書本）](https://zh.wikisource.org/wiki/%E6%BC%A2%E9%AD%8F%E5%85%AD%E6%9C%9D%E7%99%BE%E4%B8%89%E5%AE%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7037)
    * [NDL](https://dl.ndl.go.jp/info:ndljp/pid/2551362/4)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)

#### 杜錫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E9%8C%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E9%8C%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E9%8C%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E9%8C%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E9%8C%AB)
* [書籍 (IA)]
  [晉書斠注(二十七)](https://archive.org/details/02077744.cn/page/n58/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m034.html)
* 干宝[『搜神記』巻15](https://ctext.org/wiki.pl?if=gb&chapter=634334#p15)


### 巻35 列伝第5
#### 陳騫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E9%A8%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E9%A8%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E9%A8%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E9%A8%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E9%A8%AB)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [『晋書』陳騫伝　訳注](https://www.pixiv.net/novel/show.php?id=5446111)
  （@晋書訳注シリーズ）

#### 陳輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n8/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)

#### 裴秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n8/mode/2up)
* [ブログ]
  [『晋書』列伝５より、「裴秀伝」を翻訳](http://3guozhi.net/zaka2/hs1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  『晋書』裴秀伝　訳注（[前篇](https://www.pixiv.net/novel/show.php?id=6048010)・[後篇](https://www.pixiv.net/novel/show.php?id=6062445)）
  （@晋書訳注シリーズ）
* [『三国志』巻23](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B723#%E5%AD%90_%E7%A7%80)

#### 裴頠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%A0%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%A0%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%A0%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%A0%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%A0%A0)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n22/mode/2up)
* [ブログ]
  [『晋書』列伝５より、「裴頠伝」を翻訳](http://3guozhi.net/zaka2/hs3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc1)
  （@晋書簡訳所）

#### 裴楷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E6%A5%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E6%A5%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E6%A5%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E6%A5%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E6%A5%B7)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n38/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「裴楷清通」（第2句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/17)
* [『文選』所収の王倹「褚淵碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B758#%E8%A4%9A%E6%B7%B5%E7%A2%91%E6%96%87)（[国訳](https://dl.ndl.go.jp/info:ndljp/pid/1912835/381)）に「裴楷清通」とある。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc2)
  （@晋書簡訳所）

#### 裴輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc2)
  （@晋書簡訳所）

#### 裴瓚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%93%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%93%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%93%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%93%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%93%9A)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc2)
  （@晋書簡訳所）

#### 裴憲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E6%86%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E6%86%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E6%86%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E6%86%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E6%86%B2)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc3)
  （@晋書簡訳所）

#### 裴黎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%BB%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%BB%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%BB%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%BB%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%BB%8E)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴康
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E5%BA%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E5%BA%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E5%BA%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E5%BA%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E5%BA%B7)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴盾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%9B%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%9B%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%9B%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%9B%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%9B%BE)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴邵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%82%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%82%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%82%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%82%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%82%B5)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴綽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E7%B6%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E7%B6%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E7%B6%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E7%B6%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E7%B6%BD)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）

#### 裴遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E9%81%90)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m035.html)
* [ブログ]
  [巻三十五　列伝第五　裴秀（2）](https://readingnotesofjinshu.com/translation/biographies/vol-35_3#toc4)
  （@晋書簡訳所）


### 巻36 列伝第6
#### 衛瓘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E7%93%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E7%93%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E7%93%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E7%93%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E7%93%98)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n56/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「瓘靖二妙」（第43句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/42)、
  [「衛瓘撫牀」（第84句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/66)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（1）](https://readingnotesofjinshu.com/translation/biographies/vol-36_1)
  （@晋書簡訳所）

#### 衛恒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E6%81%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E6%81%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E6%81%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E6%81%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E6%81%92)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc1)
  （@晋書簡訳所）

#### 衛璪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E7%92%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E7%92%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E7%92%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E7%92%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E7%92%AA)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc2)
  （@晋書簡訳所）

#### 衛玠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E7%8E%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E7%8E%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E7%8E%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E7%8E%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E7%8E%A0)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc3)
  （@晋書簡訳所）

#### 衛展
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A1%9B%E5%B1%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A1%9B%E5%B1%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A1%9B%E5%B1%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A1%9B%E5%B1%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A1%9B%E5%B1%95)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　衛瓘（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_2#toc4)
  （@晋書簡訳所）

#### 張華
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8F%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8F%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8F%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8F%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8F%AF)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n108/mode/2up)
* [PDFあり]
  [六朝文人伝「張華」 : 『晋書』張華伝](https://dl.ndl.go.jp/info:ndljp/pid/10504415)
  （佐藤 利行）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、張華伝に引かれた
  [「鷦鷯賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/253)がある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、張華伝で言及している
  [「女史箴」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/334)がある。
* 干宝[『搜神記』巻18](https://ctext.org/wiki.pl?if=gb&chapter=232809#p10)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（1）](https://readingnotesofjinshu.com/translation/biographies/vol-36_3)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc1)
  （@晋書簡訳所）
* 吳筠[「覽古十四首」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7853#%E8%A6%BD%E5%8F%A4%E5%8D%81%E5%9B%9B%E9%A6%96)に「茂先洽聞者，幽賾咸該通。弱年賦鷦鷯，可謂達養蒙。晩節希鸞鵠，長飛戾曾穹。知進不知退，遂令其道窮。」とある。茂先は張華の字。

#### 張禕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%A6%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%A6%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%A6%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%A6%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%A6%95)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n134/mode/2up)
* [PDFあり]
  [六朝文人伝「張華」 : 『晋書』張華伝](https://dl.ndl.go.jp/info:ndljp/pid/10504415)
  （佐藤 利行）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc2)
  （@晋書簡訳所）

#### 張韙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%9F%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%9F%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%9F%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%9F%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%9F%99)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n134/mode/2up)
* [PDFあり]
  [六朝文人伝「張華」 : 『晋書』張華伝](https://dl.ndl.go.jp/info:ndljp/pid/10504415)
  （佐藤 利行）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc2)
  （@晋書簡訳所）

#### 劉卞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%8D%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%8D%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%8D%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%8D%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%8D%9E)
* [書籍 (IA)]
  [晉書斠注(二十八)](https://archive.org/details/02077745.cn/page/n136/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m036.html)
* [ブログ]
  [巻三十六　列伝第六　張華（2）](https://readingnotesofjinshu.com/translation/biographies/vol-36_4#toc3)
  （@晋書簡訳所）


### 巻37 列伝第7 宗室
* [ブログ]
  [系図](https://readingnotesofjinshu.com/translation/appendix/familytree)
  （@晋書簡訳所）

#### 安平献王（司馬孚）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AD%9A)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n2/mode/2up)
* [ブログ]
  [死ぬまで魏の純臣なり、司馬孚＆司馬望伝](http://3guozhi.net/hito/s1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（1）安平献王孚（I）](https://readingnotesofjinshu.com/translation/biographies/vol-37_1)
  （@晋書簡訳所）

#### 司馬邕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%82%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%82%95)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n14/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc1)
  （@晋書簡訳所）

#### 義陽成王（司馬望）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9C%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9C%9B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n14/mode/2up)
* [ブログ]
  [死ぬまで魏の純臣なり、司馬孚＆司馬望伝](http://3guozhi.net/hito/s2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 河間平王（司馬洪）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 司馬威
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%A8%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%A8%81)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 隨穆王（司馬整）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%95%B4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%95%B4)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 竟陵王（司馬楙）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A5%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A5%99)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc2)
  （@晋書簡訳所）

#### 太原成王（司馬輔）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%BC%94)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc3)
  （@晋書簡訳所）

#### 司馬翼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%BF%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%BF%BC)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc4)
  （@晋書簡訳所）

#### 下邳獻王（司馬晃）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%83)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n28/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc5)
  （@晋書簡訳所）

#### 太原烈王（司馬瓌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%93%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%93%8C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc6)
  （@晋書簡訳所）

#### 高陽元王（司馬珪）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%8F%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%8F%AA)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc7)
  （@晋書簡訳所）

#### 常山孝王（司馬衡）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A1%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A1%A1)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc8)
  （@晋書簡訳所）

#### 沛順王（司馬景）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（2）安平献王孚（II）](https://readingnotesofjinshu.com/translation/biographies/vol-37_2#toc9)
  （@晋書簡訳所）

#### 彭城穆王（司馬権）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A8%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A8%A9)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
  ……「五馬」の一人の彭城王は、司馬権の子の司馬雄（と思われる）。
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 司馬紘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B4%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B4%98)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n34/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
  ……「五馬」の一人の彭城王を司馬紘とする説もあるようだ。
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 司馬俊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BF%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BF%8A)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 高密文献王（司馬泰）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%B3%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%B3%B0)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc2)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 高密孝王（司馬略）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%95%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%95%A5)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n40/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc2)
  （@晋書簡訳所）
* [ブログ]
  [宗室伝　　宗廟継承](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559086353724)
  （@デイリー晋宋春秋）

#### 新蔡武哀王（司馬騰）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A8%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A8%B0)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n42/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc3)
  （@晋書簡訳所）

#### 司馬確
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%A2%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%A2%BA)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc3)
  （@晋書簡訳所）

#### 南陽王（司馬模）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A8%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A8%A1)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc4)
  （@晋書簡訳所）

#### 司馬保
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BF%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BF%9D)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（3）彭城穆王権　高密文献王泰](https://readingnotesofjinshu.com/translation/biographies/vol-37_3#toc4)
  （@晋書簡訳所）

#### 范陽康王（司馬綏）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（4）范陽康王綏](https://readingnotesofjinshu.com/translation/biographies/vol-37_4)
  （@晋書簡訳所）

#### 司馬虓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%99%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%99%93)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n50/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [巻三十七　列伝第七　宗室（4）范陽康王綏](https://readingnotesofjinshu.com/translation/biographies/vol-37_4)
  （@晋書簡訳所）

#### 済南恵王（司馬遂）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%82)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 司馬勳（司馬勲）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%8B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%8B%B2)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙剛王（司馬遜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%9C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙閔王（司馬承／司馬氶／司馬丞）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%89%BF%20OR%20%E5%8F%B8%E9%A6%AC%E6%B0%B6%20OR%20%E5%8F%B8%E9%A6%AC%E4%B8%9E)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙烈王（司馬無忌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%84%A1%E5%BF%8C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 譙敬王（司馬恬）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%81%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%81%AC)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n72/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [司馬恬　　大司馬を弾ず](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559106126284)
  （@デイリー晋宋春秋）

#### 譙忠王（司馬尚之）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n72/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [司馬尚之１　名族と対峙](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559106146862)〜
  （@デイリー晋宋春秋）

#### 司馬恢之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%81%A2%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [ブログ]
  [司馬尚之３　死と継承者と弟](https://kakuyomu.jp/works/16817139559045607356/episodes/16817139559106182024)
  （@デイリー晋宋春秋）

#### 司馬休之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [『魏書』巻37](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B737#%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)
* [『北史』巻29](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7029#%E5%8F%B8%E9%A6%AC%E4%BC%91%E4%B9%8B)

#### 司馬允之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%85%81%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 韓延之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E5%BB%B6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)
* [『魏書』巻38](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B738#%E9%9F%93%E5%BB%B6%E4%B9%8B)
* [『北史』巻27](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7027#%E9%9F%93%E5%BB%B6%E4%B9%8B)

#### 司馬愔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%84%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%84%94)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 高陽王（司馬睦）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%9D%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%9D%A6)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n84/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 任城景王（司馬陵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%99%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%99%B5)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 司馬順
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A0%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A0%86)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)

#### 西河繆王（司馬斌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%96%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%96%8C)
* [書籍 (IA)]
  [晉書斠注(二十九)](https://archive.org/details/02077746.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m037.html)


### 巻38 列伝第8 宣五王・文六王
#### 平原王（司馬榦）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%A6%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%A6%A6)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc1)
  （@晋書簡訳所）

#### 琅邪武王（司馬伷）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BC%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BC%B7)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc2)
  （@晋書簡訳所）

#### 琅邪恭王（司馬覲）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A6%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A6%B2)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n8/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc3)
  （@晋書簡訳所）

#### 武陵荘王（司馬澹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BE%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BE%B9)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n10/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc4)
  （@晋書簡訳所）

#### 東安王（司馬繇）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B9%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B9%87)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n10/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc5)
  （@晋書簡訳所）

#### 淮陵元王（司馬漼）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BC%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BC%BC)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n12/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc6)
  （@晋書簡訳所）

#### 清恵亭侯（司馬京）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BA%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BA%AC)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n12/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc7)
  （@晋書簡訳所）

#### 扶風武王（司馬駿）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A7%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A7%BF)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n14/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc8)
  （@晋書簡訳所）

#### 司馬暢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9A%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9A%A2)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc8)
  （@晋書簡訳所）

#### 新野荘王（司馬歆）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%AD%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%AD%86)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc9)
  （@晋書簡訳所）

#### 梁孝王（司馬肜）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%82%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%82%9C)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n22/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)
* [ブログ]
  [巻三十八　列伝第八　宣五王](https://readingnotesofjinshu.com/translation/biographies/vol-38_1#toc10)
  （@晋書簡訳所）

#### 斉献王（司馬攸）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%94%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%94%B8)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列伝８より、「斉王攸伝」を翻訳してみた](http://3guozhi.net/zaka2/yu1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 司馬蕤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%95%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%95%A4)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n42/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 広漢沖王（司馬贊）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%B4%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%B4%8A)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n44/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 司馬寔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AF%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AF%94)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n44/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 城陽哀王（司馬兆）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%85%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%85%86)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n44/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 遼東悼恵王（司馬定国）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AE%9A%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 広漢殤王（司馬広徳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BA%83%E5%BE%B3)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 楽安平王（司馬鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)

#### 楽平王（司馬延祚）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BB%B6%E7%A5%9A)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m038.html)


### 巻39 列伝第9
#### 王沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n52/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「王沈＆王浚伝」を翻訳](http://3guozhi.net/hito/ot1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 王浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n58/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「王沈＆王浚伝」を翻訳](http://3guozhi.net/hito/ot1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻三十九　列伝第九　王沈（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [王浚妻華芳墓誌銘](https://zh.wikisource.org/wiki/%E7%8E%8B%E6%B5%9A%E5%A6%BB%E8%8F%AF%E8%8A%B3%E5%A2%93%E8%AA%8C%E9%8A%98)

#### 荀顗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E9%A1%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E9%A1%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E9%A1%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E9%A1%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E9%A1%97)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n70/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「荀顗・荀勖伝」を翻訳](http://3guozhi.net/n/jg1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)
* [ブログ]
  [『晋書』荀顗伝　訳注](https://www.pixiv.net/novel/show.php?id=5334253)
  （@晋書訳注シリーズ）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/193)）に「荀摯競爽於晉世」とあり、注によれば「荀」は荀顗のことを指す。

#### 荀勗（荀勖）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%8B%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%8B%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%8B%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%8B%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%8B%96)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n76/mode/2up)
* [ブログ]
  [『晋書』列伝９より、「荀顗・荀勖伝」を翻訳](http://3guozhi.net/n/jg1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)
* [ブログ]
  『晋書』荀勗伝　訳注（[前篇](https://www.pixiv.net/novel/show.php?id=5145666)・[後篇](https://www.pixiv.net/novel/show.php?id=5186528)）
  （@晋書訳注シリーズ）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)に「采公曾之中經」（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/189)）および「公曾甘鳳池之失」（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/192)）とある。「公曾」は荀勗の字。

#### 荀藩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E7%B1%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E7%B1%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E7%B1%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E7%B1%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E7%B1%93)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n92/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀邃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E9%82%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E9%82%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E9%82%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E9%82%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E9%82%83)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n94/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀闓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E9%97%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E9%97%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E9%97%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E9%97%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E9%97%93)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n94/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀組
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E7%B5%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E7%B5%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E7%B5%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E7%B5%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E7%B5%84)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n96/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 荀奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n100/mode/2up)
* [ブログ]
  [巻三十九　列伝第九　荀勖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-39_4#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

#### 馮紞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AE%E7%B4%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AE%E7%B4%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AE%E7%B4%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AE%E7%B4%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AE%E7%B4%9E)
* [書籍 (IA)]
  [晉書斠注(三十)](https://archive.org/details/02077747.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m039.html)

### 巻40 列伝第10
#### 賈充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈充伝」を翻訳](http://3guozhi.net/zaka2/kj1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p13)
* 楊維楨[「贈杜彥清序」](https://zh.wikisource.org/wiki/%E8%B4%88%E6%9D%9C%E5%BD%A5%E6%B8%85%E5%BA%8F)
* [ブログ]
  『晋書』賈充伝　訳注（[壱](https://www.pixiv.net/novel/show.php?id=5485008)・[弐](https://www.pixiv.net/novel/show.php?id=5551225)）
  （@晋書訳注シリーズ）
* [『三国志』巻15](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B715#%E5%AD%90_%E5%85%85)
* [李德裕「羊祜留賈充論」](https://zh.wikisource.org/wiki/%E7%BE%8A%E7%A5%9C%E7%95%99%E8%B3%88%E5%85%85%E8%AB%96)

#### 郭槐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E6%A7%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E6%A7%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E6%A7%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E6%A7%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E6%A7%90)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n14/mode/2up)
* [PDFあり]
  [魏晉石刻資料選注( 一七 宜成宣君郭夫人之柩銘 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……郭夫人は郭槐のこと。
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [『晋書』賈充伝　訳注　(参)](https://www.pixiv.net/novel/show.php?id=5729263)
  （@晋書訳注シリーズ）

#### 韓謐（賈謐）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E8%AC%90)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n16/mode/2up)
* [PDFあり]
  [魏晉石刻資料選注( 二五 驃騎將軍韓壽墓碣 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……韓寿は賈謐の父。
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(参)](https://www.pixiv.net/novel/show.php?id=5729263#3)
  （@晋書訳注シリーズ）
* 韓謐（賈謐）の父母である韓寿と賈午の話に基づく創作物
    * [黄庭堅「酴醿」](https://zh.wikisource.org/wiki/%E5%B1%B1%E8%B0%B7%E9%9B%86%E8%A9%A9%E6%B3%A8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%A4%96%E9%9B%86%E5%8D%B712#%E9%85%B4%E9%86%BF)に「入骨濃薰賈女香」とある。
    * [周文質「蝶戀花・悟迷」](https://ctext.org/wiki.pl?if=gb&chapter=390671#p102)に「朱門深閉賈充香」とある。
    * [陸采『懷香記』](https://zh.wikisource.org/wiki/%E6%87%B7%E9%A6%99%E8%A8%98)

#### 賈混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n26/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(四)](https://www.pixiv.net/novel/show.php?id=5924762)
  （@晋書訳注シリーズ）

#### 賈模
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%A8%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%A8%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%A8%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%A8%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%A8%A1)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n26/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(四)](https://www.pixiv.net/novel/show.php?id=5924762)
  （@晋書訳注シリーズ）

#### 郭彰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E5%BD%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E5%BD%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E5%BD%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E5%BD%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E5%BD%B0)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列伝１０より、「賈謐伝」他賈氏を翻訳](http://3guozhi.net/zaka2/kj4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　賈充（2）](https://readingnotesofjinshu.com/translation/biographies/vol-40_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [『晋書』賈充伝　訳注　(四)](https://www.pixiv.net/novel/show.php?id=5924762)
  （@晋書訳注シリーズ）

#### 楊駿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E9%A7%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E9%A7%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E9%A7%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E9%A7%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E9%A7%BF)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n28/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　楊駿](https://readingnotesofjinshu.com/translation/biographies/vol-40_3#toc1)
  （@晋書簡訳所）

#### 楊珧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E7%8F%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E7%8F%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E7%8F%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E7%8F%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E7%8F%A7)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　楊駿](https://readingnotesofjinshu.com/translation/biographies/vol-40_3#toc2)
  （@晋書簡訳所）

#### 楊済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n38/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m040.html)
* [ブログ]
  [巻四十　列伝第十　楊駿](https://readingnotesofjinshu.com/translation/biographies/vol-40_3#toc3)
  （@晋書簡訳所）


### 巻41 列伝第11
#### 魏舒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E8%88%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E8%88%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E8%88%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E8%88%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E8%88%92)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n44/mode/2up)
* 干宝『搜神記』
  [巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p7)・
  [巻19](https://ctext.org/wiki.pl?if=gb&chapter=307958#p3)
* [『太平広記』巻58](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC058)
  ……娘の魏華存は有名な女仙（という話になっている）

#### 魏混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n52/mode/2up)

#### 李憙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%86%99)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n54/mode/2up)

#### 劉寔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%AF%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%AF%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%AF%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%AF%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%AF%94)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n58/mode/2up)

#### 劉智
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%99%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%99%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%99%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%99%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%99%BA)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n76/mode/2up)

#### 高光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E5%85%89)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n76/mode/2up)

#### 高韜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E9%9F%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E9%9F%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E9%9F%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E9%9F%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E9%9F%9C)
* [書籍 (IA)]
  [晉書斠注(三十一)](https://archive.org/details/02077748.cn/page/n78/mode/2up)


### 巻42 列伝第12
#### 王渾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B8%BE)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列伝１２より「王渾＆王済伝」を邦訳](http://3guozhi.net/zaka2/ok1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)

#### 王済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝１２より「王渾＆王済伝」を邦訳](http://3guozhi.net/zaka2/ok1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)

#### 王濬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%BF%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%BF%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%BF%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%BF%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%BF%AC)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n18/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [胡曾の「武昌」](https://zh.wikisource.org/wiki/%E6%AD%A6%E6%98%8C)
* [杜牧「題橫江館」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7523#%E9%A1%8C%E6%A9%AB%E6%B1%9F%E9%A4%A8)([『杜樊川絶句詳解』](https://dl.ndl.go.jp/pid/1102333/1/50?keyword=%E6%99%89%E9%BE%8D%E9%A9%A4)）……「晉龍驤」は王濬を指す。

#### 唐彬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%94%90%E5%BD%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%94%90%E5%BD%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%94%90%E5%BD%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%94%90%E5%BD%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%94%90%E5%BD%AC)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m042.html)

### 巻43 列伝第13
#### 山濤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B1%B1%E6%BF%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B1%B1%E6%BF%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B1%B1%E6%BF%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B1%B1%E6%BF%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B1%B1%E6%BF%A4)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n56/mode/2up)
* [書誌のみ]
  [『晉書』山濤伝訳注](https://cir.nii.ac.jp/crid/1520853835002721280)
  （鷹橋 明久）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「山濤識量」（第81句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/64)
  （ちなみに、「山濤識量」と評した任昉の「為范尚書讓吏部封侯第一表」も[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/430)で読めて、「山濤識量」という評言はリンク先の左ページ後ろから2行目にある）
* 『全晋文』
  [巻34](https://ctext.org/wiki.pl?if=gb&chapter=79602)
* [顏延之の「五君詠」の「阮始平」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E5%A7%8B%E5%B9%B3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　山濤（1）](https://readingnotesofjinshu.com/translation/biographies/vol-43_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/vol-43_2)
  （@晋書簡訳所）
* [唐の楊綰](https://zh.wikipedia.org/zh-tw/%E6%A5%8A%E7%B6%B0)が楊震（・邴吉）・山濤・謝安になぞらえられている。
	* [『旧唐書』巻119](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7119#%E6%A5%8A%E7%B6%B0)
	* [『新唐書』巻142](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7142#%E6%A5%8A%E7%B6%B0)
* 『關帝靈籤』第九十六籤に「山濤見王衍」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/96)
    * [画像版](https://commons.wikimedia.org/w/index.php?title=File%3A%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=98)
* 山濤の字の「巨源」と同名の人たち
    * 丘巨源
        * [『南斉書』巻52](https://zh.wikisource.org/wiki/%E5%8D%97%E9%BD%8A%E6%9B%B8/%E5%8D%B752#%E4%B8%98%E5%B7%A8%E6%BA%90)
        * [『南史』巻72](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B772#%E4%B8%98%E5%B7%A8%E6%BA%90)
    * 韋巨源
        * [『旧唐書』巻92](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B792#%E5%BE%9E%E7%A5%96%E5%85%84%E5%AD%90_%E5%B7%A8%E6%BA%90)
        * [『新唐書』巻123](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7123#%E9%9F%8B%E5%B7%A8%E6%BA%90)
    * 楊巨源
        * [『唐才子傳』卷5](https://zh.wikisource.org/wiki/%E5%94%90%E6%89%8D%E5%AD%90%E5%82%B3/%E5%8D%B75#%E6%A5%8A%E5%B7%A8%E6%BA%90)
    * 楊巨源
        * [『宋史』巻402](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7402#%E6%A5%8A%E5%B7%A8%E6%BA%90)
    * 何巨源
        * [『朱子語類』巻100（邵子之書）](https://zh.wikisource.org/wiki/%E6%9C%B1%E5%AD%90%E8%AA%9E%E9%A1%9E/100)（[訳注](https://hdl.handle.net/11094/56933)のp. 8）
    * 蕭巨源
        * [『皇明貢舉考』巻5](https://commons.wikimedia.org/w/index.php?title=File:CADAL02089103_%E7%9A%87%E6%98%8E%E8%B2%A2%E8%88%89%E8%80%83%EF%BC%88%E5%85%AD%EF%BC%89.djvu&page=142)
        * [Wikipedia](https://zh.wikipedia.org/zh-tw/%E8%95%AD%E5%B7%A8%E6%BA%90)

#### 山簡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B1%B1%E7%B0%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B1%B1%E7%B0%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B1%B1%E7%B0%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B1%B1%E7%B0%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B1%B1%E7%B0%A1)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n72/mode/2up)
* [孟浩然の「高陽池送朱二」](https://zh.wikisource.org/wiki/%E9%AB%98%E9%99%BD%E6%B1%A0%E9%80%81%E6%9C%B1%E4%BA%8C)の「當昔襄陽雄盛時，山公常醉習家池。」の「山公」は山簡を指す。
* 白居易「裴侍中晉公以集賢林亭即事詩二十六韻見贈猥蒙徴和才拙詞繁廣為五百言以伸酬獻」
    * [『白氏長慶集』（四庫全書本）巻29](https://zh.wikisource.org/wiki/%E7%99%BD%E6%B0%8F%E9%95%B7%E6%85%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B729)
    * [『全唐詩』巻452](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7452#%E8%A3%B4%E4%BE%8D%E4%B8%AD%E6%99%89%E5%85%AC%E4%BB%A5%E9%9B%86%E8%B3%A2%E6%9E%97%E4%BA%AD%E5%8D%B3%E4%BA%8B%E8%A9%A9%E4%B8%89%E5%8D%81%E5%85%AD%E9%9F%BB%E8%A6%8B%E8%B4%88%E7%8C%A5%E8%92%99%E5%BE%B5%E5%92%8C%E6%89%8D%E6%8B%99%E8%A9%9E%E7%B9%81%E8%BC%92%E5%BB%A3%E7%82%BA%E4%BA%94%E7%99%BE%E8%A8%80%E4%BB%A5%E4%BC%B8%E9%85%AC%E7%8D%BB)
    * [『続国訳漢文大成 文学部第42冊（白楽天詩集 三の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1140376/31)
* [胡曾の「高陽池」](https://zh.wikisource.org/wiki/%E9%AB%98%E9%99%BD%E6%B1%A0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　山濤（3）](https://readingnotesofjinshu.com/translation/biographies/vol-43_3#toc1)
  （@晋書簡訳所）
* [李白「峴山懷古」](https://zh.wikisource.org/wiki/%E5%B3%B4%E5%B1%B1%E6%87%B7%E5%8F%A4_(%E6%9D%8E%E7%99%BD))（[『国訳漢文大成 第8巻 第1輯』](https://dl.ndl.go.jp/pid/1685290/1/383)）……山簡を指して「山公」と述べている詩。

#### 山遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B1%B1%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B1%B1%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B1%B1%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B1%B1%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B1%B1%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　山濤（3）](https://readingnotesofjinshu.com/translation/biographies/vol-43_3#toc2)
  （@晋書簡訳所）

#### 王戎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%88%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%88%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%88%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%88%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%88%8E)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n80/mode/2up)
* [PDFあり] 
  [『晉書』王戎伝(巻四十三)訳注](http://dx.doi.org/10.18899/gei.07.04)
  （鷹橋 明久）
* [PDFあり]
  [『晋書』王戎伝訳注](https://dl.ndl.go.jp/info:ndljp/pid/10504387)
  （小松 英生）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王戎簡要」（第1句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/16)
* [ブログ]
  [『晋書』列伝１３より「王戎＆王衍伝」を訳出](http://3guozhi.net/zaka2/oj1.html)
  （@いつか書きたい『三国志』）
* [『文選』所収の王倹「褚淵碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B758#%E8%A4%9A%E6%B7%B5%E7%A2%91%E6%96%87)（[国訳](https://dl.ndl.go.jp/info:ndljp/pid/1912835/381)）に「王戎簡要」とある。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（1）](https://readingnotesofjinshu.com/translation/biographies/vol-43_4)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/%e5%b7%bb%e5%9b%9b%e5%8d%81%e4%b8%89%e3%80%80%e5%88%97%e4%bc%9d%e7%ac%ac%e5%8d%81%e4%b8%89%e3%80%80%e7%8e%8b%e6%88%8e%ef%bc%882%ef%bc%89)
  （@晋書簡訳所）
* [顏希源『百美新詠』「王戎婦」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E7%8E%8B%E6%88%8E%E5%A9%A6)
  ……[『世説新語』惑溺篇6](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E6%83%91%E6%BA%BA)の話が題材。

#### 王衍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%A1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%A1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%A1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%A1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%A1%8D)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n94/mode/2up)
* [ブログ]
  [『晋書』列伝１３より「王戎＆王衍伝」を訳出](http://3guozhi.net/zaka2/oj2.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王衍風鑑」（第75句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/61)
* 高啓の[「永嘉行」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E6%B0%B8%E5%98%89%E8%A1%8C)の「至今人説王夷甫」という句（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/143)も参照）
* [周曇の「詠史詩」の中の「王夷甫」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E7%8E%8B%E5%A4%B7%E7%94%AB)
* [胡曾の「洛陽」](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD_(%E8%83%A1%E6%9B%BE))
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（3）](https://readingnotesofjinshu.com/translation/biographies/vol-43_6)
  （@晋書簡訳所）
* [『旧唐書』巻99（張九齡）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E4%B9%9D%E9%BD%A1)で、「九齡奏曰：「祿山狼子野心，面有逆相，臣請因罪戮之，冀絕後患。」上曰：「卿勿以王夷甫知石勒故事，誤害忠良。」遂放歸藩。」と王衍と石勒に言及している。
* [『新唐書』巻126（張九齡）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7126#%E5%BC%B5%E4%B9%9D%E9%BD%A1)で、「九齡曰：「祿山狼子野心，有逆相，宜即事誅之，以絕後患。」帝曰：「卿無以王衍知石勒而害忠良。」卒不用。」と王衍と石勒に言及している。
* 『關帝靈籤』第九十六籤に「山濤見王衍」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/96)
    * [画像版](https://commons.wikimedia.org/w/index.php?title=File%3A%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=98)

#### 王澄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%BE%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%BE%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%BE%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%BE%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%BE%84)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n106/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（4）](https://readingnotesofjinshu.com/translation/biographies/vol-43_7#toc1)
  （@晋書簡訳所）

#### 郭舒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E8%88%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E8%88%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E8%88%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E8%88%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E8%88%92)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n114/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　王戎（4）](https://readingnotesofjinshu.com/translation/biographies/vol-43_7#toc2)
  （@晋書簡訳所）

#### 楽広
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%BD%E5%BA%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%BD%E5%BA%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%BD%E5%BA%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%BD%E5%BA%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%BD%E5%BA%83)
* [書籍 (IA)]
  [晉書斠注(三十二)](https://archive.org/details/02077749.cn/page/n118/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m043.html)
* [ブログ]
  [巻四十三　列伝第十三　楽広](https://readingnotesofjinshu.com/translation/biographies/vol-43_8)
  （@晋書簡訳所）


### 巻44 列伝第14
#### 鄭袤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E8%A2%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E8%A2%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E8%A2%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E8%A2%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E8%A2%A4)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n1/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb1.html)
  （@いつか書きたい『三国志』）

#### 鄭默（鄭黙）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E9%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E9%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E9%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E9%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E9%BB%99)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n7/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb2.html)
  （@いつか書きたい『三国志』）

#### 鄭球
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E7%90%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E7%90%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E7%90%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E7%90%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E7%90%83)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n9/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb2.html)
  （@いつか書きたい『三国志』）

#### 李胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n11/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb3.html)
  （@いつか書きたい『三国志』）

#### 盧欽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E6%AC%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E6%AC%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E6%AC%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E6%AC%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E6%AC%BD)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n15/mode/2up)
  ……最後の方のページが脱落している。
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc1)
  （@晋書簡訳所）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/190)）に「盧欽兼掌」とある。

#### 盧浮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E6%B5%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E6%B5%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E6%B5%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E6%B5%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E6%B5%AE)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc1)
  （@晋書簡訳所）

#### 盧珽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E7%8F%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E7%8F%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E7%8F%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E7%8F%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E7%8F%BD)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc2)
  （@晋書簡訳所）

#### 盧志
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E5%BF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E5%BF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E5%BF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E5%BF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E5%BF%97)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n19/mode/2up)
  ……最初の方のページは脱落しているので途中から。
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc2)
  （@晋書簡訳所）

#### 盧諶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E8%AB%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E8%AB%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E8%AB%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E8%AB%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E8%AB%B6)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n21/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　盧欽](https://readingnotesofjinshu.com/translation/biographies/vol-44_2#toc3)
  （@晋書簡訳所）
* [『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho02-02)に
  「また、陸士衡が文賦には、在木闕不材之質 処雁乏善鳴之分ともあり。」
  とあるが、
  [『十訓抄詳解』](https://dl.ndl.go.jp/info:ndljp/pid/945602/84)でも指摘されているとおり、これは、
  陸機の「文賦」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B717#%E6%96%87%E8%B3%A6)・
    [『国訳漢文大成　文学部第二卷　文選上巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912763/304)）
  ではなく、
  盧諶の「贈劉琨并書」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E8%B4%88%E5%8A%89%E7%90%A8%E5%B9%B6%E6%9B%B8)・
    [『国訳漢文大成　文学部第三卷　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/151)）
  にある。

#### 崔悦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B4%94%E6%82%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B4%94%E6%82%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B4%94%E6%82%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B4%94%E6%82%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B4%94%E6%82%A6)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n25/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb6.html)
  （@いつか書きたい『三国志』）

#### 華表
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%A1%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%A1%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%A1%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%A1%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%A1%A8)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n25/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb7.html)
  （@いつか書きたい『三国志』）

#### 華廙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E5%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E5%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E5%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E5%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E5%BB%99)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n27/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb7.html)
  （@いつか書きたい『三国志』）

#### 華混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb8.html)
  （@いつか書きたい『三国志』）

#### 華薈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%96%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%96%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%96%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%96%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%96%88)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb8.html)
  （@いつか書きたい『三国志』）

#### 華恒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E6%81%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E6%81%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E6%81%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E6%81%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E6%81%92)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb8.html)
  （@いつか書きたい『三国志』）

#### 華嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n35/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb9.html)
  （@いつか書きたい『三国志』）

#### 石鑒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n41/mode/2up)
* [西晉石尠墓誌](http://csid.zju.edu.cn/tomb/stone/detail?id=40288b955ab29f4b015b9fa0c49a006b&rubbingId=40288b955ab29f4b015b9fa0c4a2006c)……石尠（石尟）は石鑒の第二子。
* [PDFあり]
  [魏晉石刻資料選注( 一八 處士石定墓誌 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……石定は石鑒の孫（石尠（石尟）の子）。
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb10.html)
  （@いつか書きたい『三国志』）

#### 温羨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B8%A9%E7%BE%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B8%A9%E7%BE%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B8%A9%E7%BE%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B8%A9%E7%BE%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B8%A9%E7%BE%A8)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n45/mode/2up)
* [ブログ]
  [『晋書』列１４、漢魏からの名族](http://3guozhi.net/h/kb11.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻四十四　列伝第十四　温羨](https://readingnotesofjinshu.com/translation/biographies/vol-44_4)
  （@晋書簡訳所）

### 巻45 列伝第15
#### 劉毅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%AF%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%AF%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%AF%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%AF%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%AF%85)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n51/mode/2up)
* [杜甫「暮秋枉裴道州手札率爾遣興寄近呈蘇渙侍御」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7223#%E6%9A%AE%E7%A7%8B%E6%9E%89%E8%A3%B4%E9%81%93%E5%B7%9E%E6%89%8B%E5%8A%84%EF%BC%8C%E7%8E%87%E7%88%BE%E9%81%A3%E8%88%88%EF%BC%8C%E5%AF%84%E8%BF%91%E5%91%88%E8%98%87%E6%B8%99%E4%BE%8D%E7%A6%A6)
  （訳注は[『続国訳漢文大成 文学部 第6巻の下（杜少陵詩集 下巻の下）』](https://dl.ndl.go.jp/info:ndljp/pid/1239692/392)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 劉暾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%9A%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%9A%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%9A%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%9A%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%9A%BE)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n71/mode/2up)
* [ブログ]
  [巻四十五　列伝第十五　劉毅（2）](https://readingnotesofjinshu.com/translation/biographies/vol-45_2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 劉総
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%B7%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%B7%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%B7%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%B7%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%B7%8F)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n77/mode/2up)

#### 程衛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A8%8B%E8%A1%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A8%8B%E8%A1%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A8%8B%E8%A1%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A8%8B%E8%A1%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A8%8B%E8%A1%9B)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n77/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 和嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%92%8C%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%92%8C%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%92%8C%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%92%8C%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%92%8C%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n77/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「和嶠專車」（第66句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/55)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「長輿追專車之恨」とある。

#### 武陔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AD%A6%E9%99%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AD%A6%E9%99%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AD%A6%E9%99%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AD%A6%E9%99%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AD%A6%E9%99%94)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n83/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 任愷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BB%BB%E6%84%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BB%BB%E6%84%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BB%BB%E6%84%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BB%BB%E6%84%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BB%BB%E6%84%B7)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n87/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/45-5jingai.html)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 崔洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B4%94%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B4%94%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B4%94%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B4%94%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B4%94%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 郭奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n93/mode/2up)
* [顏延之の「五君詠」の「阮始平」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E5%A7%8B%E5%B9%B3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 侯史光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BE%AF%E5%8F%B2%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BE%AF%E5%8F%B2%E5%85%89)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n97/mode/2up)
* [ブログ]
  [えちぜんの書斎](http://www.eonet.ne.jp/~echizen/jinshu/45-7koushikou.html)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)

#### 何攀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%94%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%94%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%94%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%94%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%94%80)
* [書籍 (IA)]
  [晉書斠注(三十三)](https://archive.org/details/02077750.cn/page/n99/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m045.html)


### 巻46 列伝第16
#### 劉頌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%A0%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%A0%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%A0%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%A0%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%A0%8C)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n1/mode/2up)

#### 李重
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E9%87%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E9%87%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E9%87%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E9%87%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E9%87%8D)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n35/mode/2up)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/190)）に「李重之識會」とある。

#### 李毅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%AF%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%AF%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%AF%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%AF%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%AF%85)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n45/mode/2up)

### 巻47 列伝第17
#### 傅玄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E7%8E%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E7%8E%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E7%8E%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E7%8E%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E7%8E%84)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n53/mode/2up)

#### 傅咸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E5%92%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E5%92%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E5%92%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E5%92%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E5%92%B8)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n67/mode/2up)

#### 傅祗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E7%A5%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E7%A5%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E7%A5%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E7%A5%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E7%A5%97)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n89/mode/2up)

#### 傅宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n93/mode/2up)

#### 傅暢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%82%85%E6%9A%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%82%85%E6%9A%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%82%85%E6%9A%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%82%85%E6%9A%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%82%85%E6%9A%A2)
* [書籍 (IA)]
  [晉書斠注(三十四)](https://archive.org/details/02077751.cn/page/n95/mode/2up)


### 巻48 列伝第18
#### 向雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%91%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%91%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%91%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%91%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%91%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n1/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 段灼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B5%E7%81%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B5%E7%81%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B5%E7%81%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B5%E7%81%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B5%E7%81%BC)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n5/mode/2up)

#### 閻纘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%96%BB%E7%BA%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%96%BB%E7%BA%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%96%BB%E7%BA%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%96%BB%E7%BA%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%96%BB%E7%BA%98)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n33/mode/2up)

#### 閻亨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%96%BB%E4%BA%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%96%BB%E4%BA%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%96%BB%E4%BA%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%96%BB%E4%BA%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%96%BB%E4%BA%A8)
* [書籍 (IA)]
  [晉書斠注(三十五)](https://archive.org/details/02077752.cn/page/n49/mode/2up)


### 巻49 列伝第19
#### 阮籍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E7%B1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E7%B1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E7%B1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E7%B1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E7%B1%8D)
* [PDFあり]
  [『晉書』阮籍傳訳註](https://dl.ndl.go.jp/info:ndljp/pid/10504514)
  （鷹橋 明久）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、阮籍伝で言及している
  [「詠懷詩」十七首](https://dl.ndl.go.jp/info:ndljp/pid/1912796/93)がある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、阮籍伝で言及している
  勧進文（ [「為鄭沖勸晉王牋」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/47) ）と
  阮籍伝に引かれた
  [「詣蔣公」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/53)がある。
* 『全三国文』
   [巻44](https://ctext.org/wiki.pl?if=gb&chapter=526858#p52)・
   [巻45](https://ctext.org/wiki.pl?if=gb&chapter=584252)・
   [巻46](https://ctext.org/wiki.pl?if=gb&chapter=924489)
   （画像版の
   [巻44](https://archive.org/details/02107157.cn/page/n34/mode/2up)・
   [巻45](https://archive.org/details/02107157.cn/page/n48/mode/2up)・
   [巻46](https://archive.org/details/02107157.cn/page/n70/mode/2up)）
* [書籍 (NDL)]
  空海『三教指帰』に[「達夜博奕して亦た嗣宗に踰えたり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/11)とある
  （[注59](https://dl.ndl.go.jp/info:ndljp/pid/1040599/37)）。
* 阮籍の青白眼を踏まえた表現
  * [劉因「宿田家」](https://zh.wikisource.org/wiki/%E9%9D%99%E4%BF%AE%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B704#%E5%AE%BF%E7%94%B0%E5%AE%B6)の「青白眼誰静」という句。
  * [倪瓚「述懐」](https://zh.wikisource.org/wiki/%E5%80%AA%E9%9B%B2%E6%9E%97%E8%A9%A9%E9%9B%86_(%E5%9B%9B%E9%83%A8%E5%8F%A2%E5%88%8A%E6%9C%AC)/%E5%8D%B7%E7%AC%AC%E4%B8%80)の「白眼視俗物」という句。
* [顏延之の「五君詠」の「阮步兵」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E6%AD%A5%E5%85%B5)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/68)）
* [杜甫の「秦州雜詩其十五」](https://zh.wikisource.org/wiki/%E7%A7%A6%E5%B7%9E%E9%9B%9C%E8%A9%A9%E5%85%B6%E5%8D%81%E4%BA%94)
  （[『続国訳漢文大成　文学部第17冊』](https://dl.ndl.go.jp/info:ndljp/pid/1140052/56)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「阮籍論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/21)
  （魏禧）
* [『三国志』巻21](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B721#%E9%99%84_%E9%98%AE%E7%B1%8D)

#### 阮咸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E5%92%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E5%92%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E5%92%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E5%92%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E5%92%B8)
* [書誌のみ]
  [阮咸伝(『晋書』巻四十九)訳注 (六朝詩の語彙および表現技巧の研究)](https://cir.nii.ac.jp/crid/1520853835396667648)
  （鷹橋 明久）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「仲容青雲」（第55句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/49)
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p51)
* 白居易「[和令狐僕射小飲聽阮咸](https://zh.wikisource.org/wiki/%E5%92%8C%E4%BB%A4%E7%8B%90%E5%83%95%E5%B0%84%E5%B0%8F%E9%A3%B2%E8%81%BD%E9%98%AE%E5%92%B8)」
  ……阮咸にちなんだ[楽器の阮咸](https://ja.wikipedia.org/wiki/%E9%98%AE%E5%92%B8_(%E6%A5%BD%E5%99%A8))の方を詠んだもの。
* [顏延之の「五君詠」の「阮始平」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E9%98%AE%E5%A7%8B%E5%B9%B3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E7%9E%BB)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「阮瞻三語」（第110句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/78)
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p57)
* 干宝[『搜神記』巻16](https://ctext.org/wiki.pl?if=gb&chapter=5557#p4)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮孚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E5%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E5%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E5%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E5%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E5%AD%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮脩（阮修）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E4%BF%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E4%BF%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E4%BF%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E4%BF%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E4%BF%AE)
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p61)
* [書籍 (NDL)]
  空海『三教指帰』に[「一百の青鳧常に杖の頭に懸けたり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)とある
  （[注74・75](https://dl.ndl.go.jp/info:ndljp/pid/1040599/38)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮放
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E6%94%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E6%94%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E6%94%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E6%94%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E6%94%BE)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 阮裕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E8%A3%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E8%A3%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E8%A3%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E8%A3%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E8%A3%95)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 嵆康（嵇康）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B5%87%E5%BA%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B5%87%E5%BA%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B5%87%E5%BA%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B5%87%E5%BA%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B5%87%E5%BA%B7)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「嵇呂命駕」（第25句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/32)と
  [「叔夜玉山」（第56句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/50)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、嵆康伝に引かれた
  [「幽憤詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/102)がある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、嵆康伝に（間引きながら）引かれた
  [「與山巨源絕交書」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/99)があり、
  嵆康伝では言及しているだけの
  [「養生論」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/285)もある。
* 『全三国文』
  [巻47](https://ctext.org/wiki.pl?if=gb&chapter=383681)・
  [巻48](https://ctext.org/wiki.pl?if=gb&chapter=930561)・
  [巻49](https://ctext.org/wiki.pl?if=gb&chapter=611925)・
  [巻50](https://ctext.org/wiki.pl?if=gb&chapter=997803)・
  [巻51](https://ctext.org/wiki.pl?if=gb&chapter=409217)・
  [巻52](https://ctext.org/wiki.pl?if=gb&chapter=607386)
* [顏延之の「五君詠」の「嵇中散」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%B5%87%E4%B8%AD%E6%95%A3)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/68)）
* [顏延之の「五君詠」の「向常侍」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%90%91%E5%B8%B8%E4%BE%8D)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)
* [『三国志』巻21](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B721#%E9%99%84_%E5%B5%87%E5%BA%B7)
* [李攀龍「白雪樓」](https://zh.wikisource.org/wiki/%E6%BB%84%E6%BA%9F%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B709#%E7%99%BD%E9%9B%AA%E6%A8%93)に「無那嵇生成懶慢」とある。
* 嵆康（嵇康）の字の「叔夜」と同名の人（または字が同じ人）
    * 叔夜
        * [『論語』微子篇](https://zh.wikisource.org/wiki/%E8%AB%96%E8%AA%9E/%E5%BE%AE%E5%AD%90%E7%AC%AC%E5%8D%81%E5%85%AB)（[『漢籍國字解全書 : 先哲遺著追補』第30巻](https://dl.ndl.go.jp/pid/1226101/1/199?keyword=%E5%8F%94%E5%A4%9C)）に「周有八士：伯達、伯适、仲突、仲忽、叔夜、叔夏、季隨、季騧。」とある。
    * 柳叔夜
        * [『南史』巻73](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B773)
    * 鄭叔夜（立伝されている鄭羲の兄）
        * [『魏書』巻56](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B756#%E9%84%AD%E7%BE%B2)
        * [『北史』巻35](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7035#%E7%BE%B2%E5%BE%9E%E5%AD%90%E3%80%88%E7%BE%B2%E6%AC%A1%E5%85%84%E5%B0%8F%E7%99%BD%E5%AD%90%E3%80%89_%E8%83%A4%E4%BC%AF)
    * 康叔夜
        * [『旧唐書』巻151（伊慎）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7151#%E4%BC%8A%E6%85%8E)
        * [『資治通鑑』巻228](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7228)（[『続国訳漢文大成 経子史部』第13巻](https://dl.ndl.go.jp/pid/1239987/1/105?keyword=%E5%BA%B7%E5%8F%94%E5%A4%9C)）
        * [『資治通鑑』巻231](https://zh.wikisource.org/wiki/%E8%B3%87%E6%B2%BB%E9%80%9A%E9%91%92_(%E8%83%A1%E4%B8%89%E7%9C%81%E9%9F%B3%E6%B3%A8)/%E5%8D%B7231)（[『続国訳漢文大成 経子史部』第13巻](https://dl.ndl.go.jp/pid/1239987/1/156?keyword=%E5%BA%B7%E5%8F%94%E5%A4%9C)）
    * 宋叔夜（立伝されている宋申錫の父）
        * [『旧唐書』巻167](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7167#%E5%AE%8B%E7%94%B3%E9%8C%AB)
    * 朱叔夜
        * [『旧唐書』巻17下（大和七年十一月）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B717%E4%B8%8B#%E5%A4%A7%E5%92%8C%E4%B8%83%E5%B9%B4)
        * [『新唐書』巻164（殷侑）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7164#%E6%AE%B7%E4%BE%91)
    * 趙叔夜
        * [『宋史』巻234（宗室世系表）](https://commons.wikimedia.org/w/index.php?title=File:CADAL06060999_%E5%AE%8B%E5%8F%B2%C2%B7%E5%8D%B7%E4%BA%8C%E7%99%BE%E4%B8%89%E5%8D%81%E5%9B%9B.djvu&page=13)……[Wikisourceのこの巻](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7234)は未完成。
    * 張叔夜（しかもこの人の字は「嵇仲」である）
        * [『宋史』巻353](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7353#%E5%BC%B5%E5%8F%94%E5%A4%9C)
    * 周思兼（字が叔夜）
        * [『明史』巻208](https://zh.wikisource.org/wiki/%E6%98%8E%E5%8F%B2/%E5%8D%B7208)
* 陸龜蒙[「秘色越器」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7629#%E7%A7%98%E8%89%B2%E8%B6%8A%E5%99%A8)に「共嵇中散鬬遺杯」とあり、幸田露伴[『蝸牛庵聯話』](https://dl.ndl.go.jp/pid/1130069/1/11?keyword=%E5%B5%87%E4%B8%AD%E6%95%A3)で「嵇中散は晉の高士嵇康なり」云々と言及されている。

#### 向秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%91%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%91%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%91%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%91%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%91%E7%A7%80)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、向秀伝に引かれた
  [「思舊賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/291)がある。
* 『全晋文』
  [巻72](https://ctext.org/wiki.pl?if=gb&chapter=277540#p38)
* [顏延之の「五君詠」の「向常侍」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%90%91%E5%B8%B8%E4%BE%8D)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/69)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 劉伶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E4%BC%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E4%BC%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E4%BC%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E4%BC%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E4%BC%B6)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、劉伶伝に引かれた
  [「酒德頌」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/200)がある。
* 『全晋文』
  [巻66](https://ctext.org/wiki.pl?if=gb&chapter=171814)
* [顏延之の「五君詠」の「劉參軍」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B721#%E5%8A%89%E5%8F%83%E8%BB%8D)
  （[『国訳漢文大成　文学部第三巻　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/68)）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 謝鯤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%AF%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%AF%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%AF%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%AF%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%AF%A4)
* [東晉謝鯤墓誌](http://csid.zju.edu.cn/tomb/stone/detail?id=40288b955ab29f4b015b9fa0c4e00073&rubbingId=40288b955ab29f4b015b9fa0c4e80074)
* [PDFあり]
  [魏晉石刻資料選注( 一九 謝府君神道闕 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……謝府君こと謝纘は謝鯤の祖父。
* 干宝[『搜神記』巻18](https://ctext.org/wiki.pl?if=gb&chapter=232809#p18)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)
* [『六十種曲』](https://zh.wikisource.org/wiki/%E5%85%AD%E5%8D%81%E7%A8%AE%E6%9B%B2)所収の[『投梭記』](https://commons.wikimedia.org/w/index.php?search=%22%E6%8A%95%E6%A2%AD%E8%A8%98%22&title=Special:MediaSearch&go=Go&type=other)は謝鯤を題材にしている。

#### 胡毋輔之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E6%AF%8B%E8%BC%94%E4%B9%8B)
* [ブログ]
  [胡毋輔之伝](http://strawberrymilk.gooside.com/text/shinjo/49-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 胡毋謙之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E6%AF%8B%E8%AC%99%E4%B9%8B)
* [ブログ]
  [胡毋輔之伝](http://strawberrymilk.gooside.com/text/shinjo/49-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 畢卓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%95%A2%E5%8D%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%95%A2%E5%8D%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%95%A2%E5%8D%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%95%A2%E5%8D%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%95%A2%E5%8D%93)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [畢卓伝](http://strawberrymilk.gooside.com/text/shinjo/49-7.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [書籍 (NDL)]
  空海『三教指帰』に[「觴を提げ蟹を捕るの行専ら胸の中に蘊めり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)とある
  （[注72](https://dl.ndl.go.jp/info:ndljp/pid/1040599/38)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 王尼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%B0%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%B0%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%B0%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%B0%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%B0%BC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 羊曼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E6%9B%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E6%9B%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E6%9B%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E6%9B%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E6%9B%BC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 羊聃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E8%81%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E8%81%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E8%81%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E8%81%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E8%81%83)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)

#### 光逸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%85%89%E9%80%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%85%89%E9%80%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%85%89%E9%80%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%85%89%E9%80%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%85%89%E9%80%B8)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m049.html)


### 巻50 列伝第20
#### 曹志
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9B%B9%E5%BF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9B%B9%E5%BF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9B%B9%E5%BF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9B%B9%E5%BF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9B%B9%E5%BF%97)
* [ブログ]
  [『晋書』「曹志伝」(前半)](http://humiarisaka.blog40.fc2.com/blog-entry-60.html)と[『晋書』「曹志伝」（後半）](http://humiarisaka.blog40.fc2.com/blog-entry-61.html)
  （@「私家版 曹子建集」ブログ）
* [ブログ]
  [『晋書』列２０より、曹操の「孫」](http://3guozhi.net/s/ss1.html)
  （@いつか書きたい『三国志』）


#### 庾峻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%B3%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%B3%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%B3%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%B3%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%B3%BB)

#### 庾珉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%91%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%91%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%91%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%91%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%91%89)

#### 庾敳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%95%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%95%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%95%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%95%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%95%B3)
* [書籍 (NDL)]
  空海『三教指帰』に[「森森たる千仭此の庾嵩に比せん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注125](https://dl.ndl.go.jp/info:ndljp/pid/1040599/44)にもあるように、庾敳の字は子嵩）。

#### 郭象
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E8%B1%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E8%B1%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E8%B1%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E8%B1%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E8%B1%A1)
* [書籍 (NDL)]
  空海『三教指帰』に[「張儀郭象も遙かに瞻て聲を飲む」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/9)とあり
  （[注7](https://dl.ndl.go.jp/info:ndljp/pid/1040599/34)）、
  [「郭象が靈翰萬たび集めても何ぞ論ぜん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/29)ともある
  （[注151](https://dl.ndl.go.jp/info:ndljp/pid/1040599/61)）。

#### 庾純
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%B4%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%B4%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%B4%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%B4%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%B4%94)

#### 庾旉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%97%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%97%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%97%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%97%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%97%89)

#### 秦秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A7%A6%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A7%A6%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A7%A6%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A7%A6%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A7%A6%E7%A7%80)
* [ブログ]
  [『晋書』列２０より、曹操の「孫」](http://3guozhi.net/s/ss2.html)
  （@いつか書きたい『三国志』）


### 巻51 列伝第21
#### 皇甫謐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E8%AC%90)

#### 皇甫方回
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E6%96%B9%E5%9B%9E)

#### 摯虞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%91%AF%E8%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%91%AF%E8%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%91%AF%E8%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%91%AF%E8%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%91%AF%E8%99%9E)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/193)）に「荀摯競爽於晉世」とあり、注によれば「摯」は摯虞のことを指す。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「太叔辨洽（大叔辨給）」「摯仲辭翰（摯仲詞翰）」（第79・80句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/63)

#### 束晳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9F%E6%99%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9F%E6%99%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9F%E6%99%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9F%E6%99%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9F%E6%99%B3)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、束晳伝で言及している
  [「補亡詩」六首](https://dl.ndl.go.jp/info:ndljp/pid/1912796/22)がある。

#### 王接
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%8E%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%8E%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%8E%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%8E%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%8E%A5)


### 巻52 列伝第22
#### 郤詵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%A4%E8%A9%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%A4%E8%A9%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%A4%E8%A9%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%A4%E8%A9%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%A4%E8%A9%B5)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「郤詵一枝」（第45句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/44)

#### 阮种（阮種）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%98%AE%E7%A8%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%98%AE%E7%A8%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%98%AE%E7%A8%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%98%AE%E7%A8%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%98%AE%E7%A8%AE)

#### 華譚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%AD%9A)

#### 袁甫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E7%94%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E7%94%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E7%94%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E7%94%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E7%94%AB)


### 巻53 列伝第23
#### 愍懐太子（司馬遹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%B9)
* [ブログ]
  [『晋書』列伝２３、司馬遹「愍懐太子伝」翻訳！](http://3guozhi.net/zaka2/bin1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)

#### 司馬虨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%99%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%99%A8)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)

#### 司馬臧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%87%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%87%A7)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)

#### 司馬尚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%B0%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%B0%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m053.html)


### 巻54 列伝第24
#### 陸機
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E6%A9%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E6%A9%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E6%A9%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E6%A9%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E6%A9%9F)
* [PDFあり]
  [六朝文人伝 : 陸機・陸雲伝(晋書)](https://dl.ndl.go.jp/info:ndljp/pid/10504338)
  （長谷川 滋成）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「士衡患多」（第22句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/30)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、陸機伝に引かれた
  [「豪士賦序」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/173)と、
  [「辯亡論上下二首」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/294)と、
  [「五等論」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/302)とがある。
* [『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho02-02)に
  「また、陸士衡が文賦には、在木闕不材之質 処雁乏善鳴之分ともあり。」
  とあるが、
  [『十訓抄詳解』](https://dl.ndl.go.jp/info:ndljp/pid/945602/84)でも指摘されているとおり、これは、
  陸機の「文賦」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B717#%E6%96%87%E8%B3%A6)・
    [『国訳漢文大成　文学部第二卷　文選上巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912763/304)）
  ではなく、
  盧諶の「贈劉琨并書」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E8%B4%88%E5%8A%89%E7%90%A8%E5%B9%B6%E6%9B%B8)・
    [『国訳漢文大成　文学部第三卷　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/151)）
  にある。
* [胡曾の「華亭」](https://zh.wikisource.org/wiki/%E8%8F%AF%E4%BA%AD)
* [姚承緒『吳趨訪古錄』の「張翰墓」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/6#%E5%BC%B5%E7%BF%B0%E5%A2%93)
* [姚承緒『吳趨訪古錄』の「菽園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E8%8F%BD%E5%9C%92)
  ……冒頭の「平原」は陸機のことではなかろうか。
* [李白「行路難」三](https://zh.wikisource.org/wiki/%E8%A1%8C%E8%B7%AF%E9%9B%A3_(%E6%9C%89%E8%80%B3%E8%8E%AB%E6%B4%97%E6%BD%81%E5%B7%9D%E6%B0%B4))
  （[『続国訳漢文大成 文学部第2冊（李太白詩集 上の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139917/43)）
* [章太炎（章炳麟）「陸機贊」](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%82%8E%E6%96%87%E9%8C%84%E5%88%9D%E7%B7%A8/%E9%99%B8%E6%A9%9F%E8%B4%8A)

#### 孫拯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%8B%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%8B%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%8B%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%8B%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%8B%AF)

#### 陸雲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E9%9B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E9%9B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E9%9B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E9%9B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E9%9B%B2)
* [PDFあり]
  [六朝文人伝 : 陸機・陸雲伝(晋書)](https://dl.ndl.go.jp/info:ndljp/pid/10504338)
  （長谷川 滋成）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「鳴鶴日下」「士龍雲間」（第49・50句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/46)
* 清の時代に黃子雲という人がおり、諱の「雲」の文字が陸雲と共通するだけでなく、字も陸雲とお揃いの「士龍」である。ちなみに号は「野鴻」。
    * [百度百科](https://baike.baidu.com/item/%E9%BB%84%E5%AD%90%E4%BA%91/5582620)
    * 黃子雲[『野鴻詩的』](https://commons.wikimedia.org/w/index.php?title=File:SSID-12362315_%E6%B8%85%E8%A9%A9%E8%A9%B1_%E7%A7%8B%E7%AA%97%E9%9A%A8%E7%AD%86%E8%87%B3%E9%87%8E%E9%B4%BB%E8%A9%A9%E7%9A%84.pdf&page=34)
    * 沈藻采[『元和唯亭志』](https://commons.wikimedia.org/w/index.php?title=File%3ANLC403-312001066074-66329_%E5%85%83%E5%92%8C%E5%94%AF%E4%BA%AD%E5%BF%97_%E6%B8%85%E9%81%93%E5%85%8928%E5%B9%B4(1848)_%E5%8D%B7%E4%B8%80%E5%8D%81%E5%85%AB.pdf&page=6)
    * 徐傳詩[『星湄詩話』](https://commons.wikimedia.org/w/index.php?title=File%3ACADAL02120888_%E6%98%9F%E6%B9%84%E8%A9%A9%E8%A9%B1%EF%BC%88%E4%BA%8C%EF%BC%89.djvu&page=10)

#### 陸耽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E8%80%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E8%80%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E8%80%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E8%80%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E8%80%BD)

#### 陸喜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E5%96%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E5%96%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E5%96%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E5%96%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E5%96%9C)


### 巻55 列伝第25
#### 夏侯湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E6%B9%9B)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「岳湛連璧」（第44句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/43)

#### 夏侯淳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E6%B7%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E6%B7%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E6%B7%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E6%B7%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E6%B7%B3)

#### 夏侯承
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E4%BE%AF%E6%89%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E4%BE%AF%E6%89%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E4%BE%AF%E6%89%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E4%BE%AF%E6%89%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E4%BE%AF%E6%89%BF)

#### 潘岳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BD%98%E5%B2%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BD%98%E5%B2%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BD%98%E5%B2%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BD%98%E5%B2%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BD%98%E5%B2%B3)
* [PDFあり]
  [六朝文人伝 : 「晋書」潘岳伝](https://dl.ndl.go.jp/info:ndljp/pid/10504358)
  （坂元 悦夫）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「岳湛連璧」（第44句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/43)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、潘岳伝に引かれた
  [「藉田賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/136)と
  [「閒居賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/284)があり、
  潘岳伝では言及しているだけの
  [「西征賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/178)もある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、潘岳伝に一部が引かれた
  [「金谷集作詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/54)がある。
* [書籍 (NDL)]
  空海『三教指帰』に[「潘安が詩を詠じて彌〻哀哭を増し」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/26)とある
  （[注132](https://dl.ndl.go.jp/info:ndljp/pid/1040599/60)）。
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……「河陽一縣花」は潘岳に関する故事らしい。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）
* [周文質「蝶戀花・悟迷」](https://ctext.org/wiki.pl?if=gb&chapter=390671#p102)に「青樓空擲潘安果」とある。

#### 潘尼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BD%98%E5%B0%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BD%98%E5%B0%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BD%98%E5%B0%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BD%98%E5%B0%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BD%98%E5%B0%BC)

#### 張載
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%BC%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%BC%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%BC%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%BC%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%BC%89)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、張載伝に引かれた
  [「劍閣銘」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/337)がある。
* [『旧五代史』巻136の「史臣曰」のところ](https://zh.wikisource.org/wiki/%E8%88%8A%E4%BA%94%E4%BB%A3%E5%8F%B2/%E5%8D%B7136#%E5%8F%B2%E8%87%A3%E6%9B%B0)に張載の「劍閣銘」が引かれている。

#### 張協
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%8D%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%8D%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%8D%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%8D%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%8D%94)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、張協伝に引かれた
  [「七命」八首](https://dl.ndl.go.jp/info:ndljp/pid/1912796/365)がある。

#### 張亢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E4%BA%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E4%BA%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E4%BA%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E4%BA%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E4%BA%A2)


### 巻56 列伝第26
#### 江統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E7%B5%B1)
* [ブログ]
  [「江統伝」を訳し、『晋書』列伝２６をコンプ](http://3guozhi.net/zaka2/ss2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻五十六 列伝第二十六 江統(1)](https://readingnotesofjinshu.com/translation/biographies/vol-56_1)・
  [(2)](https://readingnotesofjinshu.com/translation/biographies/vol-56_2)
  （@晋書簡訳所）
* [ブログ]
  [徙戎論その1](https://t-s.hatenablog.com/entry/20110527/1306422647)・
  [その2](https://t-s.hatenablog.com/entry/20110528/1306555175)・
  [その3](https://t-s.hatenablog.com/entry/20110529/1306595018)・
  [その4](https://t-s.hatenablog.com/entry/20110531/1306767771)・
  [その5](https://t-s.hatenablog.com/entry/20110601/1306854098)・
  [その6](https://t-s.hatenablog.com/entry/20110602/1306940568)・
  [その7](https://t-s.hatenablog.com/entry/20110604/1307116473)・
  [その8](https://t-s.hatenablog.com/entry/20110605/1307199754)・
  [その9](https://t-s.hatenablog.com/entry/20110606/1307286166)
  （@てぃーえすのメモ帳）

#### 江虨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E8%99%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E8%99%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E8%99%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E8%99%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E8%99%A8)
* [ブログ]
  [巻五十六 列伝第二十六 江統(3)](https://readingnotesofjinshu.com/translation/biographies/vol-56_3#toc1)
  （@晋書簡訳所）

#### 江惇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E6%83%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E6%83%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E6%83%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E6%83%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E6%83%87)
* [ブログ]
  [巻五十六 列伝第二十六 江統(3)](https://readingnotesofjinshu.com/translation/biographies/vol-56_3#toc2)
  （@晋書簡訳所）

#### 孫楚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%A5%9A)
* [ブログ]
  [『晋書』列伝２６より、「孫楚伝」を翻訳](http://3guozhi.net/zaka2/ss1.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、孫楚伝に引かれた
  [「為石仲容與孫皓書」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/105)がある。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「孫楚漱石」（第71句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/58)

#### 孫統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%B5%B1)

#### 孫綽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%B6%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%B6%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%B6%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%B6%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%B6%BD)
* [書誌のみ]
  [『晋書』巻五六孫綽伝訳注](https://cir.nii.ac.jp/crid/1572261551687976448)
  （長谷川 滋成）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、孫綽伝で言及している
  [「遊天台山賦并序」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/204)がある。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「孫綽才冠」（第78句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/63)
* [書籍 (NDL)]
  空海『三教指帰』に[「玲玲として玉のごとくに振ひ孫馬を凌いで以て瑤を連ね」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注131](https://dl.ndl.go.jp/info:ndljp/pid/1040599/45)）。
* 謝肇淛の[『五雜俎』](https://zh.wikisource.org/wiki/%E4%BA%94%E9%9B%9C%E4%BF%8E/%E5%8D%B716)に「或改之曰：「孫綽賦天臺景，赤城霞起而建標；杜牧詠江南春，十里鶯啼而映綠。」」云々という言及があり、同様の言及（ただし「赤城」でなく「高城」）が蔣一葵の[『堯山堂偶雋』](https://zh.wikisource.org/wiki/%E5%A0%AF%E5%B1%B1%E5%A0%82%E5%81%B6%E9%9B%8B/%E5%8D%B7%E4%B8%89)にもある。

### 巻57 列伝第27
#### 羅憲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E6%86%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E6%86%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E6%86%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E6%86%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E6%86%B2)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [呉に益州は獲らせぬ、羅憲伝。](http://3guozhi.net/hito/rake1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 羅尚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E5%B0%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E5%B0%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E5%B0%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E5%B0%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E5%B0%9A)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [呉に益州は獲らせぬ、羅憲伝。](http://3guozhi.net/hito/rake2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 滕脩（滕修）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BB%95%E4%BF%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BB%95%E4%BF%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BB%95%E4%BF%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BB%95%E4%BF%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BB%95%E4%BF%AE)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 馬隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AC%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AC%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AC%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AC%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AC%E9%9A%86)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 胡奮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E5%A5%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E5%A5%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E5%A5%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E5%A5%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E5%A5%AE)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 陶璜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%92%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%92%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%92%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%92%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%92%9C)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 吾彦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%BE%E5%BD%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%BE%E5%BD%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%BE%E5%BD%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%BE%E5%BD%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%BE%E5%BD%A6)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%C6%F3%BD%BD%BC%B7)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 張光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%85%89)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)

#### 趙誘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B6%99%E8%AA%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B6%99%E8%AA%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B6%99%E8%AA%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B6%99%E8%AA%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B6%99%E8%AA%98)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%C6%F3%BD%BD%BC%B7)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m057.html)


### 巻58 列伝第28
#### 周処
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%87%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%87%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%87%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%87%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%87%A6)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「周處三害」（第28句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/33)
* 『漢魏六朝百三名家集』所収『陸平原集』の「晉平西將軍孝侯周處碑」
  （[デジコレの画像版](https://dl.ndl.go.jp/info:ndljp/pid/2551366/90)と[Wikisourceのテキスト版](https://zh.wikisource.org/wiki/%E6%BC%A2%E9%AD%8F%E5%85%AD%E6%9C%9D%E7%99%BE%E4%B8%89%E5%AE%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7048#%E7%A2%91)）
* [PDFあり]
  [正史成立前夜 : 碑・誌の時代](https://doi.org/10.14945/00009538)
  （山田 智）
  ……「晉平西將軍孝侯周處碑」について論じている。
* [書籍 (NDL)]
  空海『三教指帰』に[「周處心を改めて忠孝の名を得たり」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/11)とある
  （[注49](https://dl.ndl.go.jp/info:ndljp/pid/1040599/36)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周玘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E7%8E%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E7%8E%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E7%8E%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E7%8E%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E7%8E%98)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周勰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%8B%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%8B%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%8B%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%8B%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%8B%B0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周彝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%BD%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%BD%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%BD%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%BD%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%BD%9D)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周札
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%9C%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%9C%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%9C%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%9C%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%9C%AD)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周莚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%8E%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%8E%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%8E%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%8E%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%8E%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周訪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%A8%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%A8%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%A8%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%A8%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%A8%AA)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周撫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%92%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%92%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%92%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%92%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%92%AB)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周楚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%A5%9A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周瓊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E7%93%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E7%93%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E7%93%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E7%93%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E7%93%8A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周虓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%99%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%99%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%99%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%99%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%99%93)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%85%89)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)

#### 周仲孫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E4%BB%B2%E5%AD%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E4%BB%B2%E5%AD%AB)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m058.html)


### 巻59 列伝第29
#### 汝南文成王（司馬亮）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%BA%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%BA%AE)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc1)
  （@晋書簡訳所）

#### 司馬粹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B2%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B2%B9)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc2)
  （@晋書簡訳所）

#### 司馬矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%9F%A9)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc2)
  （@晋書簡訳所）

#### 司馬祐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%A5%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%A5%90)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc2)
  （@晋書簡訳所）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)

#### 司馬羕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%BE%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%BE%95)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc3)
  （@晋書簡訳所）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)

#### 司馬宗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%AE%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%AE%97)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc4)
  （@晋書簡訳所）
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)

#### 司馬熙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%86%99)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc5)
  （@晋書簡訳所）

#### 楚隠王（司馬瑋）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%91%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%91%8B)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　汝南王亮　楚王瑋](https://readingnotesofjinshu.com/translation/biographies/vol-59_2#toc6)
  （@晋書簡訳所）

#### 趙王（司馬倫）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%80%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%80%AB)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　趙王倫](https://readingnotesofjinshu.com/translation/biographies/vol-59_3)
  （@晋書簡訳所）
* [周曇の「詠史詩」の中の「賈后」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%B3%88%E5%90%8E)

#### 斉武閔王（司馬冏）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%86%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%86%8F)
* [ブログ]
  [『晋書』列伝２９より、「司馬冏伝」を抄訳](http://3guozhi.net/zaka2/sk.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　斉王冏](https://readingnotesofjinshu.com/translation/biographies/vol-59_4#toc1)
  （@晋書簡訳所）
* [周曇の「詠史詩」の中の「賈后」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%B3%88%E5%90%8E)

#### 鄭方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E6%96%B9)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　斉王冏](https://readingnotesofjinshu.com/translation/biographies/vol-59_4#toc2)
  （@晋書簡訳所）

#### 長沙厲王（司馬乂）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E4%B9%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E4%B9%82)
* [ブログ]
  [『晋書』列伝２９より、「司馬乂伝」を翻訳](http://3guozhi.net/n/sg.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　長沙王乂　成都王穎](https://readingnotesofjinshu.com/translation/biographies/vol-59_5#toc1)
  （@晋書簡訳所）
* 干宝[『搜神記』巻7](https://ctext.org/wiki.pl?if=gb&chapter=749239#p29)

#### 成都王（司馬潁）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BD%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BD%81)
* [ブログ]
  [『晋書』列伝２９より、「司馬頴伝」を翻訳](http://3guozhi.net/n/se.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　長沙王乂　成都王穎](https://readingnotesofjinshu.com/translation/biographies/vol-59_5#toc2)
  （@晋書簡訳所）
* 干宝[『搜神記』巻7](https://ctext.org/wiki.pl?if=gb&chapter=749239#p29)

#### 河間王（司馬顒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%A1%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%A1%92)
* [ブログ]
  [『晋書』列伝２９より、「司馬顒伝」を翻訳](http://3guozhi.net/zaka2/sg.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　河間王顒　東海王越](https://readingnotesofjinshu.com/translation/biographies/vol-59_6#toc1)
  （@晋書簡訳所）

#### 東海孝献王（司馬越）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%B6%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%B6%8A)
* [ブログ]
  [『晋書』列伝２９より、「司馬越伝」を翻訳](http://3guozhi.net/zaka2/et1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m059.html)
* [ブログ]
  [巻五十九　列伝第二十九　河間王顒　東海王越](https://readingnotesofjinshu.com/translation/biographies/vol-59_6#toc2)
  （@晋書簡訳所）


### 巻60 列伝第30
#### 解系
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A7%A3%E7%B3%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A7%A3%E7%B3%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A7%A3%E7%B3%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A7%A3%E7%B3%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A7%A3%E7%B3%BB)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 解結
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A7%A3%E7%B5%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A7%A3%E7%B5%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A7%A3%E7%B5%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A7%A3%E7%B5%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A7%A3%E7%B5%90)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 解育
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A7%A3%E8%82%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A7%A3%E8%82%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A7%A3%E8%82%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A7%A3%E8%82%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A7%A3%E8%82%B2)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 孫旂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%97%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%97%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%97%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%97%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%97%82)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 孟観
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E8%A6%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E8%A6%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E8%A6%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E8%A6%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E8%A6%B3)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 牽秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%89%BD%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%89%BD%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%89%BD%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%89%BD%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%89%BD%E7%A7%80)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻六十 列伝第三十 解系 孫旂 孟観 牽秀](https://readingnotesofjinshu.com/translation/biographies/vol-60_1#toc4)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 繆播
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B9%86%E6%92%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B9%86%E6%92%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B9%86%E6%92%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B9%86%E6%92%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B9%86%E6%92%AD)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 繆胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B9%86%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B9%86%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B9%86%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B9%86%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B9%86%E8%83%A4)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 皇甫重
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E9%87%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E9%87%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E9%87%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E9%87%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E9%87%8D)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 張輔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%BC%94)
* [ブログ]
  [巻六十 列伝第三十 繆播 皇甫重 張輔](https://readingnotesofjinshu.com/translation/biographies/vol-60_2#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 李含
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%90%AB)
* [ブログ]
  [巻六十 列伝第三十 李含 張方](https://readingnotesofjinshu.com/translation/biographies/vol-60_3#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 張方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%96%B9)
* [ブログ]
  [巻六十 列伝第三十 李含 張方](https://readingnotesofjinshu.com/translation/biographies/vol-60_3#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 閻鼎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%96%BB%E9%BC%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%96%BB%E9%BC%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%96%BB%E9%BC%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%96%BB%E9%BC%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%96%BB%E9%BC%8E)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 索靖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E9%9D%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E9%9D%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E9%9D%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E9%9D%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E9%9D%96)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「瓘靖二妙」（第43句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/42)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 索綝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E7%B6%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E7%B6%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E7%B6%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E7%B6%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E7%B6%9D)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)

#### 賈疋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E7%96%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E7%96%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E7%96%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E7%96%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E7%96%8B)
* [ブログ]
  [巻六十 列伝第三十 閻鼎 索靖 賈疋](https://readingnotesofjinshu.com/translation/biographies/vol-60_4#toc4)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m060.html)


### 巻61 列伝第31
#### 周浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n56/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 周嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n60/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 周謨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%AC%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%AC%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%AC%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%AC%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%AC%A8)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n66/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 周馥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%A6%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%A6%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%A6%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%A6%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%A6%A5)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n68/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 成公簡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%90%E5%85%AC%E7%B0%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%90%E5%85%AC%E7%B0%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%90%E5%85%AC%E7%B0%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%90%E5%85%AC%E7%B0%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%90%E5%85%AC%E7%B0%A1)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n74/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 苟晞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%9F%E6%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%9F%E6%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%9F%E6%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%9F%E6%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%9F%E6%99%9E)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n74/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 華軼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8F%AF%E8%BB%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8F%AF%E8%BB%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8F%AF%E8%BB%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8F%AF%E8%BB%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8F%AF%E8%BB%BC)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n88/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 劉喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n92/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [黄中通理を知っているか？劉廙、孫劉喬伝](http://3guozhi.net/hito/riy1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 劉耽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%80%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%80%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%80%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%80%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%80%BD)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n100/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* 干宝[『搜神記』巻5](https://ctext.org/wiki.pl?if=gb&chapter=503309#p4)
  （劉耽の子が出てくる）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)

#### 劉柳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%9F%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%9F%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%9F%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%9F%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%9F%B3)
* [書籍 (IA)]
  [晉書斠注(四十二)](https://archive.org/details/02077759.cn/page/n100/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m061.html)


### 巻62 列伝第32
#### 劉琨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%90%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%90%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%90%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%90%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%90%A8)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n2/mode/2up)
* [PDFあり]
  [劉琨年譜稿](https://dl.ndl.go.jp/info:ndljp/pid/10504350)
  （坂元 悦夫）
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（1）](https://readingnotesofjinshu.com/translation/biographies/vol-62_1)、
  [同（2）](https://readingnotesofjinshu.com/translation/biographies/vol-62_2)、
  [同（3）](https://readingnotesofjinshu.com/translation/biographies/vol-62_3)
  （@晋書簡訳所）
* [『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho02-02)に
  「また、陸士衡が文賦には、在木闕不材之質 処雁乏善鳴之分ともあり。」
  とあるが、
  [『十訓抄詳解』](https://dl.ndl.go.jp/info:ndljp/pid/945602/84)でも指摘されているとおり、これは、
  陸機の「文賦」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B717#%E6%96%87%E8%B3%A6)・
    [『国訳漢文大成　文学部第二卷　文選上巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912763/304)）
  ではなく、
  盧諶の「贈劉琨并書」
  （[Wikisource](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E8%B4%88%E5%8A%89%E7%90%A8%E5%B9%B6%E6%9B%B8)・
    [『国訳漢文大成　文学部第三卷　文選中巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/151)）
  にある。

#### 劉羣（劉群）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%BE%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%BE%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%BE%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%BE%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%BE%A4)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n34/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc1)
  （@晋書簡訳所）

#### 劉輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n36/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc2)
  （@晋書簡訳所）

#### 劉演
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%BC%94)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉挹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%8C%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%8C%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%8C%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%8C%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%8C%B9)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉啟（劉啓）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%95%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%95%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%95%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%95%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%95%93)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 劉述
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%BF%B0)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列３２、劉琨伝の翻訳](http://3guozhi.net/h/rk6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻六十二　列伝第三十二　劉琨（4）](https://readingnotesofjinshu.com/translation/biographies/vol-62_4#toc3)
  （@晋書簡訳所）

#### 祖逖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E9%80%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E9%80%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E9%80%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E9%80%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E9%80%96)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n40/mode/2up)
* [文天祥「正氣歌」](https://zh.wikisource.org/wiki/%E6%AD%A3%E6%B0%A3%E6%AD%8C)の「或為渡江楫　慷慨吞胡羯」という句
* [ブログ]
  [巻六十二　列伝第三十二　祖逖（1）](https://readingnotesofjinshu.com/translation/biographies/vol-62_5)
  （@晋書簡訳所）
* [胡曾の「豫州」](https://zh.wikisource.org/wiki/%E8%B1%AB%E5%B7%9E)

#### 祖納
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E7%B4%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E7%B4%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E7%B4%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E7%B4%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E7%B4%8D)
* [書籍 (IA)]
  [晉書斠注(四十三)](https://archive.org/details/02077760.cn/page/n54/mode/2up)
* [ブログ]
  [巻六十二　列伝第三十二　祖逖（2）](https://readingnotesofjinshu.com/translation/biographies/vol-62_6)
  （@晋書簡訳所）

### 巻63 列伝第33
#### 邵続
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%82%B5%E7%B6%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%82%B5%E7%B6%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%82%B5%E7%B6%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%82%B5%E7%B6%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%82%B5%E7%B6%9A)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n2/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　邵続　李矩](https://readingnotesofjinshu.com/translation/biographies/vol-63_1#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 李矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E7%9F%A9)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n8/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　邵続　李矩](https://readingnotesofjinshu.com/translation/biographies/vol-63_1#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 段匹磾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B5%E5%8C%B9%E7%A3%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B5%E5%8C%B9%E7%A3%BE)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n18/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 魏浚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E6%B5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E6%B5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E6%B5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E6%B5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E6%B5%9A)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n24/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 魏該
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E8%A9%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E8%A9%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E8%A9%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E8%A9%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E8%A9%B2)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n26/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc2)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)

#### 郭黙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E9%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E9%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E9%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E9%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E9%BB%99)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n28/mode/2up)
* [ブログ]
  [巻六十三　列伝第三十三　段匹磾　魏浚　郭黙](https://readingnotesofjinshu.com/translation/biographies/vol-63_2#toc3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m063.html)



### 巻64 列伝第34 武十三王・元四王・簡文三子
#### 毗陵悼王（司馬軌）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%BB%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%BB%8C)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 秦献王（司馬柬）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%9F%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%9F%AC)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 城陽懷王（司馬景）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%AF)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 東海沖王（司馬祗）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%A5%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%A5%97)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 始平哀王（司馬裕）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A3%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A3%95)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n42/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 淮南忠壮王（司馬允）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%85%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%85%81)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n42/mode/2up)
* [ブログ]
  [『晋書』列伝３４、「司馬允＆司馬覃伝」翻訳](http://3guozhi.net/n/si.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 代哀王（司馬演）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%BC%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%BC%94)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 新都王（司馬該）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A9%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A9%B2)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 清河康王（司馬遐）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、「司馬允＆司馬覃伝」翻訳](http://3guozhi.net/n/si.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬覃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A6%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A6%83)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n46/mode/2up)
* [ブログ]
  [『晋書』列伝３４、「司馬允＆司馬覃伝」翻訳](http://3guozhi.net/n/si.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬籥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%B1%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%B1%A5)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n48/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬銓（司馬詮）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A9%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A9%AE)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n48/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 司馬端
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%AB%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%AB%AF)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n50/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 汝陰哀王（司馬謨）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%AC%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%AC%A8)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n50/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 呉孝王（司馬晏）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%8F)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n50/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 渤海殤王（司馬恢）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%81%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%81%A2)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n52/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 琅邪孝王（司馬裒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E8%A3%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E8%A3%92)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n52/mode/2up)
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g61.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 東海哀王（司馬沖）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n52/mode/2up)
  ……途中からページ抜けの箇所に当たっている。
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g61.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 武陵威王（司馬晞）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E6%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E6%99%9E)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n54/mode/2up)
  ……末尾以外はページ抜けの箇所に当たっている。
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g62.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 梁王（司馬㻱）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E3%BB%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E3%BB%B1)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n54/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 武陵忠敬王（司馬遵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%B5)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n54/mode/2up)
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 琅邪悼王（司馬煥）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E7%85%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E7%85%A5)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n56/mode/2up)
* [ブログ]
  [『晋書』列伝３４、元帝の４人の子](http://3guozhi.net/q/g63.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [『晋書』列伝３４、武帝と元帝の子](http://3guozhi.net/h/b4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 会稽思世子（司馬道生）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 臨川献王（司馬郁）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%83%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%83%81)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)

#### 会稽文孝王（司馬道子）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E9%81%93%E5%AD%90)
* [書籍 (IA)]
  [晉書斠注(四十四)](https://archive.org/details/02077761.cn/page/n64/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m064.html)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕（司馬道子の子）・桓玄への言及がある。

### 巻65 列伝第35
#### 王導
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%B0%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%B0%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%B0%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%B0%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%B0%8E)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n2/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王導公忠」（第8句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/21)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)
* [周曇の「詠史詩」の中の「王茂弘」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E7%8E%8B%E8%8C%82%E5%BC%98)

#### 王悦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%82%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%82%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%82%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%82%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%82%A6)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n28/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王恬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%81%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%81%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%81%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%81%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%81%AC)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王洽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B4%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B4%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B4%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B4%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B4%BD)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王珣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8F%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8F%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8F%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8F%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8F%A3)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n34/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「王珣短簿」（第16句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/25)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)
* 伯遠帖
    * [王珣行书伯远帖](https://www.dpm.org.cn/collection/handwriting/228199.html)
    （@故宮博物院）
    * [伯遠帖](https://zh.wikipedia.org/wiki/%E4%BC%AF%E9%81%A0%E5%B8%96#/media/File:The_Calligraphy_Model_Boyuan_by_Wang_Xun.jpg)
    （@ Wikipedia）
* [姚承緒『吳趨訪古錄』の「王珣琴臺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%99%84%EF%BC%9A%E7%8E%8B%E7%8F%A3%E7%90%B4%E8%87%BA)
* [姚承緒『吳趨訪古錄』の「雲巖寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%9B%B2%E5%B7%96%E5%AF%BA)
* [姚承緒『吳趨訪古錄』の「短簿祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E7%9F%AD%E7%B0%BF%E7%A5%A0)
* 「團扇郎」
    * [『樂府詩集』45卷](https://zh.wikisource.org/wiki/%E6%A8%82%E5%BA%9C%E8%A9%A9%E9%9B%86/045%E5%8D%B7#%E5%9C%98%E6%89%87%E9%83%8E%E5%85%AD%E9%A6%96)
    * [高啓「團扇郎」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E5%9C%98%E6%89%87%E9%83%8E)（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/97)）

#### 王珉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8F%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8F%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8F%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8F%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8F%89)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)
* [姚承緒『吳趨訪古錄』の「古杉」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%99%84%EF%BC%9A%E5%8F%A4%E6%9D%89)
* [姚承緒『吳趨訪古錄』の「雲巖寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%9B%B2%E5%B7%96%E5%AF%BA)
* [姚承緒『吳趨訪古錄』の「景德寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/4#%E6%99%AF%E5%BE%B7%E5%AF%BA)
* [姚承緒『吳趨訪古錄』の「短簿祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E7%9F%AD%E7%B0%BF%E7%A5%A0)
* 「團扇郎」
    * [『樂府詩集』45卷](https://zh.wikisource.org/wiki/%E6%A8%82%E5%BA%9C%E8%A9%A9%E9%9B%86/045%E5%8D%B7#%E5%9C%98%E6%89%87%E9%83%8E%E5%85%AD%E9%A6%96)
    * [高啓「團扇郎」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E5%9C%98%E6%89%87%E9%83%8E)（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/97)）

#### 王協
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%8D%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%8D%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%8D%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%8D%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%8D%94)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n42/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王謐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%AC%90)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n44/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王劭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%8A%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%8A%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%8A%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%8A%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%8A%AD)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王薈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%96%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%96%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%96%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%96%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%96%88)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

#### 王廞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BB%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BB%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BB%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BB%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BB%9E)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m065.html)

### 巻66 列伝第36
#### 劉弘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%BC%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%BC%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%BC%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%BC%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%BC%98)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n54/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [（劉馥＋劉表）／２＝劉弘伝](http://3guozhi.net/hito/rk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m066.html)

#### 陶侃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E4%BE%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E4%BE%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E4%BE%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E4%BE%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E4%BE%83)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n70/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* 『關帝靈籤』第八十籤に「陶侃卜牛眠」とある（[『晋書』巻58](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7058)周訪伝の末尾（附伝の後）にある話を指すと思われる）。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/80)のみ……[画像版](https://commons.wikimedia.org/w/index.php?title=File:%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=82)は「郭璞召陰葬地」という違う内容。

#### 陶洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%9E%BB)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶夏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E5%A4%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E5%A4%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E5%A4%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E5%A4%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E5%A4%8F)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶琦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%90%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%90%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%90%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%90%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%90%A6)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶旗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%97%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%97%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%97%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%97%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%97%97)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶斌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%96%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%96%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%96%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%96%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%96%8C)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶称
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E7%A7%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E7%A7%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E7%A7%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E7%A7%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E7%A7%B0)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n106/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶範
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E8%8C%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E8%8C%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E8%8C%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E8%8C%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E8%8C%83)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶岱
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E5%B2%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E5%B2%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E5%B2%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E5%B2%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E5%B2%B1)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶臻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E8%87%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E8%87%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E8%87%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E8%87%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E8%87%BB)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶輿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E8%BC%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E8%BC%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E8%BC%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E8%BC%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E8%BC%BF)
* [書籍 (IA)]
  [晉書斠注(四十五)](https://archive.org/details/02077762.cn/page/n108/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)


### 巻67 列伝第37
#### 温嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B8%A9%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B8%A9%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B8%A9%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B8%A9%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B8%A9%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n2/mode/2up)
* [關漢卿『溫太真玉鏡臺』](https://zh.wikisource.org/wiki/%E6%BA%AB%E5%A4%AA%E7%9C%9F%E7%8E%89%E9%8F%A1%E8%87%BA)
* 朱鼎『玉鏡臺記』……
  [Wikisource](https://zh.wikisource.org/wiki/%E7%8E%89%E9%8F%A1%E8%87%BA%E8%A8%98)、
  [中国都市芸能研究会](http://www.chengyan.wagang.jp/?%E5%85%AD%E5%8D%81%E7%A8%AE%E6%9B%B2/%E7%8E%89%E9%8F%A1%E8%87%BA%E8%A8%98)、
  [Wikimedia（画像版）](https://commons.wikimedia.org/w/index.php?title=File%3ACADAL06386264_%E7%8E%89%E9%8F%A1%E8%87%BA%E8%A8%98.djvu&page=1)
* [胡曾の「牛渚」](https://zh.wikisource.org/wiki/%E7%89%9B%E6%B8%9A_(%E8%83%A1%E6%9B%BE))
* [溫嶠墓誌](http://csid.zju.edu.cn/tomb/stone/detail?id=8a8fbda74ca22703014ca22a72fb16ce&rubbingId=8a8fbda74d093780014d270157b216e1)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「前郡尹溫太真劉真長，或功銘鼎彝，或德標素尚」とある。「太真」は温嶠の字。

#### 郗鑒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n32/mode/2up)
* [『隋書』巻35（経籍志四）](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B735#%E5%88%A5%E9%9B%86)に「晉太尉《郗鑒集》十卷錄一卷」
* [『旧唐書』巻47（経籍志下）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B747#%E5%88%A5%E9%9B%86)に「《郤鑒集》十卷」
* [『新唐書』巻60（芸文志四）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7060)に「郗鑒集十卷」
* [『太平広記』巻28](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC028)

#### 郗愔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E6%84%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E6%84%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E6%84%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E6%84%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E6%84%94)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n46/mode/2up)
* [蘇軾「郗方回郗嘉賓父子事」](https://zh.wikisource.org/wiki/%E9%83%97%E6%96%B9%E5%9B%9E%E9%83%97%E5%98%89%E8%B3%93%E7%88%B6%E5%AD%90%E4%BA%8B)
* [劉義慶『幽明錄』](https://ctext.org/wiki.pl?if=gb&chapter=303891#p83)に郗愔の話あり。
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)（中嶋 長文・伊藤 令子・ 平田 昌司）のp. 397, No. 80でも読める。

#### 郗超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n48/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「郗超髯參」（第15句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/25)
* [蘇軾「郗方回郗嘉賓父子事」](https://zh.wikisource.org/wiki/%E9%83%97%E6%96%B9%E5%9B%9E%E9%83%97%E5%98%89%E8%B3%93%E7%88%B6%E5%AD%90%E4%BA%8B)
* [『搜神後記』巻2「杜不愆」](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E5%BE%8C%E8%A8%98/02#%E6%9D%9C%E4%B8%8D%E6%84%86)
  （[『国訳漢文大成』第十二巻](https://dl.ndl.go.jp/info:ndljp/pid/1913008/87)に訓読あり）
* 「與桓溫箋」
    * [『淳化閣帖』巻2](https://dl.ndl.go.jp/info:ndljp/pid/2587028/26)
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p4)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
* 「與親友書論支道林」（支道林は支遁のこと）
    * [『高僧伝』巻4支遁伝](https://cbetaonline.dila.edu.tw/zh/T2059_004)（「郄超」と書かれている。この直前の郗超と謝安の会話は[『世説新語』品藻篇67](https://ctext.org/wiki.pl?if=gb&chapter=380418#p690)と同様）
    * [『歴代三宝紀』巻7](https://cbetaonline.dila.edu.tw/zh/T2034_007)（「却超」と書かれている）
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p6)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
* 「與謝慶緒書論三幡義」（謝慶緒は巻94の謝敷のこと）
    * 『文選』所収の[孫綽の「遊天台山賦」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B711#%E9%81%8A%E5%A4%A9%E5%8F%B0%E5%B1%B1%E8%B3%A6)の注に「郤敬輿與謝慶緒書論三幡義曰」云々とある。
      なお、「遊天台山賦」本文の日本語は[『国訳漢文大成』第二卷](https://dl.ndl.go.jp/info:ndljp/pid/1912763/204)で読める。
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p8)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
    * これと関連するのか分からないが、『文選』所収の[王儉の「褚淵碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B758#%E8%A4%9A%E6%B7%B5%E7%A2%91%E6%96%87)の注に「謝慶緒答郤敬書曰」云々とある。
      なお、「褚淵碑文」本文の日本語は[『国訳漢文大成』第四卷](https://dl.ndl.go.jp/info:ndljp/pid/1912835/380)で読める。
* 「奉法要」
    * [『弘明集』巻13](https://cbetaonline.dila.edu.tw/zh/T2102_013)
    * 『全晋文』巻110
      （[テキスト版](https://ctext.org/wiki.pl?if=gb&chapter=372164#p10)・
        [画像版](https://archive.org/details/02107193.cn/page/n2/mode/2up)）
    * [書籍 (NDL)]
    [『中国仏教通史』第一巻](https://dl.ndl.go.jp/info:ndljp/pid/2989703)
    （塚本善隆）
    第六章第二節・附
    [「郗超の仏教要義（奉法要）——在俗貴族の実践的仏教要義の範例として——」](https://dl.ndl.go.jp/info:ndljp/pid/2989703/188)
    （福永光司訳）
    * [書籍 (NDL)]
    [『弘明集研究 巻下 (訳注篇 下(巻6-14))』](https://dl.ndl.go.jp/pid/12223260)
    （牧田諦亮 編）
    [「弘明集巻十三　（一）佛教信仰の要義〔奉法要〕」](https://dl.ndl.go.jp/pid/12223260/1/194)
    * [「奉法要」でCiNii Researchを検索](https://cir.nii.ac.jp/all?q=%E5%A5%89%E6%B3%95%E8%A6%81)
    * [「奉法要」でNDLを検索](https://ndlonline.ndl.go.jp/#!/search?searchCode=SIMPLE&lang=jp&keyword=%E5%A5%89%E6%B3%95%E8%A6%81)
    * [「奉法要」で次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A5%89%E6%B3%95%E8%A6%81&searchfield=&withouthighlight=)
* 『文館詞林』巻157所収「答傅郎詩」
  ……[早稲田大学図書館古典籍総合データベース](https://www.wul.waseda.ac.jp/kotenseki/html/he16/he16_00993/index.html)で見られる。
    * [PDF版](https://archive.wul.waseda.ac.jp/kosho/he16/he16_00993/he16_00993_0002/he16_00993_0002.pdf#page=32)
    * JPEG版
      （[1ページ目](https://archive.wul.waseda.ac.jp/kosho/he16/he16_00993/he16_00993_0002/he16_00993_0002_p0032.jpg)・
      [2ページ目](https://archive.wul.waseda.ac.jp/kosho/he16/he16_00993/he16_00993_0002/he16_00993_0002_p0033.jpg)）
* [釋僧祐撰『出三藏記集』巻12](https://cbetaonline.dila.edu.tw/zh/T2145_012)の「宋明帝勅中書侍郎陸澄撰法論目錄序第一」に挙げられたもので郗超に関わるもの。
    * 本無難問(郗嘉賓竺法汰難并郗答往反四首)
    * 郗與法𤀹書
    * 郗與開法師書
    * 郗與支法師書
    * 支書與郗嘉賓
    * 奉法要(郗嘉賓)
    * 通神呪(郗嘉賓)
    * 明感論(郗嘉賓)
    * 論三行上(郗嘉賓)
    * 敘通三行(郗嘉賓)
    * 郗與謝慶緒書往反五首
    * 論三行下(郗嘉賓)
    * 郗與傅叔玉書往反三首
    * 全生論(郗嘉賓)
    * 五陰三達釋(郗嘉賓)
* [釋慧皎撰『高僧傳』卷14・序錄](https://cbetaonline.dila.edu.tw/zh/T2059_014)に
  「中書郎郄景興《東山僧傳》、治中張孝秀《廬山僧傳》、中書陸明霞《沙門傳》，各競舉一方，不通今古；務存一善，不及餘行。」
  とあるので、郗超に『東山僧傳』という著作があったと分かる。
  「景興偶採居山之人，僧寶偏綴遊方之士，法濟唯張高逸之例，法安止命志節之科。」
  「孝秀染毫，復獲景興之誚。」
  ともある。
* [『隋書』巻35（経籍志四）](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B735#%E5%88%A5%E9%9B%86)に「晉中書郎《郤超集合》九卷梁十卷」
* [『旧唐書』巻47（経籍志下）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B747#%E5%88%A5%E9%9B%86)に「《郤超集合》十五卷」
* [『新唐書』巻60（藝文志四）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7060)に「郗超集十五卷」
* [傅亮の『光世音應驗記』の「呂竦」の条](https://zh.wikisource.org/wiki/%E5%85%89%E4%B8%96%E9%9F%B3%E6%87%89%E9%A9%97%E8%A8%98#%E5%91%82%E7%AB%A6)の最後にも「竦後與郗嘉賓周旋，郗口所説。」と出てくる。
  [『法苑珠林』巻65](https://cbetaonline.dila.edu.tw/zh/T2122_065)も同様。
* [ブログ]
  [郗超相关史料拾遗](https://sanjingjiuhuang.net/chaomemo/)
  （@一把甘蔗渣）
* [于知微「明堂令于大猷碑」](https://zh.wikisource.org/wiki/%E6%98%8E%E5%A0%82%E4%BB%A4%E6%96%BC%E5%A4%A7%E7%8C%B7%E7%A2%91)に「郗超以蕃伯之望，職總■河」および「郗嘉賓之卓犖高雋」とある。
* [趙蕃『淳熈稿』巻13](https://zh.wikisource.org/wiki/%E6%B7%B3%E7%86%88%E7%A8%BF_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B713#%E8%A8%AA%E5%AD%AB%E5%AD%90%E9%80%B2%E5%AD%90%E8%82%85%E6%96%BC%E9%BB%84%E5%A0%B0)に「劉伯山書來云有施主為造一亭劉子澄名曰竹溪索詩為賦二首」とある二首目の中に「今人那有郗嘉賓」とある。
* 黄庭堅・撰、任淵・注『山谷内集詩注』巻17の「武昌松風閣」に対する注に「世説曰郗嘉賓開庫得錢一日乞與親友周旋略盡」とある。
    * [早稲田大学図書館古典籍総合データベース](https://www.wul.waseda.ac.jp/kotenseki/html/he18/he18_01286/index.html)の[画像版](https://archive.wul.waseda.ac.jp/kosho/he18/he18_01286/he18_01286_0009/he18_01286_0009_p0013.jpg)
    * [Wikisourceのテキスト版](https://zh.wikisource.org/wiki/%E5%B1%B1%E8%B0%B7%E9%9B%86%E8%A9%A9%E6%B3%A8_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%86%85%E9%9B%86%E5%8D%B717#%E6%AD%A6%E6%98%8C%E6%9D%BE%E9%A2%A8%E9%96%A3)
* 郗超の書に関して
    * [『南斉書』巻33・王僧虔伝](https://zh.wikisource.org/wiki/%E5%8D%97%E9%BD%8A%E6%9B%B8/%E5%8D%B733#%E7%8E%8B%E5%83%A7%E8%99%94)および[『南史』巻22・王僧虔伝](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B722#%E5%AD%90_%E5%83%A7%E8%99%94)に引く『論書』に「郗嘉賓草亞于二王，緊媚過其父」とある。
    * [李嗣真『書後品』](https://zh.wikisource.org/wiki/%E6%9B%B8%E5%BE%8C%E5%93%81)では中上品とされる。
    * [張懷瓘『書斷』巻中](https://zh.wikisource.org/wiki/%E6%9B%B8%E6%96%B7/%E5%8D%B7%E4%B8%AD)に「子超字景興，小字嘉賓，亦善書。」とある。
    * [竇臮『述書賦』](https://ctext.org/wiki.pl?if=gb&chapter=644061#p6)に「景興當年，曷云世乏。正草輕利，脫略古法。跡因心而謂何，為吏士之所多。惜森然之俊爽，嗟薎爾於中和。」「郗超，字景興，愔子。晉臨海太守。今見具姓名行草書共四紙。」とある。
    * [『宣和書譜』巻14](https://zh.wikisource.org/wiki/%E5%AE%A3%E5%92%8C%E6%9B%B8%E8%AD%9C#%E5%8D%B7%E5%8D%81%E5%9B%9B)に「子超尤得家傳之妙」とある。
* [姚承緒『吳趨訪古錄』の「短簿祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E7%9F%AD%E7%B0%BF%E7%A5%A0)
* 王讜の[『唐語林』巻5](https://zh.wikisource.org/wiki/%E5%94%90%E8%AA%9E%E6%9E%97/%E5%8D%B7%E4%BA%94)の郗昂と源乾曜のやりとりは、[『世説新語』雅量篇27](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E9%9B%85%E9%87%8F)の「入幕の賓」をネタにしており、直接的には郗超と謝安に言及している。

#### 郗僧施
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E5%83%A7%E6%96%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E5%83%A7%E6%96%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E5%83%A7%E6%96%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E5%83%A7%E6%96%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E5%83%A7%E6%96%BD)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n56/mode/2up)

#### 郗曇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E6%9B%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E6%9B%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E6%9B%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E6%9B%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E6%9B%87)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n56/mode/2up)

#### 郗恢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E6%81%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E6%81%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E6%81%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E6%81%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E6%81%A2)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n56/mode/2up)
* [劉義慶『幽明錄』](https://ctext.org/wiki.pl?if=gb&chapter=303891#p146)に郗恢の話あり。
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)（中嶋 長文・伊藤 令子・ 平田 昌司）のp. 429, No. 143でも読める。

#### 郗隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%97%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%97%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%97%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%97%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%97%E9%9A%86)
* [書籍 (IA)]
  [晉書斠注(四十六)](https://archive.org/details/02077763.cn/page/n60/mode/2up)


### 巻68 列伝第38
#### 顧栄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E6%A0%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E6%A0%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E6%A0%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E6%A0%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E6%A0%84)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk1.html)
  （@いつか書きたい『三国志』）
* 高啓の[「顧榮廟」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B704#%E9%A1%A7%E6%A6%AE%E5%BB%9F)（[『国訳漢文大成. 続 文学部第76冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140757/13)も参照）
* [姚承緒『吳趨訪古錄』の「顧榮祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/3#%E9%A1%A7%E6%A6%AE%E7%A5%A0%E3%80%88%E5%92%8C%E9%AB%98%E9%9D%92%E9%82%B1%E9%9F%BB%E3%80%89)
* [姚承緒『吳趨訪古錄』の「永定寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E6%B0%B8%E5%AE%9A%E5%AF%BA%E3%80%88%E6%AC%A1%E9%9F%8B%E8%98%87%E5%B7%9E%E3%80%8A%E6%B0%B8%E5%AE%9A%E7%B2%BE%E8%88%8D%E3%80%8B%E9%9F%BB%E3%80%89)
  ……永定寺は顧栄の家の跡地にあるらしい（[『江南通志』巻31](https://zh.wikisource.org/wiki/%E6%B1%9F%E5%8D%97%E9%80%9A%E5%BF%97_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7031)）

#### 紀瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%80%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%80%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%80%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%80%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%80%E7%9E%BB)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk3.html)
  （@いつか書きたい『三国志』）

#### 賀循
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%80%E5%BE%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%80%E5%BE%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%80%E5%BE%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%80%E5%BE%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%80%E5%BE%AA)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n36/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk6.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「賀循儒宗」（第77句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/62)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/187)）に「賀生達禮之宗」とある。

#### 楊方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E6%96%B9)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n54/mode/2up)
* [書誌のみ]
  [楊方詩訳注 : 并『晋書』本伝訳](https://cir.nii.ac.jp/crid/1571135651788146176)
  （長谷川 滋成）
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk10.html)
  （@いつか書きたい『三国志』）

#### 薛兼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%96%9B%E5%85%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%96%9B%E5%85%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%96%9B%E5%85%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%96%9B%E5%85%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%96%9B%E5%85%BC)
* [書籍 (IA)]
  [晉書斠注(四十七)](https://archive.org/details/02077764.cn/page/n56/mode/2up)
* [ブログ]
  [『晋書』列３８、呉の人たち](http://3guozhi.net/n/gk10.html)
  （@いつか書きたい『三国志』）


### 巻69 列伝第39
#### 劉隗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%9A%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%9A%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%9A%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%9A%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%9A%97)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉波
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%B3%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%B3%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%B3%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%B3%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%B3%A2)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n10/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉訥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%A8%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%A8%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%A8%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%A8%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%A8%A5)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n16/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉疇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%96%87)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n16/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉劭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%8A%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%8A%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%8A%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%8A%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%8A%AD)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n18/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉黄老
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%BB%84%E8%80%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%BB%84%E8%80%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%BB%84%E8%80%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%BB%84%E8%80%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%BB%84%E8%80%81)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n18/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 刁協
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%88%81%E5%8D%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%88%81%E5%8D%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%88%81%E5%8D%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%88%81%E5%8D%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%88%81%E5%8D%94)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n18/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r4.html)
  （@いつか書きたい『三国志』）

#### 刁彝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%88%81%E5%BD%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%88%81%E5%BD%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%88%81%E5%BD%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%88%81%E5%BD%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%88%81%E5%BD%9D)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n24/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r4.html)
  （@いつか書きたい『三国志』）

#### 刁逵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%88%81%E9%80%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%88%81%E9%80%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%88%81%E9%80%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%88%81%E9%80%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%88%81%E9%80%B5)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n26/mode/2up)

#### 戴若思（戴淵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E6%B7%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E6%B7%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E6%B7%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E6%B7%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E6%B7%B5)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r5.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  空海『三教指帰』に[「戴淵志を變じて將軍の位に登り」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/11)とある
  （[注48](https://dl.ndl.go.jp/info:ndljp/pid/1040599/36)）。

#### 戴邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E9%82%88)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n32/mode/2up)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r6.html)
  （@いつか書きたい『三国志』）

#### 周顗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%A1%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%A1%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%A1%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%A1%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%A1%97)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n36/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「周嵩狼抗」（第13句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/23)
* [ブログ]
  [『晋書』列３９、元帝の逃げ場](http://3guozhi.net/h/r7.html)
  （@いつか書きたい『三国志』）

#### 周閔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%96%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%96%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%96%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%96%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%96%94)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n48/mode/2up)


### 巻70 列伝第40
#### 応詹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BF%9C%E8%A9%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BF%9C%E8%A9%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BF%9C%E8%A9%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BF%9C%E8%A9%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BF%9C%E8%A9%B9)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n52/mode/2up)

#### 甘卓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%94%98%E5%8D%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%94%98%E5%8D%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%94%98%E5%8D%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%94%98%E5%8D%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%94%98%E5%8D%93)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n64/mode/2up)

#### 鄧騫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E9%A8%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E9%A8%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E9%A8%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E9%A8%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E9%A8%AB)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n74/mode/2up)

#### 卞壼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9E%E5%A3%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9E%E5%A3%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9E%E5%A3%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9E%E5%A3%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9E%E5%A3%BC)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n74/mode/2up)

#### 卞敦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9E%E6%95%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9E%E6%95%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9E%E6%95%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9E%E6%95%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9E%E6%95%A6)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n92/mode/2up)

#### 劉超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n94/mode/2up)

#### 鍾雅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%8D%BE%E9%9B%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%8D%BE%E9%9B%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%8D%BE%E9%9B%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%8D%BE%E9%9B%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%8D%BE%E9%9B%85)
* [書籍 (IA)]
  [晉書斠注(四十八)](https://archive.org/details/02077765.cn/page/n100/mode/2up)


### 巻71 列伝第41
#### 孫恵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%81%B5)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n2/mode/2up)

#### 熊遠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%86%8A%E9%81%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%86%8A%E9%81%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%86%8A%E9%81%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%86%8A%E9%81%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%86%8A%E9%81%A0)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n10/mode/2up)

#### 王鑑（王鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n20/mode/2up)

#### 陳頵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E9%A0%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E9%A0%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E9%A0%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E9%A0%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E9%A0%B5)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n26/mode/2up)

#### 高崧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E5%B4%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E5%B4%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E5%B4%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E5%B4%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E5%B4%A7)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n32/mode/2up)


### 巻72 列伝第42
#### 郭璞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%92%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%92%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%92%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%92%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%92%9E)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n38/mode/2up)
* [PDFあり]
  [六朝文人伝 : 「晋書」郭璞伝](https://dl.ndl.go.jp/info:ndljp/pid/10504359)
  （長谷川 滋成）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、郭璞伝で言及している
  [「江賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/230)がある。
* 干宝『搜神記』
  [巻3](https://ctext.org/wiki.pl?if=gb&chapter=412733#p14)・
  [巻4](https://ctext.org/wiki.pl?if=gb&chapter=879820#p13)
* [劉克莊の「郭璞墓」](https://zh.wikisource.org/wiki/%E5%BE%8C%E6%9D%91%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E9%83%AD%E7%92%9E%E5%A2%93)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/187)）に「郭璞誓以淮水」とある。
* 『關帝靈籤』第八十籤に「郭璞召陰葬地」とある。
    * [画像版](https://commons.wikimedia.org/w/index.php?title=File:%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.%E5%A7%91%E8%98%87%E9%88%95%E6%B0%8F%E8%97%8F%E6%9D%BF.%E6%B8%85%E5%88%8A%E6%9C%AC.pdf&page=82)のみ……[テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/80)は「陶侃卜牛眠」という違う内容。

#### 葛洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%9B%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%9B%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%9B%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%9B%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%9B%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(四十九)](https://archive.org/details/02077766.cn/page/n70/mode/2up)
* [墨浪子『西湖佳話』巻1「葛嶺仙跡」](https://zh.wikisource.org/wiki/%E8%A5%BF%E6%B9%96%E4%BD%B3%E8%A9%B1/01)


### 巻73 列伝第43
#### 庾亮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E4%BA%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E4%BA%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E4%BA%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E4%BA%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E4%BA%AE)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n2/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、庾亮伝に引かれた
  [「讓中書令表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/419)がある。
* 干宝[『搜神記』巻9](https://ctext.org/wiki.pl?if=gb&chapter=581900#p14)
* 李白「[陪宋中丞武昌夜飲懷古](https://zh.wikisource.org/wiki/%E9%99%AA%E5%AE%8B%E4%B8%AD%E4%B8%9E%E6%AD%A6%E6%98%8C%E5%A4%9C%E9%A3%B2%E6%87%B7%E5%8F%A4)」
  （[『国訳漢文大成　続　文学部』第10冊](https://dl.ndl.go.jp/info:ndljp/pid/1139993/81)）
* [『隋書』巻15音楽志下](https://zh.wikisource.org/wiki/%E9%9A%8B%E6%9B%B8/%E5%8D%B715)
  （文康楽のところ）
* [『顔氏家訓』書証第十七](https://ctext.org/yan-shi-jia-xun/shu-zheng/zh#n92153)
  ……同じく「猶文康象庾亮耳」の記述あり。
* [ブログ]
  [デク](https://t-s.hatenablog.com/entry/20110318/1300374101)
  （@てぃーえすのメモ帳）
  ……『顔氏家訓』の上記箇所を含む段落について。
* [『本朝文粋』巻14](https://dl.ndl.go.jp/info:ndljp/pid/2544432/37)の[菅三品（菅原文時）「為謙徳公修報恩善願文」](https://dl.ndl.go.jp/info:ndljp/pid/2544432/54)に、庾亮を指す「[南樓翫月之人、月與秋期、而身何去](https://dl.ndl.go.jp/info:ndljp/pid/2544432/55)」という部分がある（この直前の部分が[『十訓抄』](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-11)に引用されているため、[『十訓抄詳解』の注](https://dl.ndl.go.jp/info:ndljp/pid/945602/34)で解説されている）。
* 唐の謝観の「白賦」に「夜登庾亮之樓，月明千里。」という部分があり、ここが文天祥の[「五色賦記」](https://zh.wikisource.org/wiki/%E6%96%87%E5%B1%B1%E5%85%88%E7%94%9F%E6%96%87%E9%9B%86/%E5%8D%B709#%E4%BA%94%E8%89%B2%E8%B3%A6%E8%A8%98)、謝肇淛の[『五雜俎』](https://zh.wikisource.org/wiki/%E4%BA%94%E9%9B%9C%E4%BF%8E/%E5%8D%B716)、蔣一葵の[『堯山堂偶雋』](https://zh.wikisource.org/wiki/%E5%A0%AF%E5%B1%B1%E5%A0%82%E5%81%B6%E9%9B%8B/%E5%8D%B7%E4%B8%89)に引かれている。

#### 庾彬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%BD%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%BD%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%BD%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%BD%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%BD%AC)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n24/mode/2up)

#### 庾羲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%BE%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%BE%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%BE%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%BE%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%BE%B2)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n24/mode/2up)

#### 庾龢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%BE%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%BE%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%BE%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%BE%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%BE%A2)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n26/mode/2up)

#### 庾懌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%87%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%87%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%87%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%87%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%87%8C)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n28/mode/2up)

#### 庾冰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%86%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%86%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%86%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%86%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%86%B0)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n30/mode/2up)

#### 庾希
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%B8%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%B8%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%B8%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%B8%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%B8%8C)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n38/mode/2up)
  ……途中からページ抜けの箇所に当たっている。

#### 庾襲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E8%A5%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E8%A5%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E8%A5%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E8%A5%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E8%A5%B2)

#### 庾友
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%8F%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%8F%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%8F%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%8F%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%8F%8B)

#### 庾蘊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E8%98%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E8%98%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E8%98%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E8%98%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E8%98%8A)

#### 庾倩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E5%80%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E5%80%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E5%80%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E5%80%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E5%80%A9)

#### 庾邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%82%88)

#### 庾柔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%9F%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%9F%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%9F%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%9F%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%9F%94)

#### 庾條
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%A2%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%A2%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%A2%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%A2%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%A2%9D)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n40/mode/2up)
  ……末尾以外はページ抜けの箇所に当たっている。

#### 庾翼
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%BF%BC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%BF%BC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%BF%BC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%BF%BC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%BF%BC)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n40/mode/2up)
* [ブログ]
  [キャプテン翼 北伐篇 『晋書 庾翼伝（抄訳）』 第１回](http://tombstonedriver.blog.fc2.com/blog-entry-130.html)〜
  [キャプテン翼 北伐篇 『晋書 庾翼伝（抄訳）』 最終回](http://tombstonedriver.blog.fc2.com/blog-entry-138.html)（第８回）
  （@大司馬府 作戦日誌）

#### 庾方之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%96%B9%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%96%B9%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n50/mode/2up)

#### 庾爰之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%88%B0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%88%B0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n50/mode/2up)


### 巻74 列伝第44
#### 桓彝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%BD%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%BD%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%BD%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%BD%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%BD%9D)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n54/mode/2up)
* 高啓の[「桓簡公廟」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B712#%E6%A1%93%E7%B0%A1%E5%85%AC%E5%BB%9F)（[『国訳漢文大成. 続 文学部第83冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140864/35)も参照）

#### 桓雲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E9%9B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E9%9B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E9%9B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E9%9B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E9%9B%B2)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n58/mode/2up)

#### 桓豁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E8%B1%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E8%B1%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E8%B1%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E8%B1%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E8%B1%81)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n60/mode/2up)

#### 桓石虔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E8%99%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E8%99%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E8%99%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E8%99%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E8%99%94)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n64/mode/2up)

#### 桓振
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%8C%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%8C%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%8C%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%8C%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%8C%AF)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n68/mode/2up)

#### 桓石秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n70/mode/2up)

#### 桓石民
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E6%B0%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E6%B0%91)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n72/mode/2up)

#### 桓石生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓石綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓石康
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%9F%B3%E5%BA%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%9F%B3%E5%BA%B7)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓秘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%A7%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%A7%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%A7%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%A7%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%A7%98)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n74/mode/2up)

#### 桓沖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n76/mode/2up)

#### 桓嗣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%97%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%97%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%97%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%97%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%97%A3)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n88/mode/2up)

#### 桓胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n90/mode/2up)

#### 桓謙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E8%AC%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E8%AC%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E8%AC%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E8%AC%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E8%AC%99)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n90/mode/2up)

#### 桓修
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E4%BF%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E4%BF%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E4%BF%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E4%BF%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E4%BF%AE)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n92/mode/2up)

#### 徐寧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E5%AF%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E5%AF%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E5%AF%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E5%AF%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E5%AF%A7)
* [書籍 (IA)]
  [晉書斠注(五十)](https://archive.org/details/02077767.cn/page/n94/mode/2up)


### 巻75 列伝第45
#### 王湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B9%9B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n2/mode/2up)

#### 王承
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%89%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%89%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%89%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%89%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%89%BF)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n6/mode/2up)

#### 王述
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%BF%B0)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n8/mode/2up)

#### 王坦之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%9D%A6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%9D%A6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n16/mode/2up)
* [蘇軾「蝶戀花」](https://zh.wikisource.org/wiki/%E8%9D%B6%E6%88%80%E8%8A%B1%EF%BC%88%E5%90%8C%E5%AE%89%E7%94%9F%E6%97%A5%E6%94%BE%E9%AD%9A%EF%BC%8C%E5%8F%96%E9%87%91%E5%85%89%E6%98%8E%E7%B6%93%E6%95%91%E9%AD%9A%E4%BA%8B%EF%BC%89)の「膝上王文度」という比喩は、王坦之が成人してもまだ父の王述が王坦之を膝の上にだっこしていた（！）という話による。
  （[林雪云「蘇軾の妻妾に対する観念」](http://doi.org/10.24729/00004371)に、この詞の訓読あり）
* [書籍 (NDL)]
  [『先哲像伝 近世畸人伝 百家琦行伝』](https://dl.ndl.go.jp/pid/1186674/1/25)
  （得斎原義胤・他）
  ……林讀耕齋が幼児の頃、讀耕齋をだっこした父の林羅山が「是膝上王文度也」と言っている。

#### 王愷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%84%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%84%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%84%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%84%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%84%B7)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n28/mode/2up)

#### 王愉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%84%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%84%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%84%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%84%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%84%89)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n28/mode/2up)

#### 王国宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%9B%BD%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%9B%BD%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n30/mode/2up)

#### 王忱
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BF%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BF%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BF%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BF%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BF%B1)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n36/mode/2up)

#### 王綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n38/mode/2up)

#### 王嶠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%B6%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%B6%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%B6%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%B6%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%B6%A0)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n40/mode/2up)

#### 袁悦之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E6%82%A6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E6%82%A6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n42/mode/2up)

#### 祖台之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E5%8F%B0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E5%8F%B0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n42/mode/2up)
* [PDFあり]
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)
  （中嶋 長文・伊藤 令子・ 平田 昌司）
  ……p. 326〜 に祖台之の「志怪」あり。
* 祖台之の「志怪」には [Wikisource 版](https://zh.wikisource.org/wiki/%E7%A5%96%E5%8F%B0%E4%B9%8B%E5%BF%97%E6%80%AA)もある。

#### 荀崧
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%B4%A7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%B4%A7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%B4%A7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%B4%A7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%B4%A7)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n42/mode/2up)

#### 荀蕤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E8%95%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E8%95%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E8%95%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E8%95%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E8%95%A4)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n54/mode/2up)

#### 荀羨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E7%BE%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E7%BE%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E7%BE%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E7%BE%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E7%BE%A8)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n54/mode/2up)

#### 范汪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E6%B1%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E6%B1%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E6%B1%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E6%B1%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E6%B1%AA)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n58/mode/2up)

#### 范寧（范甯）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E7%94%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E7%94%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E7%94%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E7%94%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E7%94%AF)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n66/mode/2up)

#### 范堅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%A0%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%A0%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%A0%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%A0%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%A0%85)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n78/mode/2up)

#### 劉惔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%83%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%83%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%83%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%83%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%83%94)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n80/mode/2up)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「前郡尹溫太真劉真長，或功銘鼎彝，或德標素尚」とある。「真長」は劉惔の字。

#### 張憑
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%86%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%86%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%86%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%86%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%86%91)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n86/mode/2up)

#### 韓伯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E4%BC%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E4%BC%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E4%BC%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E4%BC%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E4%BC%AF)
* [書籍 (IA)]
  [晉書斠注(五十一)](https://archive.org/details/02077768.cn/page/n88/mode/2up)
* 干宝[『搜神記』巻5](https://ctext.org/wiki.pl?if=gb&chapter=503309#p4)
  （韓伯の子が出てくる）


### 巻76 列伝第46
#### 王舒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%88%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%88%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%88%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%88%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%88%92)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n2/mode/2up)

#### 王允之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%85%81%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%85%81%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n8/mode/2up)

#### 王廙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BB%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BB%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BB%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BB%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BB%99)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n10/mode/2up)

#### 王彬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BD%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BD%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BD%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BD%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BD%AC)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n18/mode/2up)

#### 王彪之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BD%AA%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BD%AA%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n22/mode/2up)

#### 王棱
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%A3%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%A3%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%A3%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%A3%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%A3%B1)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n36/mode/2up)

#### 王侃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E4%BE%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E4%BE%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E4%BE%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E4%BE%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E4%BE%83)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n36/mode/2up)

#### 虞潭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%BD%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%BD%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%BD%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%BD%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%BD%AD)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n36/mode/2up)

#### 虞嘯父
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E5%98%AF%E7%88%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E5%98%AF%E7%88%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E5%98%AF%E7%88%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E5%98%AF%E7%88%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E5%98%AF%E7%88%B6)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n42/mode/2up)

#### 虞𩦎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%F0%A9%A6%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%F0%A9%A6%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%F0%A9%A6%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%F0%A9%A6%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%F0%A9%A6%8E)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n44/mode/2up)

#### 顧衆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E8%A1%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E8%A1%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E8%A1%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E8%A1%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E8%A1%86)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n44/mode/2up)

#### 張闓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%97%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%97%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%97%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%97%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%97%93)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n50/mode/2up)


### 巻77 列伝第47
#### 陸曄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E6%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E6%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E6%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E6%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E6%9B%84)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n56/mode/2up)

#### 陸玩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E7%8E%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E7%8E%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E7%8E%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E7%8E%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E7%8E%A9)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n58/mode/2up)
* [姚承緒『吳趨訪古錄』の「靈巖寺」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E9%9D%88%E5%B7%96%E5%AF%BA)
  ……序文によると、陸玩が寄進した土地に建てた寺院を題材にした詩らしい。

#### 陸納
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B8%E7%B4%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B8%E7%B4%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B8%E7%B4%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B8%E7%B4%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B8%E7%B4%8D)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n64/mode/2up)

#### 何充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n68/mode/2up)

#### 褚翜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E7%BF%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E7%BF%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E7%BF%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E7%BF%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E7%BF%9C)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n78/mode/2up)

#### 蔡謨
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%94%A1%E8%AC%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%94%A1%E8%AC%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%94%A1%E8%AC%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%94%A1%E8%AC%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%94%A1%E8%AC%A8)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n82/mode/2up)
* [ブログ]
  [餓狼おどる海　『晋書 蔡謨伝』 第一回](http://tombstonedriver.blog.fc2.com/blog-entry-114.html)〜
  [餓狼おどる海　『晋書 蔡謨伝』 最終回](http://tombstonedriver.blog.fc2.com/blog-entry-128.html) （第十一回）
  （@大司馬府 作戦日誌）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/187)）に「蔡公儒林之亞」とある。

#### 諸葛恢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AB%B8%E8%91%9B%E6%81%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AB%B8%E8%91%9B%E6%81%A2)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n104/mode/2up)

#### 殷浩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E6%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E6%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E6%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E6%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E6%B5%A9)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n108/mode/2up)
* [ブログ]
  [永和十年（354年） 桓温の殷浩を断罪する上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-96.html)
  （@大司馬府 作戦日誌）

#### 顧悦之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E6%82%A6%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E6%82%A6%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n122/mode/2up)

#### 蔡裔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%94%A1%E8%A3%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%94%A1%E8%A3%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%94%A1%E8%A3%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%94%A1%E8%A3%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%94%A1%E8%A3%94)
* [書籍 (IA)]
  [晉書斠注(五十二)](https://archive.org/details/02077769.cn/page/n122/mode/2up)


### 巻78 列伝第48
#### 孔愉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E6%84%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E6%84%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E6%84%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E6%84%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E6%84%89)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n1/mode/2up)
* 干宝[『搜神記』巻20](https://ctext.org/wiki.pl?if=gb&chapter=629401#p7)
* [『十訓抄』1の4](http://yatanavi.org/text/jikkinsho/s_jikkinsho01-04)

#### 孔汪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E6%B1%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E6%B1%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E6%B1%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E6%B1%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E6%B1%AA)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n7/mode/2up)

#### 孔安国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%AE%89%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%AE%89%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n7/mode/2up)

#### 孔祗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E7%A5%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E7%A5%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E7%A5%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E7%A5%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E7%A5%97)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n9/mode/2up)

#### 孔坦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%9D%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%9D%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%9D%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%9D%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%9D%A6)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n9/mode/2up)

#### 孔厳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%8E%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%8E%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%8E%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%8E%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%8E%B3)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n21/mode/2up)

#### 孔群
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E7%BE%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E7%BE%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E7%BE%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E7%BE%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E7%BE%A4)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n25/mode/2up)

#### 孔沉（孔沈）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n27/mode/2up)

#### 孔廞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E5%BB%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E5%BB%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E5%BB%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E5%BB%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E5%BB%9E)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n29/mode/2up)

#### 丁潭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%81%E6%BD%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%81%E6%BD%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%81%E6%BD%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%81%E6%BD%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%81%E6%BD%AD)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n29/mode/2up)

#### 張茂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8C%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8C%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8C%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8C%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8C%82)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n35/mode/2up)

#### 陶回
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E5%9B%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E5%9B%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E5%9B%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E5%9B%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E5%9B%9E)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n35/mode/2up)


### 巻79 列伝第49
#### 謝尚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E5%B0%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E5%B0%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E5%B0%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E5%B0%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E5%B0%9A)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n41/mode/2up)
* [PDFあり]
  [西晋謝尚伝--『晋書』謝尚伝訳注](https://dl.ndl.go.jp/info:ndljp/pid/10504663)
  （小松 英生）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「謝尚鴝鵒」（第96句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/72)
* 干宝[『搜神記』巻2](https://ctext.org/wiki.pl?if=gb&chapter=95966#p18)
* [李白「夜泊牛渚懷古」](https://zh.wikisource.org/wiki/%E5%A4%9C%E6%B3%8A%E7%89%9B%E6%B8%9A%E6%87%B7%E5%8F%A4)
  （[『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/88)）
* [崔塗「牛渚夜泊」](https://zh.wikisource.org/wiki/%E7%89%9B%E6%B8%9A%E5%A4%9C%E6%B3%8A)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* [李白「對雪醉後贈王歷陽」](https://zh.wikisource.org/wiki/%E5%B0%8D%E9%9B%AA%E9%86%89%E5%BE%8C%E8%B4%88%E7%8E%8B%E6%AD%B7%E9%99%BD)（[『国訳漢文大成』続 文学部第6冊](https://dl.ndl.go.jp/pid/1139956/1/59)）に「謝尚自能鴝鵒舞」とある。

#### 謝安
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E5%AE%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E5%AE%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E5%AE%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E5%AE%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E5%AE%89)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n49/mode/2up)
* [PDFあり]
  [魏晉石刻資料選注( 一九 謝府君神道闕 )](http://hdl.handle.net/2433/70897)
  （三國時代の出土文字資料班）……謝府君こと謝纘は謝安の曽祖父。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「謝安高潔」（第7句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/20)
* [『冊府元龜』卷三百十](https://zh.wikisource.org/wiki/%E5%86%8A%E5%BA%9C%E5%85%83%E9%BE%9C_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B70310)と[『言行龜鑒』卷五・出處門](https://zh.wikisource.org/wiki/%E8%A8%80%E8%A1%8C%E9%BE%9C%E9%91%92/%E5%8D%B75)で馮道を謝安と比べている。
* [ブログ]
  [古文詩詞常用的典故：東山再起](https://kknews.cc/culture/vn8m2qy.html)
  （@每日頭條）
  ……「東山再起」やそれに類する表現を使って謝安の故事を織り込んだ詩文がいろいろ挙げられている。ネット上で読めるものへのリンクを以下に貼る。
    * [文康『兒女英雄傳』第三九回](https://zh.wikisource.org/wiki/%E5%85%92%E5%A5%B3%E8%8B%B1%E9%9B%84%E5%82%B3/%E7%AC%AC%E4%B8%89%E5%8D%81%E4%B9%9D%E5%9B%9E)
    * [杜甫「暮秋枉裴道州手札率爾遣興寄近呈蘇渙侍御」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7223#%E6%9A%AE%E7%A7%8B%E6%9E%89%E8%A3%B4%E9%81%93%E5%B7%9E%E6%89%8B%E5%8A%84%EF%BC%8C%E7%8E%87%E7%88%BE%E9%81%A3%E8%88%88%EF%BC%8C%E5%AF%84%E8%BF%91%E5%91%88%E8%98%87%E6%B8%99%E4%BE%8D%E7%A6%A6)
        * [『続国訳漢文大成 文学部 第6巻の下（杜少陵詩集 下巻の下）』](https://dl.ndl.go.jp/info:ndljp/pid/1239692/392)
    * [溫庭筠「題裴晉公林亭」](https://zh.wikisource.org/wiki/%E9%A1%8C%E8%A3%B4%E6%99%89%E5%85%AC%E6%9E%97%E4%BA%AD)
    * [李白「送裴十八圖南歸嵩山」](https://zh.wikisource.org/wiki/%E9%80%81%E8%A3%B4%E5%8D%81%E5%85%AB%E5%9C%96%E5%8D%97%E6%AD%B8%E5%B5%A9%E5%B1%B1)
        * [『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/58)
    * [李白「梁園吟」](https://zh.wikisource.org/wiki/%E6%A2%81%E5%9C%92%E5%90%9F)
        * [『続国訳漢文大成 文学部第4冊（李太白詩集 上の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139937/24)
    * [王惲「望海潮・為故相雲叟公壽」](https://zh.wikisource.org/wiki/%E7%A7%8B%E6%BE%97%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7074#%E4%BA%8C)
    * [鄭廷玉『忍字記』第四折](https://zh.wikisource.org/wiki/%E5%B8%83%E8%A2%8B%E5%92%8C%E5%B0%9A%E5%BF%8D%E5%AD%97%E8%A8%98#%E7%AC%AC%E5%9B%9B%E6%8A%98)
    * [李白「送梁四歸東平」](https://zh.wikisource.org/wiki/%E9%80%81%E6%A2%81%E5%9B%9B%E6%AD%B8%E6%9D%B1%E5%B9%B3)
        * [『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/107)
    * [謝靈運「還舊園作見顏范二中書」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B725#%E9%82%84%E8%88%8A%E5%9C%92%E4%BD%9C%E8%A6%8B%E9%A1%8F%E8%8C%83%E4%BA%8C%E4%B8%AD%E6%9B%B8)
        * [『国訳漢文大成 第3巻（文選 中巻）』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/162)
    * [王維「戲贈張五弟諲」](https://zh.wikisource.org/wiki/%E6%88%B2%E8%B4%88%E5%BC%B5%E4%BA%94%E5%BC%9F%E8%AB%B2)
        * [『続国訳漢文大成 文学部第70冊（王右丞集の一）』](https://dl.ndl.go.jp/info:ndljp/pid/1140684/53)
    * [王維「與工部李侍郎書」](https://zh.wikisource.org/wiki/%E8%88%87%E5%B7%A5%E9%83%A8%E6%9D%8E%E4%BE%8D%E9%83%8E%E6%9B%B8)
* 白居易「裴侍中晉公以集賢林亭即事詩二十六韻見贈猥蒙徴和才拙詞繁廣為五百言以伸酬獻」
    * [『白氏長慶集』（四庫全書本）巻29](https://zh.wikisource.org/wiki/%E7%99%BD%E6%B0%8F%E9%95%B7%E6%85%B6%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B729)
    * [『全唐詩』巻452](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7452#%E8%A3%B4%E4%BE%8D%E4%B8%AD%E6%99%89%E5%85%AC%E4%BB%A5%E9%9B%86%E8%B3%A2%E6%9E%97%E4%BA%AD%E5%8D%B3%E4%BA%8B%E8%A9%A9%E4%B8%89%E5%8D%81%E5%85%AD%E9%9F%BB%E8%A6%8B%E8%B4%88%E7%8C%A5%E8%92%99%E5%BE%B5%E5%92%8C%E6%89%8D%E6%8B%99%E8%A9%9E%E7%B9%81%E8%BC%92%E5%BB%A3%E7%82%BA%E4%BA%94%E7%99%BE%E8%A8%80%E4%BB%A5%E4%BC%B8%E9%85%AC%E7%8D%BB)
    * [『続国訳漢文大成 文学部第42冊（白楽天詩集 三の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1140376/31)
* [胡曾の「東山」](https://zh.wikisource.org/wiki/%E6%9D%B1%E5%B1%B1)
* [姚承緒『吳趨訪古錄』の「弇山園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E5%BC%87%E5%B1%B1%E5%9C%92)
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「謝安論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/23)
  （侯方域）
* [李白「登金陵冶城西北謝安墩」](https://zh.wikisource.org/wiki/%E7%99%BB%E9%87%91%E9%99%B5%E5%86%B6%E5%9F%8E%E8%A5%BF%E5%8C%97%E8%AC%9D%E5%AE%89%E5%A2%A9)
    * [『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/14)
* [唐の楊綰](https://zh.wikipedia.org/zh-tw/%E6%A5%8A%E7%B6%B0)が楊震（・邴吉）・山濤・謝安になぞらえられている。
	* [『旧唐書』巻119](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B7119#%E6%A5%8A%E7%B6%B0)
	* [『新唐書』巻142](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7142#%E6%A5%8A%E7%B6%B0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* 杜甫「入衡州」に「謝安乘興長」という句がある。
    * [『全唐詩』卷223](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7223#%E5%85%A5%E8%A1%A1%E5%B7%9E)
    * [『九家集注杜詩』卷16](https://zh.wikisource.org/wiki/%E4%B9%9D%E5%AE%B6%E9%9B%86%E6%B3%A8%E6%9D%9C%E8%A9%A9_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B716#%E5%85%A5%E8%A1%A1%E5%B7%9E)
    * [『国訳漢文大成』続 文学部第24の下冊](https://dl.ndl.go.jp/pid/1140152/1/128)
* 王讜の[『唐語林』巻5](https://zh.wikisource.org/wiki/%E5%94%90%E8%AA%9E%E6%9E%97/%E5%8D%B7%E4%BA%94)の郗昂と源乾曜のやりとりは、[『世説新語』雅量篇27](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E9%9B%85%E9%87%8F)の「入幕の賓」をネタにしており、直接的には郗超と謝安に言及している。
* [李華「謝文靖」](https://zh.wikisource.org/wiki/%E8%AC%9D%E6%96%87%E9%9D%96)……「文靖」は謝安の諡。苻堅や謝玄らにも言及している。
* [王丘「詠史」](https://zh.wikisource.org/wiki/%E8%A9%A0%E5%8F%B2_(%E7%8E%8B%E4%B8%98))に「偉哉謝安石，攜妓入東山。」とある。
* 謝安の字の「安石」と同名の人たち
    * 韋安石
        * [『旧唐書』巻92](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B792#%E9%9F%8B%E5%AE%89%E7%9F%B3)
        * [『新唐書』巻122](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7122#%E9%9F%8B%E5%AE%89%E7%9F%B3)
    * 王安石
        * [『宋史』巻327](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7327#%E7%8E%8B%E5%AE%89%E7%9F%B3)
* [『金瓶梅』第49回](https://zh.wikisource.org/wiki/%E9%87%91%E7%93%B6%E6%A2%85/%E7%AC%AC49%E5%9B%9E)に「蔡御史看見，欲進不能，欲退不舍。便說道：「四泉，你如何這等愛厚？恐使不得。」西門慶笑道：「與昔日東山之游，又何異乎？」蔡御史道：「恐我不如安石之才，而君有王右軍之高致矣。」」という会話があり、「東山之游」と「安石之才」が謝安への言及、「王右軍之高致」が王羲之への言及である。

#### 羊曇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E6%9B%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E6%9B%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E6%9B%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E6%9B%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E6%9B%87)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝瑤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%91%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%91%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%91%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%91%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%91%A4)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝琰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%90%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%90%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%90%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%90%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%90%B0)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/394)）に「謝琰功高而後至」とある。

#### 謝混
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%B7%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%B7%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%B7%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%B7%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%B7%B7)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n71/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E5%A5%95)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n73/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝玄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%8E%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%8E%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%8E%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%8E%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%8E%84)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n75/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)
* [『文選』所収の謝霊運「述祖德詩」二首](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B719#%E8%BF%B0%E7%A5%96%E5%BE%B7%E8%A9%A9%E4%BA%8C%E9%A6%96)（[『国訳漢文大成』第三卷](https://dl.ndl.go.jp/pid/1912796/1/25)）は謝霊運の祖父である謝玄について述べたもの。
* [李華「謝文靖」](https://zh.wikisource.org/wiki/%E8%AC%9D%E6%96%87%E9%9D%96)……「文靖」は謝安の諡。苻堅や謝玄らにも言及している。

#### 何謙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E8%AC%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E8%AC%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E8%AC%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E8%AC%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E8%AC%99)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 戴逯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E9%80%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E9%80%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E9%80%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E9%80%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E9%80%AF)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝万
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E4%B8%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E4%B8%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E4%B8%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E4%B8%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E4%B8%87)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝韶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%9F%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%9F%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%9F%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%9F%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%9F%B6)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n95/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝朗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%9C%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%9C%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%9C%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%9C%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%9C%97)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n95/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝重
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%87%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%87%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%87%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%87%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%87%8D)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n97/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝絢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%B5%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%B5%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%B5%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%B5%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%B5%A2)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n97/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝石
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E7%9F%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E7%9F%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E7%9F%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E7%9F%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E7%9F%B3)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n97/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

#### 謝邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E9%82%88)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n101/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m079.html)

### 巻80 列伝第50
#### 王羲之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%BE%B2%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n105/mode/2up)
* [書誌のみ・なぜか2と3のみ]
  [晋書王羲之伝訳注](https://cir.nii.ac.jp/all?q=%E7%8E%8B%E7%BE%B2%E4%B9%8B%E4%BC%9D%E8%A8%B3%E6%B3%A8&count=20&sortorder=0)
  （杉村 邦彦）
* [書誌のみ]
  [六朝文人傳 : 王羲之(『晉書』卷八十)](https://cir.nii.ac.jp/crid/1520290885032417664)
  （森野 繁夫）
* 李白「[王右軍](https://zh.wikisource.org/wiki/%E7%8E%8B%E5%8F%B3%E8%BB%8D)」
  （[『国訳漢文大成　続　文学部』第10冊](https://dl.ndl.go.jp/info:ndljp/pid/1139993/66)）
* 蘇軾「[題王逸少帖](https://zh.wikisource.org/wiki/%E9%A1%8C%E7%8E%8B%E9%80%B8%E5%B0%91%E5%B8%96)」
  （[『国訳漢文大成　続　文学部』第61冊](https://dl.ndl.go.jp/info:ndljp/pid/1140559/34)）
* 王羲之の書を[検索](https://theme.npm.edu.tw/opendata/DigitImageSets.aspx?Key=%e7%8e%8b%e7%be%b2%e4%b9%8b%5E12%5E13)
  （@故宮Open Data專區）
* [書籍 (NDL)]
  空海『三教指帰』に[「鍾張王歐も毫を擲って耻を懷かん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注108](https://dl.ndl.go.jp/info:ndljp/pid/1040599/41)）。
* [李白「登金陵冶城西北謝安墩」](https://zh.wikisource.org/wiki/%E7%99%BB%E9%87%91%E9%99%B5%E5%86%B6%E5%9F%8E%E8%A5%BF%E5%8C%97%E8%AC%9D%E5%AE%89%E5%A2%A9)
    * [『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/14)
* [王師乾「王右軍祠堂碑」](https://zh.wikisource.org/wiki/%E7%8E%8B%E5%8F%B3%E8%BB%8D%E7%A5%A0%E5%A0%82%E7%A2%91)
* [『金瓶梅』第49回](https://zh.wikisource.org/wiki/%E9%87%91%E7%93%B6%E6%A2%85/%E7%AC%AC49%E5%9B%9E)に「蔡御史看見，欲進不能，欲退不舍。便說道：「四泉，你如何這等愛厚？恐使不得。」西門慶笑道：「與昔日東山之游，又何異乎？」蔡御史道：「恐我不如安石之才，而君有王右軍之高致矣。」」という会話があり、「東山之游」と「安石之才」が謝安への言及、「王右軍之高致」が王羲之への言及である。
* [錢選「王羲之觀鵝圖」](https://www.metmuseum.org/ja/art/collection/search/40081)

#### 王凝之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%87%9D%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%87%9D%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n131/mode/2up)

#### 王徽之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BE%BD%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BE%BD%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n133/mode/2up)
* [清少納言『枕草子』の「五月ばかりに月もなくいとくらき夜……」の段](https://ja.wikisource.org/wiki/%E6%9E%95%E8%8D%89%E7%B4%99_(%E5%9C%8B%E6%96%87%E5%A4%A7%E8%A7%80))の「くれたけ」と「このきみ」は王徽之の逸話を踏まえている。
* [服部南郭の「聞笛」](https://dl.ndl.go.jp/info:ndljp/pid/2559343/40)にある「三弄都作断腸声」の「三弄」は、桓伊と王徽之の故事に基づく。
    * [ブログ]
      [漢文日録２６．３．１５](http://www.mugyu.biz-web.jp/nikki.26.03.15.htm)
      （@肝冷斎日録）
    * 「江戸中期の儒者・漢詩人服部南郭の漢詩に「笛を聞く」という作品がある。この作の最後の句は「三弄都て断腸の声と為る」（さんろうすべてだんちょうのこえとなる）となっているが、句頭の「三弄」という言葉は「「三路が笛・三弄が笛」の物語り」といわれる説話を示唆しているらしい（『大言海』による）。この説話の内容を知りたいが何か資料はないか。」という[レファレンス事例](https://crd.ndl.go.jp/reference/detail?page=ref_view&id=1000284888)
      （@レファレンス協同データベース）
* [李白「對雪醉後贈王歷陽」](https://zh.wikisource.org/wiki/%E5%B0%8D%E9%9B%AA%E9%86%89%E5%BE%8C%E8%B4%88%E7%8E%8B%E6%AD%B7%E9%99%BD)（[『国訳漢文大成』続 文学部第6冊](https://dl.ndl.go.jp/pid/1139956/1/59)）に「子猷聞風動窗竹」とある。子猷は王徽之の字。

#### 王楨之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%A5%A8%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%A5%A8%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n137/mode/2up)

#### 王操之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%93%8D%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%93%8D%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n137/mode/2up)

#### 王献之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8C%AE%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8C%AE%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n137/mode/2up)
* 王献之の書を[検索](https://theme.npm.edu.tw/opendata/DigitImageSets.aspx?Key=%e7%8e%8b%e7%8d%bb%e4%b9%8b)
  （@故宮Open Data專區）
* [『樂府詩集』45卷「桃葉歌」](https://zh.wikisource.org/wiki/%E6%A8%82%E5%BA%9C%E8%A9%A9%E9%9B%86/045%E5%8D%B7#%E6%A1%83%E8%91%89%E6%AD%8C%E4%B8%89%E9%A6%96)
* [書籍 (NDL)]
  空海『三教指帰』に[「鍾張王歐も毫を擲って耻を懷かん」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/13)とある
  （[注108](https://dl.ndl.go.jp/info:ndljp/pid/1040599/41)）。
* [姚承緒『吳趨訪古錄』の「辟疆園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/2#%E8%BE%9F%E7%96%86%E5%9C%92)
  ……王献之と絡めて[『世說新語』簡傲篇17](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E7%B0%A1%E5%82%B2)で語られる顧辟疆の庭園を題材にした詩。
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「王献之論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/27)
  （呉成佐）

#### 許邁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A8%B1%E9%82%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A8%B1%E9%82%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A8%B1%E9%82%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A8%B1%E9%82%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A8%B1%E9%82%81)
* [書籍 (IA)]
  [晉書斠注(五十三)](https://archive.org/details/02077770.cn/page/n145/mode/2up)


### 巻81 列伝第51
#### 王遜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%81%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%81%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%81%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%81%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%81%9C)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n1/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/191)）に「捐駒」とあり、これは注によれば王遜の行為を指す。

#### 蔡豹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%94%A1%E8%B1%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%94%A1%E8%B1%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%94%A1%E8%B1%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%94%A1%E8%B1%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%94%A1%E8%B1%B9)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n7/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B8%DE%BD%BD%B0%EC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 羊鑑（羊鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n11/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B8%DE%BD%BD%B0%EC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 劉胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n13/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 桓宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n17/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 桓伊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E4%BC%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E4%BC%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E4%BC%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E4%BC%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E4%BC%8A)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n23/mode/2up)
* [服部南郭の「聞笛」](https://dl.ndl.go.jp/info:ndljp/pid/2559343/40)にある「三弄都作断腸声」の「三弄」は、桓伊と王徽之の故事に基づく。
    * [ブログ]
      [漢文日録２６．３．１５](http://www.mugyu.biz-web.jp/nikki.26.03.15.htm)
      （@肝冷斎日録）
    * 「江戸中期の儒者・漢詩人服部南郭の漢詩に「笛を聞く」という作品がある。この作の最後の句は「三弄都て断腸の声と為る」（さんろうすべてだんちょうのこえとなる）となっているが、句頭の「三弄」という言葉は「「三路が笛・三弄が笛」の物語り」といわれる説話を示唆しているらしい（『大言海』による）。この説話の内容を知りたいが何か資料はないか。」という[レファレンス事例](https://crd.ndl.go.jp/reference/detail?page=ref_view&id=1000284888)
      （@レファレンス協同データベース）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 朱伺
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9C%B1%E4%BC%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9C%B1%E4%BC%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9C%B1%E4%BC%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9C%B1%E4%BC%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9C%B1%E4%BC%BA)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n29/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛穆之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E7%A9%86%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E7%A9%86%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n41/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛璩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E7%92%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E7%92%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E7%92%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E7%92%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E7%92%A9)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n45/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛脩之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E8%84%A9%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n49/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)
* [『宋書』巻48](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B748#%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [『南史』巻16](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B716#%E6%AF%9B%E4%BF%AE%E4%B9%8B)
* [『魏書』巻43](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B743#%E6%AF%9B%E8%84%A9%E4%B9%8B)
* [『北史』巻27](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7027#%E6%AF%9B%E4%BF%AE%E4%B9%8B)
* [ブログ]
  [毛脩之１　刺史の家柄](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054892456514)〜
  (@ゆるふわ晋宋春秋２)

#### 毛安之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E5%AE%89%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E5%AE%89%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 毛徳祖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AF%9B%E5%BE%B3%E7%A5%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AF%9B%E5%BE%B3%E7%A5%96)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 劉遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n55/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%B8%DE%BD%BD%B0%EC)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 鄧嶽（鄧岳）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E5%B2%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E5%B2%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E5%B2%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E5%B2%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E5%B2%B3)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n57/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 鄧遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n59/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)

#### 朱序
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9C%B1%E5%BA%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9C%B1%E5%BA%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9C%B1%E5%BA%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9C%B1%E5%BA%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9C%B1%E5%BA%8F)
* [書籍 (IA)]
  [晉書斠注(五十四)](https://archive.org/details/02077771.cn/page/n61/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m081.html)


### 巻82 列伝第52
#### 陳寿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E5%AF%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E5%AF%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E5%AF%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E5%AF%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E5%AF%BF)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n1/mode/2up)
* [書誌のみ]
  [前四史撰者列傳の研究『晉書』陳壽傳譯注](https://cir.nii.ac.jp/crid/1520853833570345472)
  （前四史撰者列傳研究ゼミナール）
* [PDFあり]
  [六朝文人伝 : 『晋書』(巻八十二)陳寿伝](https://cir.nii.ac.jp/crid/1541980095194737152)
  （佐藤 利行）
* [ブログ]
  [陳寿伝](https://estar.jp/novels/25594548/viewer?page=185)
  （@淡々晋書）
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「陳壽論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/28)
  （朱彛尊（朱彝尊））
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 王長文
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%95%B7%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%95%B7%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%95%B7%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%95%B7%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%95%B7%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n5/mode/2up)
* [ブログ]
  [王長文伝](https://estar.jp/novels/25594548/viewer?page=226)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 虞溥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%BA%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%BA%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%BA%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%BA%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%BA%A5)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n9/mode/2up)
* [ブログ]
  [『晋書』虞溥伝を訳し、『江表伝』の作者を知る](http://3guozhi.net/b/gf.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [虞溥伝](https://estar.jp/novels/25594548/viewer?page=229)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 司馬彪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B8%E9%A6%AC%E5%BD%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B8%E9%A6%AC%E5%BD%AA)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n13/mode/2up)
* [ブログ]
  [司馬彪伝](https://estar.jp/novels/25594548/viewer?page=235)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 王隠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%9A%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%9A%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%9A%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%9A%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%9A%A0)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n17/mode/2up)
* [ブログ]
  [王隠伝](https://estar.jp/novels/25594548/viewer?page=239)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 虞預
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E9%A0%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E9%A0%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E9%A0%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E9%A0%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E9%A0%90)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n21/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 孫盛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%9B%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%9B%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%9B%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%9B%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%9B%9B)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n29/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 孫潜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%BD%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%BD%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%BD%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%BD%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%BD%9C)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 孫放
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%94%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%94%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%94%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%94%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%94%BE)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 干宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B9%B2%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B9%B2%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B9%B2%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B9%B2%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B9%B2%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n37/mode/2up)
* [PDFあり]
  [六朝文人伝 : 『晋書』(巻八十二)干宝伝](https://dl.ndl.go.jp/info:ndljp/pid/10504525)
  （高西 成介）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、干宝伝で言及している『晉紀』のうち、
  [「論晉武帝革命」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/235)と
  [「總論」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/236)とがある。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第十二巻　晋唐小説』には、干宝伝にその序を引く
  [『搜神記』](https://dl.ndl.go.jp/info:ndljp/pid/1913008/47)の部分訳がある。
* 干宝[『搜神記』](https://ctext.org/wiki.pl?if=gb&res=839038)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 鄧粲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E7%B2%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E7%B2%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E7%B2%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E7%B2%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E7%B2%B2)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n43/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 謝沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n45/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 習鑿歯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BF%92%E9%91%BF%E6%AD%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BF%92%E9%91%BF%E6%AD%AF)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n47/mode/2up)
* [ブログ]
  [『晋書』列伝５２、蜀漢正統論の習鑿歯伝を翻訳](http://3guozhi.net/q/sss1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)

#### 徐広
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E5%BA%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E5%BA%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E5%BA%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E5%BA%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E5%BA%83)
* [書籍 (IA)]
  [晉書斠注(五十五)](https://archive.org/details/02077772.cn/page/n63/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m082.html)
* [ブログ]
  [徐広１　　晋国の知恵袋](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054894418359)〜
  (@ゆるふわ晋宋春秋２)
* [『宋書』巻55](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B755#%E5%BE%90%E5%BB%A3)
* [『南史』巻33](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B733#%E5%BE%90%E5%BB%A3)


### 巻83 列伝第53
#### 顧和
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E5%92%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E5%92%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E5%92%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E5%92%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E5%92%8C)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n1/mode/2up)

#### 袁瓌（袁瑰）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E7%91%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E7%91%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E7%91%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E7%91%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E7%91%B0)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n9/mode/2up)

#### 袁喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n13/mode/2up)

#### 袁山松
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E5%B1%B1%E6%9D%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E5%B1%B1%E6%9D%BE)
* [姚承緒『吳趨訪古錄』の「滬瀆壘」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/7#%E6%BB%AC%E7%80%86%E5%A3%98)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n15/mode/2up)

#### 袁猷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E7%8C%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E7%8C%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E7%8C%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E7%8C%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E7%8C%B7)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n17/mode/2up)

#### 袁準
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E6%BA%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E6%BA%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E6%BA%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E6%BA%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E6%BA%96)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n19/mode/2up)

#### 袁耽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E8%80%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E8%80%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E8%80%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E8%80%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E8%80%BD)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n19/mode/2up)

#### 袁質
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E8%B3%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E8%B3%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E8%B3%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E8%B3%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E8%B3%AA)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n21/mode/2up)

#### 袁湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E6%B9%9B)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n21/mode/2up)
* [『宋書』巻52](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B752#%E8%A2%81%E6%B9%9B)
* [『南史』巻26](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B726#%E8%A2%81%E6%B9%9B)
* [ブログ]
  [袁湛１　　陳郡の名家](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054893896064)〜
  (@ゆるふわ晋宋春秋２)

#### 袁豹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E8%B1%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E8%B1%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E8%B1%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E8%B1%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E8%B1%B9)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n21/mode/2up)
* [『宋書』巻52](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B752#%E5%BC%9F_%E8%B1%B9)
* [『南史』巻26](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B726#%E5%BC%9F_%E8%B1%B9)
* [ブログ]
  [袁豹１　　雅俗の善きひと](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054893956605)〜
  (@ゆるふわ晋宋春秋２)

#### 江逌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E9%80%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E9%80%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E9%80%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E9%80%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E9%80%8C)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n23/mode/2up)

#### 江灌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E7%81%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E7%81%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E7%81%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E7%81%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E7%81%8C)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n31/mode/2up)
* [ブログ]
  [江逌伝補伝江灌伝](http://strawberrymilk.gooside.com/text/shinjo/83-3-1.html)
  （@漢籍訳出プロジェクト「漢々學々」）

#### 江績
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B1%9F%E7%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B1%9F%E7%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B1%9F%E7%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B1%9F%E7%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B1%9F%E7%B8%BE)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n33/mode/2up)
* [ブログ]
  [江逌伝補伝・江績伝](http://strawberrymilk.gooside.com/text/shinjo/83-3-2.html)
  （@漢籍訳出プロジェクト「漢々學々」）

#### 車胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BB%8A%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BB%8A%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BB%8A%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BB%8A%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BB%8A%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n35/mode/2up)
* [ブログ]
  [車胤伝](http://strawberrymilk.gooside.com/text/shinjo/83-4.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [書籍 (NDL)]
  空海『三教指帰』に[「雪螢を猶ほ怠るに拉ぎ」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/8)とあり
  （[注10](https://dl.ndl.go.jp/info:ndljp/pid/1040599/33)）、
  [「數十の熠燿囊の中に聚めず」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/12)ともある
  （[注73](https://dl.ndl.go.jp/info:ndljp/pid/1040599/38)）。

#### 殷顗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E9%A1%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E9%A1%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E9%A1%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E9%A1%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E9%A1%97)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n37/mode/2up)
* [ブログ]
  [殷顗伝](http://strawberrymilk.gooside.com/text/shinjo/83-5.html)
  （@漢籍訳出プロジェクト「漢々學々」）

#### 王雅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%9B%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%9B%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%9B%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%9B%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%9B%85)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n39/mode/2up)
* [ブログ]
  [王雅伝](http://strawberrymilk.gooside.com/text/shinjo/83-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）


### 巻84 列伝第54
#### 王恭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%81%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%81%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%81%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%81%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%81%AD)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n47/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕・桓玄への言及がある。

#### 庾楷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E6%A5%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E6%A5%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E6%A5%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E6%A5%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E6%A5%B7)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n59/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)

#### 劉牢之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%89%A2%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%89%A2%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n61/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕・桓玄への言及がある。

#### 劉敬宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%95%AC%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%95%AC%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n71/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)
* [『宋書』巻47](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B747#%E5%8A%89%E6%95%AC%E5%AE%A3)
* [『南史』巻17](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B717#%E5%8A%89%E6%95%AC%E5%AE%A3)
* [ブログ]
  [劉敬宣１　幼き孝子](https://kakuyomu.jp/works/1177354054891500185/episodes/1177354054891873014)〜
  (@ゆるふわ晋宋春秋２)

#### 殷仲堪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E4%BB%B2%E5%A0%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E4%BB%B2%E5%A0%AA)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n75/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)

#### 楊佺期
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E4%BD%BA%E6%9C%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E4%BD%BA%E6%9C%9F)
* [書籍 (IA)]
  [晉書斠注(五十六)](https://archive.org/details/02077773.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m084.html)


### 巻85 列伝第55
#### 劉毅
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%AF%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%AF%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%AF%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%AF%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%AF%85)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n1/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 劉邁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%82%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%82%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%82%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%82%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%82%81)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n19/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 諸葛長民
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AB%B8%E8%91%9B%E9%95%B7%E6%B0%91)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n19/mode/2up)
* [ブログ]
  [「祭已畢焉」晋書諸葛長民伝](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854985)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 何無忌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%84%A1%E5%BF%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%84%A1%E5%BF%8C)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n25/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 檀憑之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AA%80%E6%86%91%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AA%80%E6%86%91%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AA%80%E6%86%91%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AA%80%E6%86%91%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AA%80%E6%86%91%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n33/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)

#### 魏詠之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%8F%E8%A9%A0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%8F%E8%A9%A0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n35/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m085.html)


### 巻86 列伝第56
#### 張軌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%BB%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%BB%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%BB%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%BB%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%BB%8C)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n39/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [周曇の「詠史詩」の中の「前涼張軌」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E5%89%8D%E6%B6%BC%E5%BC%B5%E8%BB%8C)
* [ブログ]
  [太平御覧 張軌](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E8%BB%8C)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張寔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%AF%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%AF%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%AF%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%AF%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%AF%94)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n55/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張寔](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E5%AF%94)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張茂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8C%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8C%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8C%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8C%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8C%82)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n65/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張茂](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E8%8C%82)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張駿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%A7%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%A7%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%A7%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%A7%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%A7%BF)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n71/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張駿](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E9%A7%BF)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張重華
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E9%87%8D%E8%8F%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E9%87%8D%E8%8F%AF)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n93/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張重華](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E9%87%8D%E8%8F%AF)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張耀霊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%80%80%E9%9C%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%80%80%E9%9C%8A)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n107/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張祚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%A5%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%A5%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%A5%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%A5%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%A5%9A)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n109/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張祚](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E7%A5%9A)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張玄靚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%8E%84%E9%9D%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%8E%84%E9%9D%9A)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n115/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張玄靚](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E7%8E%84%E9%9D%9A)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

#### 張天錫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%A4%A9%E9%8C%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB)
* [書籍 (IA)]
  [晉書斠注(五十七)](https://archive.org/details/02077774.cn/page/n121/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m086.html)
* [ブログ]
  [太平御覧 張天錫](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%BC%B5%E5%A4%A9%E9%8C%AB)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E5%AF%94)

### 巻87 列伝第57
#### 涼武昭王（李暠）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%9A%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%9A%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%9A%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%9A%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%9A%A0)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n1/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m087.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E6%9D%8E%E6%9A%A0)
* [『北史』巻100](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7100)

#### 涼後主（李歆）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%AD%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%AD%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%AD%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%AD%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%AD%86)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n29/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m087.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E6%9D%8E%E6%9A%A0)
* [『北史』巻100](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7100)

### 巻88 列伝第58 孝友
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n41/mode/2up)

#### 李密
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%AF%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%AF%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%AF%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%AF%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%AF%86)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n43/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、李密伝に引かれた
  [「陳情事表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/407)がある。

#### 盛彦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%9B%E5%BD%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%9B%E5%BD%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%9B%E5%BD%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%9B%E5%BD%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%9B%E5%BD%A6)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n49/mode/2up)

#### 夏方
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E6%96%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E6%96%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E6%96%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E6%96%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E6%96%B9)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n51/mode/2up)

#### 王裒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%A3%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%A3%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%A3%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%A3%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%A3%92)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n51/mode/2up)

#### 許孜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A8%B1%E5%AD%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A8%B1%E5%AD%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A8%B1%E5%AD%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A8%B1%E5%AD%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A8%B1%E5%AD%9C)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n57/mode/2up)

#### 庾袞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E8%A2%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E8%A2%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E8%A2%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E8%A2%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E8%A2%9E)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n59/mode/2up)

#### 孫晷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%99%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%99%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%99%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%99%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%99%B7)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n69/mode/2up)

#### 顔含
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%94%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%94%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%94%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%94%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%94%E5%90%AB)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n71/mode/2up)
* 顔之推の『顔氏家訓』
    * [治家篇](https://zh.wikisource.org/wiki/%E9%A1%8F%E6%B0%8F%E5%AE%B6%E8%A8%93/%E5%8D%B7%E7%AC%AC1#%E6%B2%BB%E5%AE%B6%E7%AC%AC%E4%BA%94)に「婚姻素對，靖侯成規。」とある。
    * [止足篇](https://zh.wikisource.org/wiki/%E9%A1%8F%E6%B0%8F%E5%AE%B6%E8%A8%93/%E5%8D%B7%E7%AC%AC5#%E6%AD%A2%E8%B6%B3%E7%AC%AC%E5%8D%81%E4%B8%89)に「先祖靖侯戒子侄曰：『汝家書生門戶，世無富貴；自今仕宦不可過二千石，婚姻勿貪勢家。』」とある。
* 顔真卿の[「晉侍中右光祿大夫本州大中正西平靖侯顏公大宗碑」](https://zh.wikisource.org/wiki/%E6%99%89%E4%BE%8D%E4%B8%AD%E5%8F%B3%E5%85%89%E7%A5%BF%E5%A4%A7%E5%A4%AB%E6%9C%AC%E5%B7%9E%E5%A4%A7%E4%B8%AD%E6%AD%A3%E8%A5%BF%E5%B9%B3%E9%9D%96%E4%BE%AF%E9%A1%8F%E5%85%AC%E5%A4%A7%E5%AE%97%E7%A2%91)

#### 劉殷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%AE%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%AE%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%AE%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%AE%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%AE%B7)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n79/mode/2up)

#### 王延
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BB%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BB%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BB%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BB%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BB%B6)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n83/mode/2up)

#### 王談
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%AB%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%AB%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%AB%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%AB%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%AB%87)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n85/mode/2up)

#### 桑虞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%91%E8%99%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%91%E8%99%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%91%E8%99%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%91%E8%99%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%91%E8%99%9E)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n87/mode/2up)

#### 何琦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%90%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%90%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%90%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%90%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%90%A6)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n89/mode/2up)

#### 呉逵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%89%E9%80%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%89%E9%80%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%89%E9%80%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%89%E9%80%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%89%E9%80%B5)
* [書籍 (IA)]
  [晉書斠注(五十八)](https://archive.org/details/02077775.cn/page/n91/mode/2up)


### 巻89 列伝第59 忠義
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n2/mode/2up)

#### 嵆紹（嵇紹）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B5%87%E7%B4%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B5%87%E7%B4%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B5%87%E7%B4%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B5%87%E7%B4%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B5%87%E7%B4%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n3/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch1.html)
  （@いつか書きたい『三国志』）
* [文天祥「正氣歌」](https://zh.wikisource.org/wiki/%E6%AD%A3%E6%B0%A3%E6%AD%8C)の「為嵇侍中血」という句
* [岡本綺堂『明治劇談　ランプの下にて』](https://www.aozora.gr.jp/cards/000082/files/49526_42385.html)所収「『船弁慶』と『夢物語』」
  ……「手水などが要るものか。稽侍中の血、洗う勿れじゃ。」という台詞にまつわる話がある（「稽」は「嵇」の誤植または誤入力か）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(2)嵆紹 王豹](https://readingnotesofjinshu.com/translation/biographies/vol-89_2#toc1)
  （@晋書簡訳所）
* 高啓の[「永嘉行」](https://zh.wikisource.org/wiki/%E5%A4%A7%E5%85%A8%E9%9B%86_(%E9%AB%99%E5%95%93,_%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B701#%E6%B0%B8%E5%98%89%E8%A1%8C)の「帝衣濺血忠臣死」という句（[『国訳漢文大成. 続 文学部第73冊』の訳注](https://dl.ndl.go.jp/info:ndljp/pid/1140719/143)も参照）
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「稽紹非忠臣論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/18)
  （兪長城（字が寧世））

#### 嵆含（嵇含）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B5%87%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B5%87%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B5%87%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B5%87%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B5%87%E5%90%AB)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n11/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(2)嵆紹 王豹](https://readingnotesofjinshu.com/translation/biographies/vol-89_2#toc2)
  （@晋書簡訳所）

#### 王豹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%B1%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%B1%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%B1%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%B1%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%B1%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n15/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(2)嵆紹 王豹](https://readingnotesofjinshu.com/translation/biographies/vol-89_2#toc3)
  （@晋書簡訳所）

#### 劉沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n21/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch7.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(3)劉沈 麹允](https://readingnotesofjinshu.com/translation/biographies/vol-89_3#toc1)
  （@晋書簡訳所）

#### 麹允
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BA%B9%E5%85%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BA%B9%E5%85%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BA%B9%E5%85%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BA%B9%E5%85%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BA%B9%E5%85%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n25/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(3)劉沈 麹允](https://readingnotesofjinshu.com/translation/biographies/vol-89_3#toc2)
  （@晋書簡訳所）

#### 焦嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%84%A6%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%84%A6%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%84%A6%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%84%A6%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%84%A6%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n27/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [巻八十九 列伝第五十九 忠義(3)劉沈 麹允](https://readingnotesofjinshu.com/translation/biographies/vol-89_3#toc2)
  （@晋書簡訳所）

#### 賈渾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%B8%BE)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n27/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 王育
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%82%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%82%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%82%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%82%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%82%B2)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n29/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 韋忠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%8B%E5%BF%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%8B%E5%BF%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%8B%E5%BF%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%8B%E5%BF%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%8B%E5%BF%A0)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n29/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 辛勉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E5%8B%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E5%8B%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E5%8B%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E5%8B%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E5%8B%89)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n31/mode/2up)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch10.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 辛賓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E8%B3%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E8%B3%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E8%B3%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E8%B3%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E8%B3%93)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n33/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 劉敏元
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%95%8F%E5%85%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%95%8F%E5%85%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%95%8F%E5%85%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%95%8F%E5%85%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%95%8F%E5%85%83)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n33/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列傳５９「忠義」](http://3guozhi.net/n/ch10.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 周該
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E8%A9%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E8%A9%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E8%A9%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E8%A9%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E8%A9%B2)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n35/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 桓雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n35/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 韓階
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E9%9A%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E9%9A%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E9%9A%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E9%9A%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E9%9A%8E)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n37/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 周崎
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E5%B4%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E5%B4%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E5%B4%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E5%B4%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E5%B4%8E)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n37/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 易雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%98%93%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%98%93%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%98%93%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%98%93%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%98%93%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n37/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 楽道融
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%BD%E9%81%93%E8%9E%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%BD%E9%81%93%E8%9E%8D)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n41/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 虞悝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%82%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%82%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%82%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%82%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%82%9D)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n41/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 沈勁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B2%88%E5%8B%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B2%88%E5%8B%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B2%88%E5%8B%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B2%88%E5%8B%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B2%88%E5%8B%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n45/mode/2up)
* [ブログ]
  [『晋書』列伝５９忠義、洛陽に殉じた、東晋人・沈勁](http://3guozhi.net/q/tt1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 吉挹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%89%E6%8C%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%89%E6%8C%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%89%E6%8C%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%89%E6%8C%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%89%E6%8C%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n47/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 王諒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%AB%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%AB%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%AB%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%AB%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%AB%92)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n49/mode/2up)
* 干宝[『搜神記』巻7](https://ctext.org/wiki.pl?if=gb&chapter=749239#p42)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 宋矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AE%8B%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AE%8B%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AE%8B%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AE%8B%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AE%8B%E7%9F%A9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 車済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BB%8A%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BB%8A%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BB%8A%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BB%8A%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BB%8A%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n51/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 丁穆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%81%E7%A9%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%81%E7%A9%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%81%E7%A9%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%81%E7%A9%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%81%E7%A9%86)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n53/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 辛恭靖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E6%81%AD%E9%9D%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E6%81%AD%E9%9D%96)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n53/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 羅企生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E4%BC%81%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E4%BC%81%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E4%BC%81%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E4%BC%81%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E4%BC%81%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n55/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)

#### 張禕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%A6%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%A6%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%A6%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%A6%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%A6%95)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n57/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m089.html)


### 巻90 列伝第60 良吏
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n61/mode/2up)
  ……途中のページが欠けている。

#### 魯芝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%AF%E8%8A%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%AF%E8%8A%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%AF%E8%8A%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%AF%E8%8A%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%AF%E8%8A%9D)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n63/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 胡威
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E5%A8%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E5%A8%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E5%A8%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E5%A8%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E5%A8%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n67/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 胡奕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%83%A1%E5%A5%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%83%A1%E5%A5%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%83%A1%E5%A5%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%83%A1%E5%A5%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%83%A1%E5%A5%95)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜軫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E8%BB%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E8%BB%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E8%BB%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E8%BB%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E8%BB%AB)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜毗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E6%AF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E6%AF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E6%AF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E6%AF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E6%AF%97)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E7%A7%80)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜烈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E7%83%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E7%83%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E7%83%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E7%83%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E7%83%88)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 杜良
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E8%89%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E8%89%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E8%89%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E8%89%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E8%89%AF)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 竇允
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%AB%87%E5%85%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%AB%87%E5%85%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%AB%87%E5%85%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%AB%87%E5%85%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%AB%87%E5%85%81)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n71/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 王宏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%AE%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%AE%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%AE%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%AE%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%AE%8F)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n71/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 曹攄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9B%B9%E6%94%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9B%B9%E6%94%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9B%B9%E6%94%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9B%B9%E6%94%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9B%B9%E6%94%84)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n73/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc1)
  （@晋書簡訳所）

#### 潘京
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%BD%98%E4%BA%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%BD%98%E4%BA%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%BD%98%E4%BA%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%BD%98%E4%BA%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%BD%98%E4%BA%AC)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n77/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc2)
  （@晋書簡訳所）

#### 范晷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E6%99%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E6%99%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E6%99%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E6%99%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E6%99%B7)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n79/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc3)
  （@晋書簡訳所）

#### 范広
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%BA%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%BA%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%BA%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%BA%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%BA%83)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n79/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc3)
  （@晋書簡訳所）

#### 范稚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E7%A8%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E7%A8%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E7%A8%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E7%A8%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E7%A8%9A)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n81/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc3)
  （@晋書簡訳所）

#### 丁紹
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%81%E7%B4%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%81%E7%B4%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%81%E7%B4%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%81%E7%B4%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%81%E7%B4%B9)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n81/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry5.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [ブログ]
  [巻九十 列伝第六十 良吏(3)曹攄 潘京 范晷 丁紹](https://readingnotesofjinshu.com/translation/biographies/vol-90_3#toc4)
  （@晋書簡訳所）

#### 喬智明
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%96%AC%E6%99%BA%E6%98%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%96%AC%E6%99%BA%E6%98%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%96%AC%E6%99%BA%E6%98%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%96%AC%E6%99%BA%E6%98%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%96%AC%E6%99%BA%E6%98%8E)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n83/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)

#### 鄧攸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%A7%E6%94%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%A7%E6%94%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%A7%E6%94%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%A7%E6%94%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%A7%E6%94%B8)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n83/mode/2up)
* [ブログ]
  [『晋書』列６０「良吏」](http://3guozhi.net/n/ry6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/393)）に「鄧攸之緝熙萌庶」とある。

#### 呉隠之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%89%E9%9A%A0%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%89%E9%9A%A0%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(五十九)](https://archive.org/details/02077776.cn/page/n91/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m090.html)
* [周曇の「詠史詩」の中の「吳隱之」と「再吟」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E5%90%B3%E9%9A%B1%E4%B9%8B)

### 巻91 列伝第61 儒林
* [書誌のみ]
  [晋書儒林伝研究(訳注編)](https://cir.nii.ac.jp/all?q=%E6%99%8B%E6%9B%B8%E5%84%92%E6%9E%97%E4%BC%9D%E7%A0%94%E7%A9%B6) 1〜4
  （石黒 宣俊・野村 茂夫・中鉢 雅量・安本 博・宇野 茂彦・塘 耕次）
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n2/mode/2up)

#### 范平
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%B9%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%B9%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%B9%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%B9%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%B9%B3)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n4/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 文立
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%96%87%E7%AB%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%96%87%E7%AB%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%96%87%E7%AB%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%96%87%E7%AB%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%96%87%E7%AB%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陳邵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E9%82%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E9%82%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E9%82%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E9%82%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E9%82%B5)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n8/mode/2up)

#### 虞喜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E5%96%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E5%96%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E5%96%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E5%96%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E5%96%9C)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n8/mode/2up)

#### 劉兆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%85%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%85%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%85%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%85%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%85%86)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n12/mode/2up)

#### 氾毓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B0%BE%E6%AF%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B0%BE%E6%AF%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B0%BE%E6%AF%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B0%BE%E6%AF%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B0%BE%E6%AF%93)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n14/mode/2up)
* [『文選』所収の任昉の「奏彈劉整」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B740#%E5%A5%8F%E5%BD%88%E5%8A%89%E6%95%B4)に「氾毓字孤，家無常子。」とある（[『国訳漢文大成　文学部第四卷　文選下卷』](https://dl.ndl.go.jp/pid/1912835/1/33)も参照）。

#### 徐苗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E8%8B%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E8%8B%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E8%8B%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E8%8B%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E8%8B%97)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n16/mode/2up)

#### 崔遊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B4%94%E9%81%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B4%94%E9%81%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B4%94%E9%81%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B4%94%E9%81%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B4%94%E9%81%8A)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n18/mode/2up)

#### 范隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E9%9A%86)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n18/mode/2up)

#### 杜夷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E5%A4%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E5%A4%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E5%A4%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E5%A4%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E5%A4%B7)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n20/mode/2up)

#### 董景道
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%A3%E6%99%AF%E9%81%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%A3%E6%99%AF%E9%81%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%A3%E6%99%AF%E9%81%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%A3%E6%99%AF%E9%81%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%A3%E6%99%AF%E9%81%93)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n24/mode/2up)

#### 続咸
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B6%9A%E5%92%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B6%9A%E5%92%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B6%9A%E5%92%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B6%9A%E5%92%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B6%9A%E5%92%B8)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n26/mode/2up)

#### 徐邈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E9%82%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E9%82%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E9%82%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E9%82%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E9%82%88)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n26/mode/2up)

#### 孔衍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%94%E8%A1%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%94%E8%A1%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%94%E8%A1%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%94%E8%A1%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%94%E8%A1%8D)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n34/mode/2up)

#### 范宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n36/mode/2up)

#### 韋謏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%8B%E8%AC%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%8B%E8%AC%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%8B%E8%AC%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%8B%E8%AC%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%8B%E8%AC%8F)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n40/mode/2up)

#### 范弘之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%BC%98%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%BC%98%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n42/mode/2up)

#### 王歓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%AD%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%AD%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%AD%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%AD%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%AD%93)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n52/mode/2up)


### 巻92 列伝第62 文苑
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n54/mode/2up)

#### 応貞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BF%9C%E8%B2%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BF%9C%E8%B2%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BF%9C%E8%B2%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BF%9C%E8%B2%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BF%9C%E8%B2%9E)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n56/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、応貞伝に引かれた
  [「晉武帝華林園集詩」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/43)がある。

#### 成公綏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%90%E5%85%AC%E7%B6%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%90%E5%85%AC%E7%B6%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%90%E5%85%AC%E7%B6%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%90%E5%85%AC%E7%B6%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%90%E5%85%AC%E7%B6%8F)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n58/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、成公綏伝に引かれた
  [「嘯賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/337)がある。

#### 左思
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B7%A6%E6%80%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B7%A6%E6%80%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B7%A6%E6%80%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B7%A6%E6%80%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B7%A6%E6%80%9D)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n68/mode/2up)
* [PDFあり]
  [六朝文人傳:左思(『晋書』)](https://dl.ndl.go.jp/info:ndljp/pid/10504535)
  （佐藤 利行）
* [書籍 (NDL)]
  『国訳漢文大成　文学部第二巻　文選上巻』には、左思伝で言及している
  [「三都賦」](https://dl.ndl.go.jp/info:ndljp/pid/1912763/80)がある。

#### 趙至
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B6%99%E8%87%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B6%99%E8%87%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B6%99%E8%87%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B6%99%E8%87%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B6%99%E8%87%B3)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n76/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、趙至伝に引かれた
  [「與嵇茂齊書」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/109)がある。

#### 鄒湛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%92%E6%B9%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%92%E6%B9%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%92%E6%B9%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%92%E6%B9%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%92%E6%B9%9B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n82/mode/2up)

#### 棗拠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A3%97%E6%8B%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A3%97%E6%8B%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A3%97%E6%8B%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A3%97%E6%8B%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A3%97%E6%8B%A0)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n84/mode/2up)

#### 褚陶
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E9%99%B6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E9%99%B6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E9%99%B6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E9%99%B6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E9%99%B6)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n86/mode/2up)

#### 王沈
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B2%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B2%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B2%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B2%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B2%88)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n86/mode/2up)

#### 張翰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E7%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E7%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E7%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E7%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E7%BF%B0)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n92/mode/2up)
* [『文選』所収の「雜詩」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B729#%E9%9B%9C%E8%A9%A9_10)
  （[『国訳漢文大成 文学部第三巻』](https://dl.ndl.go.jp/info:ndljp/pid/1912796/259)）
* [姚承緒『吳趨訪古錄』の「三高祠」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/6#%E4%B8%89%E9%AB%98%E7%A5%A0)
* [姚承緒『吳趨訪古錄』の「張翰墓」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/6#%E5%BC%B5%E7%BF%B0%E5%A2%93)
* [李白「送張舍人之江東」](https://zh.wikisource.org/wiki/%E9%80%81%E5%BC%B5%E8%88%8D%E4%BA%BA%E4%B9%8B%E6%B1%9F%E6%9D%B1)
  （[『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/4)）
* [李白「行路難」三](https://zh.wikisource.org/wiki/%E8%A1%8C%E8%B7%AF%E9%9B%A3_(%E6%9C%89%E8%80%B3%E8%8E%AB%E6%B4%97%E6%BD%81%E5%B7%9D%E6%B0%B4))
  （[『続国訳漢文大成 文学部第2冊（李太白詩集 上の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139917/43)）
* [李白「金陵送張十一再遊東吳」](https://zh.wikisource.org/wiki/%E9%87%91%E9%99%B5%E9%80%81%E5%BC%B5%E5%8D%81%E4%B8%80%E5%86%8D%E9%81%8A%E6%9D%B1%E5%90%B3)
  （[『続国訳漢文大成 文学部第8冊（李太白詩集 中の四）』](https://dl.ndl.go.jp/info:ndljp/pid/1139976/74)）

#### 庾闡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E9%97%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E9%97%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E9%97%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E9%97%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E9%97%A1)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n94/mode/2up)

#### 曹毗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9B%B9%E6%AF%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9B%B9%E6%AF%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9B%B9%E6%AF%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9B%B9%E6%AF%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9B%B9%E6%AF%97)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n100/mode/2up)
* [PDFあり]
  [魯迅『古小説鈎沈』校本](http://hdl.handle.net/2433/227151)
  （中嶋 長文・伊藤 令子・ 平田 昌司）
  ……p. 586〜 に曹毗の「志怪」あり。

#### 李充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n106/mode/2up)
* [書誌のみ]
  [『晋書』巻九二李充伝訳注](https://cir.nii.ac.jp/crid/1572543026664686464)
  （長谷川 滋成）
* [『文選』所収の任昉の「王文憲集序」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B746#%E7%8E%8B%E6%96%87%E6%86%B2%E9%9B%86%E5%BA%8F)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/189)）に「刊弘度之四部」とある。「弘度」は李充の字。

#### 袁宏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A2%81%E5%AE%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A2%81%E5%AE%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A2%81%E5%AE%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A2%81%E5%AE%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A2%81%E5%AE%8F)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n112/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、袁宏伝に引かれた
  [「三國名臣序贊」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/211)がある。
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「袁宏泊渚」（第112句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/80)
* 文天祥の[「五色賦記」](https://zh.wikisource.org/wiki/%E6%96%87%E5%B1%B1%E5%85%88%E7%94%9F%E6%96%87%E9%9B%86/%E5%8D%B709#%E4%BA%94%E8%89%B2%E8%B3%A6%E8%A8%98)に「一客又曰：「『夜登庾亮之樓，月明千里』如何對？」或對曰：「秋泊袁宏之渚，水浸一天。」」という会話がある。
* [李白「夜泊牛渚懷古」](https://zh.wikisource.org/wiki/%E5%A4%9C%E6%B3%8A%E7%89%9B%E6%B8%9A%E6%87%B7%E5%8F%A4)
  （[『続国訳漢文大成 文学部第10冊（李太白詩集 下の二）』](https://dl.ndl.go.jp/info:ndljp/pid/1139993/88)）
* [崔塗「牛渚夜泊」](https://zh.wikisource.org/wiki/%E7%89%9B%E6%B8%9A%E5%A4%9C%E6%B3%8A)

#### 伏滔
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BC%8F%E6%BB%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BC%8F%E6%BB%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BC%8F%E6%BB%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BC%8F%E6%BB%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BC%8F%E6%BB%94)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n130/mode/2up)

#### 羅含
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%85%E5%90%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%85%E5%90%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%85%E5%90%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%85%E5%90%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%85%E5%90%AB)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n140/mode/2up)

#### 顧愷之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A1%A7%E6%84%B7%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A1%A7%E6%84%B7%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n142/mode/2up)

#### 郭澄之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E6%BE%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E6%BE%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十)](https://archive.org/details/02077777.cn/page/n148/mode/2up)


### 巻93 列伝第63 外戚
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n2/mode/2up)

#### 羊琇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%90%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%90%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%90%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%90%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%90%87)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n4/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)
* [『文選』所収の沈約の「齊故安陸昭王碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%BD%8A%E6%95%85%E5%AE%89%E9%99%B8%E6%98%AD%E7%8E%8B%E7%A2%91%E6%96%87)（[国訳漢文大成 第四卷](https://dl.ndl.go.jp/pid/1912835/1/394)）に「羊琇願言而匪獲」とある。

#### 王恂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%81%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%81%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%81%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%81%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%81%82)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n8/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王愷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%84%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%84%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%84%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%84%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%84%B7)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n10/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 楊文宗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E6%96%87%E5%AE%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E6%96%87%E5%AE%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E6%96%87%E5%AE%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E6%96%87%E5%AE%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E6%96%87%E5%AE%97)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 羊玄之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E7%8E%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E7%8E%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 虞豫
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E8%B1%AB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E8%B1%AB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E8%B1%AB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E8%B1%AB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E8%B1%AB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 虞胤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E8%83%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E8%83%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E8%83%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E8%83%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E8%83%A4)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n12/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 庾琛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%BE%E7%90%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%BE%E7%90%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%BE%E7%90%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%BE%E7%90%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%BE%E7%90%9B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 杜乂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E4%B9%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E4%B9%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E4%B9%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E4%B9%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E4%B9%82)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk2.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 褚裒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E8%A3%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E8%A3%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E8%A3%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E8%A3%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E8%A3%92)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n16/mode/2up)
* [ブログ]
  [『晋書』列伝６３外戚、を康帝の褚氏まで翻訳](http://3guozhi.net/q/gsk3.html)
  （@いつか書きたい『三国志』）
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「季野陽秋」（第98句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/73)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 何準（何准）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E5%87%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E5%87%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E5%87%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E5%87%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E5%87%86)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)
* [姚承緒『吳趨訪古錄』の「烏夜村」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/4#%E7%83%8F%E5%A4%9C%E6%9D%91)

#### 何澄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E6%BE%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E6%BE%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E6%BE%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E6%BE%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E6%BE%84)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王濛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%BF%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%BF%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%BF%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%BF%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%BF%9B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n26/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王脩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%84%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%84%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%84%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%84%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%84%A9)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王遐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E9%81%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E9%81%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E9%81%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E9%81%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E9%81%90)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 王蘊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E8%98%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E8%98%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E8%98%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E8%98%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E8%98%8A)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n32/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)
* 干宝[『搜神記』巻5](https://ctext.org/wiki.pl?if=gb&chapter=503309#p4)
  （王蘊の子が出てくる）

#### 王爽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%88%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%88%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%88%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%88%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%88%BD)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)

#### 褚爽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A4%9A%E7%88%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A4%9A%E7%88%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A4%9A%E7%88%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A4%9A%E7%88%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A4%9A%E7%88%BD)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m093.html)


### 巻94 列伝第64 隠逸
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n40/mode/2up)

#### 孫登
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E7%99%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E7%99%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E7%99%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E7%99%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E7%99%BB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n40/mode/2up)
* [書誌のみ]
  [「晋書」孫登伝訳注](https://cir.nii.ac.jp/crid/1520009410055687296)
  （中国文学研究会）
* [書籍 (NDL)]
  空海『三教指帰』に[「世は子登に異なり、何ぞ隻枕す可き」](https://dl.ndl.go.jp/info:ndljp/pid/1040599/14)とある
  （[注148](https://dl.ndl.go.jp/info:ndljp/pid/1040599/47)）。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 董京
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%A3%E4%BA%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%A3%E4%BA%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%A3%E4%BA%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%A3%E4%BA%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%A3%E4%BA%AC)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n44/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 夏統
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%8F%E7%B5%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%8F%E7%B5%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%8F%E7%B5%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%8F%E7%B5%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%8F%E7%B5%B1)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n48/mode/2up)
* 楊維楨[「贈杜彥清序」](https://zh.wikisource.org/wiki/%E8%B4%88%E6%9D%9C%E5%BD%A5%E6%B8%85%E5%BA%8F)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 朱沖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9C%B1%E6%B2%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9C%B1%E6%B2%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9C%B1%E6%B2%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9C%B1%E6%B2%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9C%B1%E6%B2%96)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 范粲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E7%B2%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E7%B2%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E7%B2%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E7%B2%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E7%B2%B2)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n58/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 范喬
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8C%83%E5%96%AC)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8C%83%E5%96%AC)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8C%83%E5%96%AC&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8C%83%E5%96%AC)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8C%83%E5%96%AC)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 魯勝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%AF%E5%8B%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%AF%E5%8B%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%AF%E5%8B%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%AF%E5%8B%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%AF%E5%8B%9D)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 董養
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%91%A3%E9%A4%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%91%A3%E9%A4%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%91%A3%E9%A4%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%91%A3%E9%A4%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%91%A3%E9%A4%8A)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 霍原
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9C%8D%E5%8E%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9C%8D%E5%8E%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9C%8D%E5%8E%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9C%8D%E5%8E%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9C%8D%E5%8E%9F)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭琦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%90%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%90%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%90%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%90%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%90%A6)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n72/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 伍朝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BC%8D%E6%9C%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BC%8D%E6%9C%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BC%8D%E6%9C%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BC%8D%E6%9C%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BC%8D%E6%9C%9D)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 魯褒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AD%AF%E8%A4%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AD%AF%E8%A4%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AD%AF%E8%A4%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AD%AF%E8%A4%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AD%AF%E8%A4%92)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 氾騰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B0%BE%E9%A8%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B0%BE%E9%A8%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B0%BE%E9%A8%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B0%BE%E9%A8%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B0%BE%E9%A8%B0)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 任旭
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BB%BB%E6%97%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BB%BB%E6%97%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BB%BB%E6%97%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BB%BB%E6%97%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BB%BB%E6%97%AD)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭文
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n82/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「郭文游山（郭文遊山）」（第111句）の注](http://dl.ndl.go.jp/info:ndljp/pid/958947/79)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)
* 吳筠[「天柱山天柱觀記」](https://zh.wikisource.org/wiki/%E5%A4%A9%E6%9F%B1%E5%B1%B1%E5%A4%A9%E6%9F%B1%E8%A7%80%E8%A8%98)に「於是旁訊有識，稽諸實錄，乃知昔高士郭文舉創隱於茲，以雲林為家，遂長往不複。元和貫於異類，猛獸為之馴擾。《晉書·逸人傳》具紀其事，可略而言。」とある。「文舉」は郭文の字。
* 錢鏐[「天柱觀記」](https://zh.wikisource.org/wiki/%E5%A4%A9%E6%9F%B1%E8%A7%80%E8%A8%98)に「東晉有郭文舉先生，得飛化之道，隱居此山，群虎來柔，史籍具載。」とある。

#### 龔壮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BE%94%E5%A3%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BE%94%E5%A3%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BE%94%E5%A3%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BE%94%E5%A3%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BE%94%E5%A3%AE)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n88/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 孟陋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E9%99%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E9%99%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E9%99%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E9%99%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E9%99%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n90/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 韓績
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E7%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E7%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E7%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E7%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E7%B8%BE)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n92/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 譙秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AD%99%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AD%99%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AD%99%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AD%99%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AD%99%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n94/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、譙秀伝で言及している
  [「薦譙元彥表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/421)（桓温によるもの）がある。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)
* [『三国志』巻42](https://zh.wikisource.org/wiki/%E4%B8%89%E5%9C%8B%E5%BF%97/%E5%8D%B742#%E5%AD%AB_%E8%AD%99%E7%A7%80)

#### 翟湯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BF%9F%E6%B9%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BF%9F%E6%B9%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BF%9F%E6%B9%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BF%9F%E6%B9%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BF%9F%E6%B9%AF)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n96/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 翟荘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BF%9F%E8%8D%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BF%9F%E8%8D%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BF%9F%E8%8D%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BF%9F%E8%8D%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BF%9F%E8%8D%98)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n96/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭翻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%BF%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%BF%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%BF%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%BF%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%BF%BB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n98/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 辛謐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%9B%E8%AC%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%9B%E8%AC%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%9B%E8%AC%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%9B%E8%AC%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%9B%E8%AC%90)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n100/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 劉驎之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E9%A9%8E%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E9%A9%8E%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n102/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 索襲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E8%A5%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E8%A5%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E8%A5%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E8%A5%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E8%A5%B2)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n104/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 楊軻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A5%8A%E8%BB%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A5%8A%E8%BB%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A5%8A%E8%BB%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A5%8A%E8%BB%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A5%8A%E8%BB%BB)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n106/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 公孫鳳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%85%AC%E5%AD%AB%E9%B3%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%85%AC%E5%AD%AB%E9%B3%B3)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n108/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 公孫永
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%85%AC%E5%AD%AB%E6%B0%B8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%85%AC%E5%AD%AB%E6%B0%B8)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 張忠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%BF%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%BF%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%BF%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%BF%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%BF%A0)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 石垣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%9E%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%9E%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%9E%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%9E%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%9E%A3)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n114/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 宋繊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AE%8B%E7%B9%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AE%8B%E7%B9%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AE%8B%E7%B9%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AE%8B%E7%B9%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AE%8B%E7%B9%8A)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n114/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭荷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E8%8D%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E8%8D%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E8%8D%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E8%8D%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E8%8D%B7)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n116/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 郭瑀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E7%91%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E7%91%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E7%91%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E7%91%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E7%91%80)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n116/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 祈嘉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%88%E5%98%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%88%E5%98%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%88%E5%98%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%88%E5%98%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%88%E5%98%89)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n120/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 瞿硎先生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n122/mode/2up)
* [姚承緒『吳趨訪古錄』の「重岡」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E9%87%8D%E5%B2%A1%E3%80%88%E7%94%A8%E5%94%90%E7%91%AA%E3%80%8A%E9%87%8D%E5%B2%A1%E6%98%A5%E6%9C%9B%E3%80%8B%E9%9F%BB%E3%80%89)
* [姚承緒『吳趨訪古錄』の「雙鳳」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E9%9B%99%E9%B3%B3)
* [姚承緒『吳趨訪古錄』の「樂隱園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E6%A8%82%E9%9A%B1%E5%9C%92)
* [姚承緒『吳趨訪古錄』の「瞿硎先生墓」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E7%9E%BF%E7%A1%8E%E5%85%88%E7%94%9F%E5%A2%93)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 謝敷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AC%9D%E6%95%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AC%9D%E6%95%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AC%9D%E6%95%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AC%9D%E6%95%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AC%9D%E6%95%B7)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n122/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 戴逵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E9%80%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E9%80%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E9%80%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E9%80%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E9%80%B5)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n124/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 龔玄之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BE%94%E7%8E%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BE%94%E7%8E%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n130/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 陶淡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%B7%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%B7%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%B7%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%B7%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%B7%A1)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n132/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)

#### 陶潜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E6%BD%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E6%BD%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E6%BD%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E6%BD%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E6%BD%9C)
* [書籍 (IA)]
  [晉書斠注(六十一)](https://archive.org/details/02077778.cn/page/n132/mode/2up)
* [『宋書』巻93](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B793#%E9%99%B6%E6%BD%9B)
* [『南史』巻75](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B775#%E9%99%B6%E6%BD%9B)
* [PDFあり]
  [六朝文人傳 陶潛傳(『宋書』巻九三)](https://dl.ndl.go.jp/info:ndljp/pid/10504835)
  (森野 繁夫)
  ……『晋書』巻94の陶潜伝の訳注ではないが、参考までに。
* [書籍 (NDL)]
  『国訳漢文大成　文学部第四巻　文選下巻』には、陶潜伝に引かれた
  [「歸去來」](https://dl.ndl.go.jp/info:ndljp/pid/1912835/157)がある。
* [胡曾の「武陵溪」](https://zh.wikisource.org/wiki/%E6%AD%A6%E9%99%B5%E6%BA%AA)
  ……[陶潜の「桃花源記」](https://zh.wikisource.org/wiki/%E6%A1%83%E8%8A%B1%E6%BA%90%E8%A8%98)を踏まえていると思われる。
* [姚承緒『吳趨訪古錄』の「趙孟俯石刻」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E8%B6%99%E5%AD%9F%E4%BF%AF%E7%9F%B3%E5%88%BB)
  ……「歸去來辭」に関連のある詩。
* [姚承緒『吳趨訪古錄』の「五柳園」](https://zh.wikisource.org/wiki/%E5%90%B3%E8%B6%A8%E8%A8%AA%E5%8F%A4%E9%8C%84/8#%E4%BA%94%E6%9F%B3%E5%9C%92)
* [ブログ]
  [藤澤東畡「陶靖節」七言絶句](https://www.kansai-u.ac.jp/hakuen/culture/culture_archive/post_34.html)
  （@幕末・明治・大正・昭和の大阪最大の私塾　泊園書院）
* [汪遵の「隋柳」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7602#%E9%9A%8B%E6%9F%B3)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m094.html)
* [ブログ]
  [陶潜１　　五柳先生伝](https://kakuyomu.jp/works/1177354054895306127/episodes/1177354055447832273)〜
  （@ゆるふわ晋宋春秋３）
  ……主に『宋書』を追ったもの。
* [李攀龍「白雪樓」](https://zh.wikisource.org/wiki/%E6%BB%84%E6%BA%9F%E9%9B%86_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B709#%E7%99%BD%E9%9B%AA%E6%A8%93)に「可知陶令賦歸來」とある。
* 『關帝靈籤』第三十九籤に「陶淵明賞菊」とある。
    * [テキスト版](https://zh.wikisource.org/wiki/%E9%97%9C%E8%81%96%E5%B8%9D%E5%90%9B%E9%9D%88%E7%B1%A4/39)
    * [画像版](https://zh.wikisource.org/w/index.php?title=File%3AHarvard_drs_53239458_%E9%97%9C%E5%B8%9D%E9%9D%88%E7%B1%A4.pdf&page=41)


### 巻95 列伝第65 芸術
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n2/mode/2up)

#### 陳訓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E8%A8%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E8%A8%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E8%A8%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E8%A8%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E8%A8%93)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 戴洋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%88%B4%E6%B4%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%88%B4%E6%B4%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%88%B4%E6%B4%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%88%B4%E6%B4%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%88%B4%E6%B4%8B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 韓友
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E5%8F%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E5%8F%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E5%8F%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E5%8F%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E5%8F%8B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n22/mode/2up)

#### 淳于智
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B7%B3%E4%BA%8E%E6%99%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B7%B3%E4%BA%8E%E6%99%BA)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n26/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 歩熊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AD%A9%E7%86%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AD%A9%E7%86%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AD%A9%E7%86%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AD%A9%E7%86%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AD%A9%E7%86%8A)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 杜不愆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E4%B8%8D%E6%84%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E4%B8%8D%E6%84%86)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [『搜神後記』巻2「杜不愆」](https://zh.wikisource.org/wiki/%E6%90%9C%E7%A5%9E%E5%BE%8C%E8%A8%98/02#%E6%9D%9C%E4%B8%8D%E6%84%86)
  （[『国訳漢文大成』第十二巻](https://dl.ndl.go.jp/info:ndljp/pid/1913008/87)に訓読あり）

#### 厳卿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8E%B3%E5%8D%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8E%B3%E5%8D%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8E%B3%E5%8D%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8E%B3%E5%8D%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8E%B3%E5%8D%BF)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 隗炤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9A%97%E7%82%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9A%97%E7%82%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9A%97%E7%82%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9A%97%E7%82%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9A%97%E7%82%A4)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 卜珝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9C%E7%8F%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9C%E7%8F%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9C%E7%8F%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9C%E7%8F%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9C%E7%8F%9D)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n34/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 鮑靚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AE%91%E9%9D%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AE%91%E9%9D%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AE%91%E9%9D%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AE%91%E9%9D%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AE%91%E9%9D%9A)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n36/mode/2up)
* [書籍 (NDL)]
  『蒙求』（有朋堂書店）の中の
  [「鮑靚記井」（第53句）の注](https://dl.ndl.go.jp/info:ndljp/pid/958947/48)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 呉猛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%89%E7%8C%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%89%E7%8C%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%89%E7%8C%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%89%E7%8C%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%89%E7%8C%9B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 幸霊
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B9%B8%E9%9C%8A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B9%B8%E9%9C%8A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B9%B8%E9%9C%8A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B9%B8%E9%9C%8A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B9%B8%E9%9C%8A)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n44/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 仏図澄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BB%8F%E5%9B%B3%E6%BE%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BB%8F%E5%9B%B3%E6%BE%84)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n48/mode/2up)
* [『高僧伝』巻9](https://cbetaonline.dila.edu.tw/zh/T2059_009)
* [『文選』所収の王巾「頭陀寺碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%A0%AD%E9%99%80%E5%AF%BA%E7%A2%91%E6%96%87)にある「澄什結轍於山西」（『国訳漢文大成　文学部第四巻　文選下巻』の[該当ページ](https://dl.ndl.go.jp/info:ndljp/pid/1912835/388)）の「澄」は仏図澄を指す。
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 麻襦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BA%BB%E8%A5%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BA%BB%E8%A5%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BA%BB%E8%A5%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BA%BB%E8%A5%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BA%BB%E8%A5%A6)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n66/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 単道開
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%98%E9%81%93%E9%96%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%98%E9%81%93%E9%96%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%98%E9%81%93%E9%96%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%98%E9%81%93%E9%96%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%98%E9%81%93%E9%96%8B)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n68/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [『高僧伝』巻9](https://cbetaonline.dila.edu.tw/zh/T2059_009)

#### 黄泓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%BB%84%E6%B3%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%BB%84%E6%B3%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%BB%84%E6%B3%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%BB%84%E6%B3%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%BB%84%E6%B3%93)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n72/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 索紞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E7%B4%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E7%B4%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E7%B4%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E7%B4%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E7%B4%9E)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n74/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 孟欽
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E6%AC%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E6%AC%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E6%AC%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E6%AC%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E6%AC%BD)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n78/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王嘉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%98%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%98%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%98%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%98%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%98%89)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n78/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 僧渉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%83%A7%E6%B8%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%83%A7%E6%B8%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%83%A7%E6%B8%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%83%A7%E6%B8%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%83%A7%E6%B8%89)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n82/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 郭黁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%83%AD%E9%BB%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%83%AD%E9%BB%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%83%AD%E9%BB%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%83%AD%E9%BB%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%83%AD%E9%BB%81)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n84/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 鳩摩羅什
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%B3%A9%E6%91%A9%E7%BE%85%E4%BB%80)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n86/mode/2up)
* [『高僧伝』巻2](https://cbetaonline.dila.edu.tw/zh/T2059_002)
* 『欽定古今圖書集成』（方輿彙編　職方典　第511巻）の「[淨土樹](https://zh.wikisource.org/wiki/%E6%AC%BD%E5%AE%9A%E5%8F%A4%E4%BB%8A%E5%9C%96%E6%9B%B8%E9%9B%86%E6%88%90/%E6%96%B9%E8%BC%BF%E5%BD%99%E7%B7%A8/%E8%81%B7%E6%96%B9%E5%85%B8/%E7%AC%AC0511%E5%8D%B7)」
* [『佛說阿彌陀經註釋會要』](https://cbetaonline.dila.edu.tw/zh/B0024_001)
* [『海八德經』](https://cbetaonline.dila.edu.tw/zh/T0035_001)
* [『佛說放牛經』](https://cbetaonline.dila.edu.tw/zh/T0123_001)
* [『大莊嚴論經』](https://cbetaonline.dila.edu.tw/zh/T0201_001)
* [『眾經撰雜譬喻』](https://cbetaonline.dila.edu.tw/zh/T0208_001)
* [『摩訶般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0223_001)
* [『小品般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0227_001)
* [『金剛般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0235_001)
* [『佛說仁王般若波羅蜜經』](https://cbetaonline.dila.edu.tw/zh/T0245_001)
* [『摩訶般若波羅蜜大明呪經』](https://cbetaonline.dila.edu.tw/zh/T0250_001)
* [『妙法蓮華經』](https://cbetaonline.dila.edu.tw/zh/T0262_001)
* [『十住經』](https://cbetaonline.dila.edu.tw/zh/T0286_001)
* [『佛說莊嚴菩提心經』](https://cbetaonline.dila.edu.tw/zh/T0307_001)
* [『佛說須摩提菩薩經』](https://cbetaonline.dila.edu.tw/zh/T0335_001)
* [『佛說阿彌陀經』](https://cbetaonline.dila.edu.tw/zh/T0366_001)
* [『集一切福德三昧經』](https://cbetaonline.dila.edu.tw/zh/T0382_001)
* [『佛垂般涅槃略說教誡經』](https://cbetaonline.dila.edu.tw/zh/T0389_001)
* [『自在王菩薩經』](https://cbetaonline.dila.edu.tw/zh/T0420_001)
* [『佛說千佛因緣經』](https://cbetaonline.dila.edu.tw/zh/T0426_001)
* [『佛說彌勒下生成佛經』](https://cbetaonline.dila.edu.tw/zh/T0454_001)
* [『佛說彌勒大成佛經』](https://cbetaonline.dila.edu.tw/zh/T0456_001)
* [『文殊師利問菩提經』](https://cbetaonline.dila.edu.tw/zh/T0464_001)
* [『維摩詰所說經』](https://cbetaonline.dila.edu.tw/zh/T0475_001)
* [『持世經』](https://cbetaonline.dila.edu.tw/zh/T0482_001)
* [『不思議光菩薩所說經』](https://cbetaonline.dila.edu.tw/zh/T0484_001)
* [『思益梵天所問經』](https://cbetaonline.dila.edu.tw/zh/T0586_001)
* [『禪祕要法經』](https://cbetaonline.dila.edu.tw/zh/T0613_001)
* [『坐禪三昧經』](https://cbetaonline.dila.edu.tw/zh/T0614_001)
* [『菩薩訶色欲法經』](https://cbetaonline.dila.edu.tw/zh/T0615_001)
* [『禪法要解』](https://cbetaonline.dila.edu.tw/zh/T0616_001)
* [『思惟略要法』](https://cbetaonline.dila.edu.tw/zh/T0617_001)
* [『大樹緊那羅王所問經』](https://cbetaonline.dila.edu.tw/zh/T0625_001)
* [『佛說首楞嚴三昧經』](https://cbetaonline.dila.edu.tw/zh/T0642_001)
* [『諸法無行經』](https://cbetaonline.dila.edu.tw/zh/T0650_001)
* [『佛藏經』](https://cbetaonline.dila.edu.tw/zh/T0653_001)
* [『佛說華手經』](https://cbetaonline.dila.edu.tw/zh/T0657_001)
* [『燈指因緣經』](https://cbetaonline.dila.edu.tw/zh/T0703_001)
* [『孔雀王呪經』](https://cbetaonline.dila.edu.tw/zh/T0988_001)
* [『十誦律』](https://cbetaonline.dila.edu.tw/zh/T1435_001)
* [『十誦比丘波羅提木叉戒本』](https://cbetaonline.dila.edu.tw/zh/T1436_001)
* [『梵網經』](https://cbetaonline.dila.edu.tw/zh/T1484_001)
* [『清淨毘尼方廣經』](https://cbetaonline.dila.edu.tw/zh/T1489_001)
* [『大智度論』](https://cbetaonline.dila.edu.tw/zh/T1509_001)
* [『十住毘婆沙論』](https://cbetaonline.dila.edu.tw/zh/T1521_001)
* [『中論』](https://cbetaonline.dila.edu.tw/zh/T1564_001)
* [『十二門論』](https://cbetaonline.dila.edu.tw/zh/T1568_001)
* [『百論』](https://cbetaonline.dila.edu.tw/zh/T1569_001)
* [『成實論』](https://cbetaonline.dila.edu.tw/zh/T1646_001)
* [『發菩提心經論』](https://cbetaonline.dila.edu.tw/zh/T1659_001)
* [『鳩摩羅什法師大義』](https://cbetaonline.dila.edu.tw/zh/T1856_001)
* [『馬鳴菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2046_001)
* [『龍樹菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2047a_001)
* [『龍樹菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2047b_001)
* [『提婆菩薩傳』](https://cbetaonline.dila.edu.tw/zh/T2048_001)
* [『文選』所収の王巾「頭陀寺碑文」](https://zh.wikisource.org/wiki/%E6%98%AD%E6%98%8E%E6%96%87%E9%81%B8/%E5%8D%B759#%E9%A0%AD%E9%99%80%E5%AF%BA%E7%A2%91%E6%96%87)にある「澄什結轍於山西」（『国訳漢文大成　文学部第四巻　文選下巻』の[該当ページ](https://dl.ndl.go.jp/info:ndljp/pid/1912835/388)）の「什」は鳩摩羅什を指す。
* [ブログ]
  [鳩摩羅什１　懐胎](https://kakuyomu.jp/works/1177354055365070928/episodes/16816452219208220039)〜
  （@ゆるふわ中原戦記２）
* 『今昔物語集』の[「鳩摩羅焔奉盗仏伝震旦語」](http://yatanavi.org/text/k_konjaku/k_konjaku6-5)
  （リンク先のテキストデータは[やたがらすナビ](http://yatanavi.org/index.html)）

#### 僧曇霍
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%83%A7%E6%9B%87%E9%9C%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%83%A7%E6%9B%87%E9%9C%8D)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n102/mode/2up)
* [『高僧伝』巻10](https://cbetaonline.dila.edu.tw/zh/T2059_010)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 臺産（台産）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8F%B0%E7%94%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8F%B0%E7%94%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8F%B0%E7%94%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8F%B0%E7%94%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8F%B0%E7%94%A3)
* [書籍 (IA)]
  [晉書斠注(六十二)](https://archive.org/details/02077779.cn/page/n104/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)


### 巻96 列伝第66 列女
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n2/mode/2up)

#### 羊耽妻辛憲英
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%BE%8A%E8%80%BD%E5%A6%BB%E8%BE%9B%E6%86%B2%E8%8B%B1)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n4/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 杜有道妻厳憲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E6%9C%89%E9%81%93%E5%A6%BB%E5%8E%B3%E6%86%B2)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n6/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王渾妻鍾琰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%B8%BE%E5%A6%BB%E9%8D%BE%E7%90%B0)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n8/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 鄭袤妻曹氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E8%A2%A4%E5%A6%BB%E6%9B%B9%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n10/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 愍懐太子妃王恵風
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%84%8D%E6%87%90%E5%A4%AA%E5%AD%90%E5%A6%83%E7%8E%8B%E6%81%B5%E9%A2%A8)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n12/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「愍懐太子の妃」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/219)
* [『太平御覧』巻664](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%BD/0664)
  （[四庫全書本](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%BD_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B70664)）に引く『南嶽魏夫人内傳』や
  陶弘景の[『真誥』](https://zh.wikisource.org/wiki/%E7%9C%9F%E8%AA%A5/%E5%8D%B7013)（[「眞誥研究 : 譯注篇」](http://hdl.handle.net/2433/98008)（吉川忠夫・麥谷邦夫）の474ページ）では、通りすがりの女仙に助けられる。

#### 鄭休妻石氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%84%AD%E4%BC%91%E5%A6%BB%E7%9F%B3%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n14/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陶侃母湛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B6%E4%BE%83%E6%AF%8D%E6%B9%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n14/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「陶侃の母」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/219)

#### 賈渾妻宗氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B3%88%E6%B8%BE%E5%A6%BB%E5%AE%97%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n16/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「賈渾の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/221)

#### 梁緯妻辛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A2%81%E7%B7%AF%E5%A6%BB%E8%BE%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n16/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「梁緯の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/220)

#### 許延妻杜氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A8%B1%E5%BB%B6%E5%A6%BB%E6%9D%9C%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「許延の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/222)

#### 虞潭母孫氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%99%9E%E6%BD%AD%E6%AF%8D%E5%AD%AB%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n18/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「虞潭が母」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/220)

#### 周顗母李絡秀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%A8%E9%A1%97%E6%AF%8D%E6%9D%8E%E7%B5%A1%E7%A7%80)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n20/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 張茂妻陸氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%8C%82%E5%A6%BB%E9%99%B8%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n22/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 尹虞二女
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B0%B9%E8%99%9E%E4%BA%8C%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n22/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 荀崧小女灌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8D%80%E5%B4%A7%E5%B0%8F%E5%A5%B3%E7%81%8C)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n24/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王凝之妻謝道韞
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%87%9D%E4%B9%8B%E5%A6%BB%E8%AC%9D%E9%81%93%E9%9F%9E)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n24/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* 蘇軾「[題王逸少帖](https://zh.wikisource.org/wiki/%E9%A1%8C%E7%8E%8B%E9%80%B8%E5%B0%91%E5%B8%96)」
  （[『国訳漢文大成　続　文学部』第61冊](https://dl.ndl.go.jp/info:ndljp/pid/1140559/34)）の「謝家夫人淡豐容，蕭然自有林下風。」の部分。
* [王士禎『池北偶談』巻15](https://zh.wikisource.org/wiki/%E6%B1%A0%E5%8C%97%E5%81%B6%E8%AB%87/%E5%8D%B7%E5%8D%81%E4%BA%94)に「謝道韞硯」という話がある。

#### 劉臻妻陳氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%87%BB%E5%A6%BB%E9%99%B3%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n28/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 皮京妻龍憐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%AE%E4%BA%AC%E5%A6%BB%E9%BE%8D%E6%86%90)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n28/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 孟昶妻周氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E6%98%B6%E5%A6%BB%E5%91%A8%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n30/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 何無忌母劉氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BD%95%E7%84%A1%E5%BF%8C%E6%AF%8D%E5%8A%89%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 劉聡妻劉娥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%81%A1%E5%A6%BB%E5%8A%89%E5%A8%A5)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n32/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 王広女
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BA%83%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BA%83%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n34/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 陝婦人
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%9D%E5%A9%A6%E4%BA%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%9D%E5%A9%A6%E4%BA%BA)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n36/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 靳康女
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9D%B3%E5%BA%B7%E5%A5%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9D%B3%E5%BA%B7%E5%A5%B3)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n36/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 韋逞母宋氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%8B%E9%80%9E%E6%AF%8D%E5%AE%8B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n36/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「韋が母宋氏」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/224)

#### 張天錫妾閻氏・薛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E5%A4%A9%E9%8C%AB%E5%A6%BE%E9%96%BB%E6%B0%8F%E3%83%BB%E8%96%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n38/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「張天錫が妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/222)

#### 苻堅妾張氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E5%A0%85%E5%A6%BE%E5%BC%B5%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 竇滔妻蘇氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%AB%87%E6%BB%94%E5%A6%BB%E8%98%87%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n40/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [顏希源『百美新詠』「蘇蕙」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E8%98%87%E8%95%99)

#### 苻登妻毛氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E7%99%BB%E5%A6%BB%E6%AF%9B%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n42/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「苻登の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/223)
* [ブログ]
  [晋書　苻登・毛氏列伝](http://blog.livedoor.jp/oboro0083/archives/2498254.html)
  （@★香華酔月☆）

#### 慕容垂妻段元妃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%9E%82%E5%A6%BB%E6%AE%B5%E5%85%83%E5%A6%83)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n42/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [段元妃１　後段后](https://kakuyomu.jp/works/1177354055365070928/episodes/16816700428295830475)〜
  （@ゆるふわ中原戦記２）

#### 段豊妻慕容氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B5%E8%B1%8A%E5%A6%BB%E6%85%95%E5%AE%B9%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)

#### 呂纂妻楊氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E7%BA%82%E5%A6%BB%E6%A5%8A%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「呂纂の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/223)

#### 呂紹妻張氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E7%B4%B9%E5%A6%BB%E5%BC%B5%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [書籍 (NDL)]
  『漢文叢書 第21 古列女傳・女四書』所収の「新續列女傳」の中の
  [「呂紹の妻」](https://dl.ndl.go.jp/info:ndljp/pid/1118536/224)

#### 涼武昭王李玄盛后尹氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B6%BC%E6%AD%A6%E6%98%AD%E7%8E%8B%E6%9D%8E%E7%8E%84%E7%9B%9B%E5%90%8E%E5%B0%B9%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n48/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)


### 巻97 列伝第67 四夷（東夷・西戎・南蛮・北狄）
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n54/mode/2up)

#### 夫餘国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%AB%E9%A4%98%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%AB%E9%A4%98%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 三韓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B8%89%E9%9F%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B8%89%E9%9F%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B8%89%E9%9F%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B8%89%E9%9F%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B8%89%E9%9F%93)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

##### 馬韓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AC%E9%9F%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AC%E9%9F%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AC%E9%9F%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AC%E9%9F%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AC%E9%9F%93)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n60/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

##### 辰韓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BE%B0%E9%9F%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BE%B0%E9%9F%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BE%B0%E9%9F%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BE%B0%E9%9F%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BE%B0%E9%9F%93)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n64/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 肅慎氏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%82%85%E6%85%8E%E6%B0%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%82%85%E6%85%8E%E6%B0%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%82%85%E6%85%8E%E6%B0%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%82%85%E6%85%8E%E6%B0%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%82%85%E6%85%8E%E6%B0%8F)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n66/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 倭人
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%80%AD%E4%BA%BA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%80%AD%E4%BA%BA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%80%AD%E4%BA%BA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%80%AD%E4%BA%BA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%80%AD%E4%BA%BA)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)
* [『宋書』巻97](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B797#%E6%9D%B1%E5%A4%B7)
* [『南史』巻79](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B779#%E5%80%AD%E5%9C%8B)
* [『北史』巻94](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7094#%E5%80%AD)

#### 裨離
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%A8%E9%9B%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%A8%E9%9B%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%A8%E9%9B%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%A8%E9%9B%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%A8%E9%9B%A2)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n80/mode/2up)
* [ブログ]
  [東夷伝中裨離等十国条](http://strawberrymilk.gooside.com/text/shinjo/97-6.html)
  （@漢籍訳出プロジェクト「漢々學々」）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 吐谷渾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%90%90%E8%B0%B7%E6%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%90%90%E8%B0%B7%E6%B8%BE)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n82/mode/2up)
* [『宋書』巻96](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B796)
* [『南史』巻79](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B779#%E6%B2%B3%E5%8D%97%E5%9C%8B)
* [『魏書』巻101](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7101#%E5%90%90%E8%B0%B7%E6%B8%BE)
* [『北史』巻96](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7096#%E5%90%90%E8%B0%B7%E6%B8%BE)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 焉耆国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%84%89%E8%80%86%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%84%89%E8%80%86%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%84%89%E8%80%86%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%84%89%E8%80%86%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%84%89%E8%80%86%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n96/mode/2up)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E7%84%89%E8%80%86)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E7%84%89%E8%80%86%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 亀茲国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%BA%80%E8%8C%B2%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%BA%80%E8%8C%B2%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n100/mode/2up)
* [『南史』巻79](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B779#%E9%BE%9C%E8%8C%B2%E5%9C%8B)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E9%BE%9C%E8%8C%B2)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E9%BE%9C%E8%8C%B2%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 大宛国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%A7%E5%AE%9B%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%A7%E5%AE%9B%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n100/mode/2up)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 康居国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BA%B7%E5%B1%85%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BA%B7%E5%B1%85%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n102/mode/2up)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E5%9A%88%E5%99%A0)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E5%BA%B7%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 大秦国
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A4%A7%E7%A7%A6%E5%9B%BD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A4%A7%E7%A7%A6%E5%9B%BD)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n104/mode/2up)
* [『魏書』巻102](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B7102#%E5%A4%A7%E7%A7%A6%E5%9C%8B)
* [『北史』巻97](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7097#%E5%A4%A7%E7%A7%A6%E5%9C%8B)
* [PDFあり]
  [南朝正史西戎伝と『魏書』吐谷渾・高昌伝の訳注](http://hdl.handle.net/11173/190)
  （小谷 仲男・菅沼 愛語）……実は『晋書』の西戎伝の訳注も含まれる。
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

#### 林邑
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9E%97%E9%82%91)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9E%97%E9%82%91)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9E%97%E9%82%91&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9E%97%E9%82%91)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9E%97%E9%82%91)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n110/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)
* [『宋書』巻97](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B797#%E5%8D%97%E5%A4%B7)
* [『南史』巻78](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B778#%E6%9E%97%E9%82%91%E5%9C%8B)
* [『北史』巻95](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7095#%E6%9E%97%E9%82%91)

#### 扶南
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%89%B6%E5%8D%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%89%B6%E5%8D%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%89%B6%E5%8D%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%89%B6%E5%8D%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%89%B6%E5%8D%97)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n122/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)
* [『宋書』巻97](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B797#%E5%8D%97%E5%A4%B7)
* [『南史』巻78](https://zh.wikisource.org/wiki/%E5%8D%97%E5%8F%B2/%E5%8D%B778#%E6%89%B6%E5%8D%97%E5%9C%8B)

#### 匈奴
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8C%88%E5%A5%B4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8C%88%E5%A5%B4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8C%88%E5%A5%B4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8C%88%E5%A5%B4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8C%88%E5%A5%B4)
* [書籍 (IA)]
  [晉書斠注(六十三)](https://archive.org/details/02077780.cn/page/n126/mode/2up)
* [ブログ]
  [巻九十七 列伝六十七 四夷(4)北狄](https://readingnotesofjinshu.com/translation/biographies/vol-97_4)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m097.html)

### 巻98 列伝第68
#### 王敦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%95%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%95%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%95%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%95%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%95%A6)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』列伝６８、王敦伝](http://3guozhi.net/h/ot1.html)
  （@いつか書きたい『三国志』）

#### 沈充
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B2%88%E5%85%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B2%88%E5%85%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B2%88%E5%85%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B2%88%E5%85%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B2%88%E5%85%85)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n34/mode/2up)

#### 桓温
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%B8%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%B8%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%B8%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%B8%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%B8%A9)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』桓温伝の翻訳と小考](http://3guozhi.net/q/ke1.html)
  （@いつか書きたい『三国志』）
* 『欽定古今圖書集成』（方輿彙編　職方典　第511巻）の「[桓公堆](https://zh.wikisource.org/wiki/%E6%AC%BD%E5%AE%9A%E5%8F%A4%E4%BB%8A%E5%9C%96%E6%9B%B8%E9%9B%86%E6%88%90/%E6%96%B9%E8%BC%BF%E5%BD%99%E7%B7%A8/%E8%81%B7%E6%96%B9%E5%85%B8/%E7%AC%AC0511%E5%8D%B7)」
* 李白「[桓公井](https://zh.wikisource.org/wiki/%E5%A7%91%E5%AD%B0%E5%8D%81%E8%A9%A0#%E6%A1%93%E5%85%AC%E4%BA%95)」
  （[『国訳漢文大成　続　文学部』第10冊](https://dl.ndl.go.jp/info:ndljp/pid/1139993/92)）
* 王安石「[白紵山](https://zh.wikisource.org/wiki/%E7%99%BD%E7%B4%B5%E5%B1%B1)」
* [ブログ]
  [永和七年（351年） 桓温の武昌に次して止みたる後の上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-109.html)
  （@大司馬府 作戦日誌）
* [ブログ]
  [隆和元年（362年）五月 桓温の洛陽に遷都を請う上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-87.html)
  （@大司馬府 作戦日誌）
* [ブログ]
  [興寧二年（364年）五月 桓温の入参朝政を辞する上疏文](http://tombstonedriver.blog.fc2.com/blog-entry-88.html)
  （@大司馬府 作戦日誌）
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……題材の一つが桓温（最後の「桓大司馬」云々のところ）。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）
* [顏希源『百美新詠』「李勢女」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E6%9D%8E%E5%8B%A2%E5%A5%B3)
  ……[『世説新語』賢媛篇21](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E8%B3%A2%E5%AA%9B)の話が題材。この詩の「老奴」は桓温のこと。

#### 桓熙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%86%99)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓済
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%B8%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%B8%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%B8%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%B8%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%B8%88)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓歆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E6%AD%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E6%AD%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E6%AD%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E6%AD%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E6%AD%86)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓禕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%A6%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%A6%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%A6%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%A6%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%A6%95)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 桓偉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E5%81%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E5%81%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E5%81%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E5%81%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E5%81%89)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n72/mode/2up)

#### 孟嘉
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%9F%E5%98%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%9F%E5%98%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%9F%E5%98%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%9F%E5%98%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%9F%E5%98%89)
* [書籍 (IA)]
  [晉書斠注(六十四)](https://archive.org/details/02077781.cn/page/n74/mode/2up)


### 巻99 列伝第69
#### 桓玄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%A1%93%E7%8E%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%A1%93%E7%8E%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%A1%93%E7%8E%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%A1%93%E7%8E%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%A1%93%E7%8E%84)
* [書籍 (IA)]
  [晉書斠注(六十五)](https://archive.org/details/02077782.cn/page/n2/mode/2up)
* [『魏書』巻97](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B797#%E6%A1%93%E7%8E%84)
* [蘇轍「乞誅竄呂惠卿狀」](https://zh.wikisource.org/wiki/%E6%AC%92%E5%9F%8E%E9%9B%86/38#%E3%80%90%E4%B9%9E%E8%AA%85%E7%AB%84%E5%91%82%E6%83%A0%E5%8D%BF%E7%8B%80%E3%80%88%E5%8D%81%E4%B9%9D%E6%97%A5%E3%80%89%E3%80%91)（[『唐宋八家文講義 4 増訂』](https://dl.ndl.go.jp/pid/1104295/1/50)）……「至於呂布事丁原，則殺丁原，事董卓，則殺董卓。劉牢之事王恭，則反王恭，事司馬元顯，則反元顯。背逆人理，世所共疑。故呂布見誅於曹公，而牢之見殺於桓氏，皆以其平生反復，勢不可存。夫曹、桓，古之奸雄，駕馭英豪，何所不有，然推究利害，終畏此人。」という、劉牢之・王恭・司馬元顕・桓玄への言及がある。

#### 卞範之
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8D%9E%E7%AF%84%E4%B9%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8D%9E%E7%AF%84%E4%B9%8B)
* [書籍 (IA)]
  [晉書斠注(六十五)](https://archive.org/details/02077782.cn/page/n52/mode/2up)

#### 殷仲文
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%AE%B7%E4%BB%B2%E6%96%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%AE%B7%E4%BB%B2%E6%96%87)
* [書籍 (IA)]
  [晉書斠注(六十五)](https://archive.org/details/02077782.cn/page/n54/mode/2up)
* [書籍 (NDL)]
  『国訳漢文大成　文学部第三巻　文選中巻』には、殷仲文伝に引かれた
  [「解尚書表」](https://dl.ndl.go.jp/info:ndljp/pid/1912796/423)がある。
* [庾信「枯樹賦」](https://zh.wikisource.org/wiki/%E6%9E%AF%E6%A8%B9%E8%B3%A6)
  ……題材の一つが殷仲文。
  （[興膳宏「枯木にさく詩 : 詩的イメージの一系譜」](https://doi.org/10.14989/177467)を参照）

### 巻100 列伝第70
#### 王弥
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%BC%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%BC%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%BC%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%BC%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%BC%A5)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [王弥伝](https://estar.jp/novels/25594548/viewer?page=107)
  （@淡々晋書）
* [ブログ]
  [巻一百　列伝第七十　王弥　張昌　陳敏](https://readingnotesofjinshu.com/translation/biographies/vol-100_1#toc1)
  （@晋書簡訳所）

#### 張昌
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E6%98%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E6%98%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E6%98%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E6%98%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E6%98%8C)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n8/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y3.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王弥　張昌　陳敏](https://readingnotesofjinshu.com/translation/biographies/vol-100_1#toc2)
  （@晋書簡訳所）

#### 陳敏
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E6%95%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E6%95%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E6%95%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E6%95%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E6%95%8F)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n14/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y4.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王弥　張昌　陳敏](https://readingnotesofjinshu.com/translation/biographies/vol-100_1#toc3)
  （@晋書簡訳所）

#### 王如
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%A6%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%A6%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%A6%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%A6%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%A6%82)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n22/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%CE%F3%D1%A3%BC%B7%BD%BD)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y6.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc1)
  （@晋書簡訳所）

#### 杜曾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E6%9B%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E6%9B%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E6%9B%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E6%9B%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E6%9B%BE)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n24/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y7.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc2)
  （@晋書簡訳所）

#### 杜弢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%9C%E5%BC%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%9C%E5%BC%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%9C%E5%BC%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%9C%E5%BC%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%9C%E5%BC%A2)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y8.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc3)
  （@晋書簡訳所）

#### 王機
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E6%A9%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E6%A9%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E6%A9%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E6%A9%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E6%A9%9F)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n36/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc4)
  （@晋書簡訳所）

#### 王矩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%9F%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%9F%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%9F%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%9F%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%9F%A9)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n38/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　王如　杜曾　杜弢　王機](https://readingnotesofjinshu.com/translation/biographies/vol-100_2#toc4)
  （@晋書簡訳所）

#### 祖約
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A5%96%E7%B4%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A5%96%E7%B4%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A5%96%E7%B4%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A5%96%E7%B4%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A5%96%E7%B4%84)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n40/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　祖約　蘇峻](https://readingnotesofjinshu.com/translation/biographies/vol-100_3#toc1)
  （@晋書簡訳所）


#### 蘇峻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%98%87%E5%B3%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%98%87%E5%B3%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%98%87%E5%B3%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%98%87%E5%B3%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%98%87%E5%B3%BB)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n44/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百　列伝第七十　祖約　蘇峻](https://readingnotesofjinshu.com/translation/biographies/vol-100_3#toc2)
  （@晋書簡訳所）

#### 孫恩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%AD%AB%E6%81%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%AD%AB%E6%81%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%AD%AB%E6%81%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%AD%AB%E6%81%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%AD%AB%E6%81%A9)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n54/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）

#### 盧循
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9B%A7%E5%BE%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9B%A7%E5%BE%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9B%A7%E5%BE%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9B%A7%E5%BE%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9B%A7%E5%BE%AA)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n64/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）

#### 譙縦
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%AD%99%E7%B8%A6)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%AD%99%E7%B8%A6)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%AD%99%E7%B8%A6&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%AD%99%E7%B8%A6)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%AD%99%E7%B8%A6)
* [書籍 (IA)]
  [晉書斠注(六十六)](https://archive.org/details/02077783.cn/page/n70/mode/2up)
* [ブログ]
  [『晋書』列伝７０、反逆者の列伝](http://3guozhi.net/s/y9.html)
  （@いつか書きたい『三国志』）


### 載記
### 巻101 載記第1
#### 載記序
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%BC%89%E8%A8%98%E5%BA%8F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%BC%89%E8%A8%98%E5%BA%8F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%BC%89%E8%A8%98%E5%BA%8F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%BC%89%E8%A8%98%E5%BA%8F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%BC%89%E8%A8%98%E5%BA%8F)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n2/mode/2up)
* [ブログ]
  [巻一百一　載記第一　劉元海（1）](https://readingnotesofjinshu.com/translation/records/vol-101_1)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m101.html)

#### 劉元海（劉淵）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%B7%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%B7%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%B7%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%B7%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%B7%B5)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n4/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%B0%EC%A1%A1%CE%AD%B8%B5%B3%A4)
* [ブログ]
  [『晋書』から劉元海伝を訳し、西晋から自立する](http://3guozhi.net/a/re1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百一　載記第一　劉元海（2）](https://readingnotesofjinshu.com/translation/records/vol-101_2)
  （@晋書簡訳所）
* [ブログ]
  [劉元海伝](https://estar.jp/novels/25594548/viewer?page=79)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m101.html)
* 干宝[『搜神記』巻14](https://ctext.org/wiki.pl?if=gb&chapter=549265#p10)
* [ブログ]
  [太平御覧 劉淵](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%8A%89%E6%B7%B5)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%81%B0)

#### 劉和
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%92%8C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%92%8C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%92%8C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%92%8C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%92%8C)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n28/mode/2up)
* [ブログ]
  [『晋書』載記の劉淵伝より、劉和と劉宣](http://3guozhi.net/a/re7.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百一　載記第一　劉元海（3）](https://readingnotesofjinshu.com/translation/records/vol-101_3)
  （@晋書簡訳所）
* [ブログ]
  [劉和伝](https://estar.jp/novels/25594548/viewer?page=105)
  （@淡々晋書）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m101.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%81%B0)

#### 劉宣
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E5%AE%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E5%AE%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E5%AE%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E5%AE%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E5%AE%A3)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n30/mode/2up)
* [ブログ]
  [『晋書』載記の劉淵伝より、劉和と劉宣](http://3guozhi.net/a/re7.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百一　載記第一　劉元海（3）](https://readingnotesofjinshu.com/translation/records/vol-101_3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m101.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%81%B0)


### 巻102 載記第2
#### 劉聡
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E8%81%A1)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E8%81%A1)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E8%81%A1&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E8%81%A1)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E8%81%A1)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n34/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C6%F3%A1%A1%CE%AD%E6%E2)
* [ブログ]
  [『晋書』載記の劉聡伝を訳し、胡漢融合の可能性を探る](http://3guozhi.net/a/re9.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百二 載記第二 劉聡(1)](https://readingnotesofjinshu.com/translation/records/vol-102_1)・[同(2)](https://readingnotesofjinshu.com/translation/records/vol-102_2)
  （@晋書簡訳所）
* 周曇の「詠史詩」
    * [「懷帝」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E6%87%B7%E5%B8%9D)
    * [「前趙劉聰」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E5%89%8D%E8%B6%99%E5%8A%89%E8%81%B0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m102.html)
* [ブログ]
  [太平御覧 劉聡](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%8A%89%E8%81%A1)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%81%B0)

#### 劉粲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E7%B2%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E7%B2%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E7%B2%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E7%B2%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E7%B2%B2)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n86/mode/2up)
* [ブログ]
  [『晋書』載記の劉粲伝と陳元達伝から、漢の末路を辿る](http://3guozhi.net/a/re25.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百二 載記第二 劉聡(3)](https://readingnotesofjinshu.com/translation/records/vol-102_3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m102.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%81%B0)

#### 陳元達
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%B3%E5%85%83%E9%81%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%B3%E5%85%83%E9%81%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%B3%E5%85%83%E9%81%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%B3%E5%85%83%E9%81%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%B3%E5%85%83%E9%81%94)
* [書籍 (IA)]
  [晉書斠注(六十七)](https://archive.org/details/02077784.cn/page/n88/mode/2up)
* [ブログ]
  [『晋書』載記の劉粲伝と陳元達伝から、漢の末路を辿る](http://3guozhi.net/a/re25.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百二 載記第二 劉聡(3)](https://readingnotesofjinshu.com/translation/records/vol-102_3)
  （@晋書簡訳所）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m102.html)

### 巻103 載記第3
#### 劉曜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%8A%89%E6%9B%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%8A%89%E6%9B%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%8A%89%E6%9B%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%8A%89%E6%9B%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%8A%89%E6%9B%9C)
* [書籍 (IA)]
  [晉書斠注(六十八)](https://archive.org/details/02077785.cn/page/n2/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%BB%B0%A1%A1%CE%AD%CD%CB)
* [ブログ]
  [『晋書』載記の劉曜伝から、匈奴の儒教皇帝の限界を見る](http://3guozhi.net/a/ry1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [巻一百三　載記第三　劉曜（1）](https://readingnotesofjinshu.com/translation/records/vol-103_1)・[同（2）](https://readingnotesofjinshu.com/translation/records/vol-103_2)
  （@晋書簡訳所）
* [ブログ]
  [太平御覧 劉曜](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E5%8A%89%E6%9B%9C)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m103.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%81%B0)


### 巻104 載記第4
#### 石勒・上
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%8B%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%8B%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%8B%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%8B%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%8B%92)
* [書籍 (IA)]
  [晉書斠注(六十八)](https://archive.org/details/02077785.cn/page/n56/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻一百四　載記第四　石勒上（1）](https://readingnotesofjinshu.com/translation/records/vol-104_1)・[同（2）](https://readingnotesofjinshu.com/translation/records/vol-104_2)・[同（3）](https://readingnotesofjinshu.com/translation/records/vol-104_3)
  （@晋書簡訳所）
* [ブログ]
  [石勒載記、上](https://estar.jp/novels/25594548/viewer?page=116)
  （@淡々晋書）
* [胡曾の「洛陽」](https://zh.wikisource.org/wiki/%E6%B4%9B%E9%99%BD_(%E8%83%A1%E6%9B%BE))
* [石勒　五胡十六国時代の黒き英雄王　第一部「戦乱の世に降り立つ」①　石勒の生い立ち](https://histomiyain.com/2021/06/04/%e7%9f%b3%e5%8b%92%e3%80%80%e4%ba%94%e8%83%a1%e5%8d%81%e5%85%ad%e5%9b%bd%e6%99%82%e4%bb%a3%e3%81%ae%e9%bb%92%e3%81%8d%e8%8b%b1%e9%9b%84%e7%8e%8b%e3%80%80%e7%ac%ac%e4%b8%80%e9%83%a8%e3%80%8c%e6%88%a6/)〜
  （@歴史の宮殿）
* [ブログ]
  [太平御覧 石勒](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E7%9F%B3%E5%8B%92)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)
* [『旧唐書』巻99（張九齡）](https://zh.wikisource.org/wiki/%E8%88%8A%E5%94%90%E6%9B%B8/%E5%8D%B799#%E5%BC%B5%E4%B9%9D%E9%BD%A1)で、「九齡奏曰：「祿山狼子野心，面有逆相，臣請因罪戮之，冀絕後患。」上曰：「卿勿以王夷甫知石勒故事，誤害忠良。」遂放歸藩。」と石勒と王衍に言及している。
* [『新唐書』巻126（張九齡）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7126#%E5%BC%B5%E4%B9%9D%E9%BD%A1)で、「九齡曰：「祿山狼子野心，有逆相，宜即事誅之，以絕後患。」帝曰：「卿無以王衍知石勒而害忠良。」卒不用。」と石勒と王衍に言及している。
* [『新唐書』巻209（來俊臣）](https://zh.wikisource.org/wiki/%E6%96%B0%E5%94%90%E6%9B%B8/%E5%8D%B7209#%E4%BE%86%E4%BF%8A%E8%87%A3)で、「俊臣知羣臣不敢斥己，乃有異圖，常自比石勒，欲告皇嗣及廬陵王與南北衙謀反，因得騁志。」と石勒に言及している。
* [『宋史』巻343（林希）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7343#%E6%9E%97%E5%B8%8C)で、「哲宗問：「神宗殿曰宣光，前代有此名乎？」希對曰：「此石勒殿名也。」乃更為顯承。」と石勒の名前を出しているが、[『晋書』巻107（石季龍下）](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7107#%E7%9F%B3%E8%99%8E)には「石韜起堂于太尉府，號曰宣光殿，梁長九丈。」とある。


### 巻105 載記第5
#### 石勒・下
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%8B%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%8B%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%8B%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%8B%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%8B%92)
* [書籍 (IA)]
  [晉書斠注(六十九)](https://archive.org/details/02077786.cn/page/n2/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [巻一百五　載記第五　石勒下（1）](https://readingnotesofjinshu.com/translation/records/vol-105_1)・[同（2）](https://readingnotesofjinshu.com/translation/records/vol-105_2)
  （@晋書簡訳所）
* 朱元璋[『明太祖寶訓』卷四・評古](https://zh.wikisource.org/wiki/%E6%98%8E%E5%A4%AA%E7%A5%96%E5%AF%B6%E8%A8%93/%E5%8D%B74#%E8%A9%95%E5%8F%A4)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [胡曾の「豫州」](https://zh.wikisource.org/wiki/%E8%B1%AB%E5%B7%9E)
* [ブログ]
  [石勒載記、下](https://estar.jp/novels/25594548/viewer?page=244)
  （@淡々晋書）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)

#### 石弘
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E5%BC%98)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E5%BC%98)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E5%BC%98&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E5%BC%98)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E5%BC%98)
* [書籍 (IA)]
  [晉書斠注(六十九)](https://archive.org/details/02077786.cn/page/n46/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [晋書簡訳所](https://readingnotesofjinshu.com/translation/records/vol-105_3)
* [ブログ]
  [付、石弘伝](https://estar.jp/novels/25594548/viewer?page=319)
  （@淡々晋書）
* [ブログ]
  [太平御覧 石弘](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E7%9F%B3%E5%BC%98)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)

#### 張賓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BC%B5%E8%B3%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BC%B5%E8%B3%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BC%B5%E8%B3%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BC%B5%E8%B3%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BC%B5%E8%B3%93)
* [書籍 (IA)]
  [晉書斠注(六十九)](https://archive.org/details/02077786.cn/page/n56/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%B8%DE%A1%A1%C9%ED%D1%A3%C4%A5%C9%D0)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [晋書簡訳所](https://readingnotesofjinshu.com/translation/records/vol-105_3)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [ブログ]
  [付、張賓伝](https://estar.jp/novels/25594548/viewer?page=316)
  （@淡々晋書）

### 巻106 載記第6
#### 石季龍（石虎）・上
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%99%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%99%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%99%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%99%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%99%8E)
* [書籍 (IA)]
  [晉書斠注(六十九)](https://archive.org/details/02077786.cn/page/n60/mode/2up)
* [ブログ]
  [巻一百六　載記第六　石季龍上（1）](https://readingnotesofjinshu.com/translation/records/vol-106_1)・[同（2）](https://readingnotesofjinshu.com/translation/records/vol-106_2)
  （@晋書簡訳所）
* [ブログ]
  [太平御覧 石虎](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E7%9F%B3%E8%99%8E)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)


### 巻107 載記第7
#### 石季龍（石虎）・下
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E8%99%8E)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E8%99%8E)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E8%99%8E&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E8%99%8E)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E8%99%8E)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n2/mode/2up)
* [ブログ]
  [巻一百七　載記第七　石季龍下（1）](https://readingnotesofjinshu.com/translation/records/vol-107_1)
  （@晋書簡訳所）
* 朱元璋[『明太祖寶訓』卷四・評古](https://zh.wikisource.org/wiki/%E6%98%8E%E5%A4%AA%E7%A5%96%E5%AF%B6%E8%A8%93/%E5%8D%B74#%E8%A9%95%E5%8F%A4)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)
* [『宋史』巻343（林希）](https://zh.wikisource.org/wiki/%E5%AE%8B%E5%8F%B2/%E5%8D%B7343#%E6%9E%97%E5%B8%8C)で、「哲宗問：「神宗殿曰宣光，前代有此名乎？」希對曰：「此石勒殿名也。」乃更為顯承。」と石勒の名前を出しているが、[『晋書』巻107（石季龍下）](https://zh.wikisource.org/wiki/%E6%99%89%E6%9B%B8/%E5%8D%B7107#%E7%9F%B3%E8%99%8E)には「石韜起堂于太尉府，號曰宣光殿，梁長九丈。」とある。

#### 石世
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E4%B8%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E4%B8%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E4%B8%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E4%B8%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E4%B8%96)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n20/mode/2up)
* [ブログ]
  [巻一百七　載記第七　石季龍下（2）](https://readingnotesofjinshu.com/translation/records/vol-107_2)
  （@晋書簡訳所）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)

#### 石遵
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E9%81%B5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E9%81%B5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E9%81%B5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E9%81%B5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E9%81%B5)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n24/mode/2up)
* [ブログ]
  [巻一百七　載記第七　石季龍下（2）](https://readingnotesofjinshu.com/translation/records/vol-107_2)
  （@晋書簡訳所）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)

#### 石鑑（石鑒）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E9%91%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E9%91%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E9%91%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E9%91%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E9%91%92)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n28/mode/2up)
* [ブログ]
  [巻一百七　載記第七　石季龍下（2）](https://readingnotesofjinshu.com/translation/records/vol-107_2)
  （@晋書簡訳所）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)

#### 冉閔（石閔）
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9F%B3%E9%96%94)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9F%B3%E9%96%94)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9F%B3%E9%96%94&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9F%B3%E9%96%94)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9F%B3%E9%96%94)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n34/mode/2up)
* [ブログ]
  [巻一百七　載記第七　石季龍下（3）](https://readingnotesofjinshu.com/translation/records/vol-107_3)
  （@晋書簡訳所）
* [ブログ]
  [「冉閔」のカテゴリ](https://histomiyain.com/category/%e6%ad%b4%e5%8f%b2/%e4%b8%ad%e5%9b%bd%e5%8f%b2/%e4%ba%94%e8%83%a1%e5%8d%81%e5%85%ad%e5%9b%bd%e6%99%82%e4%bb%a3/%e5%86%89%e9%96%94/)
  （@歴史の宮殿）
* [ブログ]
  [太平御覧 石閔](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E7%9F%B3%E9%96%94)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E7%9F%B3%E5%8B%92)

### 巻108 載記第8
#### 慕容廆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%BB%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%BB%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%BB%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%BB%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%BB%86)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n52/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m108.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 裴嶷
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%A3%B4%E5%B6%B7)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%A3%B4%E5%B6%B7)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%A3%B4%E5%B6%B7&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%A3%B4%E5%B6%B7)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%A3%B4%E5%B6%B7)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n74/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m108.html)

#### 高瞻
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%AB%98%E7%9E%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%AB%98%E7%9E%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%AB%98%E7%9E%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%AB%98%E7%9E%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%AB%98%E7%9E%BB)
* [書籍 (IA)]
  [晉書斠注(七十)](https://archive.org/details/02077787.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m108.html)


### 巻109 載記第9
#### 慕容皝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E7%9A%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E7%9A%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E7%9A%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E7%9A%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E7%9A%9D)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n2/mode/2up)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 慕容翰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E7%BF%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E7%BF%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E7%BF%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E7%BF%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E7%BF%B0)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n32/mode/2up)

#### 陽裕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%BD%E8%A3%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%BD%E8%A3%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%BD%E8%A3%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%BD%E8%A3%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%BD%E8%A3%95)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n34/mode/2up)


### 巻110 載記第10
#### 慕容儁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%84%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%84%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%84%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%84%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%84%81)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n38/mode/2up)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 韓恒
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%9F%93%E6%81%92)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%9F%93%E6%81%92)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%9F%93%E6%81%92&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%9F%93%E6%81%92)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%9F%93%E6%81%92)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n66/mode/2up)

#### 李産
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E7%94%A3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E7%94%A3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E7%94%A3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E7%94%A3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E7%94%A3)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n70/mode/2up)

#### 李績
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E7%B8%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E7%B8%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E7%B8%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E7%B8%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E7%B8%BE)
* [書籍 (IA)]
  [晉書斠注(七十一)](https://archive.org/details/02077788.cn/page/n72/mode/2up)


### 巻111 載記第11
#### 慕容暐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E6%9A%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E6%9A%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E6%9A%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E6%9A%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E6%9A%90)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n2/mode/2up)
* [ブログ]
  [「紫微垣」晋書孝武帝本紀他](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854995)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [慕容暐１　前燕最終皇帝](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054894646081)〜[慕容暐５　暗殺失敗し死す](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054894687150)
  （@ゆるふわ中原戦記１）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 慕容恪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E6%81%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E6%81%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E6%81%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E6%81%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E6%81%AA)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n28/mode/2up)

#### 陽騖
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%99%BD%E9%A8%96)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%99%BD%E9%A8%96)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%99%BD%E9%A8%96&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%99%BD%E9%A8%96)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%99%BD%E9%A8%96)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n30/mode/2up)

#### 皇甫真
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%9A%87%E7%94%AB%E7%9C%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%9A%87%E7%94%AB%E7%9C%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%9A%87%E7%94%AB%E7%9C%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%9A%87%E7%94%AB%E7%9C%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%9A%87%E7%94%AB%E7%9C%9F)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n32/mode/2up)


### 巻112 載記第12
#### 苻洪
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E6%B4%AA)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E6%B4%AA)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E6%B4%AA&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E6%B4%AA)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E6%B4%AA)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n42/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m112.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)

#### 苻健
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E5%81%A5)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E5%81%A5)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E5%81%A5&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E5%81%A5)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E5%81%A5)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n46/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m112.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)

#### 苻生
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E7%94%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E7%94%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E7%94%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E7%94%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E7%94%9F)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n56/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m112.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)

#### 苻雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m112.html)

#### 王堕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E5%A0%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E5%A0%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E5%A0%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E5%A0%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E5%A0%95)
* [書籍 (IA)]
  [晉書斠注(七十二)](https://archive.org/details/02077789.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m112.html)


### 巻113 載記第13
#### 苻堅・上
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E5%A0%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E5%A0%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E5%A0%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E5%A0%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E5%A0%85)
* [書籍 (IA)]
  [晉書斠注(七十三)](https://archive.org/details/02077790.cn/page/n2/mode/2up)
* [ブログ]
  [「紫微垣」苻堅載記１](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884870531)〜[「紫微垣」苻堅戴記７（完）](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054885257985)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [苻堅１　　苻堅誕生す](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054894774922)
  （@ゆるふわ中原戦記１）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m113.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)


### 巻114 載記第14
#### 苻堅・下
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E5%A0%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E5%A0%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E5%A0%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E5%A0%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E5%A0%85)
* [書籍 (IA)]
  [晉書斠注(七十三)](https://archive.org/details/02077790.cn/page/n64/mode/2up)
* [ブログ]
  [苻堅２　　離反](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054894790297)〜
  （@ゆるふわ中原戦記１）
* 朱元璋[『明太祖寶訓』卷四・評古](https://zh.wikisource.org/wiki/%E6%98%8E%E5%A4%AA%E7%A5%96%E5%AF%B6%E8%A8%93/%E5%8D%B74#%E8%A9%95%E5%8F%A4)
* [周曇の「詠史詩」の中の「苻堅」と「再吟」と「又吟」](https://zh.wikisource.org/wiki/%E5%85%A8%E5%94%90%E8%A9%A9/%E5%8D%B7729#%E8%8B%BB%E5%A0%85)
* [胡曾の「東晉」](https://zh.wikisource.org/wiki/%E6%9D%B1%E6%99%89)
* [胡曾の「八公山」](https://zh.wikisource.org/wiki/%E5%85%AB%E5%85%AC%E5%B1%B1_(%E8%83%A1%E6%9B%BE))
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m114.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)
* [杜牧「西江懷古」](https://zh.wikisource.org/wiki/%E5%BE%A1%E5%AE%9A%E5%85%A8%E5%94%90%E8%A9%A9_(%E5%9B%9B%E5%BA%AB%E5%85%A8%E6%9B%B8%E6%9C%AC)/%E5%8D%B7522#%E8%A5%BF%E6%B1%9F%E6%87%B7%E5%8F%A4)……「苻堅投箠更荒唐」とある。
* [李華「謝文靖」](https://zh.wikisource.org/wiki/%E8%AC%9D%E6%96%87%E9%9D%96)……「文靖」は謝安の諡。苻堅や謝玄らにも言及している。

#### 王猛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%8E%8B%E7%8C%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%8E%8B%E7%8C%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%8E%8B%E7%8C%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%8E%8B%E7%8C%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%8E%8B%E7%8C%9B)
* [書籍 (IA)]
  [晉書斠注(七十三)](https://archive.org/details/02077790.cn/page/n114/mode/2up)
* 『太白陰經（神機制敵太白陰經）』將有智謀篇
    * [Wikisource](https://zh.wikisource.org/wiki/%E5%A4%AA%E7%99%BD%E9%99%B0%E7%B6%93/%E5%8D%B7%E4%B8%80)
    * [Internet Archive](https://archive.org/details/06047922.cn/page/n20/mode/2up)
* [書籍 (NDL)]
  [『清名家史論文』巻6](https://dl.ndl.go.jp/info:ndljp/pid/775786)所収
  [「王猛論」](https://dl.ndl.go.jp/info:ndljp/pid/775786/22)
  （侯方域）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m114.html)

#### 苻融
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E8%9E%8D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E8%9E%8D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E8%9E%8D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E8%9E%8D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E8%9E%8D)
* [書籍 (IA)]
  [晉書斠注(七十三)](https://archive.org/details/02077790.cn/page/n124/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m114.html)

#### 苻朗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E6%9C%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E6%9C%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E6%9C%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E6%9C%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E6%9C%97)
* [書籍 (IA)]
  [晉書斠注(七十三)](https://archive.org/details/02077790.cn/page/n130/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m114.html)


### 巻115 載記第15
#### 苻丕
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E4%B8%95)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E4%B8%95)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E4%B8%95&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E4%B8%95)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E4%B8%95)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n2/mode/2up)
* [ブログ]
  [苻丕１　　鄴都脱出](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054895088437)〜
  （@ゆるふわ中原戦記１）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m115.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)

#### 苻登
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%8B%BB%E7%99%BB)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%8B%BB%E7%99%BB)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%8B%BB%E7%99%BB&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%8B%BB%E7%99%BB)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%8B%BB%E7%99%BB)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n16/mode/2up)
* [ブログ]
  [苻登１　　苻堅の従孫](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054895506994)〜
  （@ゆるふわ中原戦記１）
* [ブログ]
  [晋書　苻登・毛氏列伝](http://blog.livedoor.jp/oboro0083/archives/2498254.html)
  （@★香華酔月☆）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m115.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E8%8B%BB%E5%81%A5)

#### 索泮
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%B4%A2%E6%B3%AE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%B4%A2%E6%B3%AE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%B4%A2%E6%B3%AE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%B4%A2%E6%B3%AE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%B4%A2%E6%B3%AE)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n34/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m115.html)

#### 徐嵩
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%BE%90%E5%B5%A9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%BE%90%E5%B5%A9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%BE%90%E5%B5%A9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%BE%90%E5%B5%A9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%BE%90%E5%B5%A9)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m115.html)

### 巻116 載記第16
#### 姚弋仲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A7%9A%E5%BC%8B%E4%BB%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A7%9A%E5%BC%8B%E4%BB%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A7%9A%E5%BC%8B%E4%BB%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A7%9A%E5%BC%8B%E4%BB%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A7%9A%E5%BC%8B%E4%BB%B2)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n42/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m116.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%A7%9A%E8%90%87)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%BE%8C%E7%A7%A6%E5%A7%9A%E6%B0%8F)

#### 姚襄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A7%9A%E8%A5%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A7%9A%E8%A5%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A7%9A%E8%A5%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A7%9A%E8%A5%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A7%9A%E8%A5%84)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n48/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m116.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%A7%9A%E8%90%87)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%BE%8C%E7%A7%A6%E5%A7%9A%E6%B0%8F)

#### 姚萇
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A7%9A%E8%90%87)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A7%9A%E8%90%87)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A7%9A%E8%90%87&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A7%9A%E8%90%87)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A7%9A%E8%90%87)
* [書籍 (IA)]
  [晉書斠注(七十四)](https://archive.org/details/02077791.cn/page/n56/mode/2up)
* [ブログ]
  [「紫微垣」晋書孝武帝本紀他](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854995)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [姚萇１　　兄を救う](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054900820137)〜
  （@ゆるふわ中原戦記１）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m116.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%A7%9A%E8%90%87)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%BE%8C%E7%A7%A6%E5%A7%9A%E6%B0%8F)


### 巻117 載記第17
#### 姚興・上
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A7%9A%E8%88%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A7%9A%E8%88%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A7%9A%E8%88%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A7%9A%E8%88%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A7%9A%E8%88%88)
* [書籍 (IA)]
  [晉書斠注(七十五)](https://archive.org/details/02077792.cn/page/n2/mode/2up)
* [ブログ]
  [姚興１　　後事を統べる](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054917871972)〜
  （@ゆるふわ中原戦記１）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m117.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%A7%9A%E8%90%87)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%BE%8C%E7%A7%A6%E5%A7%9A%E6%B0%8F)


### 巻118 載記第18
#### 姚興・下
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A7%9A%E8%88%88)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A7%9A%E8%88%88)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A7%9A%E8%88%88&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A7%9A%E8%88%88)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A7%9A%E8%88%88)
* [書籍 (IA)]
  [晉書斠注(七十五)](https://archive.org/details/02077792.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m118.html)
* [ブログ]
  [姚興３７　傾落](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354054934210518)〜
  （@ゆるふわ中原戦記１）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%A7%9A%E8%90%87)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%BE%8C%E7%A7%A6%E5%A7%9A%E6%B0%8F)

#### 尹緯
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B0%B9%E7%B7%AF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B0%B9%E7%B7%AF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B0%B9%E7%B7%AF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B0%B9%E7%B7%AF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B0%B9%E7%B7%AF)
* [書籍 (IA)]
  [晉書斠注(七十五)](https://archive.org/details/02077792.cn/page/n64/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m118.html)
* [ブログ]
  [尹緯１　　豪傑宰相](https://kakuyomu.jp/works/1177354054894645693/episodes/1177354055281688402)〜
  （@ゆるふわ中原戦記１）


### 巻119 載記第19
#### 姚泓
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%A7%9A%E6%B3%93)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%A7%9A%E6%B3%93)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%A7%9A%E6%B3%93&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%A7%9A%E6%B3%93)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%A7%9A%E6%B3%93)
* [書籍 (IA)]
  [晉書斠注(七十六)](https://archive.org/details/02077793.cn/page/n2/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m119.html)
* [ブログ]
  [姚泓１　　孝友寬和のひと](https://kakuyomu.jp/works/1177354055365070928/episodes/1177354055380565527)〜
  （@ゆるふわ中原戦記２）
* [『太平広記』巻29](https://zh.wikisource.org/wiki/%E5%A4%AA%E5%B9%B3%E5%BB%A3%E8%A8%98/%E5%8D%B7%E7%AC%AC029#%E5%A7%9A%E6%B3%93)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%A7%9A%E8%90%87)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%BE%8C%E7%A7%A6%E5%A7%9A%E6%B0%8F)

### 巻120 載記第20
#### 李特
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E7%89%B9)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E7%89%B9)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E7%89%B9&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E7%89%B9)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E7%89%B9)
* [書籍 (IA)]
  [晉書斠注(七十六)](https://archive.org/details/02077793.cn/page/n34/mode/2up)
* [ブログ]
  [解體晉書](http://jinshu.fc2web.com/)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m120.html)
* [ブログ]
  [太平御覧 李特](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E6%9D%8E%E7%89%B9)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）

#### 李流
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%B5%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%B5%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%B5%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%B5%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%B5%81)
* [書籍 (IA)]
  [晉書斠注(七十六)](https://archive.org/details/02077793.cn/page/n60/mode/2up)
* [ブログ]
  [『晋書』載記より、「李流・李庠・李雄伝」を翻訳](http://3guozhi.net/zaka2/rt1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m120.html)
* [ブログ]
  [太平御覧 李流](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E6%9D%8E%E6%B5%81)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）

#### 李庠
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%BA%A0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%BA%A0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%BA%A0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%BA%A0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%BA%A0)
* [書籍 (IA)]
  [晉書斠注(七十六)](https://archive.org/details/02077793.cn/page/n66/mode/2up)
* [ブログ]
  [『晋書』載記より、「李流・李庠・李雄伝」を翻訳](http://3guozhi.net/zaka2/rt1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m120.html)

### 巻121 載記第21
#### 李雄
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E9%9B%84)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E9%9B%84)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E9%9B%84&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E9%9B%84)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E9%9B%84)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n2/mode/2up)
* [ブログ]
  [『晋書』載記より、「李流・李庠・李雄伝」を翻訳](http://3guozhi.net/zaka2/rt1.html)
  （@いつか書きたい『三国志』）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m121.html)
* [ブログ]
  [太平御覧 李雄](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E6%9D%8E%E9%9B%84)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E6%9D%8E%E9%9B%84)

#### 李班
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E7%8F%AD)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E7%8F%AD)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E7%8F%AD&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E7%8F%AD)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E7%8F%AD)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n22/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m121.html)
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E6%9D%8E%E9%9B%84)

#### 李期
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E6%9C%9F)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E6%9C%9F)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E6%9C%9F&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E6%9C%9F)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E6%9C%9F)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n24/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m121.html)
* [ブログ]
  [太平御覧 李期](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E6%9D%8E%E6%9C%9F)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E6%9D%8E%E9%9B%84)

#### 李寿
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%AF%BF)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%AF%BF)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%AF%BF&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%AF%BF)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%AF%BF)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n28/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m121.html)
* [ブログ]
  [太平御覧 李寿](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E6%9D%8E%E5%AF%BF)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E6%9D%8E%E9%9B%84)

#### 李勢
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%9D%8E%E5%8B%A2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%9D%8E%E5%8B%A2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%9D%8E%E5%8B%A2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%9D%8E%E5%8B%A2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%9D%8E%E5%8B%A2)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n36/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m121.html)
* [ブログ]
  [太平御覧 李勢](https://nukotania.com/puki/transl_00/?%E5%A4%AA%E5%B9%B3%E5%BE%A1%E8%A6%A7+%E6%9D%8E%E5%8B%A2)
  （@ぬこだいすき - あなたとわたしのげんじつとうひ）
* [『魏書』巻96](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B796#%E6%9D%8E%E9%9B%84)
* [顏希源『百美新詠』「李勢女」](https://zh.wikisource.org/wiki/%E7%99%BE%E7%BE%8E%E6%96%B0%E8%A9%A0#%E6%9D%8E%E5%8B%A2%E5%A5%B3)
  ……[『世説新語』賢媛篇21](https://zh.wikisource.org/wiki/%E4%B8%96%E8%AA%AA%E6%96%B0%E8%AA%9E/%E8%B3%A2%E5%AA%9B)の話が題材。


### 巻122 載記第22
#### 呂光
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E5%85%89)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E5%85%89)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E5%85%89&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E5%85%89)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E5%85%89)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n48/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%C6%F3%A1%A1%CF%A4%B8%F7%A1%A6%CF%A4%BB%BC%A1%A6%CF%A4%CE%B4)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m122.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%91%82%E5%85%89)

#### 呂纂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E7%BA%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E7%BA%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E7%BA%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E7%BA%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E7%BA%82)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n72/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%C6%F3%A1%A1%CF%A4%B8%F7%A1%A6%CF%A4%BB%BC%A1%A6%CF%A4%CE%B4)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m122.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%91%82%E5%85%89)

#### 呂隆
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%91%82%E9%9A%86)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%91%82%E9%9A%86)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%91%82%E9%9A%86&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%91%82%E9%9A%86)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%91%82%E9%9A%86)
* [書籍 (IA)]
  [晉書斠注(七十七)](https://archive.org/details/02077794.cn/page/n86/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%C6%F3%A1%A1%CF%A4%B8%F7%A1%A6%CF%A4%BB%BC%A1%A6%CF%A4%CE%B4)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m122.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%91%82%E5%85%89)


### 巻123 載記第23
#### 慕容垂
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%9E%82)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%9E%82)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%9E%82&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%9E%82)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%9E%82)
* [書籍 (IA)]
  [晉書斠注(七十八)](https://archive.org/details/02077795.cn/page/n2/mode/2up)
* [ブログ]
  [慕容垂１　慕容覇](https://kakuyomu.jp/works/1177354055365070928/episodes/16816452221394947107)〜
  （@ゆるふわ中原戦記２）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m123.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

### 巻124 載記第24
#### 慕容宝
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%AE%9D)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%AE%9D)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%AE%9D&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%AE%9D)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%AE%9D)
* [書籍 (IA)]
  [晉書斠注(七十八)](https://archive.org/details/02077795.cn/page/n38/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%BB%CD%A1%A1%CA%E9%CD%C6%D5%EF%A1%A6%CA%E9%CD%C6%C0%B9%A1%A6%CA%E9%CD%C6%F4%A6%A1%A6%CA%E9%CD%C6%B1%C0)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m124.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 慕容盛
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E7%9B%9B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E7%9B%9B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E7%9B%9B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E7%9B%9B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E7%9B%9B)
* [書籍 (IA)]
  [晉書斠注(七十八)](https://archive.org/details/02077795.cn/page/n50/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%BB%CD%A1%A1%CA%E9%CD%C6%D5%EF%A1%A6%CA%E9%CD%C6%C0%B9%A1%A6%CA%E9%CD%C6%F4%A6%A1%A6%CA%E9%CD%C6%B1%C0)
* [ブログ]
  [「紫微垣」晋書孝武帝本紀他](https://kakuyomu.jp/works/1177354054884854958/episodes/1177354054884854995)
  （@楽しい！　漢文ごっこ）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m124.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 慕容熙
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E7%86%99)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E7%86%99)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E7%86%99&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E7%86%99)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E7%86%99)
* [書籍 (IA)]
  [晉書斠注(七十八)](https://archive.org/details/02077795.cn/page/n68/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m124.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 慕容雲
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E9%9B%B2)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E9%9B%B2)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E9%9B%B2&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E9%9B%B2)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E9%9B%B2)
* [書籍 (IA)]
  [晉書斠注(七十八)](https://archive.org/details/02077795.cn/page/n76/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m124.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)


### 巻125 載記第25
#### 乞伏国仁
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B9%9E%E4%BC%8F%E5%9B%BD%E4%BB%81)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B9%9E%E4%BC%8F%E5%9B%BD%E4%BB%81)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B9%9E%E4%BC%8F%E5%9B%BD%E4%BB%81&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B9%9E%E4%BC%8F%E5%9B%BD%E4%BB%81)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B9%9E%E4%BC%8F%E5%9B%BD%E4%BB%81)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n2/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m125.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E4%B9%9E%E4%BC%8F%E5%9C%8B%E4%BB%81)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E8%A5%BF%E7%A7%A6%E4%B9%9E%E4%BC%8F%E6%B0%8F)

#### 乞伏乾帰
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B9%9E%E4%BC%8F%E4%B9%BE%E5%B8%B0)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B9%9E%E4%BC%8F%E4%B9%BE%E5%B8%B0)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B9%9E%E4%BC%8F%E4%B9%BE%E5%B8%B0&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B9%9E%E4%BC%8F%E4%B9%BE%E5%B8%B0)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B9%9E%E4%BC%8F%E4%B9%BE%E5%B8%B0)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n12/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m125.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E4%B9%9E%E4%BC%8F%E5%9C%8B%E4%BB%81)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E8%A5%BF%E7%A7%A6%E4%B9%9E%E4%BC%8F%E6%B0%8F)

#### 乞伏熾磐
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E4%B9%9E%E4%BC%8F%E7%86%BE%E7%A3%90)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E4%B9%9E%E4%BC%8F%E7%86%BE%E7%A3%90)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E4%B9%9E%E4%BC%8F%E7%86%BE%E7%A3%90&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E4%B9%9E%E4%BC%8F%E7%86%BE%E7%A3%90)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E4%B9%9E%E4%BC%8F%E7%86%BE%E7%A3%90)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n28/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m125.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E4%B9%9E%E4%BC%8F%E5%9C%8B%E4%BB%81)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E8%A5%BF%E7%A7%A6%E4%B9%9E%E4%BC%8F%E6%B0%8F)

#### 馮跋
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AE%E8%B7%8B)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AE%E8%B7%8B)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AE%E8%B7%8B&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AE%E8%B7%8B)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AE%E8%B7%8B)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n40/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m125.html)
* [『魏書』巻97](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B797#%E9%A6%AE%E8%B7%8B)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%8C%97%E7%87%95%E9%A6%AE%E6%B0%8F)

#### 馮素弗
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E9%A6%AE%E7%B4%A0%E5%BC%97)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E9%A6%AE%E7%B4%A0%E5%BC%97)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E9%A6%AE%E7%B4%A0%E5%BC%97&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E9%A6%AE%E7%B4%A0%E5%BC%97)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E9%A6%AE%E7%B4%A0%E5%BC%97)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n58/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m125.html)


### 巻126 載記第26
#### 禿髪烏孤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A6%BF%E9%AB%AA%E7%83%8F%E5%AD%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A6%BF%E9%AB%AA%E7%83%8F%E5%AD%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A6%BF%E9%AB%AA%E7%83%8F%E5%AD%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A6%BF%E9%AB%AA%E7%83%8F%E5%AD%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A6%BF%E9%AB%AA%E7%83%8F%E5%AD%A4)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n62/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m126.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E7%A6%BF%E9%AB%AE%E7%83%8F%E5%AD%A4)

#### 禿髪利鹿孤
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A6%BF%E9%AB%AA%E5%88%A9%E9%B9%BF%E5%AD%A4)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A6%BF%E9%AB%AA%E5%88%A9%E9%B9%BF%E5%AD%A4)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A6%BF%E9%AB%AA%E5%88%A9%E9%B9%BF%E5%AD%A4&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A6%BF%E9%AB%AA%E5%88%A9%E9%B9%BF%E5%AD%A4)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A6%BF%E9%AB%AA%E5%88%A9%E9%B9%BF%E5%AD%A4)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n70/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m126.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E7%A6%BF%E9%AB%AE%E7%83%8F%E5%AD%A4)

#### 禿髪傉檀
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E7%A6%BF%E9%AB%AA%E5%82%89%E6%AA%80)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E7%A6%BF%E9%AB%AA%E5%82%89%E6%AA%80)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E7%A6%BF%E9%AB%AA%E5%82%89%E6%AA%80&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E7%A6%BF%E9%AB%AA%E5%82%89%E6%AA%80)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E7%A6%BF%E9%AB%AA%E5%82%89%E6%AA%80)
* [書籍 (IA)]
  [晉書斠注(七十九)](https://archive.org/details/02077796.cn/page/n78/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m126.html)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E7%A6%BF%E9%AB%AE%E7%83%8F%E5%AD%A4)


### 巻127 載記第27
#### 慕容徳
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E5%BE%B3)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E5%BE%B3)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E5%BE%B3&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E5%BE%B3)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E5%BE%B3)
* [書籍 (IA)]
  [晉書斠注(八十)](https://archive.org/details/02077797.cn/page/n2/mode/2up)
* [ブログ]
  [慕容徳１　先臣後君の相](https://kakuyomu.jp/works/1177354055365070928/episodes/16816700429178638351)〜
  （@ゆるふわ中原戦記２）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m127.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)


### 巻128 載記第28
#### 慕容超
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E8%B6%85)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E8%B6%85)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E8%B6%85&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E8%B6%85)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E8%B6%85)
* [書籍 (IA)]
  [晉書斠注(八十)](https://archive.org/details/02077797.cn/page/n36/mode/2up)
* [ブログ]
  [慕容超１　敵地にて](https://kakuyomu.jp/works/1177354055365070928/episodes/16816927859548247759)〜
  （@ゆるふわ中原戦記２）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m128.html)
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E6%85%95%E5%AE%B9%E5%BB%86)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E7%87%95%E6%85%95%E5%AE%B9%E6%B0%8F)

#### 慕容鍾
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%85%95%E5%AE%B9%E9%8D%BE)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%85%95%E5%AE%B9%E9%8D%BE)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%85%95%E5%AE%B9%E9%8D%BE&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%85%95%E5%AE%B9%E9%8D%BE)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%85%95%E5%AE%B9%E9%8D%BE)
* [書籍 (IA)]
  [晉書斠注(八十)](https://archive.org/details/02077797.cn/page/n62/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%C8%AC%20%CA%E9%CD%C6%C4%B6%A1%A6%CA%E9%CD%C6%BE%E1%A1%A6%C9%F5%D5%D5)
* [ブログ]
  [慕容鐘　　南燕の佐命功臣](https://kakuyomu.jp/works/1177354055365070928/episodes/16816927860291911689)
  （@ゆるふわ中原戦記２）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m128.html)

#### 封孚
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E5%B0%81%E5%AD%9A)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E5%B0%81%E5%AD%9A)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E5%B0%81%E5%AD%9A&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E5%B0%81%E5%AD%9A)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E5%B0%81%E5%AD%9A)
* [書籍 (IA)]
  [晉書斠注(八十)](https://archive.org/details/02077797.cn/page/n62/mode/2up)
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%C8%AC%20%CA%E9%CD%C6%C4%B6%A1%A6%CA%E9%CD%C6%BE%E1%A1%A6%C9%F5%D5%D5)
* [ブログ]
  [封孚　　　直剛の士](https://kakuyomu.jp/works/1177354055365070928/episodes/16816927860362978864)
  （@ゆるふわ中原戦記２）
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m128.html)


### 巻129 載記第29
#### 沮渠蒙遜
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E6%B2%AE%E6%B8%A0%E8%92%99%E9%81%9C)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E6%B2%AE%E6%B8%A0%E8%92%99%E9%81%9C)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E6%B2%AE%E6%B8%A0%E8%92%99%E9%81%9C&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E6%B2%AE%E6%B8%A0%E8%92%99%E9%81%9C)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E6%B2%AE%E6%B8%A0%E8%92%99%E9%81%9C)
* [書籍 (IA)]
  [晉書斠注(八十一)](https://archive.org/details/02077798.cn/page/n2/mode/2up)
* [『宋書』巻98](https://zh.wikisource.org/wiki/%E5%AE%8B%E6%9B%B8/%E5%8D%B798#%E8%83%A1)
* [『魏書』巻99](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B799#%E6%B2%AE%E6%B8%A0%E8%92%99%E9%81%9C)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%8C%97%E6%B6%BC%E6%B2%AE%E6%B8%A0%E6%B0%8F)
* [PDFあり]
  [南北朝正史の沮渠蒙遜伝、氐胡伝、宕昌伝訳注](http://hdl.handle.net/11173/1619)
  （小谷 仲男・菅沼 愛語）……『晋書』の沮渠蒙遜伝の訳ではないが、『魏書』と『宋書』の沮渠蒙遜伝の訳注が載っている。
* [ブログ]
  [とある魏晉南北朝史オタクの史書翻譯・論文要約](https://rikutyousi.memo.wiki/d/%BA%DC%B5%AD%C2%E8%C6%F3%BD%BD%B6%E5%A1%A1%DD%FC%B5%F4%CC%D8%C2%BD)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m129.html)
* [ブログ]
  [五胡十六国時代　河西回廊の魔法使い・沮渠蒙遜①　舞台となる河西回廊とは？](https://histomiyain.com/2020/06/22/%e4%ba%94%e8%83%a1%e5%8d%81%e5%85%ad%e5%9b%bd%e6%99%82%e4%bb%a3%e3%80%80%e6%b2%b3%e8%a5%bf%e5%9b%9e%e5%bb%8a%e3%81%ae%e9%ad%94%e6%b3%95%e4%bd%bf%e3%81%84%e3%83%bb%e6%b2%ae%e6%b8%a0%e8%92%99%e9%81%9c/) 〜
  [五胡十六国時代　河西回廊の魔法使い・沮渠蒙遜㉔　最終回　431年～439年　沮渠蒙遜の死と北涼の滅亡](https://histomiyain.com/2020/11/08/%e4%ba%94%e8%83%a1%e5%8d%81%e5%85%ad%e5%9b%bd%e6%99%82%e4%bb%a3%e3%80%80%e6%b2%b3%e8%a5%bf%e5%9b%9e%e5%bb%8a%e3%81%ae%e9%ad%94%e6%b3%95%e4%bd%bf%e3%81%84%e3%83%bb%e6%b2%ae%e6%b8%a0%e8%92%99-24/)
  （@歴史の宮殿）


### 巻130 載記第30
#### 赫連勃勃
* [CiNii Research検索](https://cir.nii.ac.jp/ja/all?q=%E8%B5%AB%E9%80%A3%E5%8B%83%E5%8B%83)
* [NDL検索](https://iss.ndl.go.jp/api/openurl?any=%E8%B5%AB%E9%80%A3%E5%8B%83%E5%8B%83)
* [次世代デジタルライブラリーを検索](https://lab.ndl.go.jp/dl/fulltext?keyword=%E8%B5%AB%E9%80%A3%E5%8B%83%E5%8B%83&searchfield=)
* [「魏晋南北ブログ」を検索](http://gishinnanboku.blog.fc2.com/?q=%E8%B5%AB%E9%80%A3%E5%8B%83%E5%8B%83)
* [「五胡十六国の歴史を語るブログ」を検索](http://blog.livedoor.jp/masudazigo-2nd/search?q=%E8%B5%AB%E9%80%A3%E5%8B%83%E5%8B%83)
* [書籍 (IA)]
  [晉書斠注(八十一)](https://archive.org/details/02077798.cn/page/n30/mode/2up)
* [ブログ]
  [いつか読みたい晋書訳](http://3guozhi.net/sy/m130.html)
* [ブログ]
  [勃勃１　　凶雄の誕生](https://kakuyomu.jp/works/16816927860605786408/episodes/16816927860605951444)〜
  （@デイリー中原戦記）
* [『魏書』巻95](https://zh.wikisource.org/wiki/%E9%AD%8F%E6%9B%B8/%E5%8D%B795#%E5%8A%89%E8%99%8E)
* [『北史』巻93](https://zh.wikisource.org/wiki/%E5%8C%97%E5%8F%B2/%E5%8D%B7093#%E5%A4%8F%E8%B5%AB%E9%80%A3%E6%B0%8F)

